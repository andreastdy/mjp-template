(function () {

    var laroute = (function () {

        var routes = {

            absolute: false,
            rootUrl: 'http://localhost',
            routes : [{"host":null,"methods":["GET","HEAD"],"uri":"api\/user","name":null,"action":"Closure"},{"host":null,"methods":["GET","HEAD"],"uri":"\/","name":null,"action":"Closure"},{"host":null,"methods":["GET","HEAD"],"uri":"setting\/ppn","name":"setting.ppn","action":"App\Http\Controllers\ppnController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"setting\/city","name":"setting.city","action":"App\Http\Controllers\cityController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"setting\/city\/add","name":"setting.city.add","action":"App\Http\Controllers\cityController@add"},{"host":null,"methods":["POST"],"uri":"setting\/city\/create","name":"setting.city.create","action":"App\Http\Controllers\cityController@create"},{"host":null,"methods":["GET","HEAD"],"uri":"setting\/city\/edit","name":"setting.city.edit","action":"App\Http\Controllers\cityController@edit"},{"host":null,"methods":["GET","HEAD"],"uri":"setting\/biaya","name":"setting.biaya","action":"App\Http\Controllers\biayaController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"setting\/biaya\/add","name":"setting.biaya.add","action":"App\Http\Controllers\biayaController@add"},{"host":null,"methods":["POST"],"uri":"setting\/biaya\/create","name":"setting.biaya.create","action":"App\Http\Controllers\biayaController@create"},{"host":null,"methods":["GET","HEAD"],"uri":"setting\/biaya\/edit","name":"setting.biaya.edit","action":"App\Http\Controllers\biayaController@edit"},{"host":null,"methods":["GET","HEAD"],"uri":"setting\/pelabuhan","name":"setting.pelabuhan","action":"App\Http\Controllers\pelabuhanController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"setting\/pelabuhan\/add","name":"setting.pelabuhan.add","action":"App\Http\Controllers\pelabuhanController@add"},{"host":null,"methods":["POST"],"uri":"setting\/pelabuhan\/create","name":"setting.pelabuhan.create","action":"App\Http\Controllers\pelabuhanController@create"},{"host":null,"methods":["GET","HEAD"],"uri":"setting\/pelabuhan\/edit","name":"setting.pelabuhan.edit","action":"App\Http\Controllers\pelabuhanController@edit"},{"host":null,"methods":["GET","HEAD"],"uri":"setting\/asset","name":"setting.asset","action":"App\Http\Controllers\assetController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"setting\/asset\/add","name":"setting.asset.add","action":"App\Http\Controllers\assetController@add"},{"host":null,"methods":["GET","HEAD"],"uri":"setting\/asset\/edit","name":"setting.asset.edit","action":"App\Http\Controllers\assetController@edit"},{"host":null,"methods":["GET","HEAD"],"uri":"setting\/karyawan","name":"setting.karyawan","action":"App\Http\Controllers\karyawanController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"setting\/karyawan\/add","name":"setting.karyawan.add","action":"App\Http\Controllers\karyawanController@add"},{"host":null,"methods":["GET","HEAD"],"uri":"setting\/karyawan\/edit","name":"setting.karyawan.edit","action":"App\Http\Controllers\karyawanController@edit"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/pelayaran","name":"master.pelayaran","action":"App\Http\Controllers\pelayaranController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/pelayaran\/add","name":"master.pelayaran.add","action":"App\Http\Controllers\pelayaranController@add"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/pelayaran\/data","name":"master.pelayaran.data","action":"App\Http\Controllers\pelayaranController@getData"},{"host":null,"methods":["POST"],"uri":"master\/pelayaran\/create","name":"master.pelayaran.create","action":"App\Http\Controllers\pelayaranController@create"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/pelayaran\/edit\/{id}","name":"master.pelayaran.edit","action":"App\Http\Controllers\pelayaranController@edit"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/pelayaran\/detail","name":"master.pelayaran.detail","action":"App\Http\Controllers\pelayaranController@detail"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/pelayaran\/detailadd","name":"master.pelayaran.detailadd","action":"App\Http\Controllers\pelayaranController@detailadd"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/customer","name":"master.customer","action":"App\Http\Controllers\customerController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/customer\/add","name":"master.customer.add","action":"App\Http\Controllers\customerController@add"},{"host":null,"methods":["POST"],"uri":"master\/customer\/create","name":"master.customer.create","action":"App\Http\Controllers\customerController@create"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/customer\/edit","name":"master.customer.edit","action":"App\Http\Controllers\customerController@edit"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/consignee","name":"master.consignee","action":"App\Http\Controllers\consigneeController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/consignee\/add","name":"master.consignee.add","action":"App\Http\Controllers\consigneeController@add"},{"host":null,"methods":["POST"],"uri":"master\/consignee\/create","name":"master.consignee.create","action":"App\Http\Controllers\consigneeController@create"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/consignee\/edit","name":"master.consignee.edit","action":"App\Http\Controllers\consigneeController@edit"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/barang","name":"master.barang","action":"App\Http\Controllers\barangController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/barang\/add","name":"master.barang.add","action":"App\Http\Controllers\barangController@add"},{"host":null,"methods":["POST"],"uri":"master\/barang\/create","name":"master.barang.create","action":"App\Http\Controllers\barangController@create"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/barang\/edit","name":"master.barang.edit","action":"App\Http\Controllers\barangController@edit"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/dooring","name":"master.dooring","action":"App\Http\Controllers\dooringController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/dooring\/add","name":"master.dooring.add","action":"App\Http\Controllers\dooringController@add"},{"host":null,"methods":["POST"],"uri":"master\/dooring\/create","name":"master.dooring.create","action":"App\Http\Controllers\dooringController@create"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/dooring\/edit","name":"master.dooring.edit","action":"App\Http\Controllers\dooringController@edit"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/dooring\/detail","name":"master.dooring.detail","action":"App\Http\Controllers\dooringController@detail"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/dooring\/detailadd","name":"master.dooring.detailadd","action":"App\Http\Controllers\dooringController@detailadd"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/trucking","name":"master.trucking","action":"App\Http\Controllers\truckingController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/trucking\/add","name":"master.trucking.add","action":"App\Http\Controllers\truckingController@add"},{"host":null,"methods":["POST"],"uri":"master\/trucking\/create","name":"master.trucking.create","action":"App\Http\Controllers\truckingController@create"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/trucking\/edit","name":"master.trucking.edit","action":"App\Http\Controllers\truckingController@edit"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/trucking\/detail","name":"master.trucking.detail","action":"App\Http\Controllers\truckingController@detail"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/trucking\/detailadd","name":"master.trucking.detailadd","action":"App\Http\Controllers\truckingController@detailadd"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/trucking\/updateharga","name":"master.trucking.updateharga","action":"App\Http\Controllers\truckingController@updateharga"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/sector","name":"master.sector","action":"App\Http\Controllers\sectorController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/sector\/add","name":"master.sector.add","action":"App\Http\Controllers\sectorController@add"},{"host":null,"methods":["POST"],"uri":"master\/sector\/create","name":"master.sector.create","action":"App\Http\Controllers\sectorController@create"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/sector\/edit","name":"master.sector.edit","action":"App\Http\Controllers\sectorController@edit"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/trip","name":"master.trip","action":"App\Http\Controllers\tripController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/trip\/add","name":"master.trip.add","action":"App\Http\Controllers\tripController@add"},{"host":null,"methods":["POST"],"uri":"master\/trip\/create","name":"master.trip.create","action":"App\Http\Controllers\tripController@create"},{"host":null,"methods":["GET","HEAD"],"uri":"master\/trip\/edit","name":"master.trip.edit","action":"App\Http\Controllers\tripController@edit"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/customerproposal","name":"transaction.customerproposal","action":"App\Http\Controllers\customerProposalController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/customerproposal\/add","name":"transaction.customerproposal.add","action":"App\Http\Controllers\customerProposalController@add"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/customerproposal\/edit","name":"transaction.customerproposal.edit","action":"App\Http\Controllers\customerProposalController@edit"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/customerproposal\/detailadd","name":"transaction.customerproposal.detailadd","action":"App\Http\Controllers\customerProposalController@detailadd"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/proposalapproval","name":"transaction.proposalapproval","action":"App\Http\Controllers\proposalApprovalController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/proposalapproval\/detail","name":"transaction.proposalapproval.detail","action":"App\Http\Controllers\proposalApprovalController@detail"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/archivedproposal","name":"transaction.archivedproposal","action":"App\Http\Controllers\archivedProposalController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/archivedproposal\/detail","name":"transaction.archivedproposal.detail","action":"App\Http\Controllers\archivedProposalController@detail"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/customerorder","name":"transaction.customerorder","action":"App\Http\Controllers\customerOrderController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/customerorder\/add","name":"transaction.customerorder.add","action":"App\Http\Controllers\customerOrderController@add"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/customerorder\/edit","name":"transaction.customerorder.edit","action":"App\Http\Controllers\customerOrder@edit"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/customerorder\/detail","name":"transaction.customerorder.detail","action":"App\Http\Controllers\customerOrderController@detail"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/archivedorder","name":"transaction.archivedorder","action":"App\Http\Controllers\archivedOrderController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/archivedorder\/detail","name":"transaction.archivedorder.detail","action":"App\Http\Controllers\archivedOrderController@detail"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/updateorder","name":"transaction.updateorder","action":"App\Http\Controllers\updateOrderController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/suratjalan","name":"transaction.suratjalan","action":"App\Http\Controllers\suratjalanController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/suratjalan\/add","name":"transaction.suratjalan.add","action":"App\Http\Controllers\suratjalanController@add"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/suratjalan\/edit","name":"transaction.suratjalan.edit","action":"App\Http\Controllers\suratjalanController@edit"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/suratjalan\/doadd","name":"transaction.suratjalan.doadd","action":"App\Http\Controllers\suratjalanController@doadd"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/suratjalan\/detailsuratjalanadd","name":"transaction.suratjalan.detailsuratjalanadd","action":"App\Http\Controllers\suratjalanController@detailsuratjalanadd"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/bapb","name":"transaction.bapb","action":"App\Http\Controllers\BAPBController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/bapb\/add","name":"transaction.bapb.add","action":"App\Http\Controllers\BAPBController@add"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/bapb\/edit","name":"transaction.bapb.edit","action":"App\Http\Controllers\BAPBController@edit"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/bapb\/detailadd","name":"transaction.bapb.detailadd","action":"App\Http\Controllers\BAPBController@detailadd"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/archivedbapb","name":"transaction.archivedbapb","action":"App\Http\Controllers\archivedBAPBController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/archivedbapb\/detail","name":"transaction.archivedbapb.detail","action":"App\Http\Controllers\archivedBAPBController@detail"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/packinglist","name":"transaction.packinglist","action":"App\Http\Controllers\packingListController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/packinglist\/add","name":"transaction.packinglist.add","action":"App\Http\Controllers\packingListController@add"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/packinglist\/edit","name":"transaction.packinglist.edit","action":"App\Http\Controllers\packingListController@edit"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/kasir","name":"transaction.kasir","action":"App\Http\Controllers\kasirController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/kasir\/add","name":"transaction.kasir.add","action":"App\Http\Controllers\kasirController@add"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/kasir\/edit","name":"transaction.kasir.edit","action":"App\Http\Controllers\kasir@edit"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/invoice","name":"transaction.invoice","action":"App\Http\Controllers\invoiceController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/invoice\/add","name":"transaction.invoice.add","action":"App\Http\Controllers\invoiceController@add"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/invoice\/edit","name":"transaction.invoice.edit","action":"App\Http\Controllers\invoiceController@edit"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/invoice\/additionaladd","name":"transaction.invoice.additionaladd","action":"App\Http\Controllers\invoiceController@additionaladd"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/invoicelcl","name":"transaction.invoicelcl","action":"App\Http\Controllers\invoicelclController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/invoicelcl\/add","name":"transaction.invoicelcl.add","action":"App\Http\Controllers\invoicelclController@add"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/invoicelcl\/edit","name":"transaction.invoicelcl.edit","action":"App\Http\Controllers\invoicelclController@edit"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/custominvoice","name":"transaction.custominvoice","action":"App\Http\Controllers\custominvoiceController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/custominvoice\/add","name":"transaction.custominvoice.add","action":"App\Http\Controllers\custominvoiceController@add"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/custominvoice\/edit","name":"transaction.custominvoice.edit","action":"App\Http\Controllers\custominvoiceController@edit"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/penagihan","name":"transaction.penagihan","action":"App\Http\Controllers\penagihanController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/penagihan\/add","name":"transaction.penagihan.add","action":"App\Http\Controllers\penagihanController@add"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/penagihan\/edit","name":"transaction.penagihan.edit","action":"App\Http\Controllers\penagihanController@edit"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/pelunasan","name":"transaction.pelunasan","action":"App\Http\Controllers\pelunasanController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/pelunasan\/add","name":"transaction.pelunasan.add","action":"App\Http\Controllers\pelunasanController@add"},{"host":null,"methods":["GET","HEAD"],"uri":"transaction\/pelunasan\/edit","name":"transaction.pelunasan.edit","action":"App\Http\Controllers\pelunasanController@edit"}],
            prefix: '',

            route : function (name, parameters, route) {
                route = route || this.getByName(name);

                if ( ! route ) {
                    return undefined;
                }

                return this.toRoute(route, parameters);
            },

            url: function (url, parameters) {
                parameters = parameters || [];

                var uri = url + '/' + parameters.join('/');

                return this.getCorrectUrl(uri);
            },

            toRoute : function (route, parameters) {
                var uri = this.replaceNamedParameters(route.uri, parameters);
                var qs  = this.getRouteQueryString(parameters);

                if (this.absolute && this.isOtherHost(route)){
                    return "//" + route.host + "/" + uri + qs;
                }

                return this.getCorrectUrl(uri + qs);
            },

            isOtherHost: function (route){
                return route.host && route.host != window.location.hostname;
            },

            replaceNamedParameters : function (uri, parameters) {
                uri = uri.replace(/\{(.*?)\??\}/g, function(match, key) {
                    if (parameters.hasOwnProperty(key)) {
                        var value = parameters[key];
                        delete parameters[key];
                        return value;
                    } else {
                        return match;
                    }
                });

                // Strip out any optional parameters that were not given
                uri = uri.replace(/\/\{.*?\?\}/g, '');

                return uri;
            },

            getRouteQueryString : function (parameters) {
                var qs = [];
                for (var key in parameters) {
                    if (parameters.hasOwnProperty(key)) {
                        qs.push(key + '=' + parameters[key]);
                    }
                }

                if (qs.length < 1) {
                    return '';
                }

                return '?' + qs.join('&');
            },

            getByName : function (name) {
                for (var key in this.routes) {
                    if (this.routes.hasOwnProperty(key) && this.routes[key].name === name) {
                        return this.routes[key];
                    }
                }
            },

            getByAction : function(action) {
                for (var key in this.routes) {
                    if (this.routes.hasOwnProperty(key) && this.routes[key].action === action) {
                        return this.routes[key];
                    }
                }
            },

            getCorrectUrl: function (uri) {
                var url = this.prefix + '/' + uri.replace(/^\/?/, '');

                if ( ! this.absolute) {
                    return url;
                }

                return this.rootUrl.replace('/\/?$/', '') + url;
            }
        };

        var getLinkAttributes = function(attributes) {
            if ( ! attributes) {
                return '';
            }

            var attrs = [];
            for (var key in attributes) {
                if (attributes.hasOwnProperty(key)) {
                    attrs.push(key + '="' + attributes[key] + '"');
                }
            }

            return attrs.join(' ');
        };

        var getHtmlLink = function (url, title, attributes) {
            title      = title || url;
            attributes = getLinkAttributes(attributes);

            return '<a href="' + url + '" ' + attributes + '>' + title + '</a>';
        };

        return {
            // Generate a url for a given controller action.
            // laroute.action('HomeController@getIndex', [params = {}])
            action : function (name, parameters) {
                parameters = parameters || {};

                return routes.route(name, parameters, routes.getByAction(name));
            },

            // Generate a url for a given named route.
            // laroute.route('routeName', [params = {}])
            route : function (route, parameters) {
                parameters = parameters || {};

                return routes.route(route, parameters);
            },

            // Generate a fully qualified URL to the given path.
            // laroute.route('url', [params = {}])
            url : function (route, parameters) {
                parameters = parameters || {};

                return routes.url(route, parameters);
            },

            // Generate a html link to the given url.
            // laroute.link_to('foo/bar', [title = url], [attributes = {}])
            link_to : function (url, title, attributes) {
                url = this.url(url);

                return getHtmlLink(url, title, attributes);
            },

            // Generate a html link to the given route.
            // laroute.link_to_route('route.name', [title=url], [parameters = {}], [attributes = {}])
            link_to_route : function (route, title, parameters, attributes) {
                var url = this.route(route, parameters);

                return getHtmlLink(url, title, attributes);
            },

            // Generate a html link to the given controller action.
            // laroute.link_to_action('HomeController@getIndex', [title=url], [parameters = {}], [attributes = {}])
            link_to_action : function(action, title, parameters, attributes) {
                var url = this.action(action, parameters);

                return getHtmlLink(url, title, attributes);
            }

        };

    }).call(this);

    /**
     * Expose the class either via AMD, CommonJS or the global object
     */
    if (typeof define === 'function' && define.amd) {
        define(function () {
            return laroute;
        });
    }
    else if (typeof module === 'object' && module.exports){
        module.exports = laroute;
    }
    else {
        window.laroute = laroute;
    }

}).call(this);

