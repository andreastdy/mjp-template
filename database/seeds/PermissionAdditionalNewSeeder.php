<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models;

class PermissionAdditionalNewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        
        DB::table('permissions')->insert(['id'=>'61','name'=>'view role','display_name'=>'View Master Role','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'62','name'=>'add role','display_name'=>'Add Master Role','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'63','name'=>'view extra cost claim','display_name'=>'View Extra Cost/Claim','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'64','name'=>'add extra cost claim','display_name'=>'Add Extra Cost/Claim','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);

        $role = \Spatie\Permission\Models\Role::where('name', 'SUPERADMIN')->first();
        $role->syncPermissions(Permission::all());

        $user = Models\User::where('name', 'admin')->first();
        $user->syncPermissions(Permission::all());


    }
}
