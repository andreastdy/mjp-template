<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(GeneralSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(PermissionAdditionalSeeder::class);
        $this->call(PermissionAdditionalNewSeeder::class);
        $this->call(PermissionAdditionalNew2Seeder::class);
        $this->call(BiayaAdditionalSeeder::class);
    }
}
