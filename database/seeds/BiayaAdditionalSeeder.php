<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class BiayaAdditionalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        
        DB::table('masterbiaya')->insert([
            'id_biaya' => '12',
            'kode_biaya' => 'OPTMJP',
            'nama_biaya' => 'OPT MJP',
            'nominal_biaya' => '450000',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(), 
            'created_by' => 'admin',
            'updated_by' => 'admin',
        ]);

        

    }
}
