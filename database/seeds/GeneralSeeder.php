<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class GeneralSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'tes@gmail.com',
            'password' => bcrypt('1234'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(), 
            'username' => 'admin',
        ]);

        DB::table('ppn')->insert([
            'id_ppn' => '1',
            'ppn' => '1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(), 
            'created_by' => 'admin',
            'updated_by' => 'admin',
        ]);

        DB::table('masterbiaya')->insert([
            'id_biaya' => '1',
            'kode_biaya' => '20SUR',
            'nama_biaya' => 'SURCHARGE',
            'nominal_biaya' => '1000000',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(), 
            'created_by' => 'admin',
            'updated_by' => 'admin',
        ]);

        DB::table('masterbiaya')->insert([
            'id_biaya' => '2',
            'kode_biaya' => '40SUR',
            'nama_biaya' => 'SURCHARGE',
            'nominal_biaya' => '2000000',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(), 
            'created_by' => 'admin',
            'updated_by' => 'admin',
        ]);

        DB::table('masterbiaya')->insert([
            'id_biaya' => '3',
            'kode_biaya' => '20OPTD',
            'nama_biaya' => 'OPT DOORING',
            'nominal_biaya' => '500000',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(), 
            'created_by' => 'admin',
            'updated_by' => 'admin',
        ]);

        DB::table('masterbiaya')->insert([
            'id_biaya' => '4',
            'kode_biaya' => '40OPTD',
            'nama_biaya' => 'OPT DOORING',
            'nominal_biaya' => '1000000',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(), 
            'created_by' => 'admin',
            'updated_by' => 'admin',
        ]);

        DB::table('masterbiaya')->insert([
            'id_biaya' => '5',
            'kode_biaya' => 'OPTSBY',
            'nama_biaya' => 'OPT SURABAYA',
            'nominal_biaya' => '150000',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(), 
            'created_by' => 'admin',
            'updated_by' => 'admin',
        ]);

        DB::table('masterbiaya')->insert([
            'id_biaya' => '6',
            'kode_biaya' => '20THCP',
            'nama_biaya' => 'THC P',
            'nominal_biaya' => '350000',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(), 
            'created_by' => 'admin',
            'updated_by' => 'admin',
        ]);

        DB::table('masterbiaya')->insert([
            'id_biaya' => '7',
            'kode_biaya' => '20THCD',
            'nama_biaya' => 'THC D',
            'nominal_biaya' => '700000',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(), 
            'created_by' => 'admin',
            'updated_by' => 'admin',
        ]);

        DB::table('masterbiaya')->insert([
            'id_biaya' => '8',
            'kode_biaya' => '40THCP',
            'nama_biaya' => 'THC P',
            'nominal_biaya' => '400000',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(), 
            'created_by' => 'admin',
            'updated_by' => 'admin',
        ]);

        DB::table('masterbiaya')->insert([
            'id_biaya' => '9',
            'kode_biaya' => '40THCD',
            'nama_biaya' => 'THC D',
            'nominal_biaya' => '800000',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(), 
            'created_by' => 'admin',
            'updated_by' => 'admin',
        ]);

        DB::table('masterbiaya')->insert([
            'id_biaya' => '10',
            'kode_biaya' => 'M3',
            'nama_biaya' => 'M3',
            'nominal_biaya' => '2000',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(), 
            'created_by' => 'admin',
            'updated_by' => 'admin',
        ]);

        DB::table('masterbiaya')->insert([
            'id_biaya' => '11',
            'kode_biaya' => 'TON',
            'nama_biaya' => 'TON',
            'nominal_biaya' => '10000',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(), 
            'created_by' => 'admin',
            'updated_by' => 'admin',
        ]);

        

    }
}
