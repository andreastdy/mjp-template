<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models;

class PermissionAdditionalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        
        DB::table('permissions')->insert(['id'=>'57','name'=>'view detail proposal','display_name'=>'View Detail Proposal','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'58','name'=>'add invoice','display_name'=>'Add Invoice','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'59','name'=>'add invoice lcl','display_name'=>'Add Invoice LCL','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'60','name'=>'view monitoring order','display_name'=>'View Monitoring Order','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);

        $role = \Spatie\Permission\Models\Role::where('name', 'SUPERADMIN')->first();
        $role->syncPermissions(Permission::all());

        $user = Models\User::where('name', 'admin')->first();
        $user->syncPermissions(Permission::all());


    }
}
