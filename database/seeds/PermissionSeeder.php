<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('permissions')->insert(['id'=>'1','name'=>'edit ppn','display_name'=>'Edit Setting PPN','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'2','name'=>'view biaya','display_name'=>'View Setting Biaya','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'3','name'=>'edit biaya','display_name'=>'Edit Setting Biaya','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'4','name'=>'view pelayaran','display_name'=>'View Master Pelayaran','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'5','name'=>'edit pelayaran','display_name'=>'Edit Master Pelayaran','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'6','name'=>'view customer','display_name'=>'View Master Customer','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'7','name'=>'edit customer','display_name'=>'Edit Master Customer','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'8','name'=>'view consignee','display_name'=>'View Master Consignee','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'9','name'=>'edit consignee','display_name'=>'Edit Master Consignee','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'10','name'=>'view barang','display_name'=>'View Master Barang','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'11','name'=>'edit barang','display_name'=>'Edit Master Barang','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'12','name'=>'view dooring','display_name'=>'View Master Dooring','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'13','name'=>'edit dooring','display_name'=>'Edit Master Dooring','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'14','name'=>'view trucking','display_name'=>'View Master Trucking','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'15','name'=>'edit trucking','display_name'=>'Edit Master Trucking','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'16','name'=>'view sector','display_name'=>'View Master Sector','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'17','name'=>'edit sector','display_name'=>'Edit Master Sector','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'18','name'=>'view city','display_name'=>'View Master City','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'19','name'=>'edit city','display_name'=>'Edit Master City','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'20','name'=>'view pelabuhan','display_name'=>'View Master Pelabuhan','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'21','name'=>'edit pelabuhan','display_name'=>'Edit Master Pelabuhan','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'22','name'=>'view trip','display_name'=>'View Master Trip','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'23','name'=>'edit trip','display_name'=>'Edit Master Trip','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'24','name'=>'open akses trip','display_name'=>'Open Akses Master Trip','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'25','name'=>'view proposal','display_name'=>'View Proposal','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'26','name'=>'edit proposal','display_name'=>'Edit Proposal','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'27','name'=>'print proposal','display_name'=>'Print Proposal','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'28','name'=>'approve proposal','display_name'=>'Approve Proposal','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'29','name'=>'view order','display_name'=>'View Order','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'30','name'=>'edit order','display_name'=>'Edit Order','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'31','name'=>'execute order','display_name'=>'Execute Order','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'32','name'=>'view surat jalan','display_name'=>'View Surat Jalan','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'33','name'=>'edit surat jalan','display_name'=>'Edit Surat Jalan','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'34','name'=>'view BAPB','display_name'=>'View BAPB','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'35','name'=>'edit BAPB','display_name'=>'Edit BAPB','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'36','name'=>'print BAPB','display_name'=>'Print BAPB','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'37','name'=>'update tanggal BAPB','display_name'=>'Update Tanggal BAPB','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'38','name'=>'view packing list','display_name'=>'View Packing List','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'39','name'=>'print packing list','display_name'=>'Print Packing List','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'40','name'=>'view invoice','display_name'=>'View Invoice','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'41','name'=>'edit invoice','display_name'=>'Edit Invoice','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'42','name'=>'print invoice','display_name'=>'Print Invoice','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'43','name'=>'verify invoice','display_name'=>'Verify Invoice','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'44','name'=>'view invoice lcl','display_name'=>'View Invoice LCL','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'45','name'=>'edit invoice lcl','display_name'=>'Edit Invoice LCL','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'46','name'=>'print invoice lcl','display_name'=>'Print Invoice LCL','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'47','name'=>'verify invoice lcl','display_name'=>'Verify Invoice LCL','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'48','name'=>'view custom invoice','display_name'=>'View Custom Invoice','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'49','name'=>'edit custom invoice','display_name'=>'Edit Custom Invoice','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'50','name'=>'view penagihan','display_name'=>'View Penagihan','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'51','name'=>'edit penagihan','display_name'=>'Edit Penagihan','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'52','name'=>'view pelunasan','display_name'=>'View Pelunasan','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'53','name'=>'edit pelunasan','display_name'=>'Edit Pelunasan','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'54','name'=>'view user','display_name'=>'View Setting User','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'55','name'=>'edit user','display_name'=>'Edit Setting User','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);
        DB::table('permissions')->insert(['id'=>'56','name'=>'update status order','display_name'=>'Update Status Order','guard_name'=>'web','created_at' => Carbon::now(),'updated_at' => Carbon::now()]);

        $role = Role::create(['name' => 'SUPERADMIN']);
        $role->syncPermissions(Permission::all());

        $user = Models\User::where('name', 'admin')->first();
        $user->syncPermissions(Permission::all());

    }
}
