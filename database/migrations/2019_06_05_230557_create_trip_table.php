<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip', function (Blueprint $table) {
            $table->increments('id_trip');
            $table->string('kode_trip');
            $table->string('kode_trip_kota',10);
            $table->integer('kode_trip_nomor');
            $table->string('nama_kapal',50)->nullable();
            $table->dateTime('tanggal_berangkat');
            $table->integer('id_pelabuhan_tujuan');
            $table->integer('status');//0=open;1=complete
            $table->timestamps();
            $table->softDeletes();
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip');
    }
}
