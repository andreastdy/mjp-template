<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDooringdetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dooringdetail', function (Blueprint $table) {
            $table->increments('id_dooringdetail');
            $table->integer('id_dooring');
            $table->integer('id_kota_pelabuhan');
            $table->integer('id_kota_tujuan');
            $table->string('keterangan')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dooringdetail');
    }
}
