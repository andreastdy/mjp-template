<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDooringTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dooring', function (Blueprint $table) {
            $table->increments('id_dooring');
            $table->timestamps();
            $table->softDeletes();
            $table->string('nama',50);
            $table->string('alamat');
            $table->integer('id_kota');
            $table->string('rute');
            $table->string('rekening');
            $table->string('name_cp',100);
            $table->string('telp_cp');
            $table->string('email');
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dooring');
    }
}
