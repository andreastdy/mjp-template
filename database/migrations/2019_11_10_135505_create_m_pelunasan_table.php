<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMPelunasanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_pelunasan', function (Blueprint $table) {
            $table->increments('id_m_pelunasan');
            $table->string('kode_pelunasan');    
            $table->integer('id_customer');
            $table->dateTime('tgl_pelunasan'); 
            $table->text('keterangan')->nullable();
            $table->integer('total_bayar');
            $table->integer('status'); //0=belum verify; 1=sudah verify
            $table->timestamps();
            $table->softDeletes();
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_pelunasan');
    }
}
