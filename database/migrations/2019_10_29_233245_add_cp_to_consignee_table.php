<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCpToConsigneeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('consignee')){
            Schema::table('consignee', function (Blueprint $table) {
                $table->string('name_cp',100)->after('telp');
                $table->string('telp_cp')->after('name_cp');
            });
        }
        else return;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('consignee', function (Blueprint $table) {
            $table->dropColumn(['name_cp','telp_cp']);
        });
    }
}
