<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustominvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custominvoice', function (Blueprint $table) {
            $table->increments('id_custominvoice');
            $table->string('no_custominvoice');  
            $table->dateTime('tgl_custominvoice');
            $table->integer('id_customer');
            $table->string('keterangan_biaya')->nullable();  
            $table->integer('nominal_biaya');
            $table->string('kode_penagihan');  
            $table->dateTime('tgl_penagihan');
            $table->timestamps();
            $table->softDeletes();
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custominvoice');
    }
}
