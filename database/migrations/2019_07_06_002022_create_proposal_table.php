<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProposalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposal', function (Blueprint $table) {
            $table->increments('id_proposal');
            $table->string('no_proposal',50);
            $table->integer('id_customer');
            $table->integer('no_urut');
            $table->integer('customer_credit');
            $table->string('tipe_proposal',20);
            $table->string('keterangan_muat')->nullable();
            $table->longText('termcondition')->nullable();
            $table->integer('status');//1=pending,2=completed,3=approved,4=rejected,5=archived
            $table->dateTime('approved_at')->nullable();
            $table->string('approved_by')->nullable();
            $table->dateTime('rejected_at')->nullable();
            $table->string('rejected_by')->nullable();
            $table->timestamps();
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('deleted_by')->nullable();
            $table->dateTime('archived_at')->nullable();
            $table->string('archived_by')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proposal');
    }
}
