<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuratjalanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suratjalan', function (Blueprint $table) {
            $table->increments('id_suratjalan');
            $table->integer('id_order');
            $table->integer('id_kodetrip');
            $table->string('no_suratjalan');
            $table->string('alamat_pengambilan');
            $table->integer('id_consignee');
            $table->string('no_plattruck');
            $table->string('keterangan_total')->nullable();
            $table->string('no_container')->nullable();
            $table->string('seal')->nullable();
            $table->string('type_suratjalan');
            $table->string('freight_size')->nullable();
            $table->string('lcl_size')->nullable();
            $table->integer('status');
            $table->string('keterangan_status')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suratjalan');
    }
}
