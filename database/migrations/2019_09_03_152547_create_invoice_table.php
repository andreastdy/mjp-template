<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice', function (Blueprint $table) {
            $table->increments('id_invoice');
            $table->string('no_invoice');      
            $table->integer('id_trip');
            $table->integer('id_customer');
            $table->dateTime('tgl_invoice');
            $table->string('bukti_bayar')->nullable(); 
            $table->dateTime('tgl_penagihan')->nullable();
            $table->string('kode_penagihan')->nullable();   
            $table->integer('tax');
            $table->integer('include_tax');//0=no;1=yes default 1
            $table->integer('total');
            $table->integer('nominal_dibayar');//default 0
            $table->string('catatan')->nullable();
            $table->integer('status');//0=awal;1=validate;2=lunas default 0
            $table->timestamps();
            $table->softDeletes();
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('deleted_by')->nullable();
            $table->string('verified_by')->nullable();
            $table->dateTime('verified_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice');
    }
}
