<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->increments('id_order');
            $table->string('type_order');
            $table->integer('id_proposaldetail');
            $table->integer('id_customer');
            $table->integer('id_kota_asal');
            $table->integer('id_pelabuhan_asal');
            $table->integer('id_kota_tujuan');
            $table->integer('id_pelabuhan_tujuan');
            $table->integer('id_pelayaran');
            $table->string('freight_size')->nullable();
            $table->string('lcl_size')->nullable();
            $table->string('jenis_barang');
            $table->string('alamat_asal');
            $table->string('alamat_tujuan');
            $table->integer('nominal_pelayaran_order');
            $table->integer('nominal_m3')->nullable();
            $table->integer('nominal_ton')->nullable();
            $table->integer('nominal_optsurabaya')->nullable();
            $table->integer('nominal_thcp')->nullable();
            $table->integer('nominal_thcd')->nullable();
            $table->integer('jumlah_order')->nullable();
            $table->dateTime('jadwal_pengambilan');
            $table->integer('status');//0=not ready;1=executed
            $table->dateTime('executed_at')->nullable();
            $table->string('executed_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
