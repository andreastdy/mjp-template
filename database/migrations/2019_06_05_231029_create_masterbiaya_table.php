<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterbiayaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('masterbiaya', function (Blueprint $table) {
            $table->increments('id_biaya');
            $table->timestamps();
            $table->softDeletes();
            $table->string('kode_biaya',10);
            $table->string('nama_biaya',50);
            $table->integer('nominal_biaya');
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('masterbiaya');
    }
}
