<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDodetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dodetail', function (Blueprint $table) {
            $table->increments('id_dodetail');
            $table->integer('id_do');
            $table->string('kode_barang');     
            $table->string('nama_barang');       
            $table->integer('panjang')->nullable();
            $table->integer('lebar')->nullable();
            $table->integer('tinggi')->nullable();
            $table->integer('m3')->nullable();
            $table->integer('ton')->nullable();
            $table->integer('kolli')->nullable();
            $table->string('satuan');    
            $table->timestamps();
            $table->softDeletes();
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dodetail');
    }
}
