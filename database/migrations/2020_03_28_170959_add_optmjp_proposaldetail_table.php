<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOptmjpProposaldetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (Schema::hasTable('proposaldetail')){
            Schema::table('proposaldetail', function (Blueprint $table) {
                $table->integer('nominal_optmjp')->after('nominal_optdooring');
            });
        }
        else return;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('proposaldetail', function (Blueprint $table) {
            $table->dropColumn(['nominal_optmjp']);
        });
    }
}
