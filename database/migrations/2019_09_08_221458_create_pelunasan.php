<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePelunasan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pelunasan', function (Blueprint $table) {
            $table->increments('id_pelunasan');
            $table->integer('id_invoice');      
            $table->string('tipe_invoice');     
            $table->string('bukti_bayar');     
            $table->dateTime('tgl_pelunasan'); 
            $table->integer('pph23');
            $table->integer('potongan_admin');
            $table->integer('claim');      
            $table->integer('nominal_dibayar');
            $table->string('keterangan')->nullable();
            $table->integer('status');//0=awal;1=validate default 0
            $table->timestamps();
            $table->softDeletes();
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pelunasan');
    }
}
