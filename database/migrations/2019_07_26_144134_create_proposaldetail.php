<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProposaldetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposaldetail', function (Blueprint $table) {
            $table->increments('id_proposaldetail');
            $table->integer('id_proposal');
            $table->dateTime('valid_from');
            $table->dateTime('valid_to');
            $table->integer('id_pelabuhan_asal');
            $table->integer('id_pelabuhan_tujuan');
            $table->string('type_proposal');
            $table->string('jenis_proposal');
            $table->string('jenis_barang');
            $table->string('alamat_asal');
            $table->string('alamat_tujuan');
            $table->string('freight_size')->nullable();
            $table->string('lcl_size')->nullable();
            $table->integer('nominal_m3')->nullable();
            $table->integer('nominal_ton')->nullable();
            $table->integer('id_pelayaran');
            $table->integer('nominal_pelayaran');
            $table->integer('nominal_extrapelayaran')->nullable();
            $table->string('keterangan_extrapelayaran')->nullable();
            $table->string('notes_pelayaran')->nullable();;
            $table->integer('id_truckingdetail');
            $table->integer('nominal_trucking');
            $table->integer('id_dooring');
            $table->integer('nominal_dooring');
            $table->integer('nominal_extradooring');
            $table->integer('nominal_optsurabaya')->nullable();
            $table->integer('nominal_thcp')->nullable();
            $table->integer('nominal_thcd')->nullable();
            $table->integer('nominal_surcharge')->nullable();
            $table->integer('nominal_optdooring')->nullable();
            $table->integer('nominal_total');
            $table->integer('status');//1=pending;2=complete
            $table->timestamps();
            $table->softDeletes();
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proposaldetail');
    }
}
