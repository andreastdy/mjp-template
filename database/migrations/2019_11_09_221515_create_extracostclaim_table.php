<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtracostclaimTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extracostclaim', function (Blueprint $table) {
            $table->increments('id_extracostclaim');
            $table->string('kode_biaya');    
            $table->integer('id_customer');
            $table->integer('id_invoice');
            $table->string('no_invoice');      
            $table->string('tipe_invoice');
            $table->dateTime('tgl_biaya'); 
            $table->string('tipe_biaya');// claim;extracost
            $table->integer('nominal_biaya');
            $table->text('deskripsi');
            $table->integer('status');//0=awal;1=lunas; default 0
            $table->timestamps();
            $table->softDeletes();
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extracostclaim');
    }
}
