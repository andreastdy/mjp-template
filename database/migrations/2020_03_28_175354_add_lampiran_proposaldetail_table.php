<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLampiranProposaldetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (Schema::hasTable('proposaldetail')){
            Schema::table('proposaldetail', function (Blueprint $table) {
                $table->longText('lampiran')->nullable()->after('alamat_tujuan');
            });
        }
        else return;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('proposaldetail', function (Blueprint $table) {
            $table->dropColumn(['lampiran']);
        });
    }
}
