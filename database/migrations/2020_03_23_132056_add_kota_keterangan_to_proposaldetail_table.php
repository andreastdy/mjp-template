<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKotaKeteranganToProposaldetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('proposaldetail')){
            Schema::table('proposaldetail', function (Blueprint $table) {
                $table->integer('id_kota_asal')->after('valid_to');
                $table->integer('id_kota_tujuan')->after('id_pelabuhan_asal');
                $table->String('keteranganpengiriman')->nullable()->after('jenis_barang');
            });
        }
        else return;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proposaldetail', function (Blueprint $table) {
            $table->dropColumn(['id_kota_asal','id_kota_tujuan','keteranganpengiriman']);
        });
    }
}
