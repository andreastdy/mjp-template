<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNominalDibayarToCustominvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('custominvoice')){
            Schema::table('custominvoice', function (Blueprint $table) {
                $table->integer('nominal_dibayar')->after('tgl_penagihan');
                $table->integer('status')->after('nominal_dibayar'); //0=awal, 1=lunas
            });
        }
        else return;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('custominvoice', function (Blueprint $table) {
            $table->dropColumn(['nominal_dibayar','status']);
        });
    }
}
