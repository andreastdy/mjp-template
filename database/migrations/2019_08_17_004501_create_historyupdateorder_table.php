<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryupdateorderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historyupdateorder', function (Blueprint $table) {
            $table->increments('id_history');
            $table->integer('id_order');            
            $table->integer('id_suratjalan');            
            $table->integer('status');//0=Barang Diterima,1=Kapal Berangkat,2=Kapal Tiba,3=Kapal Sandar,4=Kapal Bongkar, 5=Barang Diterima Dooring, 6=Barang Diterima Customer
            $table->string('keterangan')->nullable();
            $table->timestamps();
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historyupdateorder');
    }
}
