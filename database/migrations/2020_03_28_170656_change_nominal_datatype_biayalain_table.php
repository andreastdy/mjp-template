<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeNominalDatatypeBiayalainTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (Schema::hasTable('biayalain')){
            Schema::table('biayalain', function (Blueprint $table) {
                $table->String('nominalbiayalain')->change();
            });
        }
        else return;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('biayalain', function (Blueprint $table) {
            $table->integer('nominalbiayalain')->change();
        });
    }
}
