<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBuktibayarToPelunasanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('m_pelunasan')){
            Schema::table('m_pelunasan', function (Blueprint $table) {
                $table->string('bukti_bayar')->after('tgl_pelunasan')->nullable();
            });
        }
        else return;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_pelunasan', function (Blueprint $table) {
            $table->dropColumn(['bukti_bayar']);
        });
    }
}
