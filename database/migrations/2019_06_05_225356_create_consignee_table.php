<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsigneeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consignee', function (Blueprint $table) {
            $table->increments('id_consignee');
            $table->timestamps();
            $table->softDeletes();
            $table->string('nama',50);
            $table->string('alamat');
            $table->integer('id_kota');
            $table->string('telp');
            $table->string('keterangan')->nullable();
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consignee');
    }
}
