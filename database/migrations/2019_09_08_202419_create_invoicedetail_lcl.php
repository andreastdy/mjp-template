<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicedetailLcl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoicedetail_lcl', function (Blueprint $table) {
            $table->increments('id_invoicedetail_lcl');
            $table->integer('id_invoice_lcl');
            $table->string('no_invoice_lcl');  
            $table->integer('id_suratjalan'); 
            $table->integer('biaya');
            $table->string('biaya_tambahan_1')->nullable();
            $table->integer('nominal_biaya_tambahan_1')->nullable();
            $table->string('biaya_tambahan_2')->nullable();
            $table->integer('nominal_biaya_tambahan_2')->nullable();
            $table->string('biaya_tambahan_3')->nullable();
            $table->integer('nominal_biaya_tambahan_3')->nullable();
            $table->integer('total');
            $table->timestamps();
            $table->softDeletes();
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoicedetail_lcl');
    }
}
