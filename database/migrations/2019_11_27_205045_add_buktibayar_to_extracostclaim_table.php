<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBuktibayarToExtracostclaimTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('extracostclaim')){
            Schema::table('extracostclaim', function (Blueprint $table) {
                $table->string('bukti_bayar')->after('deskripsi')->nullable();
            });
        }
        else return;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('extracostclaim', function (Blueprint $table) {
            $table->dropColumn(['bukti_bayar']);
        });
    }
}
