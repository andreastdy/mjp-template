<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDPelunasanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('d_pelunasan', function (Blueprint $table) {
            $table->increments('id_d_pelunasan');
            $table->integer('id_m_pelunasan');
            $table->integer('id_detail');
            $table->string('no_detail');   
            $table->string('tipe_detail');   
            $table->integer('nominal_detail'); 
            $table->timestamps();
            $table->softDeletes();
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('d_pelunasan');
    }
}
