<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPphToMPelunasanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('m_pelunasan')){
            Schema::table('m_pelunasan', function (Blueprint $table) {
                $table->integer('pph23')->after('keterangan');
                $table->integer('admin_bank')->after('pph23');
                $table->integer('lain_lain')->after('admin_bank');
            });
        }
        else return;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_pelunasan', function (Blueprint $table) {
            $table->dropColumn(['pph23','admin_bank','lain_lain']);
        });
    }
}
