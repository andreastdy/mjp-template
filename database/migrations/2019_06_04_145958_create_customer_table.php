<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer', function (Blueprint $table) {
            $table->increments('id_customer');
            $table->string('nama_pt',50);
            $table->string('name_short',20);
            $table->string('alamat_pt');
            $table->string('alamat_gudang_1');
            $table->string('alamat_gudang_2')->nullable();
            $table->string('alamat_gudang_3')->nullable();
            $table->integer('id_kota');
            $table->string('kodepos',10);
            $table->string('telp');
            $table->string('name_cp',100);
            $table->string('telp_cp');
            $table->string('email_1');
            $table->string('email_2')->nullable();
            $table->string('email_3')->nullable();
            $table->integer('lama_kredit');
            $table->string('keterangan')->nullable();
            $table->timestamps();
            $table->string('created_by');
            $table->string('updated_by');
            $table->softDeletes();
            $table->string('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer');
    }
}
