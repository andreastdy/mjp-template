<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBuktibayarToCustominvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('custominvoice')){
            Schema::table('custominvoice', function (Blueprint $table) {
                $table->string('bukti_bayar')->after('tgl_penagihan')->nullable();
            });
        }
        else return;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('custominvoice', function (Blueprint $table) {
            $table->dropColumn(['bukti_bayar']);
        });
    }
}
