<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTruckingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trucking', function (Blueprint $table) {
            $table->increments('id_trucking');
            $table->timestamps();
            $table->softDeletes();
            $table->string('nama',50);
            $table->string('alamat');
            $table->string('rekening');
            $table->string('name_cp',100);
            $table->string('telp_cp',20);
            $table->string('email',50);
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trucking');
    }
}
