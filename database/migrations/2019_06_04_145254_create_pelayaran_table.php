<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePelayaranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pelayaran', function (Blueprint $table) {
            $table->increments('id_pelayaran');
            $table->timestamps();
            $table->softDeletes();
            $table->string('name_short',50);
            $table->string('name_full',100);
            $table->string('alamat');
            $table->integer('id_kota');
            $table->integer('kodepos');
            $table->string('telp');
            $table->string('name_cp',100);
            $table->string('telp_cp');
            $table->string('email');
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pelayaran');
    }
}
