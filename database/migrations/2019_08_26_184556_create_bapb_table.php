<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBapbTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bapb', function (Blueprint $table) {
            $table->increments('id_bapb');       
            $table->integer('id_order');     
            $table->string('no_bapb');      
            $table->integer('id_trip');          
            $table->integer('id_dooring');
            $table->integer('id_consignee');      
            $table->dateTime('tanggal_kirim')->nullable();
            $table->dateTime('tanggal_kembali')->nullable();
            $table->string('type_bapb');        
            $table->timestamps();
            $table->softDeletes();
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bapb');
    }
}
