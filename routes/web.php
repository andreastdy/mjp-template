<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('admin/register', function () {
    return view('dashboard/index');
});

Route::group(['middleware' => ['guest']], function() {
    Route::get('login', ['uses' => 'loginController@index', 'as' => 'login']);
    Route::post('login', ['uses' => 'loginController@Authenticate', 'as' => 'authenticate']);    
});

Route::get('logout', ['uses' => 'loginController@destroy', 'as' => 'logout']);

Route::group(['middleware' => ['auth']], function() {
    Route::get('/', function () {
        return view('dashboard/index');
    });

    Route::get('change_password', ['uses' => 'loginController@ubahPassword', 'as' => 'ubah_password']);        
    Route::post('change-password', ['uses' => 'loginController@changePassword', 'as' => 'change_password']);    

    Route::group(['prefix' => 'setting'], function () {
        Route::group(['middleware' => ['permission:edit ppn']], function () {
            Route::get('/ppn',['uses' => 'ppnController@index','as' => 'setting.ppn']);
            Route::post('/ppn/update',['uses' => 'ppnController@update','as' => 'setting.ppn.update']);
        });
        
        Route::group(['middleware' => ['permission:view city']], function () {
            Route::get('/city',['uses' => 'cityController@index','as' => 'setting.city']);
            Route::get('/city/data', ['uses' => 'cityController@getData', 'as' => 'setting.city.data']);

            Route::group(['middleware' => ['permission:edit city']], function () {
                Route::get('/city/add',['uses' => 'cityController@add','as' => 'setting.city.add']);
                Route::post('/city/create',['uses' => 'cityController@create','as' => 'setting.city.create']);
                Route::get('/city/edit/{id}',['uses' => 'cityController@edit','as' => 'setting.city.ubah']);
                Route::post('/city/update/{id}',['uses' => 'cityController@update','as' => 'setting.city.update']);
                Route::post('/city/{id}',['uses' => 'cityController@delete','as' => 'setting.city.delete']);
            });
        });
            
        Route::group(['middleware' => ['permission:view biaya']], function () {
            Route::get('/biaya',['uses' => 'biayaController@index','as' => 'setting.biaya']);
            Route::get('/biaya/data', ['uses' => 'biayaController@getData', 'as' => 'setting.biaya.data']);

            Route::group(['middleware' => ['permission:edit biaya']], function () {
                Route::get('/biaya/add',['uses' => 'biayaController@add','as' => 'setting.biaya.add']);
                Route::post('/biaya/create',['uses' => 'biayaController@create','as' => 'setting.biaya.create']);
                Route::get('/biaya/edit/{id}',['uses' => 'biayaController@edit','as' => 'setting.biaya.ubah']);
                Route::post('/biaya/update/{id}',['uses' => 'biayaController@update','as' => 'setting.biaya.update']);
                Route::post('/biaya/{id}',['uses' => 'biayaController@delete','as' => 'setting.biaya.delete']);
            });
        });
            
        Route::group(['middleware' => ['permission:view pelabuhan']], function () {
            Route::get('/pelabuhan',['uses' => 'pelabuhanController@index','as' => 'setting.pelabuhan']);
            Route::get('/pelabuhan/data', ['uses' => 'pelabuhanController@getData', 'as' => 'setting.pelabuhan.data']);

            Route::group(['middleware' => ['permission:edit pelabuhan']], function () {
                Route::get('/pelabuhan/add',['uses' => 'pelabuhanController@add','as' => 'setting.pelabuhan.add']);
                Route::post('/pelabuhan/create',['uses' => 'pelabuhanController@create','as' => 'setting.pelabuhan.create']);
                Route::get('/pelabuhan/edit/{id}',['uses' => 'pelabuhanController@edit','as' => 'setting.pelabuhan.ubah']);
                Route::post('/pelabuhan/update/{id}',['uses' => 'pelabuhanController@update','as' => 'setting.pelabuhan.update']);
                Route::post('/pelabuhan/{id}',['uses' => 'pelabuhanController@delete','as' => 'setting.pelabuhan.delete']);
            });
        });

        Route::group(['middleware' => ['permission:view user']], function () {
            Route::get('/user',['uses' => 'userController@index','as' => 'setting.user']);
            Route::get('/user/data', ['uses' => 'userController@getData', 'as' => 'setting.user.data']);
            Route::get('/user/getpermission/{name}', ['uses' => 'userController@getPermission', 'as' => 'setting.user.getpermission']);

            Route::group(['middleware' => ['permission:edit user']], function () {
                Route::get('/user/add',['uses' => 'userController@add','as' => 'setting.user.add']);
                Route::post('/user/create',['uses' => 'userController@create','as' => 'setting.user.create']);
                Route::get('/user/edit/{id}',['uses' => 'userController@edit','as' => 'setting.user.ubah']);
                Route::post('/user/update/{id}',['uses' => 'userController@update','as' => 'setting.user.update']);
                Route::post('/user/{id}',['uses' => 'userController@delete','as' => 'setting.user.delete']);
            });
        });

        Route::group(['middleware' => ['permission:view role']], function () {
            Route::get('/role',['uses' => 'roleController@index','as' => 'setting.role']);
            Route::get('/role/data', ['uses' => 'roleController@getData', 'as' => 'setting.role.data']);
            // Route::get('/role/getpermission/{name}', ['uses' => 'roleController@getPermission', 'as' => 'setting.role.getpermission']);

            Route::group(['middleware' => ['permission:add role']], function () {
                Route::get('/role/add',['uses' => 'roleController@add','as' => 'setting.role.add']);
                Route::post('/role/create',['uses' => 'roleController@create','as' => 'setting.role.create']);
                Route::get('/role/edit/{name}',['uses' => 'roleController@edit','as' => 'setting.role.ubah']);
                Route::post('/role/update/{name}',['uses' => 'roleController@update','as' => 'setting.role.update']);
                Route::post('/role/{name}',['uses' => 'roleController@delete','as' => 'setting.role.delete']);
            });
        });
            
            
            Route::get('/asset',['uses' => 'assetController@index','as' => 'setting.asset']);
            Route::get('/asset/add',['uses' => 'assetController@add','as' => 'setting.asset.add']);
            Route::get('/asset/edit',['uses' => 'assetController@edit','as' => 'setting.asset.edit']);
            
            Route::get('/karyawan',['uses' => 'karyawanController@index','as' => 'setting.karyawan']);
            Route::get('/karyawan/add',['uses' => 'karyawanController@add','as' => 'setting.karyawan.add']);
            Route::get('/karyawan/edit',['uses' => 'karyawanController@edit','as' => 'setting.karyawan.edit']);
            // Route::get('/data/{id?}',['uses' => 'EmployeeController@getData','as' => 'employee.data']);
            // Route::post('/store', ['uses' => 'EmployeeController@store', 'as' => 'employee.store']);
            // Route::post('/update/{id}', ['uses' => 'EmployeeController@update', 'as' => 'employee.update']);
            // Route::get('/delete/{id}', ['uses' => 'EmployeeController@delete', 'as' => 'employee.delete']);
        });
    
 Route::group(['prefix' => 'master'], function () {
     
    Route::group(['middleware' => ['permission:view pelayaran']], function () {
        Route::get('/pelayaran',['uses' => 'pelayaranController@index','as' => 'master.pelayaran']);
        Route::get('/pelayaran/add',['uses' => 'pelayaranController@add','as' => 'master.pelayaran.add']);
        Route::get('/pelayaran/data', ['uses' => 'pelayaranController@getData', 'as' => 'master.pelayaran.data']);
        Route::post('/pelayaran/create',['uses' => 'pelayaranController@create','as' => 'master.pelayaran.create']);
        Route::get('/pelayaran/edit/{id}',['uses' => 'pelayaranController@edit','as' => 'master.pelayaran.ubah']);
        Route::post('/pelayaran/edit/{id}',['uses' => 'pelayaranController@update','as' => 'master.pelayaran.update']);
        Route::get('/pelayaran/detail/{id}',['uses' => 'pelayaranController@detail','as' => 'master.pelayaran.detail']);
        Route::get('/pelayaran/datadetail/{id}', ['uses' => 'pelayaranController@getDataDetail', 'as' => 'master.pelayaran.datadetail']);
        Route::get('/pelayaran/detailadd/{id}',['uses' => 'pelayaranController@addDetail','as' => 'master.pelayaran.detailadd']);
        Route::post('/pelayaran/detailadd/{id}',['uses' => 'pelayaranController@createDetail','as' => 'master.pelayaran.detailcreate']);
        Route::get('/pelayaran/detailadd/edit/{id}',['uses' => 'pelayaranController@editDetail','as' => 'master.pelayaran.ubahdetail']);
        Route::post('/pelayaran/detailadd/edit/{id}',['uses' => 'pelayaranController@updateDetail','as' => 'master.pelayaran.detailupdate']);
        Route::post('/pelayaran/detail/{id}',['uses' => 'pelayaranController@deleteDetail','as' => 'master.pelayaran.detaildelete']);
    });
        
    Route::group(['middleware' => ['permission:view customer']], function () {
        Route::get('/customer',['uses' => 'customerController@index','as' => 'master.customer']);
        Route::get('/customer/data', ['uses' => 'customerController@getData', 'as' => 'master.customer.data']);
        Route::get('/customer/add',['uses' => 'customerController@add','as' => 'master.customer.add']);
        Route::post('/customer/create',['uses' => 'customerController@create','as' => 'master.customer.create']);
        Route::get('/customer/edit/{id}',['uses' => 'customerController@edit','as' => 'master.customer.ubah']);
        Route::post('/customer/update/{id}',['uses' => 'customerController@update','as' => 'master.customer.update']);
        Route::post('/customer/{id}',['uses' => 'customerController@delete','as' => 'master.customer.delete']);
    });
        
    Route::group(['middleware' => ['permission:view consignee']], function () {
        Route::get('/consignee',['uses' => 'consigneeController@index','as' => 'master.consignee']);
        Route::get('/consignee/data', ['uses' => 'consigneeController@getData', 'as' => 'master.consignee.data']);

        Route::group(['middleware' => ['permission:edit consignee']], function () {
            Route::get('/consignee/add',['uses' => 'consigneeController@add','as' => 'master.consignee.add']);
            Route::post('/consignee/create',['uses' => 'consigneeController@create','as' => 'master.consignee.create']);
            Route::get('/consignee/edit/{id}',['uses' => 'consigneeController@edit','as' => 'master.consignee.ubah']);
            Route::post('/consignee/update/{id}',['uses' => 'consigneeController@update','as' => 'master.consignee.update']);
            Route::post('/consignee/{id}',['uses' => 'consigneeController@delete','as' => 'master.consignee.delete']);
        });
    });
        
    Route::group(['middleware' => ['permission:view barang']], function () {
        Route::get('/barang',['uses' => 'barangController@index','as' => 'master.barang']);
        Route::get('/barang/data', ['uses' => 'barangController@getData', 'as' => 'master.barang.data']);

        Route::group(['middleware' => ['permission:edit barang']], function () {
            Route::get('/barang/add',['uses' => 'barangController@add','as' => 'master.barang.add']);
            Route::post('/barang/create',['uses' => 'barangController@create','as' => 'master.barang.create']);
            Route::get('/barang/edit/{id}',['uses' => 'barangController@edit','as' => 'master.barang.ubah']);
            Route::post('/barang/update/{id}',['uses' => 'barangController@update','as' => 'master.barang.update']);
            Route::post('/barang/{id}',['uses' => 'barangController@delete','as' => 'master.barang.delete']);
        });
    });
        
    Route::group(['middleware' => ['permission:view dooring']], function () {
        Route::get('/dooring',['uses' => 'dooringController@index','as' => 'master.dooring']);
        Route::get('/dooring/data', ['uses' => 'dooringController@getData', 'as' => 'master.dooring.data']);
        Route::get('/dooring/add',['uses' => 'dooringController@add','as' => 'master.dooring.add']);
        Route::post('/dooring/create',['uses' => 'dooringController@create','as' => 'master.dooring.create']);
        Route::get('/dooring/edit/{id}',['uses' => 'dooringController@edit','as' => 'master.dooring.ubah']);
        Route::post('/dooring/edit/{id}',['uses' => 'dooringController@update','as' => 'master.dooring.update']);
        Route::get('/dooring/detail/{id}',['uses' => 'dooringController@detail','as' => 'master.dooring.detail']);
        Route::get('/dooring/datadetail/{id}', ['uses' => 'dooringController@getDataDetail', 'as' => 'master.dooring.datadetail']);
        Route::get('/dooring/detailadd/{id}',['uses' => 'dooringController@addDetail','as' => 'master.dooring.detailadd']);
        Route::post('/dooring/detailadd/{id}',['uses' => 'dooringController@createDetail','as' => 'master.dooring.detailcreate']);
        Route::get('/dooring/detailadd/edit/{id}',['uses' => 'dooringController@editDetail','as' => 'master.dooring.ubahdetail']);
        Route::post('/dooring/detailadd/edit/{id}',['uses' => 'dooringController@updateDetail','as' => 'master.dooring.detailupdate']);
        Route::post('/dooring/detail/{id}',['uses' => 'dooringController@deleteDetail','as' => 'master.dooring.detaildelete']);
    });

    Route::group(['middleware' => ['permission:view trucking']], function () {
        Route::get('/trucking',['uses' => 'truckingController@index','as' => 'master.trucking']);
        Route::get('/trucking/data', ['uses' => 'truckingController@getData', 'as' => 'master.trucking.data']);
        Route::get('/trucking/add',['uses' => 'truckingController@add','as' => 'master.trucking.add']);
        Route::post('/trucking/create',['uses' => 'truckingController@create','as' => 'master.trucking.create']);
        Route::get('/trucking/edit/{id}',['uses' => 'truckingController@edit','as' => 'master.trucking.ubah']);
        Route::post('/trucking/edit/{id}',['uses' => 'truckingController@update','as' => 'master.trucking.update']);
        Route::get('/trucking/detail/{id}',['uses' => 'truckingController@detail','as' => 'master.trucking.detail']);
        Route::get('/trucking/datadetail/{id}', ['uses' => 'truckingController@getDataDetail', 'as' => 'master.trucking.datadetail']);
        Route::get('/trucking/detailadd/{id}',['uses' => 'truckingController@addDetail','as' => 'master.trucking.detailadd']);
        Route::post('/trucking/detailadd/{id}',['uses' => 'truckingController@createDetail','as' => 'master.trucking.detailcreate']);
        Route::get('/trucking/detailadd/edit/{id}',['uses' => 'truckingController@editDetail','as' => 'master.trucking.ubahdetail']);
        Route::post('/trucking/detailadd/edit/{id}',['uses' => 'truckingController@updateDetail','as' => 'master.trucking.detailupdate']);
        Route::post('/trucking/detail/{id}',['uses' => 'truckingController@deleteDetail','as' => 'master.trucking.detaildelete']);
        Route::get('/trucking/updateharga/{id}',['uses' => 'truckingController@ubahHarga','as' => 'master.trucking.ubahharga']);
        Route::post('/trucking/updateharga/{id}',['uses' => 'truckingController@updateHarga','as' => 'master.trucking.updateharga']);
    });

    Route::group(['middleware' => ['permission:view sector']], function () {
        Route::get('/sector',['uses' => 'sectorController@index','as' => 'master.sector']);
        Route::get('/sector/data', ['uses' => 'sectorController@getData', 'as' => 'master.sector.data']);

        Route::group(['middleware' => ['permission:edit sector']], function () {
            Route::get('/sector/add',['uses' => 'sectorController@add','as' => 'master.sector.add']);
            Route::post('/sector/create',['uses' => 'sectorController@create','as' => 'master.sector.create']);
            Route::get('/sector/edit/{id}',['uses' => 'sectorController@edit','as' => 'master.sector.ubah']);
            Route::post('/sector/update/{id}',['uses' => 'sectorController@update','as' => 'master.sector.update']);
            Route::post('/sector/{id}',['uses' => 'sectorController@delete','as' => 'master.sector.delete']);
        });
    });

    Route::group(['middleware' => ['permission:view trip']], function () {
        Route::get('/trip',['uses' => 'tripController@index','as' => 'master.trip']);
        Route::get('/trip/data', ['uses' => 'tripController@getData', 'as' => 'master.trip.data']);
        Route::post('/trip/complete/{id}',['uses' => 'tripController@complete','as' => 'master.trip.complete']);
        Route::post('/trip/reopen/{id}',['uses' => 'tripController@reopen','as' => 'master.trip.reopen']);

        Route::group(['middleware' => ['permission:edit trip']], function () {
            Route::get('/trip/add',['uses' => 'tripController@add','as' => 'master.trip.add']);
            Route::post('/trip/create',['uses' => 'tripController@create','as' => 'master.trip.create']);
            Route::get('/trip/edit/{id}',['uses' => 'tripController@edit','as' => 'master.trip.ubah']);
            Route::post('/trip/update/{id}',['uses' => 'tripController@update','as' => 'master.trip.update']);
        });
    });
        

});


 Route::group(['prefix' => 'transaction'], function () {

    Route::group(['middleware' => ['permission:view proposal']], function () {
        Route::get('/customerproposal',['uses' => 'customerProposalController@index','as' => 'transaction.customerproposal']);
        Route::get('/customerproposal/data', ['uses' => 'customerProposalController@getData', 'as' => 'transaction.customerproposal.data']);
        Route::get('/customerproposal/add',['uses' => 'customerProposalController@add','as' => 'transaction.customerproposal.add']);
        Route::post('/customerproposal/create',['uses' => 'customerProposalController@create','as' => 'transaction.customerproposal.create']);
        Route::get('/customerproposal/edit/{id}',['uses' => 'customerProposalController@edit','as' => 'transaction.customerproposal.edit']);
        Route::post('/customerproposal/archive/{id}',['uses' => 'customerProposalController@archiveproposal','as' => 'transaction.customerproposal.archive']);
        Route::get('/customerproposal/datadetail/{id}', ['uses' => 'customerProposalController@getDataDetail', 'as' => 'transaction.customerproposal.datadetail']);
        Route::post('/customerproposal/update/{id}',['uses' => 'customerProposalController@update','as' => 'transaction.customerproposal.update']);
        Route::get('/customerproposal/dataTrucking/{id}/{size}', ['uses' => 'customerProposalController@getDataTrucking', 'as' => 'transaction.customerproposal.datatrucking']);
        Route::get('/customerproposal/detailadd/{id}',['uses' => 'customerProposalController@detailadd','as' => 'transaction.customerproposal.detailadd']);
        Route::post('/customerproposal/detailadd/{id}',['uses' => 'customerProposalController@createDetail','as' => 'transaction.customerproposal.detailcreate']);
        Route::get('/customerproposal/detailedit/{id}',['uses' => 'customerProposalController@detailedit','as' => 'transaction.customerproposal.detailedit']);
        Route::post('/customerproposal/detailedit/{id}',['uses' => 'customerProposalController@updateDetail','as' => 'transaction.customerproposal.detailupdate']);
        Route::post('/customerproposal/detail/{id}',['uses' => 'customerProposalController@deleteDetail','as' => 'transaction.customerproposal.detaildelete']);
        Route::post('/customerproposal/complete/{id}',['uses' => 'customerProposalController@completeProposal','as' => 'transaction.customerproposal.completeproposal']);
        Route::get('/customerproposal/detailview/{id}',['uses' => 'customerProposalController@detailview','as' => 'transaction.customerproposal.detailview']);
        Route::get('/customerproposal/copyproposal/{id}',['uses' => 'customerProposalController@copyproposal','as' => 'transaction.customerproposal.copyproposal']);
        Route::post('/customerproposal/deletebiayalain/{id}',['uses' => 'customerProposalController@deleteBiayaLain','as' => 'transaction.customerproposal.deletebiayalain']);
    });
       
    Route::group(['middleware' => ['permission:approve proposal']], function () {
        Route::get('/proposalapproval',['uses' => 'proposalApprovalController@index','as' => 'transaction.proposalapproval']);
        Route::get('/proposalapproval/data', ['uses' => 'proposalApprovalController@getData', 'as' => 'transaction.proposalapproval.data']);
        Route::get('/proposalapproval/view/{id}',['uses' => 'proposalApprovalController@view','as' => 'transaction.proposalapproval.view']);
        Route::get('/proposalapproval/preview-pdf/{id}/{mode}',['uses' => 'proposalApprovalController@previewPDF','as' => 'transaction.proposalapproval.previewpdf']);
        Route::post('/proposalapproval/saveprint-pdf/{id}',['uses' => 'proposalApprovalController@saveprintPDF','as' => 'transaction.proposalapproval.saveprint']);
        Route::get('/proposalapproval/generate-pdf/{id}',['uses' => 'proposalApprovalController@generatePDF','as' => 'transaction.proposalapproval.print']);
        Route::get('/proposalapproval/detail',['uses' => 'proposalApprovalController@detail','as' => 'transaction.proposalapproval.detail']);
        Route::get('/proposalapproval/detailview/{id}',['uses' => 'proposalApprovalController@detailview','as' => 'transaction.proposalapproval.detailview']);
        Route::post('/proposalapproval/approve/{id}',['uses' => 'proposalApprovalController@approveProposal','as' => 'transaction.proposalapproval.approveproposal']);
        Route::post('/proposalapproval/reject/{id}',['uses' => 'proposalApprovalController@rejectProposal','as' => 'transaction.proposalapproval.rejectproposal']);
    });

        // Route::get('/archivedproposal',['uses' => 'archivedProposalController@index','as' => 'transaction.archivedproposal']);
        // Route::get('/archivedproposal/detail',['uses' => 'archivedProposalController@detail','as' => 'transaction.archivedproposal.detail']);
        
    Route::group(['middleware' => ['permission:view order']], function () {
        Route::get('/customerorder',['uses' => 'customerOrderController@index','as' => 'transaction.customerorder']);
        Route::get('/customerorder/data', ['uses' => 'customerOrderController@getData', 'as' => 'transaction.customerorder.data']);
        Route::post('/customerorder/execute/{id}',['uses' => 'customerOrderController@execute','as' => 'transaction.customerorder.execute','middleware' => ['permission:execute order']]);

        Route::group(['middleware' => ['permission:edit order']], function () {
            Route::get('/customerorder/dataproposal', ['uses' => 'customerOrderController@getDataProposal', 'as' => 'transaction.customerorder.dataproposal']);
            Route::get('/customerorder/add/{id}',['uses' => 'customerOrderController@add','as' => 'transaction.customerorder.add']);
            Route::post('/customerorder/add/{id}',['uses' => 'customerOrderController@create','as' => 'transaction.customerorder.create']);
            Route::post('/customerorder/delete/{id}',['uses' => 'customerOrderController@delete','as' => 'transaction.customerorder.delete']);
            Route::get('/customerorder/edit/{id}',['uses' => 'customerOrderController@edit','as' => 'transaction.customerorder.edit']);
            Route::post('/customerorder/edit/{id}',['uses' => 'customerOrderController@update','as' => 'transaction.customerorder.update']);
            Route::get('/customerorder/detail/{id}',['uses' => 'customerOrderController@detail','as' => 'transaction.customerorder.detail']);
        });
    });
        
        Route::get('/archivedorder',['uses' => 'archivedOrderController@index','as' => 'transaction.archivedorder']);
        Route::get('/archivedorder/detail',['uses' => 'archivedOrderController@detail','as' => 'transaction.archivedorder.detail']);

    Route::group(['middleware' => ['permission:view monitoring order']], function () {
        Route::get('/updateorder',['uses' => 'updateOrderController@index','as' => 'transaction.updateorder']);
        Route::get('/updateorder/data', ['uses' => 'updateOrderController@getData', 'as' => 'transaction.updateorder.data']);
        Route::get('/updateorder/listbarang/{id}', ['uses' => 'updateOrderController@getListBarang', 'as' => 'transaction.updateorder.listbarang']);
        Route::get('/updateorder/edit/{id}',['uses' => 'updateOrderController@edit','as' => 'transaction.updateorder.edit','middleware' => ['permission:update status order']]);
        Route::post('/updateorder/updatestatus/{id}',['uses' => 'updateOrderController@updateStatus','as' => 'transaction.updateorder.updatestatus']);
    });
        
    Route::group(['middleware' => ['permission:view surat jalan']], function () {
        Route::get('/suratjalan',['uses' => 'suratjalanController@index','as' => 'transaction.suratjalan']);
        Route::get('/suratjalan/data', ['uses' => 'suratjalanController@getData', 'as' => 'transaction.suratjalan.data']);
        Route::get('/suratjalan/dataorder', ['uses' => 'suratjalanController@getDataOrder', 'as' => 'transaction.suratjalan.dataorder']);
        Route::get('/suratjalan/add/{id}',['uses' => 'suratjalanController@add','as' => 'transaction.suratjalan.add']);
        Route::post('/suratjalan/add/{id}',['uses' => 'suratjalanController@create','as' => 'transaction.suratjalan.create']);
        Route::get('/suratjalan/edit/{id}',['uses' => 'suratjalanController@edit','as' => 'transaction.suratjalan.edit']);
        Route::post('/suratjalan/edit/{id}',['uses' => 'suratjalanController@update','as' => 'transaction.suratjalan.update']);
        Route::get('/suratjalan/view/{id}',['uses' => 'suratjalanController@view','as' => 'transaction.suratjalan.view']);
        Route::post('/suratjalan/delete/{id}',['uses' => 'suratjalanController@delete','as' => 'transaction.suratjalan.delete']);
        Route::get('/suratjalan/dataDO/{id}', ['uses' => 'suratjalanController@getDataDO', 'as' => 'transaction.suratjalan.datado']);
        Route::get('/suratjalan/doadd/{id}',['uses' => 'suratjalanController@doadd','as' => 'transaction.suratjalan.doadd']);
        Route::post('/suratjalan/doadd/{id}',['uses' => 'suratjalanController@createDO','as' => 'transaction.suratjalan.createdo']);
        Route::get('/suratjalan/doedit/{id}',['uses' => 'suratjalanController@doedit','as' => 'transaction.suratjalan.doedit']);
        Route::post('/suratjalan/doedit/{id}',['uses' => 'suratjalanController@updateDO','as' => 'transaction.suratjalan.updatedo']);
        Route::post('/suratjalan/dodelete/{id}',['uses' => 'suratjalanController@deleteDO','as' => 'transaction.suratjalan.deletedo']);
        Route::post('/suratjalan/dodetaildelete/{id}',['uses' => 'suratjalanController@deleteDODetail','as' => 'transaction.suratjalan.deletedodetail']);
        Route::get('/suratjalan/doview/{id}',['uses' => 'suratjalanController@doview','as' => 'transaction.suratjalan.viewdo']);
        Route::get('/suratjalan/dataBarang/{id}', ['uses' => 'suratjalanController@getDataBarang', 'as' => 'transaction.suratjalan.databarang']);
        Route::get('/suratjalan/listdataBarang', ['uses' => 'suratjalanController@getListDataBarang', 'as' => 'transaction.suratjalan.listdatabarang']);
        Route::get('/suratjalan/listBarang/{id}', ['uses' => 'suratjalanController@getListBarang', 'as' => 'transaction.suratjalan.listbarang']);
        Route::get('/suratjalan/doviewfromview/{id}',['uses' => 'suratjalanController@doviewfromview','as' => 'transaction.suratjalan.viewfromviewdo']);
        Route::get('/suratjalan/detailsuratjalanadd',['uses' => 'suratjalanController@detailsuratjalanadd','as' => 'transaction.suratjalan.detailsuratjalanadd']);
    });
        
        
        
    Route::group(['middleware' => ['permission:view BAPB']], function () {
        Route::get('/bapb',['uses' => 'BAPBController@index','as' => 'transaction.bapb']);
        Route::get('/bapb/data',['uses' => 'BAPBController@getData','as' => 'transaction.bapb.data']);
        Route::post('/bapb/updateDate/{id}',['uses' => 'BAPBController@updateDate','as' => 'transaction.bapb.updateDate','middleware' => ['permission:update tanggal BAPB']]);
        Route::get('/bapb/datadooring',['uses' => 'BAPBController@getDataDooringBelumBack','as' => 'transaction.bapb.datadooring']);
        Route::get('/bapb/generate-pdf/{iddooring}',['uses' => 'BAPBController@generatePDFBelumKembali','as' => 'transaction.bapb.print','middleware' => ['permission:print BAPB']]);

        Route::group(['middleware' => ['permission:edit BAPB']], function () {
            Route::get('/bapb/add/{id}',['uses' => 'BAPBController@add','as' => 'transaction.bapb.add']);
            Route::get('/bapb/datado/{idorder}/{kodetrip}',['uses' => 'BAPBController@getDataDO','as' => 'transaction.bapb.datado']);
            Route::post('/bapb/add',['uses' => 'BAPBController@create','as' => 'transaction.bapb.create']);
            Route::get('/bapb/edit/{id}',['uses' => 'BAPBController@edit','as' => 'transaction.bapb.edit']);
            Route::post('/bapb/edit/{id}',['uses' => 'BAPBController@update','as' => 'transaction.bapb.update']);
            Route::post('/bapb/deletedo/{id}',['uses' => 'BAPBController@deleteDO','as' => 'transaction.bapb.deletedo']);
            //Route::get('/customerproposal/detail',['uses' => 'pelayaranController@detail','as' => 'master.pelayaran.detail']);
            Route::get('/bapb/detailadd',['uses' => 'BAPBController@detailadd','as' => 'transaction.bapb.detailadd']);
        });
    });
        

        Route::get('/archivedbapb',['uses' => 'archivedBAPBController@index','as' => 'transaction.archivedbapb']);
        Route::get('/archivedbapb/detail',['uses' => 'archivedBAPBController@detail','as' => 'transaction.archivedbapb.detail']);

    Route::group(['middleware' => ['permission:view packing list']], function () {
        Route::get('/packinglist',['uses' => 'packingListController@index','as' => 'transaction.packinglist']);
        Route::get('/packinglist/data',['uses' => 'packingListController@getData','as' => 'transaction.packinglist.data']);
        Route::get('/packinglist/datadetail/{idtrip}/{iddoor}/{idpelayaran}',['uses' => 'packingListController@getDataDetail','as' => 'transaction.packinglist.datadetail']);
        Route::get('/packinglist/add',['uses' => 'packingListController@add','as' => 'transaction.packinglist.add']);
        Route::get('/packinglist/edit',['uses' => 'packingListController@edit','as' => 'transaction.packinglist.edit']);
    });
        
        Route::get('/kasir',['uses' => 'kasirController@index','as' => 'transaction.kasir']);
        Route::get('/kasir/add',['uses' => 'kasirController@add','as' => 'transaction.kasir.add']);
        Route::get('/kasir/edit',['uses' => 'kasir@edit','as' => 'transaction.kasir.edit']);

    Route::group(['middleware' => ['permission:view invoice']], function () {
        Route::get('/invoice',['uses' => 'invoiceController@index','as' => 'transaction.invoice']);
        Route::get('/invoice/data',['uses' => 'invoiceController@getData','as' => 'transaction.invoice.data']);
        Route::get('/invoice/datatrip',['uses' => 'invoiceController@getDataTrip','as' => 'transaction.invoice.datatrip']);
        Route::get('/invoice/add/{idtrip}/{idcustomer}',['uses' => 'invoiceController@add','as' => 'transaction.invoice.add']);
        Route::post('/invoice/add',['uses' => 'invoiceController@create','as' => 'transaction.invoice.create']);
        Route::post('/invoice/verify/{id}',['uses' => 'invoiceController@verify','as' => 'transaction.invoice.verify','middleware' => ['permission:verify invoice']]);
        Route::post('/invoice/delete/{id}',['uses' => 'invoiceController@delete','as' => 'transaction.invoice.delete']);
        Route::get('/invoice/edit/{id}/{idtrip}/{idcustomer}',['uses' => 'invoiceController@edit','as' => 'transaction.invoice.edit','middleware' => ['permission:edit invoice']]);
        Route::post('/invoice/edit/{id}',['uses' => 'invoiceController@update','as' => 'transaction.invoice.update']);
        Route::post('/invoice/deletecont/{id}',['uses' => 'invoiceController@deleteCont','as' => 'transaction.invoice.deletecont']);
        Route::get('/invoice/view/{id}/{idtrip}/{idcustomer}',['uses' => 'invoiceController@view','as' => 'transaction.invoice.view']);
        Route::get('/invoice/generate-pdf',['uses' => 'invoiceController@generatePDFInvoice','as' => 'transaction.invoice.print','middleware' => ['permission:print invoice']]);
    });
        
    Route::group(['middleware' => ['permission:view invoice lcl']], function () {
        Route::get('/invoicelcl',['uses' => 'invoicelclController@index','as' => 'transaction.invoicelcl']);
        Route::get('/invoicelcl/data',['uses' => 'invoicelclController@getData','as' => 'transaction.invoicelcl.data']);
        Route::get('/invoicelcl/datatrip',['uses' => 'invoicelclController@getDataTrip','as' => 'transaction.invoicelcl.datatrip']);
        Route::get('/invoicelcl/add/{idtrip}/{idcustomer}',['uses' => 'invoicelclController@add','as' => 'transaction.invoicelcl.add']);
        Route::post('/invoicelcl/add',['uses' => 'invoicelclController@create','as' => 'transaction.invoicelcl.create']);
        Route::post('/invoicelcl/verify/{id}',['uses' => 'invoicelclController@verify','as' => 'transaction.invoicelcl.verify','middleware' => ['permission:verify invoice lcl']]);
        Route::post('/invoicelcl/delete/{id}',['uses' => 'invoicelclController@delete','as' => 'transaction.invoicelcl.delete']);
        Route::get('/invoicelcl/edit/{id}/{idtrip}/{idcustomer}',['uses' => 'invoicelclController@edit','as' => 'transaction.invoicelcl.edit','middleware' => ['permission:edit invoice lcl']]);
        Route::post('/invoicelcl/edit/{id}',['uses' => 'invoicelclController@update','as' => 'transaction.invoicelcl.update']);
        Route::post('/invoicelcl/deletecont/{id}',['uses' => 'invoicelclController@deleteCont','as' => 'transaction.invoicelcl.deletecont']);
        Route::get('/invoicelcl/view/{id}/{idtrip}/{idcustomer}',['uses' => 'invoicelclController@view','as' => 'transaction.invoicelcl.view']);
    });
       
    Route::group(['middleware' => ['permission:view custom invoice']], function () {
        Route::get('/custominvoice',['uses' => 'custominvoiceController@index','as' => 'transaction.custominvoice']);
        Route::get('/custominvoice/data',['uses' => 'custominvoiceController@getData','as' => 'transaction.custominvoice.data']);

        Route::group(['middleware' => ['permission:edit custom invoice']], function () {
            Route::get('/custominvoice/add',['uses' => 'custominvoiceController@add','as' => 'transaction.custominvoice.add']);
            Route::post('/custominvoice/create',['uses' => 'custominvoiceController@create','as' => 'transaction.custominvoice.create']);
            Route::get('/custominvoice/edit/{id}',['uses' => 'custominvoiceController@edit','as' => 'transaction.custominvoice.edit']);
            Route::post('/custominvoice/update/{id}',['uses' => 'custominvoiceController@update','as' => 'transaction.custominvoice.update']);
            Route::post('/custominvoice/delete/{id}',['uses' => 'custominvoiceController@delete','as' => 'transaction.custominvoice.delete']);
        });
    });

    Route::group(['middleware' => ['permission:view extra cost claim']], function () {
        Route::get('/extracostclaim',['uses' => 'extracostclaimController@index','as' => 'transaction.extracostclaim']);
        Route::get('/extracostclaim/data',['uses' => 'extracostclaimController@getData','as' => 'transaction.extracostclaim.data']);

        Route::group(['middleware' => ['permission:add extra cost claim']], function () {
            Route::get('/extracostclaim/datacustomer',['uses' => 'extracostclaimController@getDataCustomer','as' => 'transaction.extracostclaim.datacustomer']);
            Route::get('/extracostclaim/add/{idcustomer}',['uses' => 'extracostclaimController@add','as' => 'transaction.extracostclaim.add']);
            Route::post('/extracostclaim/create/{idcustomer}',['uses' => 'extracostclaimController@create','as' => 'transaction.extracostclaim.create']);
            Route::get('/extracostclaim/edit/{id}',['uses' => 'extracostclaimController@edit','as' => 'transaction.extracostclaim.edit']);
            Route::post('/extracostclaim/update/{id}',['uses' => 'extracostclaimController@update','as' => 'transaction.extracostclaim.update']);
            Route::post('/extracostclaim/delete/{id}',['uses' => 'extracostclaimController@delete','as' => 'transaction.extracostclaim.delete']);
        });
    });
        
    Route::group(['middleware' => ['permission:view penagihan']], function () {
        Route::get('/penagihan',['uses' => 'penagihanController@index','as' => 'transaction.penagihan']);
        Route::get('/penagihan/data',['uses' => 'penagihanController@getData','as' => 'transaction.penagihan.data']);
        Route::get('/penagihan/add',['uses' => 'penagihanController@add','as' => 'transaction.penagihan.add']);
        Route::post('/penagihan/create',['uses' => 'penagihanController@create','as' => 'transaction.penagihan.create']);
        Route::get('/penagihan/datainvoice/{id}',['uses' => 'penagihanController@getDataInvoice','as' => 'transaction.penagihan.datainvoice']);
        Route::get('/penagihan/edit/{id}',['uses' => 'penagihanController@edit','as' => 'transaction.penagihan.edit']);
        Route::post('/penagihan/update/{id}',['uses' => 'penagihanController@update','as' => 'transaction.penagihan.update']);
        Route::post('/penagihan/deleteinv/{id}/{tipe_invoice}',['uses' => 'penagihanController@deleteInvoice','as' => 'transaction.penagihan.deleteinvoice']);
    });
        
    Route::group(['middleware' => ['permission:view pelunasan']], function () {
        Route::get('/pelunasan',['uses' => 'pelunasanController@index','as' => 'transaction.pelunasan']);
        Route::get('/pelunasan/data',['uses' => 'pelunasanController@getData','as' => 'transaction.pelunasan.data']);
        Route::get('/pelunasan/datainvoice',['uses' => 'pelunasanController@getDataInvoice','as' => 'transaction.pelunasan.datainvoice']);
        Route::get('/pelunasan/datacustomer',['uses' => 'pelunasanController@getDataCustomer','as' => 'transaction.pelunasan.datacustomer']);
        Route::get('/pelunasan/add/{idcustomer}',['uses' => 'pelunasanController@add','as' => 'transaction.pelunasan.add']);
        Route::post('/pelunasan/create',['uses' => 'pelunasanController@create','as' => 'transaction.pelunasan.create']);
        Route::get('/pelunasan/edit/{id}/{idcustomer}',['uses' => 'pelunasanController@edit','as' => 'transaction.pelunasan.edit']);
        Route::post('/pelunasan/update',['uses' => 'pelunasanController@update','as' => 'transaction.pelunasan.update']);
        Route::post('/pelunasan/deleteinvoice/{kodebiaya}',['uses' => 'pelunasanController@deleteInvoice','as' => 'transaction.pelunasan.deleteinvoice']);
        Route::post('/pelunasan/confirm',['uses' => 'pelunasanController@confirm','as' => 'transaction.pelunasan.confirm']);
    });

});

});