<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;
use App\Models;
use Auth;

class GeneralObserver
{
    public function creating(Model $data)
    {
        $data->created_by = Auth::user()->username;
        $data->updated_by = Auth::user()->username;
        
    }

    public function saving(Model $data)
    {
        $data->updated_by = Auth::user()->username;
        
    }

    public function deleted(Model $data)
    {
        if (in_array('Illuminate\Database\Eloquent\SoftDeletes', class_uses($data))) {
            $data->deleted_by = Auth::user()->username;
            
            $data->save();
        }
    }
}