<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pelunasan extends Model
{
    protected $table = 'pelunasan';
    protected $primaryKey = 'id_pelunasan';
    protected $dates = ['date'];

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::parse($value)->format('Y-m-d H:i:s');
    }

    public function Invoice()
    {   
        return $this->belongsTo(Invoice::class,'id_invoice','id_invoice');
    }

    public function InvoiceLCL()
    {   
        return $this->belongsTo(InvoiceLCL::class,'id_invoice_lcl','id_invoice');
    }



    use SoftDeletes;

    
}

