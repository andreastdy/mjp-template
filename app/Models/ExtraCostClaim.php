<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExtraCostClaim extends Model
{
    protected $table = 'extracostclaim';
    protected $primaryKey = 'id_extracostclaim';
    protected $dates = ['date'];

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::parse($value)->format('Y-m-d H:i:s');
    }

    use SoftDeletes;

    
}

