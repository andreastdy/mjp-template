<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceLCL extends Model
{
    protected $table = 'invoice_lcl';
    protected $primaryKey = 'id_invoice_lcl';
    protected $dates = ['date'];

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::parse($value)->format('Y-m-d H:i:s');
    }

    use SoftDeletes;

    
}

