<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DPelunasan extends Model
{
    protected $table = 'd_pelunasan';
    protected $primaryKey = 'id_d_pelunasan';


    public function MPelunasan()
    {
        return $this->belongsTo(MPelunasan::class,'id_m_pelunasan','id_m_pelunasan');
    }

    use SoftDeletes;

    
}

