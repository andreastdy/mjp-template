<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Consignee extends Model
{
    protected $table = 'consignee';
    protected $primaryKey = 'id_consignee';

    use SoftDeletes;
}
