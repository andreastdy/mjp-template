<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceDetailLCL extends Model
{
    protected $table = 'invoicedetail_lcl';
    protected $primaryKey = 'id_invoicedetail_lcl';


    public function InvoiceLCL()
    {
        return $this->belongsTo(InvoiceLCL::class,'id_invoice_lcl','id_invoice_lcl');
    }

    use SoftDeletes;

    
}

