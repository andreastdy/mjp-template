<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dooring extends Model
{
    protected $table = 'dooring';
    protected $primaryKey = 'id_dooring';

    use SoftDeletes;
}
