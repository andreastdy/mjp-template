<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceDetail extends Model
{
    protected $table = 'invoicedetail';
    protected $primaryKey = 'id_invoicedetail';


    public function Invoice()
    {
        return $this->belongsTo(Invoice::class,'id_invoice','id_invoice');
    }

    use SoftDeletes;

    
}

