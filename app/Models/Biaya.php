<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Biaya extends Model
{
    protected $table = 'masterbiaya';
    protected $primaryKey = 'id_biaya';

    use SoftDeletes;
}
