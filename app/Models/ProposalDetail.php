<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProposalDetail extends Model
{
    protected $table = 'proposaldetail';
    protected $primaryKey = 'id_proposaldetail';

    use SoftDeletes;

    public function Proposal()
    {   
        return $this->belongsTo(Proposal::class,'id_proposal','id_proposal');
    }

    public function BiayaLain()
    {
        return $this->hasMany(BiayaLain::class,'id_proposaldetail','id_proposaldetail');
    }

    // public function DetailAsal()
    // {
    //     return $this->hasMany(Pelabuhan::class,'id_pelabuhan','id_pelabuhan_asal');
    // }

    // public function DetailBongkar()
    // {
    //     return $this->hasMany(Pelabuhan::class,'id_pelabuhan','id_pelabuhan_bongkar');
    // }
}
