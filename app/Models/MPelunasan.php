<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MPelunasan extends Model
{
    protected $table = 'm_pelunasan';
    protected $primaryKey = 'id_m_pelunasan';
    protected $dates = ['date'];

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::parse($value)->format('Y-m-d H:i:s');
    }

    use SoftDeletes;

    
}

