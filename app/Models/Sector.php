<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sector extends Model
{
    protected $table = 'sector';
    protected $primaryKey = 'id_sector';

    use SoftDeletes;
}
