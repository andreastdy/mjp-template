<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ppn extends Model
{
    protected $table = 'ppn';
    protected $primaryKey = 'id_ppn';

    use SoftDeletes;
}
