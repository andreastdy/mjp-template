<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pelabuhan extends Model
{
    protected $table = 'pelabuhan';
    protected $primaryKey = 'id_pelabuhan';

    use SoftDeletes;
}
