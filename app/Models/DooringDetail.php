<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DooringDetail extends Model
{
    protected $table = 'dooringdetail';
    protected $primaryKey = 'id_dooringdetail';

    use SoftDeletes;

    public function Dooring()
    {   
        return $this->belongsTo(Dooring::class,'id_dooring','id_dooring');
    }
}
