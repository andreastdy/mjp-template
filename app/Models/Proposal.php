<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Proposal extends Model
{
    protected $table = 'proposal';
    protected $primaryKey = 'id_proposal';
    protected $dates = ['date'];

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::parse($value)->format('Y-m-d H:i:s');
    }



    use SoftDeletes;

    
}

