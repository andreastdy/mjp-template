<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TruckingDetail extends Model
{
    protected $table = 'truckingdetail';
    protected $primaryKey = 'id_truckingdetail';

    use SoftDeletes;

    public function Trucking()
    {   
        return $this->belongsTo(Trucking::class,'id_trucking','id_trucking');
    }

    public function Sector()
    {
        return $this->hasMany(Sector::class,'id_sector','id_sector');
    }
}
