<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HistoryUpdateOrder extends Model
{
    protected $table = 'historyupdateorder';
    protected $primaryKey = 'id_history';

    use SoftDeletes;

    public function SuratJalan()
    {   
        return $this->belongsTo(SuratJalan::class,'id_suratjalan','id_suratjalan');
    }

    public function Order()
    {
        return $this->belongsTo(Order::class,'id_order','id_order');
    }

}
