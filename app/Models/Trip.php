<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Trip extends Model
{
    protected $table = 'trip';
    protected $primaryKey = 'id_trip';
    protected $dates = ['date'];

    use SoftDeletes;


    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::parse($value)->format('Y-m-d H:i:s');
    }

    
    public function PelabuhanTujuan()
    {
        return $this->hasMany(Pelabuhan::class,'id_pelabuhan','id_pelabuhan_tujuan');
    }
}
