<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PelayaranDetail extends Model
{
    protected $table = 'pelayarandetail';
    protected $primaryKey = 'id_pelayarandetail';

    use SoftDeletes;

    public function Pelayaran()
    {   
        return $this->belongsTo(Pelayaran::class,'id_pelayaran','id_pelayaran');
    }

    public function DetailAsal()
    {
        return $this->hasMany(Pelabuhan::class,'id_pelabuhan','id_pelabuhan_asal');
    }

    public function DetailBongkar()
    {
        return $this->hasMany(Pelabuhan::class,'id_pelabuhan','id_pelabuhan_bongkar');
    }
}
