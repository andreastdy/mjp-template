<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BAPB extends Model
{
    protected $table = 'bapb';
    protected $primaryKey = 'id_bapb';
    protected $dates = ['date'];

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::parse($value)->format('Y-m-d H:i:s');
    }

    public function Order()
    {   
        return $this->belongsTo(Order::class,'id_order','id_order');
    }

    public function Trip()
    {   
        return $this->belongsTo(Trip::class,'id_trip','id_trip');
    }

    public function Dooring()
    {   
        return $this->belongsTo(Dooring::class,'id_dooring','id_dooring');
    }

    public function Consignee()
    {   
        return $this->belongsTo(Consignee::class,'id_consignee','id_consignee');
    }

    use SoftDeletes;

    
}

