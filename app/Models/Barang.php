<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Barang extends Model
{
    protected $table = 'barang';
    protected $primaryKey = 'id_barang';

    use SoftDeletes;
}
