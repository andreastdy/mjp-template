<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliveryOrderDetail extends Model
{
    protected $table = 'dodetail';
    protected $primaryKey = 'id_dodetail';

    use SoftDeletes;

    public function DeliveryOrder()
    {   
        return $this->belongsTo(DeliveryOrder::class,'id_do','id_do');
    }
}
