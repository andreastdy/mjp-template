<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomInvoice extends Model
{
    protected $table = 'custominvoice';
    protected $primaryKey = 'id_custominvoice';
    protected $dates = ['date'];

    use SoftDeletes;


    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::parse($value)->format('Y-m-d H:i:s');
    }

    
    public function Customer()
    {
        return $this->hasMany(Customer::class,'id_customer','id_customer');
    }
}
