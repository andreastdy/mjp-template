<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BiayaLain extends Model
{
    protected $table = 'biayalain';
    protected $primaryKey = 'id_biayalain';

    use SoftDeletes;
}
