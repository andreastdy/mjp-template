<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models;
use Carbon\Carbon;
use Auth;
use PDF;

class proposalApprovalController extends Controller
{
    public function index(){
   		return view('transaction.proposalapproval');
    }

    public function getData(){
        // $data = Models\PelayaranDetail::with('DetailBongkar')->with('DetailAsal')
        $data = Models\Proposal::leftJoin('customer','customer.id_customer','proposal.id_customer')
                                ->where('status','=','2')
                                ->orderBy('proposal.created_at', 'DESC');
        
        $data = $data->select([
            'proposal.*',
            'customer.nama_pt as nama_customer'
            // DB::raw("CASE WHEN DATE(SYSDATE())<=proposal.valid_to THEN '<font color=green>ACTIVE</font>' ELSE '<font color=red>EXPIRED</font>' END AS validity")
        ]);

        $count = $data->count();

        // if ($request->input('sort')) {
        //     $data = $data->orderBy($request->input('sort'),$request->input('order'))
        //         ->orderBy('consignee.id_consignee',$request->input('order'));
            
        // } else {
        //     $data = $data->orderBy('consignee.nama');
        // }
        // if($request->input('limit')) {
        //     $data = $data->take($request->input('limit'))
        //     ->skip($request->input('offset'));
        // }
        
        $data = $data->get();
        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }

    public function view($idproposal){
        $proposal = Models\Proposal::find($idproposal);
        $customer = Models\Customer::Get();
        return view('transaction.detailproposalapproval',compact('proposal','customer'));
    }

    public function detailview($idproposaldetail){
        $mode = 'VIEW';
        $from = 'APPROVAL';
        $proposaldetail = Models\ProposalDetail::leftJoin('proposal','proposal.id_proposal','=','proposaldetail.id_proposal')
                                                ->leftJoin('customer','customer.id_customer','=','proposal.id_customer')
                                                ->find($idproposaldetail);
        $pelabuhan = Models\Pelabuhan::leftJoin('city', 'city.id_kota', '=', 'pelabuhan.id_kota')->orderBy('city.nama_kota','asc')->get();
        $biaya = Models\Biaya::Get();     
        $pelayaran = Models\Pelayaran::Get();
        $dooring = Models\Dooring::Get();
        $trucking = Models\Trucking::Get();
        return view('transaction.detailcustomerproposaladd',compact('mode','from','proposaldetail','pelabuhan','pelayaran','dooring','trucking','biaya'));
    }

    public function approveProposal($idProposal)
    {   
        DB::beginTransaction();
        try {
            $proposal = Models\Proposal::find($idProposal);
            $proposal->status = 3;
            $proposal->approved_at = Carbon::now();
            $proposal->approved_by = Auth::user()->username;
            $proposal->save();
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Proposal Approved!'
        ]);
    }

    public function rejectProposal($idProposal)
    {   
        DB::beginTransaction();
        try {
            $proposal = Models\Proposal::find($idProposal);
            $proposal->status = 4;
            $proposal->rejected_at = Carbon::now();
            $proposal->rejected_by = Auth::user()->username;
            $proposal->save();
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Proposal Rejected!'
        ]);
    }

    public function generatePDF($idproposal)
    {
        $proposal = Models\ProposalDetail::with('BiayaLain')
                                        ->leftJoin('pelabuhan as pelabuhan_asal','pelabuhan_asal.id_pelabuhan','proposaldetail.id_pelabuhan_asal')
                                        ->leftJoin('pelabuhan as pelabuhan_tujuan','pelabuhan_tujuan.id_pelabuhan','proposaldetail.id_pelabuhan_tujuan')
                                        ->leftJoin('city as city_asal','city_asal.id_kota','pelabuhan_asal.id_kota')
                                        ->leftJoin('city as city_tujuan','city_tujuan.id_kota','pelabuhan_tujuan.id_kota')
                                        ->leftJoin('proposal','proposal.id_proposal','proposaldetail.id_proposal')
                                        ->leftJoin('customer','proposal.id_customer','customer.id_customer')
                                        ->leftJoin('consignee','proposaldetail.id_consignee','consignee.id_consignee')
                                        ->where('proposaldetail.id_proposal','=',$idproposal);
        $proposal = $proposal->select([
            'proposaldetail.*',
            'proposal.*',
            'customer.*',
            'pelabuhan_asal.nama_pelabuhan as nama_pelabuhan_asal',
            'pelabuhan_tujuan.nama_pelabuhan as nama_pelabuhan_tujuan',
            'city_asal.nama_kota as nama_kota_asal',
            'city_tujuan.nama_kota as nama_kota_tujuan',
            'consignee.nama as nama_consignee'
        ]);
        $proposal = $proposal->get();
        $data = ['idproposal' => $idproposal, 'proposal' => $proposal];
        // dd($proposal[0]->biayalain[0]->namabiayalain);
        setLocale(LC_TIME,'id');
        $pdf = PDF::loadView('transaction.generate-proposal', $data)->setPaper('A4', 'portrait');
  
        return $pdf->download('proposal.pdf');
    }

    public function previewPDF($idproposal,$from){
        $proposal = Models\Proposal::find($idproposal);
        $mode = $from;
        return view('transaction.proposalsetting',compact('mode','proposal'));
    }

    public function saveprintPDF(Request $request, $idproposal){
        DB::beginTransaction();
        try {
            $proposal = Models\Proposal::find($idproposal);
            $proposal->keterangan_muat =  $request->input('keterangan_muat');
            $proposal->termcondition =  $request->input('termcondition');
            $proposal->save();
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Proposal Printed!'
        ]);
        // generatePDF($idproposal);
    }
}
