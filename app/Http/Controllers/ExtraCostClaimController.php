<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models;
use Carbon\Carbon;
use Auth;

class ExtraCostClaimController extends Controller
{
    public function index(){
        return view('transaction.extracostclaim');
     }
 
     public function getData(Request $request){
         $data = Models\ExtraCostClaim::leftJoin('customer','extracostclaim.id_customer','customer.id_customer');
 
 
         $data = $data->select([
             'extracostclaim.*',
             'customer.nama_pt as nama_customer',
 
         ]);
 
         if ($request->input('search')) {
             $data = $data->where('extracostclaim.kode_biaya','LIKE','%'.$request->input('search').'%')
                 ->orWhere('extracostclaim.tgl_biaya','LIKE','%'.$request->input('search').'%')
                 ->orWhere('customer.nama_pt','LIKE','%'.$request->input('search').'%')
                 ->orWhere('extracostclaim.tipe_biaya','LIKE','%'.$request->input('search').'%')
                 ->orWhere('extracostclaim.nominal_biaya','LIKE','%'.$request->input('search').'%')
                 ->orWhere('extracostclaim.deskripsi','LIKE','%'.$request->input('search').'%');
         }
         
         $count = $data->count();
 
         if ($request->input('sort')) {
             $data = $data->orderBy($request->input('sort'),$request->input('order'))
                 ->orderBy('extracostclaim.kode_biaya',$request->input('order'));
             
         } else {
             $data = $data->orderBy('extracostclaim.created_at','desc');
         }
         if($request->input('limit')) {
             $data = $data->take($request->input('limit'))
             ->skip($request->input('offset'));
         }
 
         $data = $data->get();
         
     
         return response()->json([
                 'total' => $count,
                 'rows' => $data
             ]);
     }

     public function getDataCustomer(){
        $data = Models\Customer::get();

        $count = $data->count();
        
    
        return response()->json([
                'total' => $count,
                'rows' => $data
            ]);
    }
     
     public function add($idcustomer){
         $mode = 'ADD';
         $customer = Models\Customer::find($idcustomer);
         $invoicelcl = Models\InvoiceLCL::select('invoice_lcl.id_invoice_lcl as id_invoice','invoice_lcl.no_invoice_lcl as no_invoice',DB::Raw("'LCL' as type_invoice"))
                                          ->whereNotNull('invoice_lcl.kode_penagihan')
                                          ->whereNull('invoice_lcl.deleted_at')
                                          ->where('invoice_lcl.status','=','1')
                                          ->where('invoice_lcl.id_customer','=',$idcustomer);
        $custominvoice = Models\CustomInvoice::select('custominvoice.id_custominvoice as id_invoice','custominvoice.no_custominvoice as no_invoice',DB::Raw("'CUSTOM' as type_invoice"))
                                          ->whereNull('custominvoice.deleted_at')
                                          ->where('custominvoice.id_customer','=',$idcustomer);
        $invoice = Models\Invoice::select('invoice.id_invoice','invoice.no_invoice',DB::Raw("'CONTAINER' as type_invoice"))
                            ->whereNotNull('invoice.kode_penagihan')
                            ->whereNull('invoice.deleted_at')
                            ->where('invoice.status','=','1')
                            ->where('invoice.id_customer','=',$idcustomer)
                            ->union($invoicelcl)
                            ->union($custominvoice)
                            ->get();
        // dd($invoice);                       
         return view('transaction.extracostclaimadd',compact('mode','customer','invoice'));
     }
 
     public function create(Request $request,$idcustomer){
         // dd($idproposal);
         Validator::make($request->all(), [
             'cboInvoice' => 'required|not_in:0',
             'txtTglBiaya' => 'required',
             'cboTipeBiaya' => 'required|not_in:0',
             'txtNominalBiaya' => 'required',
             'txtDeskripsi' => 'required',
 
         ],[
             'cboInvoice.not_in'=>'invoice harus dipilih',  
             'cboTipeBiaya.not_in'=>'tipe biaya harus dipilih',        
         ])->validate();
 
 
         DB::beginTransaction();
         try {

            $tipe_biaya = $request->input('cboTipeBiaya');
            if($tipe_biaya=="EXTRACOST"){
                $prefix_biaya = "EX";
            }else{
                $prefix_biaya = "CL";
            }
            $invoice = explode('|',$request->input('cboInvoice'));
            $id_invoice = $invoice[0];
            $tipe_invoice = $invoice[1];
            $no_invoice = $invoice[2];

             $year = DB::select('select distinct SUBSTRING(EXTRACT(YEAR FROM CURRENT_DATE),3,4) as year from dual');
             $month = DB::select('select MONTH(NOW()) as month');
             $cekkodebiaya = DB::select('select count(*) as ada from extracostclaim where kode_biaya like "'.$prefix_biaya.$year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).'%" and deleted_at is null');
             // dd($ceknobapb[0]->ada);
             if($cekkodebiaya[0]->ada>0){
                 $nokodebiaya = DB::select('select max(kode_biaya) as nokodebiaya from extracostclaim where kode_biaya like "'.$prefix_biaya.$year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).'%" and deleted_at is null');
                 $nobaru = (int)(ltrim(substr($nokodebiaya[0]->nokodebiaya,6), '0'));
                 $nobaru++;
                 $kodebiayabaru = $prefix_biaya.$year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).str_pad($nobaru,4,"0",STR_PAD_LEFT);
             }else{
                 $nobaru = 1;
                 $kodebiayabaru = $prefix_biaya.$year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).str_pad($nobaru,4,"0",STR_PAD_LEFT);
             }
 
             $extracostclaim = new Models\ExtraCostClaim;
             $extracostclaim->kode_biaya = $kodebiayabaru;
             $extracostclaim->id_customer = $idcustomer;
             $extracostclaim->id_invoice = $id_invoice;
             $extracostclaim->no_invoice = $no_invoice;
             $extracostclaim->tipe_invoice = $tipe_invoice;
             $extracostclaim->tgl_biaya = Carbon::parse($request->input('txtTglBiaya'))->format('Y/m/d H:i:s');
             $extracostclaim->tipe_biaya = $tipe_biaya;
             $extracostclaim->nominal_biaya = str_replace( ',', '',$request->input('txtNominalBiaya'));
             $extracostclaim->deskripsi = $request->input('txtDeskripsi');
             $extracostclaim->status = 0;
             $extracostclaim->save();
 
         } catch (\Exception $e) {
             DB::rollback();
             return redirect()->back()->with('error',$e->getMessage())
                 ->withInput();
         }
         DB::commit();
         return redirect()->route('transaction.extracostclaim')
             ->with('success',('Data Saved'));
     }
 
     public function edit($idextracostclaim){
         $mode='EDIT';
         $extracostclaim = Models\ExtraCostClaim::leftJoin('customer','customer.id_customer','extracostclaim.id_customer')
                                                ->find($idextracostclaim);
        // dd($extracostclaim->id_customer);                                                
        //  $customer = Models\Customer::Get();
         $invoicelcl = Models\InvoiceLCL::select('invoice_lcl.id_invoice_lcl as id_invoice','invoice_lcl.no_invoice_lcl as no_invoice',DB::Raw("'LCL' as type_invoice"))
                                          ->whereNotNull('invoice_lcl.kode_penagihan')
                                          ->whereNull('invoice_lcl.deleted_at')
                                          ->where('invoice_lcl.status','=','1')
                                          ->where('invoice_lcl.id_customer','=',$extracostclaim->id_customer);
        $custominvoice = Models\CustomInvoice::select('custominvoice.id_custominvoice as id_invoice','custominvoice.no_custominvoice as no_invoice',DB::Raw("'CUSTOM' as type_invoice"))
                                          ->whereNull('custominvoice.deleted_at')
                                          ->where('custominvoice.id_customer','=',$extracostclaim->id_customer);
        $invoice = Models\Invoice::select('invoice.id_invoice','invoice.no_invoice',DB::Raw("'CONTAINER' as type_invoice"))
                            ->whereNotNull('invoice.kode_penagihan')
                            ->whereNull('invoice.deleted_at')
                            ->where('invoice.status','=','1')
                            ->where('invoice.id_customer','=',$extracostclaim->id_customer)
                            ->union($invoicelcl)
                            ->union($custominvoice)
                            ->get();
        return view('transaction.extracostclaimadd',compact('mode','extracostclaim','invoice'));
     }
 
     public function update(Request $request,$idextracostclaim){
         // dd($idproposal);
         Validator::make($request->all(), [
            'txtTglBiaya' => 'required',
            'txtNominalBiaya' => 'required',
            'txtDeskripsi' => 'required',

        ])->validate();
 
 
         DB::beginTransaction();
         try {
             
 
             $extracostclaim = Models\ExtraCostClaim::find($idextracostclaim);
             $extracostclaim->tgl_biaya = Carbon::parse($request->input('txtTglBiaya'))->format('Y/m/d H:i:s');
             $extracostclaim->nominal_biaya = str_replace( ',', '',$request->input('txtNominalBiaya'));
             $extracostclaim->deskripsi = $request->input('txtDeskripsi');
             $extracostclaim->save();
 
         } catch (\Exception $e) {
             DB::rollback();
             return redirect()->back()->with('error',$e->getMessage())
                 ->withInput();
         }
         DB::commit();
         return redirect()->back()
             ->with('success',('Data Updated'));
     }
 
     public function delete($idextracostclaim, Request $request)
     {   
         DB::beginTransaction();
         try {
             $extracostclaim = Models\ExtraCostClaim::find($idextracostclaim);
             $extracostclaim->delete();
             
         } catch (\Exception $e) {
             DB::rollback();
             return response()->json([
                 'status' => 'error',
                 'description' => $e->getMessage()
             ]);
         }
         DB::commit();
         // $request->session()->flash('success', 'Custom Invoice Dihapus!');
         return response()->json([
             'status' => 'success',
             'description' => 'Biaya Dihapus!'
         ]);
     }
}
