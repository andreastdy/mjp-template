<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models;
use Carbon\Carbon;
use Auth;
use PDF;

class BAPBController extends Controller
{
    public function index(){
   		return view('transaction.bapb');
    }

    public function getData(Request $request){
        $data = Models\BAPB::leftJoin('trip','bapb.id_trip','trip.id_trip')
                            ->leftJoin('dooring','bapb.id_dooring','dooring.id_dooring')
                            ->leftJoin('consignee','bapb.id_consignee','consignee.id_consignee');


        $data = $data->select([
            'bapb.*',
            'trip.kode_trip',
            'consignee.nama as nama_consignee',
            'dooring.nama as nama_dooring'

        ]);

        if ($request->input('search')) {
            $data = $data->where('trip.kode_trip','LIKE','%'.$request->input('search').'%')
                ->orWhere('bapb.no_bapb','LIKE','%'.$request->input('search').'%')
                ->orWhere('dooring.nama','LIKE','%'.$request->input('search').'%')
                ->orWhere('consignee.nama','LIKE','%'.$request->input('search').'%')
                ->orWhere('bapb.tanggal_kirim','LIKE','%'.$request->input('search').'%')
                ->orWhere('bapb.tanggal_kirim','LIKE','%'.$request->input('search').'%');
        }
        $count = $data->count();

        if ($request->input('sort')) {
            $data = $data->orderBy($request->input('sort'),$request->input('order'))
                ->orderBy('bapb.id_bapb',$request->input('order'));
            
        } else {
            $data = $data->orderBy('bapb.created_at','desc');
        }
        if($request->input('limit')) {
            $data = $data->take($request->input('limit'))
            ->skip($request->input('offset'));
        }

        $data = $data->get();
        
    
        return response()->json([
                'total' => $count,
                'rows' => $data
            ]);
    }
    
    public function add($idorder){
        $mode = 'ADD';
        $order = Models\Order::leftJoin('customer','order.id_customer','customer.id_customer')
                                ->leftJoin('proposaldetail','order.id_proposaldetail','proposaldetail.id_proposaldetail')
                                ->find($idorder);
        $trip = Models\Trip::where('status','=','0')->Get();
        $dooring  = Models\Dooring::Get();
        $consignee = Models\Consignee::Get();
        return view('transaction.bapbadd',compact('mode','order','trip','dooring','consignee'));
    }

    public function getDataDO($idtrip,$idorder){
        $data = Models\DeliveryOrder::Join('suratjalan','suratjalan.id_suratjalan','do.id_suratjalan')
                            ->leftJoin('consignee','consignee.id_consignee','do.id_consignee')
                            ->where('suratjalan.id_order','=',$idorder)
                            ->where('suratjalan.id_kodetrip','=',$idtrip);
                            // ->whereNull('do.id_bapb');


        $data = $data->select([
            'do.*',
            'consignee.*',
            'suratjalan.*'

        ]);

        $count = $data->count();

        $data = $data->get();
        
    
        return response()->json([
                'total' => $count,
                'rows' => $data
            ]);
    }

    public function create(Request $request){
        $validator = Validator::make($request->all(), [
            'id_trip'  => 'required|not_in:0',
            'id_dooring' => 'required|not_in:0',
            'id_consignee' => 'required|not_in:0',

        ],[
            'id_trip.not_in' => 'kode trip harus dipilih',
            'id_dooring.not_in' => 'dooring harus dipilih',
            'id_consignee.not_in' => 'consignee harus dipilih',
        ])->validate();

        // dd($idproposal);
        $typebapb = $request->input('type_bapb');
        $year = DB::select('select distinct SUBSTRING(EXTRACT(YEAR FROM CURRENT_DATE),3,4) as year from dual');
        $month = DB::select('select MONTH(NOW()) as month');
        $ceknobapb = DB::select('select count(*) as ada from bapb where no_bapb like "'.$year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).'%"');
        // dd($ceknobapb[0]->ada);
        if($ceknobapb[0]->ada>0){
            $nobapb = DB::select('select max(no_bapb) as nobapb from bapb where no_bapb like "%'.$year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).'%"');
            $nobaru = (int)(ltrim(substr($nobapb[0]->nobapb,4), '0'));
            $nobaru++;
            $nobapbbaru = $year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).str_pad($nobaru,4,"0",STR_PAD_LEFT);
        }else{
            $nobaru = 1;
            $nobapbbaru = $year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).str_pad($nobaru,4,"0",STR_PAD_LEFT);
        }
        // dd($nobapbbaru);
        
        DB::beginTransaction();
        try {
            $bapb = new Models\BAPB;
            $bapb->id_order = $request->input('id_order');
            $bapb->no_bapb = $nobapbbaru;
            $bapb->id_trip = $request->input('id_trip');
            $bapb->id_dooring = $request->input('id_dooring');
            $bapb->id_consignee = $request->input('id_consignee');
            $bapb->type_bapb = $typebapb;
            if($bapb->save()){
                $detail = $request->input('detail');
                foreach ($detail as $item) {
                    $do = Models\DeliveryOrder::find($item['id_do']);
                    $do->id_bapb = $bapb->id_bapb;
                    $do->save();
                }
            }

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        $request->session()->flash('success', 'BAPB Tersimpan!');
        return response()->json([
            'status' => 'success',
            'description' => 'BAPB Tersimpan!'
        ]);
    }

    public function updateDate(Request $request,$idbapb)
    {   
        DB::beginTransaction();
        try {
            $bapb = Models\bapb::find($idbapb);
            if($request->input('tanggal_kirim')!="" || $request->input('tanggal_kembali')!=""){
                if($request->input('tanggal_kirim')!=""){
                    $bapb->tanggal_kirim = Carbon::parse($request->input('tanggal_kirim'))->format('Y/m/d H:i:s');
                }
                if($request->input('tanggal_kembali')!=""){
                    $bapb->tanggal_kembali = Carbon::parse($request->input('tanggal_kembali'))->format('Y/m/d H:i:s');
                }
                $bapb->save();
            }else{
                throw new \Exception("Tanggal Kirim atau Tanggal Kembali harus diisi!");
            }
            
            
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Tanggal Kirim dan Kembali berhasil diupdate!'
        ]);
    }

    public function edit($idbapb){
        $mode = 'EDIT';
        $bapb = Models\BAPB::leftJoin('order','order.id_order','bapb.id_order')
                            ->leftJoin('customer','order.id_customer','customer.id_customer')
                            ->find($idbapb);
        $trip = Models\Trip::Get();
        $dooring  = Models\Dooring::Get();
        $consignee = Models\Consignee::Get();
        $DO = Models\DeliveryOrder::Join('suratjalan','suratjalan.id_suratjalan','do.id_suratjalan')
                            ->where('suratjalan.id_kodetrip','=',$bapb->id_trip)
                            ->where('do.id_bapb','=',$idbapb)->Get();
        $DOcount = $DO->count();
        // dd($DO->count());
        return view('transaction.bapbadd',compact('mode','bapb','trip','dooring','consignee','DO','DOcount'));
    }

    public function update(Request $request,$idbapb){
        // dd($idproposal);
        $validator = Validator::make($request->all(), [
            'id_trip'  => 'required|not_in:0',
            'id_dooring' => 'required|not_in:0',
            'id_consignee' => 'required|not_in:0',

        ],[
            'id_trip.not_in' => 'kode trip harus dipilih',
            'id_dooring.not_in' => 'dooring harus dipilih',
            'id_consignee.not_in' => 'consignee harus dipilih',
        ])->validate();
        
        DB::beginTransaction();
        try {
                $detail = $request->input('detail');
                foreach ($detail as $item) {
                    $do = Models\DeliveryOrder::find($item['id_do']);
                    $do->id_bapb = $idbapb;
                    $do->save();
                }

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        $request->session()->flash('success', 'BAPB Tersimpan!');
        return response()->json([
            'status' => 'success',
            'description' => 'BAPB Tersimpan!'
        ]);
    }

    public function deleteDO(Request $request,$iddo){
        DB::beginTransaction();
        try {
            $do = Models\DeliveryOrder::find($iddo);
            $do->id_bapb = null;
            $do->save();

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'DO Dikeluarkan!'
        ]);
    }

    public function getDataDooringBelumBack(){
        $data = Models\BAPB::distinct()->select('dooring.id_dooring','dooring.nama')
                            ->leftJoin('dooring','dooring.id_dooring','bapb.id_dooring')
                            ->whereNull('bapb.tanggal_kembali')
                            ->get();


        $count = $data->count();
        
    
        return response()->json([
                'total' => $count,
                'rows' => $data
            ]);
    }

    public function generatePDFBelumKembali($iddooring)
    {
        $listbapb = Models\BAPB::select('dooring.nama as nama_dooring','dooring.alamat','city.nama_kota','bapb.no_bapb','suratjalan.no_container','trip.kode_trip','trip.nama_kapal','trip.tanggal_berangkat','do.id_consignee','consignee.nama as nama_consignee','dodetail.nama_barang',DB::raw('SUM(dodetail.kolli) as total_kolli'))
                                ->leftJoin('dooring','dooring.id_dooring','bapb.id_dooring')
                                ->leftJoin('city','city.id_kota','dooring.id_kota')
                                ->leftJoin('trip','trip.id_trip','bapb.id_trip')
                                ->leftJoin('do','do.id_bapb','bapb.id_bapb')
                                ->leftJoin('dodetail','dodetail.id_do','do.id_do')
                                ->leftJoin('suratjalan','suratjalan.id_suratjalan','do.id_suratjalan')
                                ->leftJoin('consignee','consignee.id_consignee','do.id_consignee')
                                ->whereNull('bapb.tanggal_kembali')
                                ->where('bapb.id_dooring','=',$iddooring)
                                ->groupBy('dooring.nama','dooring.alamat','city.nama_kota','bapb.no_bapb','suratjalan.no_container','trip.kode_trip','trip.nama_kapal','trip.tanggal_berangkat','do.id_consignee','consignee.nama','dodetail.nama_barang')
                                ->get();

        $data = ['iddooring' => $iddooring, 'listbapb' => $listbapb];
        // dd($listbapb[0]);
        setLocale(LC_TIME,'id');
        $pdf = PDF::loadView('transaction.generate-bapb-not-back', $data)->setPaper('A4', 'portrait');
  
        return $pdf->download('bapb_belum_kembali.pdf');
    }

    // public function detail(){
    //     return view('transaction.detailbapbadd');
    // }
    // public function detailadd(){
    //     return view('transaction.detailbapbadd');
    // }
    // public function detailsuratjalan(){
    //     return view('transaction.detailsuratjalanadd');
    // }
    // public function detailsuratjalanadd(){
    //     return view('transaction.detailsuratjalanadd');
    // }
}
