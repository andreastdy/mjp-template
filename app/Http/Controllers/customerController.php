<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models;

class customerController extends Controller
{
    public function index(){
   		return view('master.customer');
    }

    public function getData(Request $request){
        $data = Models\Customer::leftJoin('city','city.id_kota','customer.id_kota');

        if ($request->input('search')) {
            $data = $data->where('customer.nama_pt','LIKE','%'.$request->input('search').'%')
                ->orWhere('customer.name_short','LIKE','%'.$request->input('search').'%')
                ->orWhere('customer.alamat_pt','LIKE','%'.$request->input('search').'%')
                ->orWhere('city.nama_kota','LIKE','%'.$request->input('search').'%')
                ->orWhere('customer.kodepos','LIKE','%'.$request->input('search').'%')
                ->orWhere('customer.telp','LIKE','%'.$request->input('search').'%')
                ->orWhere('customer.name_cp','LIKE','%'.$request->input('search').'%')
                ->orWhere('customer.telp_cp','LIKE','%'.$request->input('search').'%')
                ->orWhere('customer.lama_kredit','LIKE','%'.$request->input('search').'%');
        }
        $count = $data->count();

        if ($request->input('sort')) {
            $data = $data->orderBy($request->input('sort'),$request->input('order'))
                ->orderBy('customer.id_customer',$request->input('order'));
            
        } else {
            $data = $data->orderBy('customer.nama_pt','asc');
        }
        if($request->input('limit')) {
            $data = $data->take($request->input('limit'))
            ->skip($request->input('offset'));
        }

        $data = $data->get();
        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }

    
    public function add(){
        $mode = 'ADD';
        $city = Models\City::OrderBy('nama_kota','asc')->Get();
        return view('master.customeradd',compact('city','mode'));
    }

    public function create(Request $request){
        Validator::make($request->all(), [
            'txtName' => 'required',
            'txtShortName' => 'required',
            'txtAlamat' => 'required',
            'txtAlamatGudang1' => 'required',
            // 'txtAlamatGudang2' => 'required',
            // 'txtAlamatGudang3' => 'required',
            'cboKota' => 'required|not_in:0',
            'txtKodepos' => 'required',
            'txtTelp' => 'required',
            'txtNamaCP' => 'required',
            'txtTeleponCP' => 'required',
            'txtEmail1' => 'required',
            // 'txtEmail2' => 'required',
            // 'txtEmail3' => 'required',
            'txtLamaKredit' => 'required',
            // 'txtKeterangan' => 'required',

        ],[
            'cboKota.not_in' => 'kota harus dipilih',
            ])->validate();

        DB::beginTransaction();
        try {
            $customer = new Models\Customer;
            $customer->nama_pt = $request->input('txtName');
            $customer->name_short = $request->input('txtShortName');
            $customer->alamat_pt = $request->input('txtAlamat');
            $customer->alamat_gudang_1 = $request->input('txtAlamatGudang1');
            $customer->alamat_gudang_2 = $request->input('txtAlamatGudang2');
            $customer->alamat_gudang_3 = $request->input('txtAlamatGudang3');
            $customer->id_kota = $request->input('cboKota');
            $customer->kodepos = $request->input('txtKodepos');
            $customer->telp = $request->input('txtTelp');
            $customer->name_cp = $request->input('txtNamaCP');
            $customer->telp_cp = $request->input('txtTeleponCP');
            $customer->email_1 = $request->input('txtEmail1');
            $customer->email_2 = $request->input('txtEmail2');
            $customer->email_3 = $request->input('txtEmail3');
            $customer->lama_kredit = $request->input('txtLamaKredit');
            $customer->keterangan = $request->input('txtKeterangan');
            $customer->nominal_deposit = 0;
            $customer->save();

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect('master/customer')
            ->with('success',__('Data Saved'));
    }

    public function edit($idCustomer){
        $mode = 'EDIT';
        $customer = Models\Customer::find($idCustomer);
        $city = Models\City::OrderBy('nama_kota','asc')->Get();
        return view('master.customeradd',compact('mode','customer','city'));
    }

    public function update(Request $request,$idCustomer){
        Validator::make($request->all(), [
            'txtName' => 'required',
            'txtShortName' => 'required',
            'txtAlamat' => 'required',
            'txtAlamatGudang1' => 'required',
            // 'txtAlamatGudang2' => 'required',
            // 'txtAlamatGudang3' => 'required',
            'cboKota' => 'required|not_in:0',
            'txtKodepos' => 'required',
            'txtTelp' => 'required',
            'txtNamaCP' => 'required',
            'txtTeleponCP' => 'required',
            'txtEmail1' => 'required',
            // 'txtEmail2' => 'required',
            // 'txtEmail3' => 'required',
            'txtLamaKredit' => 'required',
            // 'txtKeterangan' => 'required',

        ],[
            'cboKota.not_in' => 'kota harus dipilih',
            ])->validate();

        DB::beginTransaction();
        try {
            $customer = Models\Customer::find($idCustomer);
            $customer->nama_pt = $request->input('txtName');
            $customer->name_short = $request->input('txtShortName');
            $customer->alamat_pt = $request->input('txtAlamat');
            $customer->alamat_gudang_1 = $request->input('txtAlamatGudang1');
            $customer->alamat_gudang_2 = $request->input('txtAlamatGudang2');
            $customer->alamat_gudang_3 = $request->input('txtAlamatGudang3');
            $customer->id_kota = $request->input('cboKota');
            $customer->kodepos = $request->input('txtKodepos');
            $customer->telp = $request->input('txtTelp');
            $customer->name_cp = $request->input('txtNamaCP');
            $customer->telp_cp = $request->input('txtTeleponCP');
            $customer->email_1 = $request->input('txtEmail1');
            $customer->email_2 = $request->input('txtEmail2');
            $customer->email_3 = $request->input('txtEmail3');
            $customer->lama_kredit = $request->input('txtLamaKredit');
            $customer->keterangan = $request->input('txtKeterangan');
            $customer->save();

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->back()
            ->with('success','Data Updated');
    }

    public function delete($idCustomer, Request $request)
    {   
        DB::beginTransaction();
        try {
            $customer = Models\Customer::find($idCustomer);
            $customer->delete();
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Customer Deleted.'
        ]);
    }
   
}