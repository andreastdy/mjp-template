<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use DB;
use App\Models;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Hash;
use Carbon\Carbon;


class userController extends Controller
{
    public function index(){
   	return view('setting.user');
    }

    public function getData(Request $request){
        $data = new Models\User;
        if(auth()->user()->name!='admin'){
            $data = $data->where('name','<>','admin');
        }
        
        // dd(auth()->user()->roles);
        // dd(auth()->user()->name);
        // $role = \Spatie\Permission\Models\Role::all();
        // $permissions = \Spatie\Permission\Models\Permission::all();

        // $roles = \Spatie\Permission\Models\Role::findByName('SUPERADMIN')->permissions;
        // dd($data->permissions);
        // dd($roles);

        if ($request->input('search')) {
            $data = $data->where('name','LIKE','%'.$request->input('search').'%')
                ->orWhere('username','LIKE','%'.$request->input('search').'%')
                ->orWhere('email','LIKE','%'.$request->input('search').'%');
        }
        $count = $data->count();

        if ($request->input('sort')) {
            $data = $data->orderBy($request->input('sort'),$request->input('order'))
                ->orderBy('name',$request->input('order'));
            
        } else {
            $data = $data->orderBy('name','asc');
        }
        if($request->input('limit')) {
            $data = $data->take($request->input('limit'))
            ->skip($request->input('offset'));
        }

        $data = $data->get();
        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }
    
    public function add(){
        $mode = 'ADD';
        $roles = \Spatie\Permission\Models\Role::all();
        $permissions = \Spatie\Permission\Models\Permission::all();
        // dd($data[1]->permissions[0]->display_name);
        return view('setting.useradd',compact('permissions','roles','mode'));
    }

    public function getPermission($name){
        $roles = \Spatie\Permission\Models\Role::findByName($name)->permissions->pluck("id");
        // $roles = $roles->get();
        $count = $roles->count();
        // dd($count);
        
        return response()->json([
            'total' => $count,
            'rows' => $roles
        ]);
    }

    public function create(Request $request){
        Validator::make($request->all(), [
            'nama' => 'required',
            'username' => 'required',
            'email' => 'email|required',
            'password' => 'required_with:confirm_password|same:confirm_password',
            'confirm_password' => 'required',
        ])->validate();

        try {
        DB::beginTransaction();
            $user = new Models\User;
            $user->name = $request->input('nama');
            $user->username = $request->input('username');
            $user->email = $request->input('email');
            $user->password = Hash::make($request->input('password'));
            $user->created_at = Carbon::now();
            $user->updated_at = Carbon::now();
            if($user->save()){
                // dd("masuk");
                $user->syncPermissions($request->input('detail'));
            }


        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        $request->session()->flash('success', 'User Tersimpan!');
        return response()->json([
            'status' => 'success',
            'description' => 'User Tersimpan!'
        ]);
    }

    public function edit($iduser){
        $mode = 'EDIT';
        $user = Models\User::find($iduser);
        $roles = \Spatie\Permission\Models\Role::all();
        $permissions = \Spatie\Permission\Models\Permission::all();
        $alluserpermission =  $user->getAllPermissions()->pluck("id")->toArray();
        // dd($alluserpermission);
        // dd($data[1]->permissions[0]->display_name);
        return view('setting.useradd',compact('permissions','roles','mode','alluserpermission','user'));
    }

    public function update(Request $request, $iduser){
        Validator::make($request->all(), [
            'nama' => 'required',
            'username' => 'required',
            'email' => 'email|required',
        ])->validate();
        
        try {
        DB::beginTransaction();
            $user = Models\User::find($iduser);
            $user->name = $request->input('nama');
            $user->username = $request->input('username');
            $user->email = $request->input('email');

            if($request->input('password')!=null||$request->input('password')!=""){
                if($request->input('password') != $request->input('confirm_password')){
                    throw new \Exception("Konfirmasi Password tidak sama!");
                }else{
                    $user->password = Hash::make($request->input('password'));
                }
            }
            
            $user->created_at = Carbon::now();
            $user->updated_at = Carbon::now();
            if($user->save()){
                // dd("masuk");
                $user->syncPermissions($request->input('detail'));
            }


        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        $request->session()->flash('success', 'User Tersimpan!');
        return response()->json([
            'status' => 'success',
            'description' => 'User Tersimpan!'
        ]);
    }

    public function delete($iduser, Request $request)
    {   
        DB::beginTransaction();
        try {
            $user = Models\User::find($iduser);
            $user->syncPermissions();
            $user->delete();
            
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'User Deleted.'
        ]);
    }
}
