<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models;

class barangController extends Controller
{
    public function index(){
   		return view('master.barang');
    }

    public function getData(Request $request){
        $data = new Models\Barang;

        if ($request->input('search')) {
            $data = $data->where('kode_barang','LIKE','%'.$request->input('search').'%')
                ->orWhere('nama_barang','LIKE','%'.$request->input('search').'%')
                ->orWhere('satuan','LIKE','%'.$request->input('search').'%');
        }

        $count = $data->count();

        if ($request->input('sort')) {
            $data = $data->orderBy($request->input('sort'),$request->input('order'))
                ->orderBy('id_barang',$request->input('order'));
            
        } else {
            $data = $data->orderBy('nama_barang','asc');
        }
        if($request->input('limit')) {
            $data = $data->take($request->input('limit'))
            ->skip($request->input('offset'));
        }

        $data = $data->get();
        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }
    
    public function add(){
        $mode = 'ADD';
        return view('master.barangadd', compact('mode'));
    }

    public function create(Request $request){
        Validator::make($request->all(), [
            'txtKodeBarang' => 'required',
            'txtNamaBarang' => 'required',
            'txtSatuan' => 'required',

        ])->validate();

        DB::beginTransaction();
        try {
            $barang = new Models\Barang;
            $barang->kode_barang = $request->input('txtKodeBarang');
            $barang->nama_barang = $request->input('txtNamaBarang');
            $barang->satuan = $request->input('txtSatuan');
            $barang->save();


        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect('master/barang')
            ->with('success',__('Data Saved'));
    }

    public function edit($idBarang){
        $mode = 'EDIT';
        $barang = Models\Barang::find($idBarang);
        return view('master.barangadd',compact('mode','barang'));
    }

    public function update(Request $request, $idBarang){
        Validator::make($request->all(), [
            'txtKodeBarang' => 'required',
            'txtNamaBarang' => 'required',
            'txtSatuan' => 'required',

        ])->validate();

        DB::beginTransaction();
        try {
            $barang = Models\Barang::find($idBarang);
            $barang->kode_barang = $request->input('txtKodeBarang');
            $barang->nama_barang = $request->input('txtNamaBarang');
            $barang->satuan = $request->input('txtSatuan');
            $barang->save();


        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->back()
            ->with('success',__('Data Updated'));
    }

    public function delete($idBarang, Request $request)
    {   
        DB::beginTransaction();
        try {
            $barang = Models\Barang::find($idBarang);
            $barang->delete();
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Barang Deleted.'
        ]);
    }
}
