<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models;

class biayaController extends Controller
{
    public function index(){
   	return view('setting.biaya');
    }

    public function getData(Request $request){
        $data = new Models\Biaya;

        if ($request->input('search')) {
            $data = $data->where('kode_biaya','LIKE','%'.$request->input('search').'%')
                ->orWhere('nama_biaya','LIKE','%'.$request->input('search').'%')
                ->orWhere('nominal_biaya','LIKE','%'.$request->input('search').'%');
        }

        $count = $data->count();

        if ($request->input('sort')) {
            $data = $data->orderBy($request->input('sort'),$request->input('order'))
                ->orderBy('id_biaya',$request->input('order'));
            
        } else {
            $data = $data->orderBy('nama_biaya','asc');
        }
        if($request->input('limit')) {
            $data = $data->take($request->input('limit'))
            ->skip($request->input('offset'));
        }

        $data = $data->get();
        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }
    
    public function add(){
        $mode='ADD';
        return view('setting.biayaadd',compact('mode'));
    }

    public function create(Request $request){
        Validator::make($request->all(), [
            'txtKodeBiaya' => 'required',
            'txtNamaBiaya' => 'required',
            'txtNominalBiaya' => 'required',

        ])->validate();

        DB::beginTransaction();
        try {
            $biaya = new Models\Biaya;
            $biaya->kode_biaya = $request->input('txtKodeBiaya');
            $biaya->nama_biaya = $request->input('txtNamaBiaya');
            $biaya->nominal_biaya = str_replace( ',', '',$request->input('txtNominalBiaya'));
            $biaya->save();


        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->route('setting.biaya')
            ->with('success','Data Saved');
    }

    public function edit($idBiaya){
        $mode = 'EDIT';
        $biaya = Models\Biaya::find($idBiaya);
        return view('setting.biayaadd',compact('mode','biaya'));
    }

    public function update(Request $request,$idBiaya){
        Validator::make($request->all(), [
            'txtKodeBiaya' => 'required',
            'txtNamaBiaya' => 'required',
            'txtNominalBiaya' => 'required',

        ])->validate();

        DB::beginTransaction();
        try {
            $biaya = Models\Biaya::find($idBiaya);
            $biaya->kode_biaya = $request->input('txtKodeBiaya');
            $biaya->nama_biaya = $request->input('txtNamaBiaya');
            $biaya->nominal_biaya = str_replace( ',', '',$request->input('txtNominalBiaya'));
            $biaya->save();


        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->back()
            ->with('success',__('Data Updated'));
    }

    public function delete($idBiaya, Request $request)
    {   
        DB::beginTransaction();
        try {
            $biaya = Models\Biaya::find($idBiaya);
            $biaya->delete();
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        $request->session()->flash('success', 'Biaya Dihapus!');
        return response()->json([
            'status' => 'success',
            'description' => 'Biaya Dihapus!'
        ]);
    }
}
