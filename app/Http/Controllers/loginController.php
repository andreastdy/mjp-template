<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use DB;
use App\Models;
use Hash;

class loginController extends Controller
{
    public function index(){
        return view('login');
    }

    public function Authenticate(Request $request)
    {
      $input = $request->input();

      if (Auth::attempt(['username' => $input['txtUsername'], 'password' => $input['txtPassword']])) {
        return redirect()->intended('/');
      } else {
        return redirect()
            ->route('login')
            ->with('error','Username atau password tidak ditemukan.');
      }
    }

    public function changePass()
    {
        return view('changepassword');
    }

    public function destroy()
    {
      Session::flush();
        Auth::logout();

        return redirect()->route('login');
    }

    public function ubahPassword()
    {
        return view('changepassword');
    }

    public function changePassword(Request $request)
    {
        $this->validate($request,[
            'NewPassword' => 'required|confirmed'
        ]);

        DB::beginTransaction();
        try {
            $user = Models\User::find(Auth::user()->id);
            if (Hash::check($request->input('txtPassword'),$user->password)) {
                $user->password = Hash::make($request->input('NewPassword'));
                if (!$user->save()) {
                    throw new \Exception("Unable to change password.", 1);
                }
            } else {
                throw new \Exception("Password does not match. Unable to change password.", 1);
                
            }
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with ('error',$e->getMessage());
        }
        DB::commit();
        return redirect()->back()
            ->with('success','Password is updated successfully.');
    }
}
