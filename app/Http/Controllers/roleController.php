<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use DB;
use App\Models;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Hash;
use Carbon\Carbon;

class roleController extends Controller
{
    public function index(){
        return view('setting.role');
     }
 
     public function getData(Request $request){
         $data = new Role;
        // dd($data);
         if ($request->input('search')) {
             $data = $data->where('name','LIKE','%'.$request->input('search').'%');
         }
         $count = $data->count();
 
         if ($request->input('sort')) {
             $data = $data->orderBy($request->input('sort'),$request->input('order'))
                 ->orderBy('name',$request->input('order'));
             
         } else {
             $data = $data->orderBy('name','asc');
         }
         if($request->input('limit')) {
             $data = $data->take($request->input('limit'))
             ->skip($request->input('offset'));
         }
 
         $data = $data->get();
         
         return response()->json([
             'total' => $count,
             'rows' => $data
         ]);
     }
     
     public function add(){
         $mode = 'ADD';
        //  $roles = \Spatie\Permission\Models\Role::all();
         $permissions = \Spatie\Permission\Models\Permission::all();
         // dd($data[1]->permissions[0]->display_name);
         return view('setting.roleadd',compact('permissions','mode'));
     }
 
    //  public function getPermission($name){
    //      $roles = \Spatie\Permission\Models\Role::findByName($name)->permissions->pluck("id");
    //      // $roles = $roles->get();
    //      $count = $roles->count();
    //      // dd($count);
         
    //      return response()->json([
    //          'total' => $count,
    //          'rows' => $roles
    //      ]);
    //  }
 
     public function create(Request $request){
         Validator::make($request->all(), [
             'nama' => 'required',
         ],['nama.required'=>'Nama Role harus diisi',
         ])->validate();
 
         try {
         DB::beginTransaction();
             $role = new Role;
             $role->name = $request->input('nama');
             $role->guard_name = 'web';
             $role->created_at = Carbon::now();
             $role->updated_at = Carbon::now();
             if($role->save()){
                 // dd("masuk");
                 $role->syncPermissions($request->input('detail'));
             }
 
 
         } catch (\Exception $e) {
             DB::rollback();
             return response()->json([
                 'status' => 'error',
                 'description' => $e->getMessage()
             ]);
         }
         DB::commit();
         $request->session()->flash('success', 'Role Tersimpan!');
         return response()->json([
             'status' => 'success',
             'description' => 'Role Tersimpan!'
         ]);
     }
 
     public function edit($namarole){
         $mode = 'EDIT';
         $role = Role::findByName($namarole);
         $permissions = \Spatie\Permission\Models\Permission::all();
         $alluserpermission =  Role::findByName($namarole)->permissions->pluck("id")->toArray();
         // dd($alluserpermission);
         // dd($data[1]->permissions[0]->display_name);
         return view('setting.roleadd',compact('permissions','role','mode','alluserpermission'));
     }
 
     public function update(Request $request, $namarole){
        Validator::make($request->all(), [
            'nama' => 'required',
        ],['nama.required'=>'Nama Role harus diisi',
        ])->validate();
         
         try {
         DB::beginTransaction();
             $role = Role::findByName($namarole);
             $role->name = $request->input('nama');
            //  $role->guard_name = 'web';
            //  $role->created_at = Carbon::now();
             $role->updated_at = Carbon::now();
             if($role->save()){
                 // dd("masuk");
                 $role->syncPermissions($request->input('detail'));
             }
 
 
         } catch (\Exception $e) {
             DB::rollback();
             return response()->json([
                 'status' => 'error',
                 'description' => $e->getMessage()
             ]);
         }
         DB::commit();
         $request->session()->flash('success', 'Role Tersimpan!');
         return response()->json([
             'status' => 'success',
             'description' => 'Role Tersimpan!'
         ]);
     }
 
     public function delete($namarole, Request $request)
     {   
         DB::beginTransaction();
         try {
             $role = Role::findByName($namarole);
             $role->syncPermissions();
             $role->delete();
             
             
         } catch (\Exception $e) {
             DB::rollback();
             return response()->json([
                 'status' => 'error',
                 'description' => $e->getMessage()
             ]);
         }
         DB::commit();
         return response()->json([
             'status' => 'success',
             'description' => 'Role Deleted.'
         ]);
     }
}
