<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models;

class truckingController extends Controller
{
    public function index(){
   		return view('master.trucking');
    }

    public function getData(Request $request){
        $data = new Models\Trucking;
        
        if ($request->input('search')) {
            $data = $data->where('nama','LIKE','%'.$request->input('search').'%')
                ->orWhere('alamat','LIKE','%'.$request->input('search').'%')
                ->orWhere('rekening','LIKE','%'.$request->input('search').'%')
                ->orWhere('name_cp','LIKE','%'.$request->input('search').'%')
                ->orWhere('telp_cp','LIKE','%'.$request->input('search').'%')
                ->orWhere('email','LIKE','%'.$request->input('search').'%');
        }

        $count = $data->count();

        if ($request->input('sort')) {
            $data = $data->orderBy($request->input('sort'),$request->input('order'))
                ->orderBy('id_trucking',$request->input('order'));
        } else {
            $data = $data->orderBy('nama','asc');
        }
        if($request->input('limit')) {
            $data = $data->take($request->input('limit'))
            ->skip($request->input('offset'));
        }
        
        $data = $data->get();
        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }
    
    public function add(){
        $mode = 'ADD';
        return view('master.truckingadd',compact('mode'));
    }

    public function create(Request $request){
        Validator::make($request->all(), [
            'txtName' => 'required',
            'txtAlamat' => 'required',
            'txtRekening' => 'required',
            'txtNamaCP' => 'required',
            'txtTeleponCP' => 'required',
            'txtEmail' => 'required',

        ])->validate();

        $idtrucking = 0;

        DB::beginTransaction();
        try {
            $trucking = new Models\Trucking;
            $trucking->nama = $request->input('txtName');
            $trucking->alamat = $request->input('txtAlamat');
            $trucking->rekening = $request->input('txtRekening');
            $trucking->name_cp = $request->input('txtNamaCP');
            $trucking->telp_cp = $request->input('txtTeleponCP');
            $trucking->email = $request->input('txtEmail');
            if($trucking->save()){
                $idtrucking = $trucking->id_trucking;
            }

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->route('master.trucking.detail',$idtrucking)
            ->with('success',__('Data Saved'));
    }


    public function edit($idtrucking){
        $mode = 'EDIT';
        $trucking = Models\Trucking::find($idtrucking);
        return view('master.truckingadd',compact('mode','trucking'));
    }

    public function update($idtrucking,Request $request){
        Validator::make($request->all(), [
            'txtName' => 'required',
            'txtAlamat' => 'required',
            'txtRekening' => 'required',
            'txtNamaCP' => 'required',
            'txtTeleponCP' => 'required',
            'txtEmail' => 'required',

        ])->validate();

        DB::beginTransaction();
        try {
            $trucking = Models\Trucking::find($idtrucking);
            $trucking->nama = $request->input('txtName');
            $trucking->alamat = $request->input('txtAlamat');
            $trucking->rekening = $request->input('txtRekening');
            $trucking->name_cp = $request->input('txtNamaCP');
            $trucking->telp_cp = $request->input('txtTeleponCP');
            $trucking->email = $request->input('txtEmail');
            $trucking->save();

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->back()
            ->with('success',__('Data Updated'));
    }

    public function detail($idtrucking){
        $trucking = Models\Trucking::find($idtrucking);
        return view('master.detailtrucking',compact('trucking'));
    }

    public function getDataDetail($idtrucking){
        $data = Models\TruckingDetail::with('Sector')->where('truckingdetail.id_trucking','=',$idtrucking);

        $count = $data->count();

        // if ($request->input('sort')) {
        //     $data = $data->orderBy($request->input('sort'),$request->input('order'))
        //         ->orderBy('consignee.id_consignee',$request->input('order'));
            
        // } else {
        //     $data = $data->orderBy('consignee.nama');
        // }
        // if($request->input('limit')) {
        //     $data = $data->take($request->input('limit'))
        //     ->skip($request->input('offset'));
        // }
        
        $data = $data->get();
        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }

    public function addDetail($idtrucking){
        $mode = 'ADD';
        $trucking = Models\Trucking::find($idtrucking);
        $sector = Models\Sector::get();
        return view('master.detailtruckingadd',compact('mode','trucking','sector'));
    }

    public function createDetail($idtrucking, Request $request){
        Validator::make($request->all(), [
            'cboSector' => 'required|not_in:0',
            // 'txtKeterangan' => 'required',
            'txtHarga' => 'required',
            'cboSize' => 'required',

        ],[
            'cboSector.not_in' => 'sektor harus dipilih',
            ])->validate();

        $idsector = explode('|' ,$request->input('cboSector'));

        DB::beginTransaction();
        try {
            $truckingdetail = new Models\TruckingDetail;
            $truckingdetail->id_trucking = $idtrucking;
            $truckingdetail->id_sector = $idsector[0];
            $truckingdetail->keterangan = $request->input('txtKeterangan');
            $truckingdetail->harga = str_replace( ',', '',$request->input('txtHarga'));
            $truckingdetail->size = $request->input('cboSize');
            $truckingdetail->save();

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->route('master.trucking.detail',$idtrucking)
            ->with('success',('Data Saved'));
    }

    public function editDetail($idtruckingdetail){
        $mode = 'EDIT';
        $truckingdetail = Models\TruckingDetail::with('Sector')->find($idtruckingdetail);
        $sector = Models\Sector::get();
        return view('master.detailtruckingadd',compact('mode','truckingdetail','sector'));
    }

    public function updateDetail($idtruckingdetail, Request $request){
        Validator::make($request->all(), [
            'cboSector' => 'required|not_in:0',
            // 'txtKeterangan' => 'required',
            'txtHarga' => 'required',
            'cboSize' => 'required',

        ],[
            'cboSector.not_in' => 'sektor harus dipilih',
            ])->validate();

        $idsector = explode('|' ,$request->input('cboSector'));

        DB::beginTransaction();
        try {
            $truckingdetail = Models\TruckingDetail::find($idtruckingdetail);
            $truckingdetail->id_sector = $idsector[0];
            $truckingdetail->keterangan = $request->input('txtKeterangan');
            $truckingdetail->harga = str_replace( ',', '',$request->input('txtHarga'));
            $truckingdetail->size = $request->input('cboSize');
            $truckingdetail->save();

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->back()
            ->with('success',('Data Updated'));
    }

    public function deleteDetail($idtruckingdetail, Request $request)
    {   
        DB::beginTransaction();
        try {
            $truckingdetail = Models\TruckingDetail::find($idtruckingdetail);
            $truckingdetail->delete();
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Detail Trucking Deleted.'
        ]);
    }

    public function ubahHarga($idtrucking){
        $trucking = Models\Trucking::find($idtrucking);
        $truckingdetail = Models\TruckingDetail::with('Sector')
                        ->where('id_trucking','=',$idtrucking)->get();
        return view('master.updatehargatrucking',compact('trucking','truckingdetail'));
    }

    public function updateHarga($idtrucking, Request $request){

        DB::beginTransaction();
        try {
            $truckingdetail = Models\TruckingDetail::find($request->iddetail);
            $truckingdetail->harga = $request->harga;
            $truckingdetail->save();

        } catch (\Exception $e) {
            DB::rollback();
             return response()->json([
            'status' => 'error',
            'description' => $e->getMessage()
        ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'success'
        ]);
    }
}