<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models;
use Carbon\Carbon;
use Auth;

class updateOrderController extends Controller
{
    public function index(){
   		return view('transaction.updateorder');
    }

    public function getData(){
        
        $data = Models\Order::leftJoin('pelabuhan as pelabuhan_asal','pelabuhan_asal.id_pelabuhan','order.id_pelabuhan_asal')
                                    ->leftJoin('pelabuhan as pelabuhan_tujuan','pelabuhan_tujuan.id_pelabuhan','order.id_pelabuhan_tujuan')
                                    ->leftJoin('city as city_pelabuhan_asal','city_pelabuhan_asal.id_kota','pelabuhan_asal.id_kota')
                                    ->leftJoin('city as city_pelabuhan_tujuan','city_pelabuhan_tujuan.id_kota','pelabuhan_tujuan.id_kota')
                                    ->leftJoin('city as city_asal','city_asal.id_kota','order.id_kota_asal')
                                    ->leftJoin('city as city_tujuan','city_tujuan.id_kota','order.id_kota_tujuan')
                                    ->leftJoin('pelayaran','pelayaran.id_pelayaran','order.id_pelayaran')
                                    ->leftJoin('customer','customer.id_customer','order.id_customer')
                                    ->where('order.status','=','1')
                                    ->orderBy('order.created_at', 'DESC');;
        
        $data = $data->select([
            'order.*',
            'pelabuhan_asal.nama_pelabuhan as nama_pelabuhan_asal',
            'pelabuhan_tujuan.nama_pelabuhan as nama_pelabuhan_tujuan',
            'city_pelabuhan_asal.nama_kota as nama_kotapelabuhan_asal',
            'city_pelabuhan_tujuan.nama_kota as nama_kotapelabuhan_tujuan',
            'city_asal.nama_kota as nama_kota_asal',
            'city_tujuan.nama_kota as nama_kota_tujuan',
            'pelayaran.name_full as nama_pelayaran',
            'customer.nama_pt as nama_customer'
        ]);

        $count = $data->count();

        // if ($request->input('sort')) {
        //     $data = $data->orderBy($request->input('sort'),$request->input('order'))
        //         ->orderBy('consignee.id_consignee',$request->input('order'));
            
        // } else {
        //     $data = $data->orderBy('consignee.nama');
        // }
        // if($request->input('limit')) {
        //     $data = $data->take($request->input('limit'))
        //     ->skip($request->input('offset'));
        // }
        
        $data = $data->get();
        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }

    public function edit($idorder){
        $order = Models\Order::find($idorder);
        $suratjalan = Models\SuratJalan::where('id_order','=',$idorder)->Get();
        $pelayaran = Models\Pelayaran::Get();
        $customer = Models\Customer::Get();
        $pelabuhan = Models\Pelabuhan::leftJoin('city', 'city.id_kota', '=', 'pelabuhan.id_kota')->orderBy('city.nama_kota','asc')->get();
        $city = Models\City::orderBy('nama_kota','asc')->Get();
        return view('transaction.updateorderedit',compact('order','suratjalan','pelayaran','customer','pelabuhan','city'));
    }

    public function getListBarang($idorder){
        
        $data = Models\SuratJalan::where('id_order','=',$idorder);
        
        $data = $data->select([
            'suratjalan.*'
        ]);
        
        $count = $data->count();

        // if ($request->input('sort')) {
        //     $data = $data->orderBy($request->input('sort'),$request->input('order'))
        //         ->orderBy('consignee.id_consignee',$request->input('order'));
            
        // } else {
        //     $data = $data->orderBy('consignee.nama');
        // }
        // if($request->input('limit')) {
        //     $data = $data->take($request->input('limit'))
        //     ->skip($request->input('offset'));
        // }
        
        $data = $data->get();
        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }

    public function updateStatus(Request $request, $idorder){
        
        DB::beginTransaction();
        try {
            $detail = $request->input('detail');
            // dd($detail);
            foreach ($detail as $item) {
                
                    $suratjalan = Models\SuratJalan::find($item['idsuratjalan']);
                    // $dodetail->id_do = $deliveryorder->id_do;
                    $suratjalan->status = $item['status'];
                    $suratjalan->keterangan_status = $item['keterangan_status'];
                    $suratjalan->save();

                    $history = new Models\HistoryUpdateOrder;
                    $history->id_order = $idorder;
                    $history->id_suratjalan = $item['idsuratjalan'];
                    $history->status = $item['status'];
                    $history->keterangan = $item['keterangan_status'];
                    $history->save();
                
            }

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        $request->session()->flash('success', 'Container Status Updated!');
        return response()->json([
            'status' => 'success',
            'description' => 'Container Status Updated!'
        ]);
    }
}
