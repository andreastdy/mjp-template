<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models;
use Carbon\Carbon;
use Auth;
use Terbilang;

class customerProposalController extends Controller
{
    public function index(){
   		return view('transaction.customerproposal');
    }

    public function getData(Request $request){
        // $data = Models\PelayaranDetail::with('DetailBongkar')->with('DetailAsal')
        $data = Models\Proposal::leftJoin('customer','customer.id_customer','proposal.id_customer');
        
        $data = $data->select([
            'proposal.*',
            'customer.nama_pt as nama_customer'
            // DB::raw("CASE WHEN DATE(SYSDATE())<=proposal.valid_to THEN '<font color=green>ACTIVE</font>' ELSE '<font color=red>EXPIRED</font>' END AS validity")
        ]);

        if ($request->input('search')) {
            $data = $data->where('customer.nama_pt','LIKE','%'.$request->input('search').'%')
                ->orWhere('proposal.no_proposal','LIKE','%'.$request->input('search').'%')
                ->orWhere('proposal.created_by','LIKE','%'.$request->input('search').'%')
                ->orWhere('proposal.approved_by','LIKE','%'.$request->input('search').'%');
        }
        $count = $data->count();

        if ($request->input('sort')) {
            $data = $data->orderBy($request->input('sort'),$request->input('order'))
                ->orderBy('proposal.id_proposal',$request->input('order'));
            
        } else {
            $data = $data->orderBy('proposal.created_at','desc');
        }
        if($request->input('limit')) {
            $data = $data->take($request->input('limit'))
            ->skip($request->input('offset'));
        }
        
        $data = $data->get();
        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }
    
    public function add(){
        $mode = 'ADD';
        $customer = Models\Customer::orderBy('nama_pt','asc')->Get();
        return view('transaction.customerproposaladd',compact('mode','customer'));
    }

    public function create(Request $request){
        Validator::make($request->all(), [
            'cboCustomer' => 'required|not_in:0',
            'txtCustCredit' => 'required',
            'rdoType' => 'required',

        ],[
            'cboCustomer.not_in' => 'customer harus dipilih',
            ])->validate();
        

        try {
        $customer = explode(';',$request->input('cboCustomer'));
        // dd($customer[0]);
        if ($customer[0]==0) {
            throw new \Exception("Customer Harus Dipilih", 1);
        }
        
        $year = DB::select('select distinct SUBSTRING(EXTRACT(YEAR FROM CURRENT_DATE),1,4) as year from dual');
        // dd($year[0]->year);
        $month = DB::select('select distinct SUBSTRING(EXTRACT(MONTH FROM CURRENT_DATE),1,2) as month from dual');
        // dd($month[0]->month);
        $shortname = DB::select("select IFNULL(name_short,'-') as name_short from customer where id_customer=?",[$customer[0]]);
        //dd($shortname[0]->name_short);
        

        if($request->input('rdoType')=='CONTRACT'){
            $lastnomorurut = DB::select("select IFNULL(MAX(no_urut),0)+1 as nourut from proposal where tipe_proposal='CONTRACT' and id_customer=?",[$customer[0]]);
            $noproposal = $year[0]->year.'/'.$month[0]->month.'/'.$shortname[0]->name_short.'/'.str_pad($lastnomorurut[0]->nourut,4,"0",STR_PAD_LEFT).'/K/MJP/SDA';
        }else{
            $lastnomorurut = DB::select("select IFNULL(MAX(no_urut),0)+1 as nourut from proposal where tipe_proposal='NON-CONTRACT' and  id_customer=?",[$customer[0]]);
            $noproposal = $year[0]->year.'/'.$month[0]->month.'/'.$shortname[0]->name_short.'/'.str_pad($lastnomorurut[0]->nourut,4,"0",STR_PAD_LEFT).'/NK/MJP/SDA';
        }
        
        // dd($noproposal);
        // $cekvalidity = DB::select("select CASE WHEN DATE(SYSDATE())<=STR_TO_DATE(?,'%d/%m/%Y') THEN 1 ELSE 0 END AS cekvalid FROM DUAL",[Carbon::parse($request->input('txtValidTo'))->format('d/m/Y')]);

        // $cekvalidity2 = DB::select("select CASE WHEN STR_TO_DATE(?,'%d/%m/%Y')<=STR_TO_DATE(?,'%d/%m/%Y') THEN 1 ELSE 0 END AS cekvalid FROM DUAL",[Carbon::parse($request->input('txtValidFrom'))->format('d/m/Y'),Carbon::parse($request->input('txtValidTo'))->format('d/m/Y')]);

        // dd($cekvalidity2[0]->cekvalid);

        // dd($year[0]->year);
        // dd($lastnomortrip[0]->notrip);
        DB::beginTransaction();
        
            

            // if ($cekvalidity[0]->cekvalid==0) {
            //     throw new \Exception("Valid To Harus lebih Besar atau sama dengan hari ini", 1);
            // }
            
            // if ($cekvalidity2[0]->cekvalid==0) {
            //     throw new \Exception("Valid To Harus lebih Besar atau sama dengan valid from", 1);
            // }

            $proposal = new Models\Proposal;
            $proposal->no_proposal = $noproposal;
            $proposal->id_customer = $customer[0];
            // $proposal->valid_from = Carbon::parse($request->input('txtValidFrom'))->format('Y/m/d H:i:s');
            // $proposal->valid_to = Carbon::parse($request->input('txtValidTo'))->format('Y/m/d H:i:s');
            $proposal->no_urut = $lastnomorurut[0]->nourut;
            $proposal->customer_credit = $request->input('txtCustCredit');
            $proposal->tipe_proposal = $request->input('rdoType');
            $termcondition = 'Tarif berlaku dengan ketentuan sebagai berikut :<br/>'.
                                        '<ol type="1">'.
                                        '<li>Syarat'.
                                            '<ol type="a">'.
                                            '<li>Syarat dan ketentuan pengiriman muatan terlampir menjadi bagian yang tidak terpisahkan dengan surat proposal tariff ini.</li>'.
                                            '<li>Wajib melampirkan kelengkapan dokumen pendukung yang diperlukan untuk pengiriman barang sesuai dengan aturan yang berlaku</li>'.
                                        '</ol>'.
                                    '</li>'.
                                    '<br/>'.
                                    '<li>Tarif'.
                                        '<ol type="a">'.
                                            '<li>Tarif berlaku untuk jenis muatan komoditi general cargo (non-dangerous goods).</li>'.
                                            '<li>Mohon dilakukan konfirmasi tarif dan space sebelum melakukan booking.</li>'.
                                            '<li>Pembayaran dilakukan dalam <b><i><u>'.$request->input('txtCustCredit').' ('.Terbilang::make($request->input('txtCustCredit')).') hari</u></i></b> setelah penyerahan kwitansi berita acara penerimaan barang.</li>'.
                                            '<li>Tarif belum termasuk biaya:'.
                                                '<ol type="i">'.
                                                    '<li>Asuransi muatan, dan</li>'.
                                                    '<li>PPN 1%.</li>'.
                                                '</ol>'.
                                            '</li>'.
                                        '</ol>'.
                                    '</li>'.
                                    '<br/>'.
                                    '<li>Untuk muatan LCL (Less Container Load):'.
                                        '<ol type="a">'.
                                            '<li>Minimum perhitungan tariff muatan 3 m<sup>3</sup> / ton.</li>'.
                                            '<li>Perhitungan kami berdasarkan:'.
                                                '<ol type="i">'.
                                                    '<li>Perhitungan kubikasi dan atau tonase sesuai pengukuran packing terluar barang yang dilakukan oleh PT. Mandala Jasa Pratama di depo.</li>'.
                                                    '<li>Perhitungan jumlah collie sesuai tally tanpa pengecekan kondisi barang.</li>'.
                                                '</ol>'.
                                            '</li>'.
                                            '<li>Kami tidak menerima restitusi kubikasi dan atau tonase.</li>'.
                                            '<li>Cargo akan diberangkatkan setelah container penuh.</li>'.
                                        '</ol>'.
                                    '</li>'.
                                    '<br/>'.
                                    '<li>Klaim, storage, dan biaya tambahan.'.
                                        '<ol type="a">'.
                                            '<li>Kami tidak menerima klaim atau pemotongan tagihan apabila isi container kekurangan collie atau rusak atau kadaluarsa dalam keadaan tersegel.</li>'.
                                            '<li>Free time storage (penumpukan) dan demurrage container maksimum 5 hari kerja terhitung sejak tanggal kapal tiba.</li>'.
                                            '<li>Khusus muatan berat, harga kami belum termasuk biaya forklift di Surabaya ataupun di daerah tujuan.</li>'.
                                        '</ol>'.
                                    '</li>'.
                                    '<br/>'.
                                    '<li>Kami menerima transfer pembayaran ke rekening BCA Sidoarjo 018 2152488, a/n PT Mandala Jasa Pratama.</li>'.
                                '</ol>';
            $proposal->termcondition = $termcondition;
            $proposal->status=1;
            $proposal->save();


        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();

        $idproposal = DB::select('select id_proposal from proposal where no_proposal=?',[$noproposal]);
        return redirect()->route('transaction.customerproposal.edit',$idproposal[0]->id_proposal)
            ->with('success',__('Data Saved'));
    }


    public function edit($idproposal){
        $mode = 'EDIT';
        $proposal = Models\Proposal::find($idproposal);
        $customer = Models\Customer::Get();
        return view('transaction.customerproposaladd',compact('mode','proposal','customer'));
    }

    public function update(Request $request, $idproposal){
        Validator::make($request->all(), [
            'txtCustCredit' => 'required',
            'rdoType' => 'required',

        ])->validate();
        

        try {

        DB::beginTransaction();
            $proposal = Models\Proposal::find($idproposal);
            $proposal->customer_credit = $request->input('txtCustCredit');
            $proposal->tipe_proposal = $request->input('rdoType');
            $proposal->save();


        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->back()
            ->with('success',__('Data Updated'));
    }

    public function detailadd($idproposal){
        $from = 'EDITOR';
        $mode = 'ADD';
        $proposal = Models\Proposal::leftJoin('customer','proposal.id_customer','=','customer.id_customer')->find($idproposal);
        $biaya = Models\Biaya::Get();
        $pelabuhan = Models\Pelabuhan::leftJoin('city', 'city.id_kota', '=', 'pelabuhan.id_kota')->orderBy('city.nama_kota','asc')->get();
        $pelayaran = Models\Pelayaran::Get();
        $dooring = Models\Dooring::Get();
        $sector = Models\Sector::Get();
        $city = Models\City::Get();
        $consignee = Models\Consignee::Get();
        return view('transaction.detailcustomerproposaladd',compact('from','mode','proposal','consignee','pelabuhan','city','pelayaran','dooring','sector','biaya'));
    }

    public function getDataDetail($idproposal){
        
        $data = Models\ProposalDetail::leftJoin('pelabuhan as pelabuhan_asal','pelabuhan_asal.id_pelabuhan','proposaldetail.id_pelabuhan_asal')
                                    ->leftJoin('pelabuhan as pelabuhan_tujuan','pelabuhan_tujuan.id_pelabuhan','proposaldetail.id_pelabuhan_tujuan')
                                    ->leftJoin('city as city_asal','city_asal.id_kota','proposaldetail.id_kota_asal')
                                    ->leftJoin('city as city_tujuan','city_tujuan.id_kota','proposaldetail.id_kota_tujuan')
                                    ->leftJoin('pelayaran','pelayaran.id_pelayaran','proposaldetail.id_pelayaran')
                                    ->leftJoin('truckingdetail','truckingdetail.id_truckingdetail','proposaldetail.id_truckingdetail')
                                    ->leftJoin('trucking','trucking.id_trucking','truckingdetail.id_trucking')
                                    ->leftJoin('dooring','dooring.id_dooring','proposaldetail.id_dooring')
                                    ->leftJoin('consignee','proposaldetail.id_consignee','consignee.id_consignee')
                                    ->where('id_proposal','=',$idproposal)
                                    ->orderBy('proposaldetail.created_at', 'DESC');
        
        $data = $data->select([
            'proposaldetail.*',
            'pelabuhan_asal.nama_pelabuhan as nama_pelabuhan_asal',
            'pelabuhan_tujuan.nama_pelabuhan as nama_pelabuhan_tujuan',
            'city_asal.nama_kota as nama_kota_asal',
            'city_tujuan.nama_kota as nama_kota_tujuan',
            'pelayaran.name_full as nama_pelayaran',
            'consignee.nama as nama_consignee'
            // DB::raw("CASE WHEN DATE(SYSDATE())<=proposal.valid_to THEN '<font color=green>ACTIVE</font>' ELSE '<font color=red>EXPIRED</font>' END AS validity")
        ]);

        $count = $data->count();

        // if ($request->input('sort')) {
        //     $data = $data->orderBy($request->input('sort'),$request->input('order'))
        //         ->orderBy('consignee.id_consignee',$request->input('order'));
            
        // } else {
        //     $data = $data->orderBy('consignee.nama');
        // }
        // if($request->input('limit')) {
        //     $data = $data->take($request->input('limit'))
        //     ->skip($request->input('offset'));
        // }
        
        $data = $data->get();
        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }

    public function getDataTrucking($idsector,$size){
        $data = Models\Trucking::leftJoin('truckingdetail','truckingdetail.id_trucking','trucking.id_trucking')                               
                                    ->where('truckingdetail.id_sector','=',$idsector)
                                    ->where('truckingdetail.deleted_at','=', null)
                                    // ->where(function($q) {
                                    //     $q->where('truckingdetail.size','=',$size)
                                    //     ->orWhere('truckingdetail.size','=','combo');
                                    // })
                                    ->where('truckingdetail.size','=',$size)
                                    // ->orWhere('truckingdetail.size','=','combo')
                                    ->orderBy('trucking.nama','asc');
        // dd($data);
        
        $data = $data->select([
            'trucking.nama',
            'truckingdetail.id_truckingdetail',
            'truckingdetail.harga as harga',
        ]);

        $data = $data->get();
        
      return response()->json([
            'rows' => $data
        ]);
    }

    public function createDetail(Request $request, $idproposal ){
        // dd($idproposal);
        Validator::make($request->all(), [
            'txtValidFrom' => 'required',
            // 'txtValidTo' => 'required',
            'cboKotaAsal' => 'required|not_in:0',
            'cboKotaTujuan' => 'required|not_in:0',
            'cboPortAsal' => 'required|not_in:0',
            'cboPortTujuan' => 'required|not_in:0',
            'rdoType' => 'required',
            'cboProposalType' => 'required|not_in:0',
            'cboJenisBarang' => 'required|not_in:0',
            'txtAlamatAsal' => 'required',
            'txtAlamatTujuan' => 'required',
            'cboPelayaran' => 'required|not_in:0',
            'txtHargaPelayaran' => 'required',
            'txtHargaExtraPelayaran' => 'required',
            'txtHargaTrucking' => 'required',
            'cboSector' => 'required|not_in:0',
            'cboTrucking' => 'required|not_in:0',
            'cboDooring' => 'required|not_in:0',
            'txtHargaDooring' => 'required',
            'txtHargaExtraDooring' => 'required',
            'txtHargaTotal' => 'required',

        ],[
            'rdoType.not_in' => 'tipe proposal harus dipilih',
            'cboKotaAsal.not_in' => 'kota asal harus dipilih',
            'cboKotaTujuan.not_in' => 'kota tujuan harus dipilih',
            'cboPortAsal.not_in' => 'pelabuhan asal harus dipilih',
            'cboPortTujuan.not_in' => 'pelabuhan tujuan harus dipilih',
            'cboProposalType.not_in' => 'jenis proposal harus dipilih',
            'cboJenisBarang.not_in' => 'jenis barang harus dipilih',
            'cboPelayaran.not_in' => 'pelayaran harus dipilih',
            'cboSector.not_in' => 'sektor harus dipilih',
            'cboTrucking.not_in' => 'trucking harus dipilih',
            'cboDooring.not_in' => 'dooring harus dipilih',
            ])->validate();
        
        $namabiayalain = $request->input('txtNamaBiayaLain');
        $nominalbiayalain = $request->input('txtNominalBiayaLain');
        // $tes = "";
        $i = 0;
        // dd($biayalain);
        // foreach ($nominalbiayalain as $item) {
        //     $tes = $item;
        //     $i++;
        // }

        // dd($namabiayalain[$i-1]);

        DB::beginTransaction();
        try {
            $typeproposal = $request->input('rdoType');

            $truckingdetail = explode(';',$request->input('cboSector'));
            if ($truckingdetail[0]==0) {
                throw new \Exception("Sector Trucking Harus Dipilih");
            }

            if($request->input('txtValidFrom')==$request->input('txtValidTo')){
                throw new \Exception("Periode Awal Tidak Boleh Sama Dengan Periode Akhir");
            }

            $cekvalidity = DB::select("select CASE WHEN DATE(SYSDATE())<=STR_TO_DATE(?,'%d/%m/%Y') THEN 1 ELSE 0 END AS cekvalid FROM DUAL",[Carbon::parse($request->input('txtValidTo'))->format('d/m/Y')]);

            $cekvalidity2 = DB::select("select CASE WHEN STR_TO_DATE(?,'%d/%m/%Y')<=STR_TO_DATE(?,'%d/%m/%Y') THEN 1 ELSE 0 END AS cekvalid FROM DUAL",[Carbon::parse($request->input('txtValidFrom'))->format('d/m/Y'),Carbon::parse($request->input('txtValidTo'))->format('d/m/Y')]);
            // dd($cekvalidity[0]->cekvalid . " - ". $cekvalidity2[0]->cekvalid);

            if ($cekvalidity[0]->cekvalid==0) {
                throw new \Exception("Valid To Harus lebih Besar atau sama dengan hari ini");
            }
            
            if ($cekvalidity2[0]->cekvalid==0) {
                throw new \Exception("Valid To Harus lebih Besar atau sama dengan valid from");
            }

            if($request->input('cboKotaAsal')==$request->input('cboKotaTujuan')){
                throw new \Exception("Kota Asal Tidak Boleh Sama Dengan Kota Tujuan");
            }
            if($request->input('cboPortAsal')==$request->input('cboPortTujuan')){
                throw new \Exception("Pelabuhan Asal Tidak Boleh Sama Dengan Pelabuhan Tujuan");
            }

            if($request->input('cboConsignee')!='0'||$request->input('cboConsignee')!=0){
                if($typeproposal=="CONTAINER"){
                    if($request->input('txtValidTo')!=""||$request->input('txtValidTo')!=null){
                        $cekada = DB::select("select COUNT(1) AS ada FROM proposaldetail WHERE id_proposal=? and DATE(valid_from)=STR_TO_DATE(?,'%d/%m/%Y') AND DATE(valid_to)=STR_TO_DATE(?,'%d/%m/%Y') AND id_kota_asal=? AND id_kota_tujuan=? AND type_proposal=? AND  freight_size=? and id_consignee=? and deleted_by is null",
                                        [$idproposal,Carbon::parse($request->input('txtValidFrom'))->format('d/m/Y'),Carbon::parse($request->input('txtValidTo'))->format('d/m/Y'),$request->input('cboKotaAsal'),$request->input('cboKotaTujuan'),$typeproposal,$request->input('rdoFtsize'),$request->input('cboConsignee')]);
                    }else{
                        $cekada = DB::select("select COUNT(1) AS ada FROM proposaldetail WHERE id_proposal=? and DATE(valid_from)=STR_TO_DATE(?,'%d/%m/%Y') AND id_kota_asal=? AND id_kota_tujuan=? AND type_proposal=? AND  freight_size=? and id_consignee=? and deleted_by is null",
                                        [$idproposal,Carbon::parse($request->input('txtValidFrom'))->format('d/m/Y'),$request->input('cboKotaAsal'),$request->input('cboKotaTujuan'),$typeproposal,$request->input('rdoFtsize'),$request->input('cboConsignee')]);
                    }
                }else{
                    if($request->input('txtValidTo')!=""||$request->input('txtValidTo')!=null){
                        $cekada = DB::select("select COUNT(1) AS ada FROM proposaldetail WHERE id_proposal=? and DATE(valid_from)=STR_TO_DATE(?,'%d/%m/%Y') AND DATE(valid_to)=STR_TO_DATE(?,'%d/%m/%Y') AND id_kota_asal=? AND id_kota_tujuan=? AND type_proposal=? AND  lcl_size=? and id_consignee=? and deleted_by is null",
                                            [$idproposal, Carbon::parse($request->input('txtValidFrom'))->format('d/m/Y'),Carbon::parse($request->input('txtValidTo'))->format('d/m/Y'),$request->input('cboKotaAsal'),$request->input('cboKotaTujuan'),$typeproposal,$request->input('rdolclsize'),$request->input('cboConsignee')]);
                    }else{
                        $cekada = DB::select("select COUNT(1) AS ada FROM proposaldetail WHERE id_proposal=? and DATE(valid_from)=STR_TO_DATE(?,'%d/%m/%Y') AND id_kota_asal=? AND id_kota_tujuan=? AND type_proposal=? AND  lcl_size=? and id_consignee=? and deleted_by is null",
                        [$idproposal, Carbon::parse($request->input('txtValidFrom'))->format('d/m/Y'),$request->input('cboKotaAsal'),$request->input('cboKotaTujuan'),$typeproposal,$request->input('rdolclsize'),$request->input('cboConsignee')]);
                    }
                }
            }else{
                if($typeproposal=="CONTAINER"){
                    if($request->input('txtValidTo')!=""||$request->input('txtValidTo')!=null){
                        $cekada = DB::select("select COUNT(1) AS ada FROM proposaldetail WHERE id_proposal=? and DATE(valid_from)=STR_TO_DATE(?,'%d/%m/%Y') AND DATE(valid_to)=STR_TO_DATE(?,'%d/%m/%Y') AND id_kota_asal=? AND id_kota_tujuan=? AND type_proposal=? AND  freight_size=?",
                                            [$idproposal,Carbon::parse($request->input('txtValidFrom'))->format('d/m/Y'),Carbon::parse($request->input('txtValidTo'))->format('d/m/Y'),$request->input('cboKotaAsal'),$request->input('cboKotaTujuan'),$typeproposal,$request->input('rdoFtsize')]);
                    }else{
                        $cekada = DB::select("select COUNT(1) AS ada FROM proposaldetail WHERE id_proposal=? and DATE(valid_from)=STR_TO_DATE(?,'%d/%m/%Y') AND id_kota_asal=? AND id_kota_tujuan=? AND type_proposal=? AND  freight_size=?",
                        [$idproposal,Carbon::parse($request->input('txtValidFrom'))->format('d/m/Y'),$request->input('cboKotaAsal'),$request->input('cboKotaTujuan'),$typeproposal,$request->input('rdoFtsize')]);
                    }
                }else{
                    if($request->input('txtValidTo')!=""||$request->input('txtValidTo')!=null){
                        $cekada = DB::select("select COUNT(1) AS ada FROM proposaldetail WHERE id_proposal=? and DATE(valid_from)=STR_TO_DATE(?,'%d/%m/%Y') AND DATE(valid_to)=STR_TO_DATE(?,'%d/%m/%Y') AND id_kota_asal=? AND id_kota_tujuan=? AND type_proposal=? AND  lcl_size=?",
                                            [$idproposal, Carbon::parse($request->input('txtValidFrom'))->format('d/m/Y'),Carbon::parse($request->input('txtValidTo'))->format('d/m/Y'),$request->input('cboKotaAsal'),$request->input('cboKotaTujuan'),$typeproposal,$request->input('rdolclsize')]);
                    }else{
                        $cekada = DB::select("select COUNT(1) AS ada FROM proposaldetail WHERE id_proposal=? and DATE(valid_from)=STR_TO_DATE(?,'%d/%m/%Y') AND id_kota_asal=? AND id_kota_tujuan=? AND type_proposal=? AND  lcl_size=?",
                                            [$idproposal, Carbon::parse($request->input('txtValidFrom'))->format('d/m/Y'),$request->input('cboKotaAsal'),$request->input('cboKotaTujuan'),$typeproposal,$request->input('rdolclsize')]);
                    }
                }
            }

            // dd($cekada[0]->ada);

            if ($cekada[0]->ada>0) {
                throw new \Exception("Detail Proposal yang sama sudah pernah diinputkan");
            }


            $proposaldetail = new Models\ProposalDetail;
            $proposaldetail->id_proposal = $idproposal;
            $proposaldetail->valid_from = Carbon::parse($request->input('txtValidFrom'))->format('Y/m/d H:i:s');
            if($request->input('txtValidTo')!=""||$request->input('txtValidTo')!=null){
                $proposaldetail->valid_to = Carbon::parse($request->input('txtValidTo'))->format('Y/m/d H:i:s');
            }
            $proposaldetail->id_kota_asal = $request->input('cboKotaAsal');
            $proposaldetail->id_kota_tujuan = $request->input('cboKotaTujuan');
            $proposaldetail->id_pelabuhan_asal = $request->input('cboPortAsal');
            $proposaldetail->id_pelabuhan_tujuan = $request->input('cboPortTujuan');
            if($request->input('cboConsignee')!='0'||$request->input('cboConsignee')!=0){
                $proposaldetail->id_consignee = $request->input('cboConsignee');
            }
            $proposaldetail->type_proposal = $typeproposal;
            $proposaldetail->jenis_proposal = $request->input('cboProposalType');
            $proposaldetail->jenis_barang = $request->input('cboJenisBarang');
            $proposaldetail->alamat_asal = $request->input('txtAlamatAsal');
            $proposaldetail->alamat_tujuan = $request->input('txtAlamatTujuan');
            // $proposaldetail->keteranganpengiriman = $request->input('txtKeterangan');
            $proposaldetail->lampiran = $request->input('txtLampiran');
            if($typeproposal=="CONTAINER"){
                $proposaldetail->freight_size = $request->input('rdoFtsize');
                $proposaldetail->nominal_optsurabaya = str_replace( ',', '',$request->input('txtHargaOptSby'));
                $proposaldetail->nominal_thcp = str_replace( ',', '',$request->input('txtHargaTHCP'));
                $proposaldetail->nominal_thcd = str_replace( ',', '',$request->input('txtHargaTHCD'));
                $proposaldetail->nominal_surcharge = str_replace( ',', '',$request->input('txtHargaSurcharge'));
                $proposaldetail->nominal_optdooring = str_replace( ',', '',$request->input('txtHargaOptDooring'));
                $proposaldetail->nominal_optmjp = str_replace( ',', '',$request->input('txtHargaOptMjp'));
            }else{
                $proposaldetail->lcl_size = $request->input('rdolclsize');
                if($request->input('rdolclsize')=="M3"){
                    $proposaldetail->nominal_m3 = str_replace( ',', '',$request->input('txtHargam3'));
                }else{
                    $proposaldetail->nominal_ton = str_replace( ',', '',$request->input('txtHargaton'));
                }
            }
            $proposaldetail->id_pelayaran = $request->input('cboPelayaran');
            $proposaldetail->nominal_pelayaran = str_replace( ',', '',$request->input('txtHargaPelayaran'));
            $proposaldetail->nominal_extrapelayaran = str_replace( ',', '',$request->input('txtHargaExtraPelayaran'));
            $proposaldetail->keterangan_extrapelayaran = $request->input('txtExtraPelayaran');
            $proposaldetail->notes_pelayaran = $request->input('txtNotesPelayaran');
            $proposaldetail->id_truckingdetail = $truckingdetail[0];
            $proposaldetail->nominal_trucking = str_replace( ',', '',$request->input('txtHargaTrucking'));
            $proposaldetail->id_dooring = $request->input('cboDooring');
            $proposaldetail->nominal_dooring = str_replace( ',', '',$request->input('txtHargaDooring'));
            $proposaldetail->nominal_extradooring = str_replace( ',', '',$request->input('txtHargaExtraDooring'));
            $proposaldetail->nominal_total = $request->input('txtHargaTotal');
            $proposaldetail->status = 1;
            if($proposaldetail->save()){
                if($nominalbiayalain!=null){
                    foreach($nominalbiayalain as $item) {
                        $biayalain = new Models\BiayaLain;
                        $biayalain->id_proposaldetail = $proposaldetail->id_proposaldetail;
                        $biayalain->namabiayalain = str_replace( ',', '',$namabiayalain[$i]);
                        $biayalain->nominalbiayalain = str_replace( ',', '',$item);
                        $biayalain->save();
                        $i++;
                    }
                }
            }

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->route('transaction.customerproposal.edit',$idproposal)
            ->with('success',('Detail Saved'));
    }

    public function detailedit($idproposaldetail){
        $mode = 'EDIT';
        $from = 'EDITOR';
        $proposaldetail = Models\ProposalDetail::leftJoin('proposal','proposal.id_proposal','=','proposaldetail.id_proposal')
                                                ->leftJoin('customer','customer.id_customer','=','proposal.id_customer')
                                                ->leftJoin('truckingdetail','truckingdetail.id_truckingdetail','=','proposaldetail.id_truckingdetail')
                                                ->find($idproposaldetail);
        $biaya = Models\Biaya::Get();                                       
        $pelabuhan = Models\Pelabuhan::leftJoin('city', 'city.id_kota', '=', 'pelabuhan.id_kota')->orderBy('city.nama_kota','asc')->get();
        $pelayaran = Models\Pelayaran::Get();
        $dooring = Models\Dooring::Get();
        $sector = Models\Sector::Get();
        $city = Models\City::Get();
        $consignee = Models\Consignee::Get();
        $biayalain = Models\BiayaLain::where('id_proposaldetail',$idproposaldetail)->whereNull('deleted_at')->get();
        return view('transaction.detailcustomerproposaladd',compact('mode','from','proposaldetail','consignee','pelabuhan','city','pelayaran','dooring','sector','biaya','biayalain'));
    }

    public function updateDetail(Request $request, $idproposaldetail ){
        // dd($idproposal);
        Validator::make($request->all(), [
            'txtValidFrom' => 'required',
            // 'txtValidTo' => 'required',
            'cboKotaAsal' => 'required|not_in:0',
            'cboKotaTujuan' => 'required|not_in:0',
            'cboPortAsal' => 'required|not_in:0',
            'cboPortTujuan' => 'required|not_in:0',
            'rdoType' => 'required',
            'cboProposalType' => 'required|not_in:0',
            'cboJenisBarang' => 'required|not_in:0',
            'txtAlamatAsal' => 'required',
            'txtAlamatTujuan' => 'required',
            'txtKeterangan' => 'required',
            'cboPelayaran' => 'required|not_in:0',
            'txtHargaPelayaran' => 'required',
            'txtHargaExtraPelayaran' => 'required',
            'txtHargaTrucking' => 'required',
            'cboSector' => 'required|not_in:0',
            'cboTrucking' => 'required|not_in:0',
            'cboDooring' => 'required|not_in:0',
            'txtHargaDooring' => 'required',
            'txtHargaExtraDooring' => 'required',
            'txtHargaTotal' => 'required',

        ],[
            'rdoType.required' => 'tipe proposal harus dipilih',
            'cboPortAsal.not_in' => 'pelabuhan asal harus dipilih',
            'cboPortTujuan.not_in' => 'pelabuhan tujuan harus dipilih',
            'cboProposalType.not_in' => 'jenis proposal harus dipilih',
            'cboJenisBarang.not_in' => 'jenis barang harus dipilih',
            'cboPelayaran.not_in' => 'pelayaran harus dipilih',
            'cboSector.not_in' => 'sektor harus dipilih',
            'cboTrucking.not_in' => 'trucking harus dipilih',
            'cboDooring.not_in' => 'dooring harus dipilih',
            ])->validate();

            
        $idbiayalain = $request->input('txtBiayaLainHidden');
        $namabiayalain = $request->input('txtNamaBiayaLain');
        $nominalbiayalain = $request->input('txtNominalBiayaLain');
        
        $i = 0;

        DB::beginTransaction();
        try {
            $typeproposal = $request->input('rdoType');

            $truckingdetail = explode(';',$request->input('cboSector'));
            if ($truckingdetail[0]==0) {
                throw new \Exception("Sector Trucking Harus Dipilih");
            }

            if($request->input('txtValidFrom')==$request->input('txtValidTo')){
                throw new \Exception("Periode Awal Tidak Boleh Sama Dengan Periode Akhir");
            }

            $cekvalidity = DB::select("select CASE WHEN DATE(SYSDATE())<=STR_TO_DATE(?,'%d/%m/%Y') THEN 1 ELSE 0 END AS cekvalid FROM DUAL",[Carbon::parse($request->input('txtValidTo'))->format('d/m/Y')]);

            $cekvalidity2 = DB::select("select CASE WHEN STR_TO_DATE(?,'%d/%m/%Y')<=STR_TO_DATE(?,'%d/%m/%Y') THEN 1 ELSE 0 END AS cekvalid FROM DUAL",[Carbon::parse($request->input('txtValidFrom'))->format('d/m/Y'),Carbon::parse($request->input('txtValidTo'))->format('d/m/Y')]);
            // dd($cekvalidity[0]->cekvalid . " - ". $cekvalidity2[0]->cekvalid);

            if ($cekvalidity[0]->cekvalid==0) {
                throw new \Exception("Valid To Harus lebih Besar atau sama dengan hari ini");
            }
            
            if ($cekvalidity2[0]->cekvalid==0) {
                throw new \Exception("Valid To Harus lebih Besar atau sama dengan valid from");
            }

            
            if($request->input('cboKotaAsal')==$request->input('cboKotaTujuan')){
                throw new \Exception("Kota Asal Tidak Boleh Sama Dengan Kota Tujuan");
            }
            if($request->input('cboPortAsal')==$request->input('cboPortTujuan')){
                throw new \Exception("Pelabuhan Asal Tidak Boleh Sama Dengan Pelabuhan Tujuan");
            }



            $proposaldetail = Models\ProposalDetail::find($idproposaldetail);
            // $proposaldetail->id_proposal = $idproposal;
            $proposaldetail->valid_from = Carbon::parse($request->input('txtValidFrom'))->format('Y/m/d H:i:s');
            if($request->input('txtValidTo')!=""||$request->input('txtValidTo')!=null){
                $proposaldetail->valid_to = Carbon::parse($request->input('txtValidTo'))->format('Y/m/d H:i:s');
            }
            $proposaldetail->id_kota_asal = $request->input('cboKotaAsal');
            $proposaldetail->id_kota_tujuan = $request->input('cboKotaTujuan');
            $proposaldetail->id_pelabuhan_asal = $request->input('cboPortAsal');
            $proposaldetail->id_pelabuhan_tujuan = $request->input('cboPortTujuan');
            if($request->input('cboConsignee')!='0'||$request->input('cboConsignee')!=0){
                $proposaldetail->id_consignee = $request->input('cboConsignee');
            }
            $proposaldetail->type_proposal = $typeproposal;
            $proposaldetail->jenis_proposal = $request->input('cboProposalType');
            $proposaldetail->jenis_barang = $request->input('cboJenisBarang');
            $proposaldetail->alamat_asal = $request->input('txtAlamatAsal');
            $proposaldetail->alamat_tujuan = $request->input('txtAlamatTujuan');
            // $proposaldetail->keteranganpengiriman = $request->input('txtKeterangan');
            $proposaldetail->lampiran = $request->input('txtLampiran');
            if($typeproposal=="CONTAINER"){
                $proposaldetail->freight_size = $request->input('rdoFtsize');
                $proposaldetail->nominal_optsurabaya = str_replace( ',', '',$request->input('txtHargaOptSby'));
                $proposaldetail->nominal_thcp = str_replace( ',', '',$request->input('txtHargaTHCP'));
                $proposaldetail->nominal_thcd = str_replace( ',', '',$request->input('txtHargaTHCD'));
                $proposaldetail->nominal_surcharge = str_replace( ',', '',$request->input('txtHargaSurcharge'));
                $proposaldetail->nominal_optdooring = str_replace( ',', '',$request->input('txtHargaOptDooring'));
                $proposaldetail->nominal_optmjp = str_replace( ',', '',$request->input('txtHargaOptMjp'));
            }else{
                $proposaldetail->lcl_size = $request->input('rdolclsize');
                if($request->input('rdolclsize')=="M3"){
                    $proposaldetail->nominal_m3 = str_replace( ',', '',$request->input('txtHargam3'));
                }else{
                    $proposaldetail->nominal_ton = str_replace( ',', '',$request->input('txtHargaton'));
                }
            }
            $proposaldetail->id_pelayaran = $request->input('cboPelayaran');
            $proposaldetail->nominal_pelayaran = str_replace( ',', '',$request->input('txtHargaPelayaran'));
            $proposaldetail->nominal_extrapelayaran = str_replace( ',', '',$request->input('txtHargaExtraPelayaran'));
            $proposaldetail->keterangan_extrapelayaran = str_replace( ',', '',$request->input('txtExtraPelayaran'));
            $proposaldetail->notes_pelayaran = $request->input('txtNotesPelayaran');
            $proposaldetail->id_truckingdetail = $truckingdetail[0];
            $proposaldetail->nominal_trucking = str_replace( ',', '',$request->input('txtHargaTrucking'));
            $proposaldetail->id_dooring = $request->input('cboDooring');
            $proposaldetail->nominal_dooring = str_replace( ',', '',$request->input('txtHargaDooring'));
            $proposaldetail->nominal_extradooring = str_replace( ',', '',$request->input('txtHargaExtraDooring'));
            $proposaldetail->nominal_total = $request->input('txtHargaTotal');
            if($proposaldetail->save()){
                if($nominalbiayalain!=null){
                    foreach($nominalbiayalain as $item) {
                        // dd($idbiayalain[$i]);
                        $cekbiayalain = DB::select('select count(*) as ada from biayalain where id_biayalain='.$idbiayalain[$i].' ');
                        // dd($cekbiayalain[0]->ada);
                        if($cekbiayalain[0]->ada>0){
                            $biayalain = Models\BiayaLain::find($idbiayalain[$i]);
                            $biayalain->id_proposaldetail = $proposaldetail->id_proposaldetail;
                            $biayalain->namabiayalain = str_replace( ',', '',$namabiayalain[$i]);
                            $biayalain->nominalbiayalain = str_replace( ',', '',$item);
                            $biayalain->save();
                        }else{
                            $biayalain = new Models\BiayaLain;
                            $biayalain->id_proposaldetail = $proposaldetail->id_proposaldetail;
                            $biayalain->namabiayalain = str_replace( ',', '',$namabiayalain[$i]);
                            $biayalain->nominalbiayalain = str_replace( ',', '',$item);
                            $biayalain->save();
                        }
                        
                        $i++;
                    }
                }
            }

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->back()
        ->with('success',__('Detail Updated'));
    }

    public function deletebiayalain($idbiayalain, Request $request)
    {   
        DB::beginTransaction();
        try {
            $biayalain = Models\BiayaLain::find($idbiayalain);
            $biayalain->delete();
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Biaya Dihapus.'
        ]);
    }

    public function deleteDetail($idProposalDetail, Request $request)
    {   
        DB::beginTransaction();
        try {
            $proposaldetail = Models\ProposalDetail::find($idProposalDetail);
            $proposaldetail->delete();
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Detail Proposal Dihapus.'
        ]);
    }

    public function completeProposal($idProposal)
    {   
        DB::beginTransaction();
        try {
            $proposal = Models\Proposal::find($idProposal);
            $proposal->status = 2;
            $proposal->save();
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Proposal Completed!'
        ]);
    }

    public function detailview($idproposaldetail){
        $mode = 'VIEW';
        $from = 'EDITOR';
        $proposaldetail = Models\ProposalDetail::leftJoin('proposal','proposal.id_proposal','=','proposaldetail.id_proposal')
                                                ->leftJoin('customer','customer.id_customer','=','proposal.id_customer')
                                                ->find($idproposaldetail);
        $pelabuhan = Models\Pelabuhan::leftJoin('city', 'city.id_kota', '=', 'pelabuhan.id_kota')->orderBy('city.nama_kota','asc')->get();
        $biaya = Models\Biaya::Get();     
        $pelayaran = Models\Pelayaran::Get();
        $dooring = Models\Dooring::Get();
        $trucking = Models\Trucking::Get();
        $sector = Models\Sector::Get();
        $city = Models\City::Get();
        $consignee = Models\Consignee::Get();
        $biayalain = Models\BiayaLain::where('id_proposaldetail',$idproposaldetail)->whereNull('deleted_at')->get();
        return view('transaction.detailcustomerproposaladd',compact('mode','from','proposaldetail','consignee','pelabuhan','city','pelayaran','dooring','trucking','biaya','sector','biayalain'));
    }

    public function copyproposal($idproposal){
        $proposal_old = Models\Proposal::find($idproposal);
        // $proposaldetail_old = Models\ProposalDetail::where('id_proposal',$idproposal)
        //                                             ->whereNotNull('deleted_at');
        $proposaldetail_old = DB::select('select * from proposaldetail where deleted_at is null and id_proposal=?',[$idproposal]);
        
        
        try{

            $year = DB::select('select distinct SUBSTRING(EXTRACT(YEAR FROM CURRENT_DATE),1,4) as year from dual');
        // dd($year[0]->year);
        $month = DB::select('select distinct SUBSTRING(EXTRACT(MONTH FROM CURRENT_DATE),1,2) as month from dual');
        // dd($month[0]->month);
        $shortname = DB::select("select IFNULL(name_short,'-') as name_short from customer where id_customer=?",[$proposal_old->id_customer]);
        //dd($shortname[0]->name_short);
        

        if($proposal_old->tipe_proposal=='CONTRACT'){
            $lastnomorurut = DB::select("select IFNULL(MAX(no_urut),0)+1 as nourut from proposal where tipe_proposal='CONTRACT' and id_customer=?",[$proposal_old->id_customer]);
            $noproposal = $year[0]->year.'/'.$month[0]->month.'/'.$shortname[0]->name_short.'/'.str_pad($lastnomorurut[0]->nourut,4,"0",STR_PAD_LEFT).'/K/MJP/SDA';
        }else{
            $lastnomorurut = DB::select("select IFNULL(MAX(no_urut),0)+1 as nourut from proposal where tipe_proposal='NON-CONTRACT' and  id_customer=?",[$proposal_old->id_customer]);
            $noproposal = $year[0]->year.'/'.$month[0]->month.'/'.$shortname[0]->name_short.'/'.str_pad($lastnomorurut[0]->nourut,4,"0",STR_PAD_LEFT).'/NK/MJP/SDA';
        }
            
           
            // dd($noproposal);

            DB::beginTransaction();

            $proposal = new Models\Proposal;
            $proposal->no_proposal = $noproposal;
            $proposal->id_customer = $proposal_old->id_customer;
            $proposal->no_urut = $lastnomorurut[0]->nourut;
            $proposal->customer_credit = $proposal_old->customer_credit;
            $proposal->tipe_proposal = $proposal_old->tipe_proposal;
            $proposal->termcondition =  $proposal_old->termcondition;
            $proposal->status=1;
            if($proposal->save()){
                
                $idproposalnew = DB::select('select id_proposal from proposal where no_proposal=?',[$noproposal]);
                
                foreach($proposaldetail_old as $detail){
                    
                    $proposaldetail = new Models\ProposalDetail;
                    $proposaldetail->id_proposal = $idproposalnew[0]->id_proposal;
                    $proposaldetail->valid_from = Carbon::parse($detail->valid_from)->format('Y/m/d H:i:s');
                    if($detail->valid_to!=""||$detail->valid_to!=null){
                        $proposaldetail->valid_to = Carbon::parse($detail->valid_to)->format('Y/m/d H:i:s');
                    }
                    if($detail->id_consignee!=""||$detail->id_consignee!=null){
                        $proposaldetail->valid_to = $detail->id_consignee;
                    }
                    $proposaldetail->id_kota_asal = $detail->id_kota_asal;
                    $proposaldetail->id_kota_tujuan = $detail->id_kota_tujuan;
                    $proposaldetail->id_pelabuhan_asal = $detail->id_pelabuhan_asal;
                    $proposaldetail->id_pelabuhan_tujuan = $detail->id_pelabuhan_tujuan;
                    $proposaldetail->type_proposal = $detail->type_proposal;
                    $proposaldetail->jenis_proposal = $detail->jenis_proposal;
                    $proposaldetail->jenis_barang = $detail->jenis_barang;
                    $proposaldetail->alamat_asal = $detail->alamat_asal;
                    $proposaldetail->alamat_tujuan = $detail->alamat_tujuan;
                    // $proposaldetail->keteranganpengiriman = $detail->keteranganpengiriman;
                    $proposaldetail->lampiran = $detail->lampiran;
                    if($detail->type_proposal=="CONTAINER"){
                        $proposaldetail->freight_size = $detail->freight_size;
                        $proposaldetail->nominal_optsurabaya = $detail->nominal_optsurabaya;
                        $proposaldetail->nominal_thcp = $detail->nominal_thcp;
                        $proposaldetail->nominal_thcd = $detail->nominal_thcd;
                        $proposaldetail->nominal_surcharge = $detail->nominal_surcharge;
                        $proposaldetail->nominal_optdooring = $detail->nominal_optdooring;
                        $proposaldetail->nominal_optmjp = $detail->nominal_optmjp;
                    }else{
                        $proposaldetail->lcl_size = $detail->lcl_size;
                        if($detail->lcl_size=="M3"){
                            $proposaldetail->nominal_m3 = $detail->nominal_m3;
                        }else{
                            $proposaldetail->nominal_ton = $detail->nominal_ton;
                        }
                    }
                    $proposaldetail->id_pelayaran = $detail->id_pelayaran;
                    $proposaldetail->nominal_pelayaran = $detail->nominal_pelayaran;
                    $proposaldetail->nominal_extrapelayaran = $detail->nominal_extrapelayaran;
                    $proposaldetail->keterangan_extrapelayaran = $detail->keterangan_extrapelayaran;
                    $proposaldetail->notes_pelayaran = $detail->notes_pelayaran;
                    $proposaldetail->id_truckingdetail = $detail->id_truckingdetail;
                    $proposaldetail->nominal_trucking = $detail->nominal_trucking;
                    $proposaldetail->id_dooring = $detail->id_dooring;
                    $proposaldetail->nominal_dooring = $detail->nominal_dooring;
                    $proposaldetail->nominal_extradooring = $detail->nominal_extradooring;
                    $proposaldetail->nominal_total = $detail->nominal_total;
                    $proposaldetail->status = 1;
                    // dd($proposaldetail->getAttributes());
                    if($proposaldetail->save()){
                        
                        $biayalain_old = DB::select('select * from biayalain where deleted_at is null and id_proposaldetail=?',[$detail->id_proposaldetail]);
                            if($biayalain_old!=null){
                                foreach($biayalain_old as $item) {
                                    $biayalain = new Models\BiayaLain;
                                    $biayalain->id_proposaldetail = $proposaldetail->id_proposaldetail;
                                    $biayalain->namabiayalain = $item->namabiayalain;
                                    $biayalain->nominalbiayalain = $item->nominalbiayalain;
                                    $biayalain->save();
                                }
                            }
                    }else{
                        throw new \Exception("Detail Proposal gagal disimpan");
                    }
                    
                }
            }else{
                throw new \Exception("Proposal gagal disimpan");
            }


        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage());
        }
        DB::commit();
        
        $idproposalbaru = DB::select('select id_proposal from proposal where no_proposal=?',[$noproposal]);
        
        return redirect()->route('transaction.customerproposal.edit',$idproposalbaru[0]->id_proposal)
            ->with('success',__('Data Saved'));
    }

    public function archiveproposal($idProposal)
    {   
        DB::beginTransaction();
        try {
            $proposal = Models\Proposal::find($idProposal);
            $proposal->status = 5;
            $proposal->archived_by = Auth::user()->username;
            $proposal->archived_at = Carbon::now();
            $proposal->save();
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Proposal Archived!'
        ]);
    }
}