<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models;
use Carbon\Carbon;
use Auth;

class invoicelclController extends Controller
{
    //
    public function index(){
   		return view('transaction.invoicelcl');
    }

    public function getData(Request $request){
        $data = Models\InvoiceLCL::leftJoin('trip','invoice_lcl.id_trip','trip.id_trip')
                            ->leftJoin('customer','invoice_lcl.id_customer','customer.id_customer');

        $data = $data->select([
            'invoice_lcl.*',
            'trip.kode_trip',
            'customer.nama_pt as nama_customer',

        ]);

        if ($request->input('search')) {
            $data = $data->where('trip.kode_trip','LIKE','%'.$request->input('search').'%')
                ->orWhere('invoice_lcl.no_invoice_lcl','LIKE','%'.$request->input('search').'%')
                ->orWhere('invoice_lcl.tgl_invoice_lcl','LIKE','%'.$request->input('search').'%')
                ->orWhere('invoice_lcl.created_at','LIKE','%'.$request->input('search').'%')
                ->orWhere('invoice_lcl.verified_at','LIKE','%'.$request->input('search').'%');
        }
        $count = $data->count();

        if ($request->input('sort')) {
            $data = $data->orderBy($request->input('sort'),$request->input('order'))
                ->orderBy('invoice_lcl.id_invoice_lcl',$request->input('order'));
            
        } else {
            $data = $data->orderBy('invoice_lcl.created_at','desc');
        }
        if($request->input('limit')) {
            $data = $data->take($request->input('limit'))
            ->skip($request->input('offset'));
        }

        $data = $data->get();
        
    
        return response()->json([
                'total' => $count,
                'rows' => $data
            ]);
    }

    public function getDataTrip(){
        $data = Models\SuratJalan::select('suratjalan.id_kodetrip','order.id_customer','trip.kode_trip','customer.nama_pt', DB::raw('count(suratjalan.id_kodetrip) as total'))
                            ->leftJoin('trip','suratjalan.id_kodetrip','trip.id_trip')
                            ->leftJoin('order','suratjalan.id_order','order.id_order')
                            ->leftJoin('customer','order.id_customer','customer.id_customer')
                            ->whereNull('suratjalan.deleted_at')
                            ->where('suratjalan.type_suratjalan','=','LCL')
                            ->groupBy('suratjalan.id_kodetrip','order.id_customer','trip.kode_trip','customer.nama_pt');

        $count = $data->count();


        $data = $data->get();
        
    
        return response()->json([
                'total' => $count,
                'rows' => $data
            ]);
    }
    
    public function add($idtrip,$idcustomer){
        $mode = 'ADD';
        $datainvoicelcl = Models\SuratJalan::select('suratjalan.id_suratjalan','suratjalan.no_suratjalan','proposal.no_proposal','suratjalan.keterangan_total', 'proposaldetail.nominal_total','invoicedetail_lcl.id_invoice_lcl',DB::raw('GROUP_CONCAT(bapb.no_bapb SEPARATOR ", ") AS no_bapb'))
                                         ->leftJoin('do','do.id_suratjalan','suratjalan.id_suratjalan')
                                         ->leftJoin('bapb','bapb.id_bapb','do.id_bapb')
                                         ->leftJoin('order','suratjalan.id_order','order.id_order')
                                         ->leftJoin('proposaldetail','proposaldetail.id_proposaldetail','order.id_proposaldetail')
                                         ->leftJoin('proposal','proposal.id_proposal','proposaldetail.id_proposal')
                                         ->leftJoin('invoicedetail_lcl','invoicedetail_lcl.id_suratjalan','suratjalan.id_suratjalan')
                                         ->where('suratjalan.id_kodetrip','=',$idtrip)
                                         ->where('order.id_customer','=',$idcustomer)
                                         ->where('suratjalan.type_suratjalan','=','LCL')
                                         ->whereNull('suratjalan.deleted_at')
                                         ->whereNull('do.deleted_at')
                                         ->whereNull('bapb.deleted_at')
                                         ->whereNull('order.deleted_at')
                                         ->whereNull('proposaldetail.deleted_at')
                                         ->where(function($q) {
                                            $q->whereNull('invoicedetail_lcl.id_invoice_lcl')
                                              ->orWhereNotNull('invoicedetail_lcl.deleted_at');
                                        })
                                         ->groupBy('suratjalan.id_suratjalan','suratjalan.no_suratjalan','proposal.no_proposal','suratjalan.keterangan_total', 'proposaldetail.nominal_total','invoicedetail_lcl.id_invoice_lcl')
                                         ->get();
        $customer = Models\Customer::find($idcustomer);
        $ppn = Models\ppn::find(1);
        $trip = Models\Trip::find($idtrip);
        return view('transaction.invoicelcladd',compact('mode','datainvoicelcl','customer','trip','ppn'));
    }

    public function create(Request $request){
        // dd($idproposal);
        $validator = Validator::make($request->all(), [
            'tgl_invoice_lcl'  => 'required',

        ],['tgl_invoice_lcl.required'=>'tanggal invoice harus diisi',        
        ])->validate();
        
        $year = DB::select('select distinct SUBSTRING(EXTRACT(YEAR FROM CURRENT_DATE),3,4) as year from dual');
        $month = DB::select('select MONTH(NOW()) as month');
        $ceknoinvoice = DB::select('select count(*) as ada from invoice_lcl where no_invoice_lcl like "KWILCL'.$year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).'%"');
        // dd($ceknoinvoice[0]->ada);
        if($ceknoinvoice[0]->ada>0){
            $noinvoice = DB::select('select max(no_invoice_lcl) as noinvoice from invoice_lcl where no_invoice_lcl like "KWILCL'.$year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).'%"');
            $nobaru = (int)(ltrim(substr($noinvoice[0]->noinvoice,10), '0'));
            $nobaru++;
            $noinvoicebaru = "KWILCL".$year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).str_pad($nobaru,4,"0",STR_PAD_LEFT);
        }else{
            $nobaru = 1;
            $noinvoicebaru = "KWILCL".$year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).str_pad($nobaru,4,"0",STR_PAD_LEFT);
        }
        // dd($noinvoicebaru);
        
        DB::beginTransaction();
        try {
            $invoicelcl = new Models\InvoiceLCL;
            $invoicelcl->no_invoice_lcl = $noinvoicebaru;
            $invoicelcl->id_trip = $request->input('id_trip');
            $invoicelcl->id_customer = $request->input('id_customer');
            $invoicelcl->tgl_invoice_lcl = Carbon::parse($request->input('tgl_invoice_lcl'))->format('Y/m/d H:i:s');
            $invoicelcl->tax = str_replace( ',', '',$request->input('tax'));
            $invoicelcl->include_tax = $request->input('include_tax');
            $invoicelcl->total = str_replace( ',', '',$request->input('total'));
            $invoicelcl->catatan = $request->input('catatan');
            $invoicelcl->status = 0;
            if($invoicelcl->save()){
                $detail = $request->input('detail');
                foreach ($detail as $item) {
                    $cekinvoicedetail = DB::select('select count(*) as ada from invoicedetail_lcl where deleted_at is not null and id_suratjalan='.$item['id_suratjalan'].' ');
                    if($cekinvoicedetail[0]->ada>0){
                        $idinvoicedetail = DB::select('select id_invoicedetail_lcl from invoicedetail_lcl where deleted_at is not null and id_suratjalan='.$item['id_suratjalan'].' ');
                        $invoicedetaillcl = Models\InvoiceDetailLCL::withTrashed()->find($idinvoicedetail[0]->id_invoicedetail_lcl);
                        $invoicedetaillcl->deleted_at = null;
                        $invoicedetaillcl->deleted_by = null;
                        $invoicedetaillcl->id_invoice_lcl = $invoicelcl->id_invoice_lcl;
                        $invoicedetaillcl->no_invoice_lcl = $invoicelcl->no_invoice_lcl;
                        $invoicedetaillcl->id_suratjalan = $item['id_suratjalan'];
                        $invoicedetaillcl->biaya = str_replace( ',', '',$item['biaya']);
                        if($item['biaya_tambahan_1']!=null || $item['biaya_tambahan_1']!=""){
                            $invoicedetaillcl->biaya_tambahan_1 = $item['biaya_tambahan_1'];
                            $invoicedetaillcl->nominal_biaya_tambahan_1 = str_replace( ',', '',$item['nominal_biaya_tambahan_1']);
                        }else{
                            $invoicedetaillcl->biaya_tambahan_1 = null;
                            $invoicedetaillcl->nominal_biaya_tambahan_1 = null;
                        }
                        if($item['biaya_tambahan_2']!=null || $item['biaya_tambahan_2']!=""){
                            $invoicedetaillcl->biaya_tambahan_2 = $item['biaya_tambahan_2'];
                            $invoicedetaillcl->nominal_biaya_tambahan_2 = str_replace( ',', '',$item['nominal_biaya_tambahan_2']);
                        }else{
                            $invoicedetaillcl->biaya_tambahan_2 = null;
                            $invoicedetaillcl->nominal_biaya_tambahan_2 =null;
                        }
                        if($item['biaya_tambahan_3']!=null || $item['biaya_tambahan_3']!=""){
                            $invoicedetaillcl->biaya_tambahan_3 = $item['biaya_tambahan_3'];
                            $invoicedetaillcl->nominal_biaya_tambahan_3 = str_replace( ',', '',$item['nominal_biaya_tambahan_3']);
                        }else{
                            $invoicedetaillcl->biaya_tambahan_3 = null;
                            $invoicedetaillcl->nominal_biaya_tambahan_3 = null;
                        }
                        $invoicedetaillcl->total = str_replace( ',', '',$item['total']);
                        $invoicedetaillcl->save();
                    }else{
                        $invoicedetaillcl = new Models\InvoiceDetailLCL;
                        $invoicedetaillcl->id_invoice_lcl = $invoicelcl->id_invoice_lcl;
                        $invoicedetaillcl->no_invoice_lcl = $invoicelcl->no_invoice_lcl;
                        $invoicedetaillcl->id_suratjalan = $item['id_suratjalan'];
                        $invoicedetaillcl->biaya = str_replace( ',', '',$item['biaya']);
                        if($item['biaya_tambahan_1']!=null || $item['biaya_tambahan_1']!=""){
                            $invoicedetaillcl->biaya_tambahan_1 = $item['biaya_tambahan_1'];
                            $invoicedetaillcl->nominal_biaya_tambahan_1 = str_replace( ',', '',$item['nominal_biaya_tambahan_1']);
                        }else{
                            $invoicedetaillcl->biaya_tambahan_1 = null;
                            $invoicedetaillcl->nominal_biaya_tambahan_1 = null;
                        }
                        if($item['biaya_tambahan_2']!=null || $item['biaya_tambahan_2']!=""){
                            $invoicedetaillcl->biaya_tambahan_2 = $item['biaya_tambahan_2'];
                            $invoicedetaillcl->nominal_biaya_tambahan_2 = str_replace( ',', '',$item['nominal_biaya_tambahan_2']);
                        }else{
                            $invoicedetaillcl->biaya_tambahan_2 = null;
                            $invoicedetaillcl->nominal_biaya_tambahan_2 =null;
                        }
                        if($item['biaya_tambahan_3']!=null || $item['biaya_tambahan_3']!=""){
                            $invoicedetaillcl->biaya_tambahan_3 = $item['biaya_tambahan_3'];
                            $invoicedetaillcl->nominal_biaya_tambahan_3 = str_replace( ',', '',$item['nominal_biaya_tambahan_3']);
                        }else{
                            $invoicedetaillcl->biaya_tambahan_3 = null;
                            $invoicedetaillcl->nominal_biaya_tambahan_3 = null;
                        }
                        $invoicedetaillcl->total = str_replace( ',', '',$item['total']);
                        $invoicedetaillcl->save();
                    }
                }
            }

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        $request->session()->flash('success', 'Invoice LCL Tersimpan!');
        return response()->json([
            'status' => 'success',
            'description' => 'Invoice LCL Tersimpan!'
        ]);
    }

    public function verify($idinvoicelcl)
    {   
        DB::beginTransaction();
        try {
            $invoicelcl = Models\InvoiceLCL::find($idinvoicelcl);
            $invoicelcl->status = 1;
            $invoicelcl->verified_by = Auth::user()->username;
            $invoicelcl->verified_at = Carbon::now();
            $invoicelcl->save();
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Invoice Verified!'
        ]);
    }

    public function delete($idinvoicelcl, Request $request)
    {   
        DB::beginTransaction();
        try {
            $invoicelcl = Models\InvoiceLCL::find($idinvoicelcl);
            $invoicelcl->delete();
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Invoice Deleted.'
        ]);
    }


    public function edit($idinvoicelcl,$idtrip,$idcustomer){
        $mode = 'EDIT';
        $invoicelcl = Models\InvoiceLCL::find($idinvoicelcl);
        $datainvoicelclmasuk = Models\SuratJalan::select('suratjalan.id_suratjalan','suratjalan.no_suratjalan','proposal.no_proposal','suratjalan.keterangan_total', 'proposaldetail.nominal_total','invoicedetail_lcl.id_invoice_lcl','invoicedetail_lcl.id_invoicedetail_lcl','invoicedetail_lcl.biaya','invoicedetail_lcl.biaya_tambahan_1','invoicedetail_lcl.nominal_biaya_tambahan_1','invoicedetail_lcl.biaya_tambahan_2','invoicedetail_lcl.nominal_biaya_tambahan_2','invoicedetail_lcl.biaya_tambahan_3','invoicedetail_lcl.nominal_biaya_tambahan_3','invoicedetail_lcl.total','invoicedetail_lcl.deleted_at',DB::raw('GROUP_CONCAT(bapb.no_bapb SEPARATOR ", ") AS no_bapb'))
                                         ->leftJoin('do','do.id_suratjalan','suratjalan.id_suratjalan')
                                         ->leftJoin('bapb','bapb.id_bapb','do.id_bapb')
                                         ->leftJoin('order','suratjalan.id_order','order.id_order')
                                         ->leftJoin('proposaldetail','proposaldetail.id_proposaldetail','order.id_proposaldetail')
                                         ->leftJoin('proposal','proposal.id_proposal','proposaldetail.id_proposal')
                                         ->leftJoin('invoicedetail_lcl','invoicedetail_lcl.id_suratjalan','suratjalan.id_suratjalan')
                                         ->where('suratjalan.id_kodetrip','=',$idtrip)
                                         ->where('order.id_customer','=',$idcustomer)
                                         ->where('suratjalan.type_suratjalan','=','LCL')
                                         ->whereNull('suratjalan.deleted_at')
                                         ->whereNull('do.deleted_at')
                                         ->whereNull('bapb.deleted_at')
                                         ->whereNull('order.deleted_at')
                                         ->whereNull('proposaldetail.deleted_at')
                                         ->whereNull('invoicedetail_lcl.deleted_at')
                                        //  ->where(function($q) {
                                        //     $q->whereNull('invoicedetail.id_invoice')
                                        //       ->orWhereNotNull('invoicedetail.deleted_at');
                                        // })
                                        ->groupBy('suratjalan.id_suratjalan','suratjalan.no_suratjalan','proposal.no_proposal','suratjalan.keterangan_total', 'proposaldetail.nominal_total','invoicedetail_lcl.id_invoice_lcl','invoicedetail_lcl.id_invoicedetail_lcl','invoicedetail_lcl.biaya','invoicedetail_lcl.biaya_tambahan_1','invoicedetail_lcl.nominal_biaya_tambahan_1','invoicedetail_lcl.biaya_tambahan_2','invoicedetail_lcl.nominal_biaya_tambahan_2','invoicedetail_lcl.biaya_tambahan_3','invoicedetail_lcl.nominal_biaya_tambahan_3','invoicedetail_lcl.total','invoicedetail_lcl.deleted_at');
        $datainvoicelcl = Models\SuratJalan::select('suratjalan.id_suratjalan','suratjalan.no_suratjalan','proposal.no_proposal','suratjalan.keterangan_total', 'proposaldetail.nominal_total','invoicedetail_lcl.id_invoice_lcl','invoicedetail_lcl.id_invoicedetail_lcl','invoicedetail_lcl.biaya','invoicedetail_lcl.biaya_tambahan_1','invoicedetail_lcl.nominal_biaya_tambahan_1','invoicedetail_lcl.biaya_tambahan_2','invoicedetail_lcl.nominal_biaya_tambahan_2','invoicedetail_lcl.biaya_tambahan_3','invoicedetail_lcl.nominal_biaya_tambahan_3','invoicedetail_lcl.total','invoicedetail_lcl.deleted_at',DB::raw('GROUP_CONCAT(bapb.no_bapb SEPARATOR ", ") AS no_bapb'))
                                         ->leftJoin('do','do.id_suratjalan','suratjalan.id_suratjalan')
                                         ->leftJoin('bapb','bapb.id_bapb','do.id_bapb')
                                         ->leftJoin('order','suratjalan.id_order','order.id_order')
                                         ->leftJoin('proposaldetail','proposaldetail.id_proposaldetail','order.id_proposaldetail')
                                         ->leftJoin('proposal','proposal.id_proposal','proposaldetail.id_proposal')
                                         ->leftJoin('invoicedetail_lcl','invoicedetail_lcl.id_suratjalan','suratjalan.id_suratjalan')
                                         ->where('suratjalan.id_kodetrip','=',$idtrip)
                                         ->where('order.id_customer','=',$idcustomer)
                                         ->where('suratjalan.type_suratjalan','=','LCL')
                                         ->whereNull('suratjalan.deleted_at')
                                         ->whereNull('do.deleted_at')
                                         ->whereNull('bapb.deleted_at')
                                         ->whereNull('order.deleted_at')
                                         ->whereNull('proposaldetail.deleted_at')
                                         ->where(function($q) {
                                            $q->whereNull('invoicedetail_lcl.id_invoice_lcl')
                                              ->orWhereNotNull('invoicedetail_lcl.deleted_at');
                                        })
                                         ->groupBy('suratjalan.id_suratjalan','suratjalan.no_suratjalan','proposal.no_proposal','suratjalan.keterangan_total', 'proposaldetail.nominal_total','invoicedetail_lcl.id_invoice_lcl','invoicedetail_lcl.id_invoicedetail_lcl','invoicedetail_lcl.biaya','invoicedetail_lcl.biaya_tambahan_1','invoicedetail_lcl.nominal_biaya_tambahan_1','invoicedetail_lcl.biaya_tambahan_2','invoicedetail_lcl.nominal_biaya_tambahan_2','invoicedetail_lcl.biaya_tambahan_3','invoicedetail_lcl.nominal_biaya_tambahan_3','invoicedetail_lcl.total','invoicedetail_lcl.deleted_at')
                                         ->union($datainvoicelclmasuk)
                                         ->get();
        $customer = Models\Customer::find($idcustomer);
        $ppn = Models\ppn::find(1);
        $trip = Models\Trip::find($idtrip);
        return view('transaction.invoicelcladd',compact('mode','invoicelcl','datainvoicelcl','customer','trip','ppn'));
    }

    public function update(Request $request,$idinvoicelcl){
        $validator = Validator::make($request->all(), [
            'tgl_invoice_lcl'  => 'required',

        ],['tgl_invoice_lcl.required'=>'tanggal invoice harus diisi',        
        ])->validate();
        
        DB::beginTransaction();
        try {
            $invoicelcl = Models\InvoiceLCL::find($idinvoicelcl);
            $invoicelcl->tgl_invoice_lcl = Carbon::parse($request->input('tgl_invoice_lcl'))->format('Y/m/d H:i:s');
            $invoicelcl->tax = str_replace( ',', '',$request->input('tax'));
            $invoicelcl->include_tax = $request->input('include_tax');
            $invoicelcl->total = str_replace( ',', '',$request->input('total'));
            $invoicelcl->catatan = $request->input('catatan');
            if($invoicelcl->save()){
                $detail = $request->input('detail');
                foreach ($detail as $item) {
                    if($item['id_invoicedetail_lcl']!='0'){
                        $invoicedetaillcl = Models\InvoiceDetailLCL::find($item['id_invoicedetail_lcl']);
                        $invoicedetaillcl->biaya = str_replace( ',', '',$item['biaya']);
                        if($item['biaya_tambahan_1']!=null || $item['biaya_tambahan_1']!=""){
                            $invoicedetaillcl->biaya_tambahan_1 = $item['biaya_tambahan_1'];
                            $invoicedetaillcl->nominal_biaya_tambahan_1 = str_replace( ',', '',$item['nominal_biaya_tambahan_1']);
                        }else{
                            $invoicedetaillcl->biaya_tambahan_1 = null;
                            $invoicedetaillcl->nominal_biaya_tambahan_1 = null;
                        }
                        if($item['biaya_tambahan_2']!=null || $item['biaya_tambahan_2']!=""){
                            $invoicedetaillcl->biaya_tambahan_2 = $item['biaya_tambahan_2'];
                            $invoicedetaillcl->nominal_biaya_tambahan_2 = str_replace( ',', '',$item['nominal_biaya_tambahan_2']);
                        }else{
                            $invoicedetaillcl->biaya_tambahan_2 = null;
                            $invoicedetaillcl->nominal_biaya_tambahan_2 =null;
                        }
                        if($item['biaya_tambahan_3']!=null || $item['biaya_tambahan_3']!=""){
                            $invoicedetaillcl->biaya_tambahan_3 = $item['biaya_tambahan_3'];
                            $invoicedetaillcl->nominal_biaya_tambahan_3 = str_replace( ',', '',$item['nominal_biaya_tambahan_3']);
                        }else{
                            $invoicedetaillcl->biaya_tambahan_3 = null;
                            $invoicedetaillcl->nominal_biaya_tambahan_3 = null;
                        }
                        $invoicedetaillcl->total = str_replace( ',', '',$item['total']);
                        $invoicedetaillcl->save();
                    }else{
                        $cekinvoicedetail = DB::select('select count(*) as ada from invoicedetail_lcl where deleted_at is not null and id_suratjalan='.$item['id_suratjalan'].' ');
                        if($cekinvoicedetail[0]->ada>0){
                            $idinvoicedetail = DB::select('select id_invoicedetail_lcl from invoicedetail_lcl where deleted_at is not null and id_suratjalan='.$item['id_suratjalan'].' ');
                            $invoicedetaillcl = Models\InvoiceDetailLCL::withTrashed()->find($idinvoicedetail[0]->id_invoicedetail_lcl);
                            $invoicedetaillcl->deleted_at = null;
                            $invoicedetaillcl->deleted_by = null;
                            $invoicedetaillcl->id_invoice_lcl = $invoicelcl->id_invoice_lcl;
                            $invoicedetaillcl->no_invoice_lcl = $invoicelcl->no_invoice_lcl;
                            $invoicedetaillcl->id_suratjalan = $item['id_suratjalan'];
                            $invoicedetaillcl->biaya = str_replace( ',', '',$item['biaya']);
                            if($item['biaya_tambahan_1']!=null || $item['biaya_tambahan_1']!=""){
                                $invoicedetaillcl->biaya_tambahan_1 = $item['biaya_tambahan_1'];
                                $invoicedetaillcl->nominal_biaya_tambahan_1 = str_replace( ',', '',$item['nominal_biaya_tambahan_1']);
                            }else{
                                $invoicedetaillcl->biaya_tambahan_1 = null;
                                $invoicedetaillcl->nominal_biaya_tambahan_1 = null;
                            }
                            if($item['biaya_tambahan_2']!=null || $item['biaya_tambahan_2']!=""){
                                $invoicedetaillcl->biaya_tambahan_2 = $item['biaya_tambahan_2'];
                                $invoicedetaillcl->nominal_biaya_tambahan_2 = str_replace( ',', '',$item['nominal_biaya_tambahan_2']);
                            }else{
                                $invoicedetaillcl->biaya_tambahan_2 = null;
                                $invoicedetaillcl->nominal_biaya_tambahan_2 =null;
                            }
                            if($item['biaya_tambahan_3']!=null || $item['biaya_tambahan_3']!=""){
                                $invoicedetaillcl->biaya_tambahan_3 = $item['biaya_tambahan_3'];
                                $invoicedetaillcl->nominal_biaya_tambahan_3 = str_replace( ',', '',$item['nominal_biaya_tambahan_3']);
                            }else{
                                $invoicedetaillcl->biaya_tambahan_3 = null;
                                $invoicedetaillcl->nominal_biaya_tambahan_3 = null;
                            }
                            $invoicedetaillcl->total = str_replace( ',', '',$item['total']);
                            $invoicedetaillcl->save();
                        }else{
                            $invoicedetaillcl = new Models\InvoiceDetailLCL;
                            $invoicedetaillcl->id_invoice_lcl = $invoicelcl->id_invoice_lcl;
                            $invoicedetaillcl->no_invoice_lcl = $invoicelcl->no_invoice_lcl;
                            $invoicedetaillcl->id_suratjalan = $item['id_suratjalan'];
                            $invoicedetaillcl->biaya = str_replace( ',', '',$item['biaya']);
                            if($item['biaya_tambahan_1']!=null || $item['biaya_tambahan_1']!=""){
                                $invoicedetaillcl->biaya_tambahan_1 = $item['biaya_tambahan_1'];
                                $invoicedetaillcl->nominal_biaya_tambahan_1 = str_replace( ',', '',$item['nominal_biaya_tambahan_1']);
                            }else{
                                $invoicedetaillcl->biaya_tambahan_1 = null;
                                $invoicedetaillcl->nominal_biaya_tambahan_1 = null;
                            }
                            if($item['biaya_tambahan_2']!=null || $item['biaya_tambahan_2']!=""){
                                $invoicedetaillcl->biaya_tambahan_2 = $item['biaya_tambahan_2'];
                                $invoicedetaillcl->nominal_biaya_tambahan_2 = str_replace( ',', '',$item['nominal_biaya_tambahan_2']);
                            }else{
                                $invoicedetaillcl->biaya_tambahan_2 = null;
                                $invoicedetaillcl->nominal_biaya_tambahan_2 =null;
                            }
                            if($item['biaya_tambahan_3']!=null || $item['biaya_tambahan_3']!=""){
                                $invoicedetaillcl->biaya_tambahan_3 = $item['biaya_tambahan_3'];
                                $invoicedetaillcl->nominal_biaya_tambahan_3 = str_replace( ',', '',$item['nominal_biaya_tambahan_3']);
                            }else{
                                $invoicedetaillcl->biaya_tambahan_3 = null;
                                $invoicedetaillcl->nominal_biaya_tambahan_3 = null;
                            }
                            $invoicedetaillcl->total = str_replace( ',', '',$item['total']);
                            $invoicedetaillcl->save();
                        }
                    }
                    
                }
            }

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        $request->session()->flash('success', 'Invoice Terupdate!');
        return response()->json([
            'status' => 'success',
            'description' => 'Invoice Terupdate!'
        ]);
    }

    public function deleteCont(Request $request,$iddetailinvoicelcl){
        DB::beginTransaction();
        try {
            $invoicedetaillcl = Models\InvoiceDetailLCL::find($iddetailinvoicelcl);
            $invoicedetaillcl->delete();

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Surat Jalan Dikeluarkan!'
        ]);
    }

    public function view($idinvoicelcl,$idtrip,$idcustomer){
        $mode = 'VIEW';
        $invoicelcl = Models\InvoiceLCL::find($idinvoicelcl);
        $datainvoicelcl = Models\SuratJalan::select('suratjalan.id_suratjalan','suratjalan.no_suratjalan','proposal.no_proposal','suratjalan.keterangan_total', 'proposaldetail.nominal_total','invoicedetail_lcl.id_invoice_lcl','invoicedetail_lcl.id_invoicedetail_lcl','invoicedetail_lcl.biaya','invoicedetail_lcl.biaya_tambahan_1','invoicedetail_lcl.nominal_biaya_tambahan_1','invoicedetail_lcl.biaya_tambahan_2','invoicedetail_lcl.nominal_biaya_tambahan_2','invoicedetail_lcl.biaya_tambahan_3','invoicedetail_lcl.nominal_biaya_tambahan_3','invoicedetail_lcl.total','invoicedetail_lcl.deleted_at',DB::raw('GROUP_CONCAT(bapb.no_bapb SEPARATOR ", ") AS no_bapb'))
                                         ->leftJoin('do','do.id_suratjalan','suratjalan.id_suratjalan')
                                         ->leftJoin('bapb','bapb.id_bapb','do.id_bapb')
                                         ->leftJoin('order','suratjalan.id_order','order.id_order')
                                         ->leftJoin('proposaldetail','proposaldetail.id_proposaldetail','order.id_proposaldetail')
                                         ->leftJoin('proposal','proposal.id_proposal','proposaldetail.id_proposal')
                                         ->leftJoin('invoicedetail_lcl','invoicedetail_lcl.id_suratjalan','suratjalan.id_suratjalan')
                                         ->where('suratjalan.id_kodetrip','=',$idtrip)
                                         ->where('order.id_customer','=',$idcustomer)
                                         ->whereNull('suratjalan.deleted_at')
                                         ->whereNull('do.deleted_at')
                                         ->whereNull('bapb.deleted_at')
                                         ->whereNull('order.deleted_at')
                                         ->whereNull('proposaldetail.deleted_at')
                                         ->where('invoicedetail_lcl.id_invoice_lcl','=',$idinvoicelcl)
                                         ->whereNull('invoicedetail_lcl.deleted_at')
                                        //  ->where(function($q) {
                                        //     $q->whereNull('invoicedetail.id_invoice')
                                        //       ->orWhereNotNull('invoicedetail.deleted_at');
                                        // })
                                         ->groupBy('suratjalan.id_suratjalan','suratjalan.no_suratjalan','proposal.no_proposal','suratjalan.keterangan_total', 'proposaldetail.nominal_total','invoicedetail_lcl.id_invoice_lcl','invoicedetail_lcl.id_invoicedetail_lcl','invoicedetail_lcl.biaya','invoicedetail_lcl.biaya_tambahan_1','invoicedetail_lcl.nominal_biaya_tambahan_1','invoicedetail_lcl.biaya_tambahan_2','invoicedetail_lcl.nominal_biaya_tambahan_2','invoicedetail_lcl.biaya_tambahan_3','invoicedetail_lcl.nominal_biaya_tambahan_3','invoicedetail_lcl.total','invoicedetail_lcl.deleted_at')
                                         ->get();
        $customer = Models\Customer::find($idcustomer);
        $ppn = Models\ppn::find(1);
        $trip = Models\Trip::find($idtrip);
        return view('transaction.invoicelcladd',compact('mode','invoicelcl','datainvoicelcl','customer','trip','ppn'));
    }
}
