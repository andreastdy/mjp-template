<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models;
use Carbon\Carbon;
use Auth;

class pelunasanController extends Controller
{
    public function index(){
   		return view('transaction.pelunasan');
    }

    public function getData(){
        $datalcl = Models\Pelunasan::select('pelunasan.id_pelunasan','pelunasan.tgl_pelunasan','invoice_lcl.id_invoice_lcl as id_invoice','invoice_lcl.no_invoice_lcl as no_invoice','invoice_lcl.tgl_invoice_lcl','trip.kode_trip','invoice_lcl.id_customer','invoice_lcl.bukti_bayar','invoice_lcl.total','invoice_lcl.kode_penagihan',DB::Raw("'LCL' as type_invoice"))
                            ->leftJoin('invoice_lcl','invoice_lcl.id_invoice_lcl','pelunasan.id_invoice')
                            ->leftJoin('trip','trip.id_trip','invoice_lcl.id_trip')
                            ->where('pelunasan.tipe_invoice','=','LCL');

        $data = Models\Pelunasan::select('pelunasan.id_pelunasan','pelunasan.tgl_pelunasan','invoice.id_invoice','invoice.no_invoice','invoice.tgl_invoice','trip.kode_trip','invoice.id_customer','invoice.bukti_bayar','invoice.total','invoice.kode_penagihan',DB::Raw("'CONTAINER' as type_invoice"))
                            ->leftJoin('invoice','invoice.id_invoice','pelunasan.id_invoice')
                            ->leftJoin('trip','trip.id_trip','invoice.id_trip')
                            ->where('pelunasan.tipe_invoice','=','CONTAINER')
                            ->union($datalcl)
                            ->get();

        $count = $data->count();
        
    
        return response()->json([
                'total' => $count,
                'rows' => $data
            ]);
    }

    public function getDataInvoice(){
        $datalcl = Models\InvoiceLCL::select('invoice_lcl.id_invoice_lcl','invoice_lcl.no_invoice_lcl','invoice_lcl.tgl_invoice_lcl','trip.kode_trip','invoice_lcl.id_customer','customer.nama_pt as nama_customer','invoice_lcl.bukti_bayar','invoice_lcl.total','invoice_lcl.kode_penagihan',DB::Raw("'LCL' as type_invoice"))
                            ->leftJoin('trip','trip.id_trip','invoice_lcl.id_trip')
                            ->leftJoin('customer','customer.id_customer','invoice_lcl.id_customer')
                            ->whereNotNull('invoice_lcl.kode_penagihan')
                            ->where('invoice_lcl.status','=','1');

        $data = Models\Invoice::select('invoice.id_invoice','invoice.no_invoice','invoice.tgl_invoice','trip.kode_trip','invoice.id_customer','customer.nama_pt as nama_customer','invoice.bukti_bayar','invoice.total','invoice.kode_penagihan',DB::Raw("'CONTAINER' as type_invoice"))
                            ->leftJoin('trip','trip.id_trip','invoice.id_trip')
                            ->leftJoin('customer','customer.id_customer','invoice.id_customer')
                            ->whereNotNull('invoice.kode_penagihan')
                            ->where('invoice.status','=','1')
                            ->union($datalcl)
                            ->get();

        $count = $data->count();


        
    
        return response()->json([
                'total' => $count,
                'rows' => $data
            ]);
    }
    
    public function add($idinvoice,$typeinvoice){
        $mode='ADD';
        $type_invoice = $typeinvoice;
        if($typeinvoice=='CONTAINER'){
            $invoice = Models\Invoice::select('invoice.id_invoice','invoice.no_invoice','invoice.bukti_bayar','invoice.tgl_invoice','trip.kode_trip','customer.nama_pt as nama_customer','invoice.tax','invoice.total','invoice.nominal_dibayar')
                                     ->leftJoin('trip','invoice.id_trip','trip.id_trip')
                                     ->leftJoin('customer','invoice.id_customer','customer.id_customer')
                                    ->find($idinvoice);
            $datainvoice = Models\SuratJalan::select('suratjalan.id_suratjalan','suratjalan.no_container','suratjalan.no_suratjalan','suratjalan.keterangan_total', 'proposaldetail.nominal_total','invoicedetail.id_invoice','invoicedetail.id_invoicedetail','invoicedetail.biaya','invoicedetail.biaya_tambahan_1','invoicedetail.nominal_biaya_tambahan_1','invoicedetail.biaya_tambahan_2','invoicedetail.nominal_biaya_tambahan_2','invoicedetail.biaya_tambahan_3','invoicedetail.nominal_biaya_tambahan_3','invoicedetail.total','invoicedetail.deleted_at',DB::raw('GROUP_CONCAT(bapb.no_bapb SEPARATOR ", ") AS no_bapb'))
                                         ->leftJoin('do','do.id_suratjalan','suratjalan.id_suratjalan')
                                         ->leftJoin('bapb','bapb.id_bapb','do.id_bapb')
                                         ->leftJoin('order','suratjalan.id_order','order.id_order')
                                         ->leftJoin('proposaldetail','proposaldetail.id_proposaldetail','order.id_proposaldetail')
                                         ->leftJoin('invoicedetail','invoicedetail.id_suratjalan','suratjalan.id_suratjalan')
                                         ->whereNull('suratjalan.deleted_at')
                                         ->whereNull('do.deleted_at')
                                         ->whereNull('bapb.deleted_at')
                                         ->whereNull('order.deleted_at')
                                         ->whereNull('proposaldetail.deleted_at')
                                         ->where('invoicedetail.id_invoice','=',$idinvoice)
                                         ->whereNull('invoicedetail.deleted_at')
                                        //  ->where(function($q) {
                                        //     $q->whereNull('invoicedetail.id_invoice')
                                        //       ->orWhereNotNull('invoicedetail.deleted_at');
                                        // })
                                         ->groupBy('suratjalan.id_suratjalan','suratjalan.no_container','suratjalan.no_suratjalan','suratjalan.keterangan_total', 'proposaldetail.nominal_total','invoicedetail.id_invoice','invoicedetail.id_invoicedetail','invoicedetail.biaya','invoicedetail.biaya_tambahan_1','invoicedetail.nominal_biaya_tambahan_1','invoicedetail.biaya_tambahan_2','invoicedetail.nominal_biaya_tambahan_2','invoicedetail.biaya_tambahan_3','invoicedetail.nominal_biaya_tambahan_3','invoicedetail.total','invoicedetail.deleted_at')
                                         ->get();
        }else{
            $invoice = Models\InvoiceLCL::select('invoice_lcl.id_invoice_lcl as id_invoice','invoice_lcl.no_invoice_lcl as no_invoice','invoice_lcl.bukti_bayar','invoice_lcl.tgl_invoice_lcl as tgl_invoice','trip.kode_trip','customer.nama_pt as nama_customer','invoice_lcl.tax','invoice_lcl.total','invoice_lcl.nominal_dibayar')
                                        ->leftJoin('trip','invoice_lcl.id_trip','trip.id_trip')
                                        ->leftJoin('customer','invoice_lcl.id_customer','customer.id_customer')
                                    ->find($idinvoice);
            $datainvoice = Models\SuratJalan::select('suratjalan.id_suratjalan','suratjalan.no_suratjalan','suratjalan.keterangan_total', 'proposaldetail.nominal_total','invoicedetail_lcl.id_invoice_lcl','invoicedetail_lcl.id_invoicedetail_lcl','invoicedetail_lcl.biaya','invoicedetail_lcl.biaya_tambahan_1','invoicedetail_lcl.nominal_biaya_tambahan_1','invoicedetail_lcl.biaya_tambahan_2','invoicedetail_lcl.nominal_biaya_tambahan_2','invoicedetail_lcl.biaya_tambahan_3','invoicedetail_lcl.nominal_biaya_tambahan_3','invoicedetail_lcl.total','invoicedetail_lcl.deleted_at',DB::raw('GROUP_CONCAT(bapb.no_bapb SEPARATOR ", ") AS no_bapb'))
                                         ->leftJoin('do','do.id_suratjalan','suratjalan.id_suratjalan')
                                         ->leftJoin('bapb','bapb.id_bapb','do.id_bapb')
                                         ->leftJoin('order','suratjalan.id_order','order.id_order')
                                         ->leftJoin('proposaldetail','proposaldetail.id_proposaldetail','order.id_proposaldetail')
                                         ->leftJoin('invoicedetail_lcl','invoicedetail_lcl.id_suratjalan','suratjalan.id_suratjalan')
                                         ->whereNull('suratjalan.deleted_at')
                                         ->whereNull('do.deleted_at')
                                         ->whereNull('bapb.deleted_at')
                                         ->whereNull('order.deleted_at')
                                         ->whereNull('proposaldetail.deleted_at')
                                         ->where('invoicedetail_lcl.id_invoice_lcl','=',$idinvoice)
                                         ->whereNull('invoicedetail_lcl.deleted_at')
                                        //  ->where(function($q) {
                                        //     $q->whereNull('invoicedetail.id_invoice')
                                        //       ->orWhereNotNull('invoicedetail.deleted_at');
                                        // })
                                         ->groupBy('suratjalan.id_suratjalan','suratjalan.no_suratjalan','suratjalan.keterangan_total', 'proposaldetail.nominal_total','invoicedetail_lcl.id_invoice_lcl','invoicedetail_lcl.id_invoicedetail_lcl','invoicedetail_lcl.biaya','invoicedetail_lcl.biaya_tambahan_1','invoicedetail_lcl.nominal_biaya_tambahan_1','invoicedetail_lcl.biaya_tambahan_2','invoicedetail_lcl.nominal_biaya_tambahan_2','invoicedetail_lcl.biaya_tambahan_3','invoicedetail_lcl.nominal_biaya_tambahan_3','invoicedetail_lcl.total','invoicedetail_lcl.deleted_at')
                                         ->get();
        }
        return view('transaction.pelunasanadd',compact('mode','invoice','datainvoice','type_invoice'));
    }

    public function create(Request $request, $idinvoice,$typeinvoice){
        // dd($idproposal);


        DB::beginTransaction();
        try {
            // if((int)$request->input('txtNominalDibayar') > (int)$request->input('txtNominalBelumDibayar')){
            //     throw new \Exception("Nominal Dibayar tidak boleh melebihi Nominal yang belum dibayar!");
            // }
            $pelunasan = new Models\Pelunasan;
            $pelunasan->id_invoice = $idinvoice;
            $pelunasan->tipe_invoice = $typeinvoice;
            $pelunasan->bukti_bayar = $request->input('txtBuktiBayar');
            $pelunasan->tgl_pelunasan = Carbon::parse($request->input('txtTglPelunasan'))->format('Y/m/d H:i:s');
            $pelunasan->pph23 = $request->input('txtPPH');
            $pelunasan->potongan_admin = $request->input('txtPotonganAdmin');
            $pelunasan->claim = $request->input('txtClaim');
            $pelunasan->nominal_dibayar = $request->input('txtNominalDibayar');
            $pelunasan->keterangan = $request->input('txtKeterangan');
            $pelunasan->status = 0;
            if($pelunasan->save()){
                if($typeinvoice=='CONTAINER'){
                    $invoice = Models\Invoice::find($idinvoice);
                    $invoice->bukti_bayar = $request->input('txtBuktiBayar');
                    $invoice->nominal_dibayar = $invoice->nominal_dibayar + (int)$request->input('txtNominalDibayar');
                    if($invoice->total <= $invoice->nominal_dibayar + (int)$request->input('txtNominalDibayar'))
                    {
                        $invoice->status = 2;
                    }
                    $invoice->save();
                }else{
                    $invoice = Models\InvoiceLCL::find($idinvoice);
                    $invoice->bukti_bayar = $request->input('txtBuktiBayar');
                    $invoice->nominal_dibayar = $invoice->nominal_dibayar + (int)$request->input('txtNominalDibayar');
                    if($invoice->total <= $invoice->nominal_dibayar + (int)$request->input('txtNominalDibayar'))
                    {
                        $invoice->status = 2;
                    }
                    $invoice->save();
                }
            }

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->route('transaction.pelunasan')
            ->with('success',('Data Saved'));
    }


    public function edit($idpelunasan,$idinvoice,$typeinvoice){
        $mode='EDIT';
        $type_invoice = $typeinvoice;
        $pelunasan = Models\Pelunasan::find($idpelunasan);
        if($typeinvoice=='CONTAINER'){
            $invoice = Models\Invoice::select('invoice.id_invoice','invoice.no_invoice','invoice.bukti_bayar','invoice.tgl_invoice','trip.kode_trip','customer.nama_pt as nama_customer','invoice.tax','invoice.total','invoice.nominal_dibayar')
                                     ->leftJoin('trip','invoice.id_trip','trip.id_trip')
                                     ->leftJoin('customer','invoice.id_customer','customer.id_customer')
                                    ->find($idinvoice);
            $datainvoice = Models\SuratJalan::select('suratjalan.id_suratjalan','suratjalan.no_container','suratjalan.no_suratjalan','suratjalan.keterangan_total', 'proposaldetail.nominal_total','invoicedetail.id_invoice','invoicedetail.id_invoicedetail','invoicedetail.biaya','invoicedetail.biaya_tambahan_1','invoicedetail.nominal_biaya_tambahan_1','invoicedetail.biaya_tambahan_2','invoicedetail.nominal_biaya_tambahan_2','invoicedetail.biaya_tambahan_3','invoicedetail.nominal_biaya_tambahan_3','invoicedetail.total','invoicedetail.deleted_at',DB::raw('GROUP_CONCAT(bapb.no_bapb SEPARATOR ", ") AS no_bapb'))
                                         ->leftJoin('do','do.id_suratjalan','suratjalan.id_suratjalan')
                                         ->leftJoin('bapb','bapb.id_bapb','do.id_bapb')
                                         ->leftJoin('order','suratjalan.id_order','order.id_order')
                                         ->leftJoin('proposaldetail','proposaldetail.id_proposaldetail','order.id_proposaldetail')
                                         ->leftJoin('invoicedetail','invoicedetail.id_suratjalan','suratjalan.id_suratjalan')
                                         ->whereNull('suratjalan.deleted_at')
                                         ->whereNull('do.deleted_at')
                                         ->whereNull('bapb.deleted_at')
                                         ->whereNull('order.deleted_at')
                                         ->whereNull('proposaldetail.deleted_at')
                                         ->where('invoicedetail.id_invoice','=',$idinvoice)
                                         ->whereNull('invoicedetail.deleted_at')
                                        //  ->where(function($q) {
                                        //     $q->whereNull('invoicedetail.id_invoice')
                                        //       ->orWhereNotNull('invoicedetail.deleted_at');
                                        // })
                                         ->groupBy('suratjalan.id_suratjalan','suratjalan.no_container','suratjalan.no_suratjalan','suratjalan.keterangan_total', 'proposaldetail.nominal_total','invoicedetail.id_invoice','invoicedetail.id_invoicedetail','invoicedetail.biaya','invoicedetail.biaya_tambahan_1','invoicedetail.nominal_biaya_tambahan_1','invoicedetail.biaya_tambahan_2','invoicedetail.nominal_biaya_tambahan_2','invoicedetail.biaya_tambahan_3','invoicedetail.nominal_biaya_tambahan_3','invoicedetail.total','invoicedetail.deleted_at')
                                         ->get();
        }else{
            $invoice = Models\InvoiceLCL::select('invoice_lcl.id_invoice_lcl as id_invoice','invoice_lcl.no_invoice_lcl as no_invoice','invoice_lcl.bukti_bayar','invoice_lcl.tgl_invoice_lcl as tgl_invoice','trip.kode_trip','customer.nama_pt as nama_customer','invoice_lcl.tax','invoice_lcl.total','invoice_lcl.nominal_dibayar')
                                        ->leftJoin('trip','invoice_lcl.id_trip','trip.id_trip')
                                        ->leftJoin('customer','invoice_lcl.id_customer','customer.id_customer')
                                    ->find($idinvoice);
            $datainvoice = Models\SuratJalan::select('suratjalan.id_suratjalan','suratjalan.no_suratjalan','suratjalan.keterangan_total', 'proposaldetail.nominal_total','invoicedetail_lcl.id_invoice_lcl','invoicedetail_lcl.id_invoicedetail_lcl','invoicedetail_lcl.biaya','invoicedetail_lcl.biaya_tambahan_1','invoicedetail_lcl.nominal_biaya_tambahan_1','invoicedetail_lcl.biaya_tambahan_2','invoicedetail_lcl.nominal_biaya_tambahan_2','invoicedetail_lcl.biaya_tambahan_3','invoicedetail_lcl.nominal_biaya_tambahan_3','invoicedetail_lcl.total','invoicedetail_lcl.deleted_at',DB::raw('GROUP_CONCAT(bapb.no_bapb SEPARATOR ", ") AS no_bapb'))
                                         ->leftJoin('do','do.id_suratjalan','suratjalan.id_suratjalan')
                                         ->leftJoin('bapb','bapb.id_bapb','do.id_bapb')
                                         ->leftJoin('order','suratjalan.id_order','order.id_order')
                                         ->leftJoin('proposaldetail','proposaldetail.id_proposaldetail','order.id_proposaldetail')
                                         ->leftJoin('invoicedetail_lcl','invoicedetail_lcl.id_suratjalan','suratjalan.id_suratjalan')
                                         ->whereNull('suratjalan.deleted_at')
                                         ->whereNull('do.deleted_at')
                                         ->whereNull('bapb.deleted_at')
                                         ->whereNull('order.deleted_at')
                                         ->whereNull('proposaldetail.deleted_at')
                                         ->where('invoicedetail_lcl.id_invoice_lcl','=',$idinvoice)
                                         ->whereNull('invoicedetail_lcl.deleted_at')
                                        //  ->where(function($q) {
                                        //     $q->whereNull('invoicedetail.id_invoice')
                                        //       ->orWhereNotNull('invoicedetail.deleted_at');
                                        // })
                                         ->groupBy('suratjalan.id_suratjalan','suratjalan.no_suratjalan','suratjalan.keterangan_total', 'proposaldetail.nominal_total','invoicedetail_lcl.id_invoice_lcl','invoicedetail_lcl.id_invoicedetail_lcl','invoicedetail_lcl.biaya','invoicedetail_lcl.biaya_tambahan_1','invoicedetail_lcl.nominal_biaya_tambahan_1','invoicedetail_lcl.biaya_tambahan_2','invoicedetail_lcl.nominal_biaya_tambahan_2','invoicedetail_lcl.biaya_tambahan_3','invoicedetail_lcl.nominal_biaya_tambahan_3','invoicedetail_lcl.total','invoicedetail_lcl.deleted_at')
                                         ->get();
        }
        return view('transaction.pelunasanadd',compact('mode','pelunasan','invoice','datainvoice','type_invoice'));
    }

    public function update(Request $request, $idpelunasan,$typeinvoice){
        // dd($idproposal);


        DB::beginTransaction();
        try {
            // if((int)$request->input('txtNominalDibayar') > (int)$request->input('txtNominalBelumDibayar')){
            //     throw new \Exception("Nominal Dibayar tidak boleh melebihi Nominal yang belum dibayar!");
            // }
            $pelunasan = Models\Pelunasan::find($idpelunasan);
            $pelunasan->bukti_bayar = $request->input('txtBuktiBayar');
            $pelunasan->tgl_pelunasan = Carbon::parse($request->input('txtTglPelunasan'))->format('Y/m/d H:i:s');
            $pelunasan->pph23 = $request->input('txtPPH');
            $pelunasan->potongan_admin = $request->input('txtPotonganAdmin');
            $pelunasan->claim = $request->input('txtClaim');
            $pelunasan->nominal_dibayar = $request->input('txtNominalDibayar');
            $pelunasan->keterangan = $request->input('txtKeterangan');
            // $pelunasan->status = 0;
            if($pelunasan->save()){
                if($typeinvoice=='CONTAINER'){
                    $invoice = Models\Invoice::find($pelunasan->id_invoice);
                    $invoice->bukti_bayar = $request->input('txtBuktiBayar');
                    $invoice->nominal_dibayar = $invoice->nominal_dibayar + (int)$request->input('txtNominalDibayar');
                    if($invoice->total <= $invoice->nominal_dibayar + (int)$request->input('txtNominalDibayar'))
                    {
                        $invoice->status = 2;
                    }                    
                    $invoice->save();
                }else{
                    $invoice = Models\InvoiceLCL::find($pelunasan->id_invoice);
                    $invoice->bukti_bayar = $request->input('txtBuktiBayar');
                    $invoice->nominal_dibayar = $invoice->nominal_dibayar + (int)$request->input('txtNominalDibayar');
                    if($invoice->total <= $invoice->nominal_dibayar + (int)$request->input('txtNominalDibayar'))
                    {
                        $invoice->status = 2;
                    }
                    $invoice->save();
                }
            }

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->back()
            ->with('success',('Data Updated'));
    }
}
