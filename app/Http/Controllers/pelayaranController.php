<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models;

class pelayaranController extends Controller
{
    public function index(){
   		return view('master.pelayaran');
    }

    public function getData(Request $request){
        $data = new Models\Pelayaran;
        
        if ($request->input('search')) {
            $data = $data->where('name_full','LIKE','%'.$request->input('search').'%')
                ->orWhere('alamat','LIKE','%'.$request->input('search').'%')
                ->orWhere('kodepos','LIKE','%'.$request->input('search').'%')
                ->orWhere('telp','LIKE','%'.$request->input('search').'%')
                ->orWhere('name_cp','LIKE','%'.$request->input('search').'%')
                ->orWhere('telp_cp','LIKE','%'.$request->input('search').'%')
                ->orWhere('email','LIKE','%'.$request->input('search').'%');
        }

        $count = $data->count();

        if ($request->input('sort')) {
            $data = $data->orderBy($request->input('sort'),$request->input('order'))
                ->orderBy('id_pelayaran',$request->input('order'));
            
        } else {
            $data = $data->orderBy('name_full','asc');
        }
        if($request->input('limit')) {
            $data = $data->take($request->input('limit'))
            ->skip($request->input('offset'));
        }

        $data = $data->get();
        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }
    
    public function add(){
        $mode='ADD';
        $city = Models\City::orderBy('nama_kota','asc')->Get();
        return view('master.pelayaranadd',compact('city','mode'));
    }

    public function create(Request $request){
        Validator::make($request->all(), [
            'txtShortname' => 'required',
            'txtFullname' => 'required',
            'txtAlamat' => 'required',
            'cboKota' => 'required|not_in:0',
            'txtKodepos' => 'required',
            'txtTelepon' => 'required',
            'txtNamaCP' => 'required',
            'txtTeleponCP' => 'required',
            'txtEmail' => 'required',
        ],[
            'cboKota.not_in' => 'kota harus dipilih',
            ])->validate();

        $idpelayaran = 0;

        DB::beginTransaction();
        try {
            $pelayaran = new Models\Pelayaran;
            $pelayaran->name_short = $request->input('txtShortname');
            $pelayaran->name_full = $request->input('txtFullname');
            $pelayaran->alamat = $request->input('txtAlamat');
            $pelayaran->id_kota = $request->input('cboKota');
            $pelayaran->kodepos = $request->input('txtKodepos');
            $pelayaran->telp = $request->input('txtTelepon');
            $pelayaran->name_cp = $request->input('txtNamaCP');
            $pelayaran->telp_cp = $request->input('txtTeleponCP');
            $pelayaran->email = $request->input('txtEmail');
            if($pelayaran->save()){
                $idpelayaran = $pelayaran->id_pelayaran;
            }


        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->route('master.pelayaran.detail',$idpelayaran)
            ->with('success',__('Data Saved'));
    }

    public function edit($idpelayaran){
        $mode = 'EDIT';
        $pelayaran = Models\Pelayaran::find($idpelayaran);
        $city = Models\City::orderBy('nama_kota','asc')->Get();;
        return view('master.pelayaranadd',compact('mode','pelayaran','city'));
    }

    public function update(Request $request, $idpelayaran){
        Validator::make($request->all(), [
            'txtShortname' => 'required',
            'txtFullname' => 'required',
            'txtAlamat' => 'required',
            'cboKota' => 'required|not_in:0',
            'txtKodepos' => 'required',
            'txtTelepon' => 'required',
            'txtNamaCP' => 'required',
            'txtTeleponCP' => 'required',
            'txtEmail' => 'required',
        ],[
        'cboKota.not_in' => 'kota harus dipilih',
        ])->validate();

        DB::beginTransaction();
        try {
            $pelayaran = Models\Pelayaran::find($idpelayaran);
            $pelayaran->name_short = $request->input('txtShortname');
            $pelayaran->name_full = $request->input('txtFullname');
            $pelayaran->alamat = $request->input('txtAlamat');
            $pelayaran->id_kota = $request->input('cboKota');
            $pelayaran->kodepos = $request->input('txtKodepos');
            $pelayaran->telp = $request->input('txtTelepon');
            $pelayaran->name_cp = $request->input('txtNamaCP');
            $pelayaran->telp_cp = $request->input('txtTeleponCP');
            $pelayaran->email = $request->input('txtEmail');
            $pelayaran->save();


        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->back()
            ->with('success',__('Data Updated'));
    }

    public function detail($idpelayaran){
        $pelayaran = Models\Pelayaran::find($idpelayaran);
        return view('master.detailpelayaran',compact('pelayaran'));
    }

    public function getDataDetail(Request $request, $idpelayaran){
        // $data = Models\PelayaranDetail::with('DetailBongkar')->with('DetailAsal')
        $data = Models\PelayaranDetail::leftJoin('pelabuhan as pelabuhan_asal','pelabuhan_asal.id_pelabuhan','pelayarandetail.id_pelabuhan_asal')
                                    ->leftJoin('pelabuhan as pelabuhan_bongkar','pelabuhan_bongkar.id_pelabuhan','pelayarandetail.id_pelabuhan_bongkar')
                                    ->leftJoin('city as city_asal','city_asal.id_kota','pelabuhan_asal.id_kota')
                                    ->leftJoin('city as city_bongkar','city_bongkar.id_kota','pelabuhan_bongkar.id_kota')
                                    ->where('id_pelayaran','=',$idpelayaran)
                                    ->orderBy('city_asal.nama_kota');
        
        $data = $data->select([
            'pelayarandetail.*',
            'pelabuhan_asal.nama_pelabuhan as nama_pelabuhan_asal',
            'pelabuhan_bongkar.nama_pelabuhan as nama_pelabuhan_bongkar',
            'city_asal.nama_kota as nama_kota_asal',
            'city_bongkar.nama_kota as nama_kota_bongkar'
        ]);

        $count = $data->count();

        // if ($request->input('search')) {
        //     $data = $data->where('pelabuhan_asal.nama_pelabuhan','LIKE','%'.$request->input('search').'%')
        //         ->orWhere('pelabuhan_bongkar.nama_pelabuhan','LIKE','%'.$request->input('search').'%')
        //         ->orWhere('city_asal.nama_kota','LIKE','%'.$request->input('search').'%')
        //         ->orWhere('city_bongkar.nama_kota','LIKE','%'.$request->input('search').'%')
        //         ->where('pelayarandetail.id_pelayaran','=',$idpelayaran)
        //         ->whereNull('pelayarandetail.deleted_at');
        // }

        // if ($request->input('sort')) {
        //     $data = $data->orderBy($request->input('sort'),$request->input('order'))
        //         ->orderBy('pelayarandetail.id_pelayarandetail',$request->input('order'));
            
        // } else {
        //     $data = $data->orderBy('pelabuhan_asal.nama_pelabuhan','desc');
        // }
        // if($request->input('limit')) {
        //     $data = $data->take($request->input('limit'))
        //     ->skip($request->input('offset'));
        // }
        
        $data = $data->get();
        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }

    public function addDetail($idpelayaran){
        $mode = 'ADD';
        $pelayaran = Models\Pelayaran::find($idpelayaran);
        $kotapelabuhan = Models\Pelabuhan::leftJoin('city','city.id_kota','=','pelabuhan.id_kota')
                                          ->orderBy('city.nama_kota','asc')->Get();;
        return view('master.detailpelayaranadd',compact('mode','pelayaran','kotapelabuhan'));
    }

    public function createDetail($idpelayaran, Request $request){
        Validator::make($request->all(), [
            'cboPortAsal' => 'required|not_in:0',
            'cboPortBongkar' => 'required|not_in:0',
        ],[
            'cboPortAsal.not_in' => 'pelabuhan asal harus dipilih',
            'cboPortBongkar.not_in' => 'pelabuhan bongkar harus dipilih',
            ])->validate();

        DB::beginTransaction();
        try {
            $pelayarandetail = new Models\PelayaranDetail;
            $pelayarandetail->id_pelayaran = $idpelayaran;
            $pelayarandetail->id_pelabuhan_asal = $request->input('cboPortAsal');
            $pelayarandetail->id_pelabuhan_bongkar = $request->input('cboPortBongkar');
            $pelayarandetail->save();

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->route('master.pelayaran.detail',$idpelayaran)
            ->with('success',('Data Saved'));
    }

    public function editDetail($idpelayarandetail){
        $mode = 'EDIT';
        $pelayarandetail = Models\PelayaranDetail::with('Pelayaran')->find($idpelayarandetail);
        $kotapelabuhan = Models\Pelabuhan::leftJoin('city','city.id_kota','=','pelabuhan.id_kota')
                                          ->orderBy('city.nama_kota','asc')->Get();;
        return view('master.detailpelayaranadd',compact('mode','pelayarandetail','kotapelabuhan'));
    }

    public function updateDetail($idpelayarandetail, Request $request){
        Validator::make($request->all(), [
            'cboPortAsal' => 'required|not_in:0',
            'cboPortBongkar' => 'required|not_in:0',

        ],[
            'cboPortAsal.not_in' => 'pelabuhan asal harus dipilih',
            'cboPortBongkar.not_in' => 'pelabuhan bongkar harus dipilih',
            ])->validate();

        DB::beginTransaction();
        try {
            $pelayarandetail = Models\PelayaranDetail::find($idpelayarandetail);
            $pelayarandetail->id_pelabuhan_asal = $request->input('cboPortAsal');
            $pelayarandetail->id_pelabuhan_bongkar = $request->input('cboPortBongkar');
            $pelayarandetail->save();

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->back()
            ->with('success',('Data Updated'));
    }

    public function deleteDetail($idpelayarandetail, Request $request)
    {   
        DB::beginTransaction();
        try {
            $pelayarandetail = Models\PelayaranDetail::find($idpelayarandetail);
            $pelayarandetail->delete();
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Detail Pelayaran Deleted.'
        ]);
    }
}
