<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models;

class consigneeController extends Controller
{
    public function index(){
        return view('master.consignee');
    }

    public function getData(Request $request){
        $data = Models\Consignee::leftJoin('city','city.id_kota','consignee.id_kota');
        
        $data = $data->select([
            'consignee.*',
            'city.nama_kota'
        ]);

        if ($request->input('search')) {
            $data = $data->where('consignee.nama','LIKE','%'.$request->input('search').'%')
                ->orWhere('consignee.alamat','LIKE','%'.$request->input('search').'%')
                ->orWhere('city.nama_kota','LIKE','%'.$request->input('search').'%')
                ->orWhere('consignee.telp','LIKE','%'.$request->input('search').'%')
                ->orWhere('consignee.keterangan','LIKE','%'.$request->input('search').'%')
                ->orWhere('consignee.name_cp','LIKE','%'.$request->input('search').'%')
                ->orWhere('consignee.telp_cp','LIKE','%'.$request->input('search').'%');
        }
        $count = $data->count();

        if ($request->input('sort')) {
            $data = $data->orderBy($request->input('sort'),$request->input('order'))
                ->orderBy('consignee.id_consignee',$request->input('order'));
            
        } else {
            $data = $data->orderBy('consignee.nama','asc');
        }
        if($request->input('limit')) {
            $data = $data->take($request->input('limit'))
            ->skip($request->input('offset'));
        }
        
        $data = $data->get();
        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }
    
    public function add(){
        $mode = 'ADD';
        $city = Models\City::orderBy('nama_kota','asc')->Get();
        return view('master.consigneeadd',compact('mode','city'));
    }

    public function create(Request $request){
        Validator::make($request->all(), [
            'txtNama' => 'required',
            'txtAlamat' => 'required',
            'cboKota' => 'required|not_in:0',
            'txtTelp' => 'required',
            'txtNamaCP' => 'required',
            'txtTelpCP' => 'required',
            // 'txtKeterangan' => 'required',

        ],[
            'cboKota.not_in' => 'kota harus dipilih',
            ])->validate();

        DB::beginTransaction();
        try {
            $consignee = new Models\Consignee;
            $consignee->nama = $request->input('txtNama');
            $consignee->alamat = $request->input('txtAlamat');
            $consignee->id_kota = $request->input('cboKota');
            $consignee->telp = $request->input('txtTelp');
            $consignee->keterangan = $request->input('txtKeterangan');
            $consignee->name_cp = $request->input('txtNamaCP');
            $consignee->telp_cp = $request->input('txtTelpCP');
            $consignee->save();


        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->route('master.consignee')
            ->with('success',__('Data Saved'));
    }

    public function edit($idConsignee){
        $mode = 'EDIT';
        $consignee = Models\Consignee::find($idConsignee);
        $city = Models\City::orderBy('nama_kota','asc')->Get();
        return view('master.consigneeadd',compact('mode','consignee','city'));
    }

    public function update($idConsignee, Request $request){
        Validator::make($request->all(), [
            'txtNama' => 'required',
            'txtAlamat' => 'required',
            'cboKota' => 'required|not_in:0',
            'txtTelp' => 'required',
            'txtNamaCP' => 'required',
            'txtTelpCP' => 'required',
            // 'txtKeterangan' => 'required',

        ],[
            'cboKota.not_in' => 'kota harus dipilih',
            ])->validate();

        DB::beginTransaction();
        try {
            $consignee = Models\Consignee::find($idConsignee);
            $consignee->nama = $request->input('txtNama');
            $consignee->alamat = $request->input('txtAlamat');
            $consignee->id_kota = $request->input('cboKota');
            $consignee->telp = $request->input('txtTelp');
            $consignee->keterangan = $request->input('txtKeterangan');
            $consignee->name_cp = $request->input('txtNamaCP');
            $consignee->telp_cp = $request->input('txtTelpCP');
            $consignee->save();


        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->back()
            ->with('success',('Data Updated'));
    }

    public function delete($idConsignee, Request $request)
    {   
        DB::beginTransaction();
        try {
            $consignee = Models\Consignee::find($idConsignee);
            $consignee->delete();
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Consignee Deleted.'
        ]);
    }
}