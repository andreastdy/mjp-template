<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models;

class dooringController extends Controller
{
    public function index(){
   		return view('master.dooring');
    }

    public function getData(Request $request){
        $data = Models\Dooring::leftJoin('city','city.id_kota','dooring.id_kota');
        
        $data = $data->select([
            'dooring.*',
            'city.nama_kota'
        ]);


        if ($request->input('search')) {
            $data = $data->where('dooring.nama','LIKE','%'.$request->input('search').'%')
                ->orWhere('dooring.alamat','LIKE','%'.$request->input('search').'%')
                ->orWhere('city.nama_kota','LIKE','%'.$request->input('search').'%')
                ->orWhere('dooring.rute','LIKE','%'.$request->input('search').'%')
                ->orWhere('dooring.rekening','LIKE','%'.$request->input('search').'%')
                ->orWhere('dooring.name_cp','LIKE','%'.$request->input('search').'%')
                ->orWhere('dooring.telp_cp','LIKE','%'.$request->input('search').'%')
                ->orWhere('dooring.email','LIKE','%'.$request->input('search').'%');
        }
        $count = $data->count();

        if ($request->input('sort')) {
            $data = $data->orderBy($request->input('sort'),$request->input('order'))
                ->orderBy('dooring.id_dooring',$request->input('order'));
            
        } else {
            $data = $data->orderBy('dooring.nama','asc');
        }
        if($request->input('limit')) {
            $data = $data->take($request->input('limit'))
            ->skip($request->input('offset'));
        }
        
        $data = $data->get();
        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }
    
    public function add(){
        $mode = 'ADD';
        $city = Models\City::orderBy('nama_kota','asc')->Get();
        return view('master.dooringadd',compact('city','mode'));
    }

    public function create(Request $request){
        Validator::make($request->all(), [
            'txtName' => 'required',
            'txtAlamat' => 'required',
            'cboKota' => 'required|not_in:0',
            'txtRute' => 'required',
            'txtRekening' => 'required',
            'txtNamaCP' => 'required',
            'txtTeleponCP' => 'required',
            'txtEmail' => 'required',

        ],[
            'cboKota.not_in' => 'kota harus dipilih',
            ])->validate();
        
        $iddooring = 0;

        DB::beginTransaction();
        try {
            $dooring = new Models\Dooring;
            $dooring->nama = $request->input('txtName');
            $dooring->alamat = $request->input('txtAlamat');
            $dooring->id_kota = $request->input('cboKota');
            $dooring->rute = $request->input('txtRute');
            $dooring->rekening = $request->input('txtRekening');
            $dooring->name_cp = $request->input('txtNamaCP');
            $dooring->telp_cp = $request->input('txtTeleponCP');
            $dooring->email = $request->input('txtEmail');
            if($dooring->save()){
                $iddooring = $dooring->id_dooring;
            }

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->route('master.dooring.detail',$iddooring)
            ->with('success',('Data Saved'));
    }

    public function edit($iddooring){
        $mode = 'EDIT';
        $dooring = Models\Dooring::find($iddooring);
        $city = Models\City::orderBy('nama_kota','asc')->Get();
        return view('master.dooringadd',compact('mode','dooring','city'));
    }

    public function update($iddooring, Request $request){
        Validator::make($request->all(), [
            'txtName' => 'required',
            'txtAlamat' => 'required',
            'cboKota' => 'required|not_in:0',
            'txtRute' => 'required',
            'txtRekening' => 'required',
            'txtNamaCP' => 'required',
            'txtTeleponCP' => 'required',
            'txtEmail' => 'required',

        ],[
            'cboKota.not_in' => 'kota harus dipilih',
            ])->validate();

        DB::beginTransaction();
        try {
            $dooring = Models\Dooring::find($iddooring);
            $dooring->nama = $request->input('txtName');
            $dooring->alamat = $request->input('txtAlamat');
            $dooring->id_kota = $request->input('cboKota');
            $dooring->rute = $request->input('txtRute');
            $dooring->rekening = $request->input('txtRekening');
            $dooring->name_cp = $request->input('txtNamaCP');
            $dooring->telp_cp = $request->input('txtTeleponCP');
            $dooring->email = $request->input('txtEmail');
            $dooring->save();

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->back()
            ->with('success',('Data Updated'));
    }

    public function detail($iddooring){
        $dooring = Models\Dooring::find($iddooring);
        return view('master.detaildooring',compact('dooring'));
    }

    public function getDataDetail($iddooring){
        $data = Models\DooringDetail::leftJoin('city as city_pelabuhan','city_pelabuhan.id_kota','dooringdetail.id_kota_pelabuhan')
                                    ->leftJoin('city as city_tujuan','city_tujuan.id_kota','dooringdetail.id_kota_tujuan')
                                    ->where('dooringdetail.id_dooring',$iddooring);
        
        $data = $data->select([
            'dooringdetail.*',
            'city_pelabuhan.nama_kota as nama_kota_pelabuhan',
            'city_tujuan.nama_kota as nama_kota_tujuan'
        ]);

        $count = $data->count();

        // if ($request->input('sort')) {
        //     $data = $data->orderBy($request->input('sort'),$request->input('order'))
        //         ->orderBy('consignee.id_consignee',$request->input('order'));
            
        // } else {
        //     $data = $data->orderBy('consignee.nama');
        // }
        // if($request->input('limit')) {
        //     $data = $data->take($request->input('limit'))
        //     ->skip($request->input('offset'));
        // }
        
        $data = $data->get();
        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }

    public function addDetail($iddooring){
        $mode = 'ADD';
        $dooring = Models\Dooring::find($iddooring);
        $kotapelabuhan = Models\Pelabuhan::leftJoin('city','city.id_kota','=','pelabuhan.id_kota')
                                          ->selectRaw('distinct(city.id_kota) as id_kota,nama_kota')
                                          ->orderBy('city.nama_kota','asc')
                                          ->get();
        return view('master.detaildooringadd',compact('mode','dooring','kotapelabuhan'));
    }

    public function createDetail($iddooring, Request $request){
        Validator::make($request->all(), [
            'cboKotaPelabuhanAsal' => 'required|not_in:0',
            'cboKotaPelabuhanTujuan' => 'required|not_in:0',

        ],[
            'cboKotaPelabuhanAsal.not_in' => 'kota pelabuhan asal harus dipilih',
            'cboKotaPelabuhanTujuan.not_in' => 'tujuan kota harus dipilih',
            ])->validate();

        DB::beginTransaction();
        try {
            $dooringdetail = new Models\DooringDetail;
            $dooringdetail->id_dooring = $iddooring;
            $dooringdetail->id_kota_pelabuhan = $request->input('cboKotaPelabuhanAsal');
            $dooringdetail->id_kota_tujuan = $request->input('cboKotaPelabuhanTujuan');
            $dooringdetail->keterangan = $request->input('txtKeterangan');
            $dooringdetail->save();

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->route('master.dooring.detail',$iddooring)
            ->with('success',('Data Saved'));
    }

    public function editDetail($iddooringdetail){
        $mode = 'EDIT';
        $dooringdetail = Models\DooringDetail::with('Dooring')->find($iddooringdetail);
        $kotapelabuhan = Models\Pelabuhan::leftJoin('city','city.id_kota','=','pelabuhan.id_kota')
                                          ->selectRaw('distinct(city.id_kota) as id_kota,nama_kota')
                                          ->orderBy('city.nama_kota','asc')
                                          ->get();
        return view('master.detaildooringadd',compact('mode','dooringdetail','kotapelabuhan'));
    }

    public function updateDetail($iddooringdetail, Request $request){
        Validator::make($request->all(), [
            'cboKotaPelabuhanAsal' => 'required|not_in:0',
            'cboKotaPelabuhanTujuan' => 'required|not_in:0',

        ],[
            'cboKotaPelabuhanAsal.not_in' => 'kota pelabuhan asal harus dipilih',
            'cboKotaPelabuhanTujuan.not_in' => 'tujuan kota harus dipilih',
            ])->validate();

        DB::beginTransaction();
        try {
            $dooringdetail = Models\DooringDetail::find($iddooringdetail);
            $dooringdetail->id_kota_pelabuhan = $request->input('cboKotaPelabuhanAsal');
            $dooringdetail->id_kota_tujuan = $request->input('cboKotaPelabuhanTujuan');
            $dooringdetail->keterangan = $request->input('txtKeterangan');
            $dooringdetail->save();

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->back()
            ->with('success',('Data Updated'));
    }

    public function deleteDetail($iddooringdetail, Request $request)
    {   
        DB::beginTransaction();
        try {
            $dooringdetail = Models\DooringDetail::find($iddooringdetail);
            $dooringdetail->delete();
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Detail Dooring Deleted.'
        ]);
    }
}
