<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models;
use Carbon\Carbon;
use Auth;
use PDF;

class invoiceController extends Controller
{
    public function index(){
   		return view('transaction.invoice');
    }

    public function getData(Request $request){
        $data = Models\Invoice::leftJoin('trip','invoice.id_trip','trip.id_trip')
                            ->leftJoin('customer','invoice.id_customer','customer.id_customer');

        $data = $data->select([
            'invoice.*',
            'trip.kode_trip',
            'customer.nama_pt as nama_customer',

        ]);

        if ($request->input('search')) {
            $data = $data->where('trip.kode_trip','LIKE','%'.$request->input('search').'%')
                ->orWhere('invoice.no_invoice','LIKE','%'.$request->input('search').'%')
                ->orWhere('invoice.tgl_invoice','LIKE','%'.$request->input('search').'%')
                ->orWhere('invoice.created_at','LIKE','%'.$request->input('search').'%')
                ->orWhere('invoice.verified_at','LIKE','%'.$request->input('search').'%');
        }

        $count = $data->count();

        if ($request->input('sort')) {
            $data = $data->orderBy($request->input('sort'),$request->input('order'))
                ->orderBy('invoice.id_invoice',$request->input('order'));
            
        } else {
            $data = $data->orderBy('invoice.created_at','desc');
        }
        if($request->input('limit')) {
            $data = $data->take($request->input('limit'))
            ->skip($request->input('offset'));
        }

        $data = $data->get();
        
    
        return response()->json([
                'total' => $count,
                'rows' => $data
            ]);
    }

    public function getDataTrip(){
        $data = Models\SuratJalan::select('suratjalan.id_kodetrip','order.id_customer','trip.kode_trip','customer.nama_pt', DB::raw('count(suratjalan.id_kodetrip) as total'))
                            ->leftJoin('trip','suratjalan.id_kodetrip','trip.id_trip')
                            ->leftJoin('order','suratjalan.id_order','order.id_order')
                            ->leftJoin('customer','order.id_customer','customer.id_customer')
                            ->whereNull('suratjalan.deleted_at')
                            ->where('suratjalan.type_suratjalan','=','CONTAINER')
                            ->groupBy('suratjalan.id_kodetrip','order.id_customer','trip.kode_trip','customer.nama_pt');

        $count = $data->count();


        $data = $data->get();
        
    
        return response()->json([
                'total' => $count,
                'rows' => $data
            ]);
    }
    
    public function add($idtrip,$idcustomer){
        $mode = 'ADD';
        $datainvoice = Models\SuratJalan::select('suratjalan.id_suratjalan','suratjalan.no_container','suratjalan.no_suratjalan','proposal.no_proposal','suratjalan.keterangan_total', 'proposaldetail.nominal_total','invoicedetail.id_invoice','invoicedetail.deleted_at',DB::raw('GROUP_CONCAT(bapb.no_bapb SEPARATOR ", ") AS no_bapb'))
                                         ->leftJoin('do','do.id_suratjalan','suratjalan.id_suratjalan')
                                         ->leftJoin('bapb','bapb.id_bapb','do.id_bapb')
                                         ->leftJoin('order','suratjalan.id_order','order.id_order')
                                         ->leftJoin('proposaldetail','proposaldetail.id_proposaldetail','order.id_proposaldetail')
                                         ->leftJoin('proposal','proposal.id_proposal','proposaldetail.id_proposal')
                                         ->leftJoin('invoicedetail','invoicedetail.id_suratjalan','suratjalan.id_suratjalan')
                                         ->where('suratjalan.id_kodetrip','=',$idtrip)
                                         ->where('order.id_customer','=',$idcustomer)
                                         ->where('suratjalan.type_suratjalan','=','CONTAINER')
                                         ->whereNull('suratjalan.deleted_at')
                                         ->whereNull('do.deleted_at')
                                         ->whereNull('bapb.deleted_at')
                                         ->whereNull('order.deleted_at')
                                         ->whereNull('proposal.deleted_at')
                                         ->whereNull('proposaldetail.deleted_at')
                                        
                                         ->where(function($q) {
                                            $q->whereNull('invoicedetail.id_invoice')
                                              ->orWhereNotNull('invoicedetail.deleted_at');
                                        })
                                         ->groupBy('suratjalan.id_suratjalan','suratjalan.no_container','suratjalan.no_suratjalan','proposal.no_proposal','suratjalan.keterangan_total', 'proposaldetail.nominal_total','invoicedetail.id_invoice','invoicedetail.deleted_at')
                                         ->get();
        $customer = Models\Customer::find($idcustomer);
        $ppn = Models\ppn::find(1);
        $trip = Models\Trip::find($idtrip);
        return view('transaction.invoiceadd',compact('mode','datainvoice','customer','trip','ppn'));
    }

    public function create(Request $request){
        // dd($idproposal);
        $validator = Validator::make($request->all(), [
            'tgl_invoice'  => 'required',

        ],['tgl_invoice.required'=>'tanggal invoice harus diisi',        
        ])->validate();
        
        $year = DB::select('select distinct SUBSTRING(EXTRACT(YEAR FROM CURRENT_DATE),3,4) as year from dual');
        $month = DB::select('select MONTH(NOW()) as month');
        $ceknoinvoice = DB::select('select count(*) as ada from invoice where no_invoice like "KWI'.$year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).'%"');
        // dd($ceknoinvoice[0]->ada);
        if($ceknoinvoice[0]->ada>0){
            $noinvoice = DB::select('select max(no_invoice) as noinvoice from invoice where no_invoice like "KWI'.$year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).'%"');
            $nobaru = (int)(ltrim(substr($noinvoice[0]->noinvoice,7), '0'));
            $nobaru++;
            $noinvoicebaru = "KWI".$year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).str_pad($nobaru,4,"0",STR_PAD_LEFT);
        }else{
            $nobaru = 1;
            $noinvoicebaru = "KWI".$year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).str_pad($nobaru,4,"0",STR_PAD_LEFT);
        }
        // dd($noinvoicebaru);
        
        DB::beginTransaction();
        try {
            $invoice = new Models\Invoice;
            $invoice->no_invoice = $noinvoicebaru;
            $invoice->id_trip = $request->input('id_trip');
            $invoice->id_customer = $request->input('id_customer');
            $invoice->tgl_invoice = Carbon::parse($request->input('tgl_invoice'))->format('Y/m/d H:i:s');
            $invoice->tax = str_replace( ',', '',$request->input('tax'));
            $invoice->include_tax = $request->input('include_tax');
            $invoice->total = str_replace( ',', '',$request->input('total'));
            $invoice->catatan = $request->input('catatan');
            $invoice->nominal_dibayar = 0;
            $invoice->status = 0;
            if($invoice->save()){
                $detail = $request->input('detail');
                foreach ($detail as $item) {
                        $cekinvoicedetail = DB::select('select count(*) as ada from invoicedetail where deleted_at is not null and id_suratjalan='.$item['id_suratjalan'].' ');
                        if($cekinvoicedetail[0]->ada>0){
                            // dd($cekinvoicedetail[0]->id_invoicedetail);
                            $idinvoicedetail = DB::select('select id_invoicedetail from invoicedetail where deleted_at is not null and id_suratjalan='.$item['id_suratjalan'].' ');
                            $invoicedetail = Models\InvoiceDetail::withTrashed()->find($idinvoicedetail[0]->id_invoicedetail);
                            $invoicedetail->deleted_at = null;
                            $invoicedetail->deleted_by = null;
                            $invoicedetail->biaya = str_replace( ',', '',$item['biaya']);
                            if($item['biaya_tambahan_1']!=null || $item['biaya_tambahan_1']!=""){
                                $invoicedetail->biaya_tambahan_1 = $item['biaya_tambahan_1'];
                                $invoicedetail->nominal_biaya_tambahan_1 = str_replace( ',', '',$item['nominal_biaya_tambahan_1']);
                            }else{
                                $invoicedetail->biaya_tambahan_1 = null;
                                $invoicedetail->nominal_biaya_tambahan_1 = null;
                            }
                            if($item['biaya_tambahan_2']!=null || $item['biaya_tambahan_2']!=""){
                                $invoicedetail->biaya_tambahan_2 = $item['biaya_tambahan_2'];
                                $invoicedetail->nominal_biaya_tambahan_2 = str_replace( ',', '',$item['nominal_biaya_tambahan_2']);
                            }else{
                                $invoicedetail->biaya_tambahan_2 = null;
                                $invoicedetail->nominal_biaya_tambahan_2 =null;
                            }
                            if($item['biaya_tambahan_3']!=null || $item['biaya_tambahan_3']!=""){
                                $invoicedetail->biaya_tambahan_3 = $item['biaya_tambahan_3'];
                                $invoicedetail->nominal_biaya_tambahan_3 = str_replace( ',', '',$item['nominal_biaya_tambahan_3']);
                            }else{
                                $invoicedetail->biaya_tambahan_3 = null;
                                $invoicedetail->nominal_biaya_tambahan_3 = null;
                            }
                            $invoicedetail->total = str_replace( ',', '',$item['total']);
                            $invoicedetail->save();
                        }else{
                            $invoicedetail = new Models\InvoiceDetail;
                            $invoicedetail->id_invoice = $invoice->id_invoice;
                            $invoicedetail->no_invoice = $invoice->no_invoice;
                            $invoicedetail->id_suratjalan = $item['id_suratjalan'];
                            $invoicedetail->no_container = $item['no_container'];
                            $invoicedetail->biaya = str_replace( ',', '',$item['biaya']);
                            if($item['biaya_tambahan_1']!=null || $item['biaya_tambahan_1']!=""){
                                $invoicedetail->biaya_tambahan_1 = $item['biaya_tambahan_1'];
                                $invoicedetail->nominal_biaya_tambahan_1 = str_replace( ',', '',$item['nominal_biaya_tambahan_1']);
                            }else{
                                $invoicedetail->biaya_tambahan_1 = null;
                                $invoicedetail->nominal_biaya_tambahan_1 = null;
                            }
                            if($item['biaya_tambahan_2']!=null || $item['biaya_tambahan_2']!=""){
                                $invoicedetail->biaya_tambahan_2 = $item['biaya_tambahan_2'];
                                $invoicedetail->nominal_biaya_tambahan_2 = str_replace( ',', '',$item['nominal_biaya_tambahan_2']);
                            }else{
                                $invoicedetail->biaya_tambahan_2 = null;
                                $invoicedetail->nominal_biaya_tambahan_2 =null;
                            }
                            if($item['biaya_tambahan_3']!=null || $item['biaya_tambahan_3']!=""){
                                $invoicedetail->biaya_tambahan_3 = $item['biaya_tambahan_3'];
                                $invoicedetail->nominal_biaya_tambahan_3 = str_replace( ',', '',$item['nominal_biaya_tambahan_3']);
                            }else{
                                $invoicedetail->biaya_tambahan_3 = null;
                                $invoicedetail->nominal_biaya_tambahan_3 = null;
                            }
                            $invoicedetail->total = str_replace( ',', '',$item['total']);
                            $invoicedetail->save();
                        }
                }
            }

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        $request->session()->flash('success', 'Invoice Tersimpan!');
        return response()->json([
            'status' => 'success',
            'description' => 'Invoice Tersimpan!'
        ]);
    }

    public function verify($idinvoice)
    {   
        DB::beginTransaction();
        try {
            $invoice = Models\Invoice::find($idinvoice);
            $invoice->status = 1;
            $invoice->verified_by = Auth::user()->username;
            $invoice->verified_at = Carbon::now();
            $invoice->save();
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Invoice Verified!'
        ]);
    }

    public function delete($idinvoice, Request $request)
    {   
        DB::beginTransaction();
        try {
            $invoice = Models\Invoice::find($idinvoice);
            $invoice->delete();
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Invoice Deleted.'
        ]);
    }

    public function edit($idinvoice,$idtrip,$idcustomer){
        $mode = 'EDIT';
        $invoice = Models\Invoice::find($idinvoice);
        $datainvoicemasuk = Models\SuratJalan::select('suratjalan.id_suratjalan','suratjalan.no_container','suratjalan.no_suratjalan','proposal.no_proposal','suratjalan.keterangan_total', 'proposaldetail.nominal_total','invoicedetail.id_invoice','invoicedetail.id_invoicedetail','invoicedetail.biaya','invoicedetail.biaya_tambahan_1','invoicedetail.nominal_biaya_tambahan_1','invoicedetail.biaya_tambahan_2','invoicedetail.nominal_biaya_tambahan_2','invoicedetail.biaya_tambahan_3','invoicedetail.nominal_biaya_tambahan_3','invoicedetail.total','invoicedetail.deleted_at',DB::raw('GROUP_CONCAT(bapb.no_bapb SEPARATOR ", ") AS no_bapb'))
                                                                ->leftJoin('do','do.id_suratjalan','suratjalan.id_suratjalan')
                                                                ->leftJoin('bapb','bapb.id_bapb','do.id_bapb')
                                                                ->leftJoin('order','suratjalan.id_order','order.id_order')
                                                                ->leftJoin('proposaldetail','proposaldetail.id_proposaldetail','order.id_proposaldetail')
                                                                ->leftJoin('proposal','proposal.id_proposal','proposaldetail.id_proposal')
                                                                ->leftJoin('invoicedetail','invoicedetail.id_suratjalan','suratjalan.id_suratjalan')
                                                                ->where('suratjalan.id_kodetrip','=',$idtrip)
                                                                ->where('order.id_customer','=',$idcustomer)
                                                                ->where('suratjalan.type_suratjalan','=','CONTAINER')
                                                                ->where('invoicedetail.id_invoice','=',$idinvoice)
                                                                ->whereNull('suratjalan.deleted_at')
                                                                ->whereNull('do.deleted_at')
                                                                ->whereNull('bapb.deleted_at')
                                                                ->whereNull('order.deleted_at')
                                                                ->whereNull('proposaldetail.deleted_at')
                                                                ->whereNull('invoicedetail.deleted_at')
                                                            //  ->where(function($q) {
                                                            //     $q->whereNull('invoicedetail.id_invoice')
                                                            //       ->orWhereNotNull('invoicedetail.deleted_at');
                                                            // })
                                                                ->groupBy('suratjalan.id_suratjalan','suratjalan.no_container','suratjalan.no_suratjalan','proposal.no_proposal','suratjalan.keterangan_total', 'proposaldetail.nominal_total','invoicedetail.id_invoice','invoicedetail.id_invoicedetail','invoicedetail.biaya','invoicedetail.biaya_tambahan_1','invoicedetail.nominal_biaya_tambahan_1','invoicedetail.biaya_tambahan_2','invoicedetail.nominal_biaya_tambahan_2','invoicedetail.biaya_tambahan_3','invoicedetail.nominal_biaya_tambahan_3','invoicedetail.total','invoicedetail.deleted_at');

        $datainvoice = Models\SuratJalan::select('suratjalan.id_suratjalan','suratjalan.no_container','suratjalan.no_suratjalan','proposal.no_proposal','suratjalan.keterangan_total', 'proposaldetail.nominal_total','invoicedetail.id_invoice','invoicedetail.id_invoicedetail','invoicedetail.biaya','invoicedetail.biaya_tambahan_1','invoicedetail.nominal_biaya_tambahan_1','invoicedetail.biaya_tambahan_2','invoicedetail.nominal_biaya_tambahan_2','invoicedetail.biaya_tambahan_3','invoicedetail.nominal_biaya_tambahan_3','invoicedetail.total','invoicedetail.deleted_at',DB::raw('GROUP_CONCAT(bapb.no_bapb SEPARATOR ", ") AS no_bapb'))
                                         ->leftJoin('do','do.id_suratjalan','suratjalan.id_suratjalan')
                                         ->leftJoin('bapb','bapb.id_bapb','do.id_bapb')
                                         ->leftJoin('order','suratjalan.id_order','order.id_order')
                                         ->leftJoin('proposaldetail','proposaldetail.id_proposaldetail','order.id_proposaldetail')
                                         ->leftJoin('proposal','proposal.id_proposal','proposaldetail.id_proposal')
                                         ->leftJoin('invoicedetail','invoicedetail.id_suratjalan','suratjalan.id_suratjalan')
                                         ->where('suratjalan.id_kodetrip','=',$idtrip)
                                         ->where('order.id_customer','=',$idcustomer)
                                         ->where('suratjalan.type_suratjalan','=','CONTAINER')
                                         ->whereNull('suratjalan.deleted_at')
                                         ->whereNull('do.deleted_at')
                                         ->whereNull('bapb.deleted_at')
                                         ->whereNull('order.deleted_at')
                                         ->whereNull('proposaldetail.deleted_at')
                                         ->where(function($q) {
                                            $q->whereNull('invoicedetail.id_invoice')
                                              ->orWhereNotNull('invoicedetail.deleted_at');
                                        })
                                         ->groupBy('suratjalan.id_suratjalan','suratjalan.no_container','suratjalan.no_suratjalan','proposal.no_proposal','suratjalan.keterangan_total', 'proposaldetail.nominal_total','invoicedetail.id_invoice','invoicedetail.id_invoicedetail','invoicedetail.biaya','invoicedetail.biaya_tambahan_1','invoicedetail.nominal_biaya_tambahan_1','invoicedetail.biaya_tambahan_2','invoicedetail.nominal_biaya_tambahan_2','invoicedetail.biaya_tambahan_3','invoicedetail.nominal_biaya_tambahan_3','invoicedetail.total','invoicedetail.deleted_at')
                                         ->union($datainvoicemasuk)
                                         ->get();
                                        //  dd($datainvoice);
        $customer = Models\Customer::find($idcustomer);
        $ppn = Models\ppn::find(1);
        $trip = Models\Trip::find($idtrip);
        return view('transaction.invoiceadd',compact('mode','invoice','datainvoice','customer','trip','ppn'));
    }

    public function update(Request $request,$idinvoice){
        $validator = Validator::make($request->all(), [
            'tgl_invoice'  => 'required',

        ],['tgl_invoice.required'=>'tanggal invoice harus diisi',        
        ])->validate();
        
        DB::beginTransaction();
        try {
            $invoice = Models\Invoice::find($idinvoice);
            $invoice->tgl_invoice = Carbon::parse($request->input('tgl_invoice'))->format('Y/m/d H:i:s');
            $invoice->tax = str_replace( ',', '',$request->input('tax'));
            $invoice->include_tax = $request->input('include_tax');
            $invoice->total = str_replace( ',', '',$request->input('total'));
            $invoice->catatan = $request->input('catatan');
            if($invoice->save()){
                $detail = $request->input('detail');
                foreach ($detail as $item) {
                    if($item['id_invoicedetail']!='0'){
                        $invoicedetail = Models\InvoiceDetail::find($item['id_invoicedetail']);
                        $invoicedetail->biaya = str_replace( ',', '',$item['biaya']);
                        if($item['biaya_tambahan_1']!=null || $item['biaya_tambahan_1']!=""){
                            $invoicedetail->biaya_tambahan_1 = $item['biaya_tambahan_1'];
                            $invoicedetail->nominal_biaya_tambahan_1 = str_replace( ',', '',$item['nominal_biaya_tambahan_1']);
                        }else{
                            $invoicedetail->biaya_tambahan_1 = null;
                            $invoicedetail->nominal_biaya_tambahan_1 = null;
                        }
                        if($item['biaya_tambahan_2']!=null || $item['biaya_tambahan_2']!=""){
                            $invoicedetail->biaya_tambahan_2 = $item['biaya_tambahan_2'];
                            $invoicedetail->nominal_biaya_tambahan_2 = str_replace( ',', '',$item['nominal_biaya_tambahan_2']);
                        }else{
                            $invoicedetail->biaya_tambahan_2 = null;
                            $invoicedetail->nominal_biaya_tambahan_2 =null;
                        }
                        if($item['biaya_tambahan_3']!=null || $item['biaya_tambahan_3']!=""){
                            $invoicedetail->biaya_tambahan_3 = $item['biaya_tambahan_3'];
                            $invoicedetail->nominal_biaya_tambahan_3 = str_replace( ',', '',$item['nominal_biaya_tambahan_3']);
                        }else{
                            $invoicedetail->biaya_tambahan_3 = null;
                            $invoicedetail->nominal_biaya_tambahan_3 = null;
                        }
                        $invoicedetail->total = str_replace( ',', '',$item['total']);
                        $invoicedetail->save();
                    }else{
                        $cekinvoicedetail = DB::select('select count(*) as ada from invoicedetail where deleted_at is not null and id_suratjalan='.$item['id_suratjalan'].' ');
                        if($cekinvoicedetail[0]->ada>0){
                            // dd($cekinvoicedetail[0]->id_invoicedetail);
                            $idinvoicedetail = DB::select('select id_invoicedetail from invoicedetail where deleted_at is not null and id_suratjalan='.$item['id_suratjalan'].' ');
                            $invoicedetail = Models\InvoiceDetail::withTrashed()->find($idinvoicedetail[0]->id_invoicedetail);
                            $invoicedetail->deleted_at = null;
                            $invoicedetail->deleted_by = null;
                            $invoicedetail->id_invoice = $invoice->id_invoice;
                            $invoicedetail->no_invoice = $invoice->no_invoice;
                            $invoicedetail->biaya = str_replace( ',', '',$item['biaya']);
                            if($item['biaya_tambahan_1']!=null || $item['biaya_tambahan_1']!=""){
                                $invoicedetail->biaya_tambahan_1 = $item['biaya_tambahan_1'];
                                $invoicedetail->nominal_biaya_tambahan_1 = str_replace( ',', '',$item['nominal_biaya_tambahan_1']);
                            }else{
                                $invoicedetail->biaya_tambahan_1 = null;
                                $invoicedetail->nominal_biaya_tambahan_1 = null;
                            }
                            if($item['biaya_tambahan_2']!=null || $item['biaya_tambahan_2']!=""){
                                $invoicedetail->biaya_tambahan_2 = $item['biaya_tambahan_2'];
                                $invoicedetail->nominal_biaya_tambahan_2 = str_replace( ',', '',$item['nominal_biaya_tambahan_2']);
                            }else{
                                $invoicedetail->biaya_tambahan_2 = null;
                                $invoicedetail->nominal_biaya_tambahan_2 =null;
                            }
                            if($item['biaya_tambahan_3']!=null || $item['biaya_tambahan_3']!=""){
                                $invoicedetail->biaya_tambahan_3 = $item['biaya_tambahan_3'];
                                $invoicedetail->nominal_biaya_tambahan_3 = str_replace( ',', '',$item['nominal_biaya_tambahan_3']);
                            }else{
                                $invoicedetail->biaya_tambahan_3 = null;
                                $invoicedetail->nominal_biaya_tambahan_3 = null;
                            }
                            $invoicedetail->total = str_replace( ',', '',$item['total']);
                            $invoicedetail->save();
                        }else{
                            $invoicedetail = new Models\InvoiceDetail;
                            $invoicedetail->id_invoice = $invoice->id_invoice;
                            $invoicedetail->no_invoice = $invoice->no_invoice;
                            $invoicedetail->id_suratjalan = $item['id_suratjalan'];
                            $invoicedetail->no_container = $item['no_container'];
                            $invoicedetail->biaya = str_replace( ',', '',$item['biaya']);
                            if($item['biaya_tambahan_1']!=null || $item['biaya_tambahan_1']!=""){
                                $invoicedetail->biaya_tambahan_1 = $item['biaya_tambahan_1'];
                                $invoicedetail->nominal_biaya_tambahan_1 = str_replace( ',', '',$item['nominal_biaya_tambahan_1']);
                            }else{
                                $invoicedetail->biaya_tambahan_1 = null;
                                $invoicedetail->nominal_biaya_tambahan_1 = null;
                            }
                            if($item['biaya_tambahan_2']!=null || $item['biaya_tambahan_2']!=""){
                                $invoicedetail->biaya_tambahan_2 = $item['biaya_tambahan_2'];
                                $invoicedetail->nominal_biaya_tambahan_2 = str_replace( ',', '',$item['nominal_biaya_tambahan_2']);
                            }else{
                                $invoicedetail->biaya_tambahan_2 = null;
                                $invoicedetail->nominal_biaya_tambahan_2 =null;
                            }
                            if($item['biaya_tambahan_3']!=null || $item['biaya_tambahan_3']!=""){
                                $invoicedetail->biaya_tambahan_3 = $item['biaya_tambahan_3'];
                                $invoicedetail->nominal_biaya_tambahan_3 = str_replace( ',', '',$item['nominal_biaya_tambahan_3']);
                            }else{
                                $invoicedetail->biaya_tambahan_3 = null;
                                $invoicedetail->nominal_biaya_tambahan_3 = null;
                            }
                            $invoicedetail->total = str_replace( ',', '',$item['total']);
                            $invoicedetail->save();
                        }
                    }
                    
                }
            }

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        $request->session()->flash('success', 'Invoice Terupdate!');
        return response()->json([
            'status' => 'success',
            'description' => 'Invoice Terupdate!'
        ]);
    }

    public function deleteCont(Request $request,$iddetailinvoice){
        DB::beginTransaction();
        try {
            $invoicedetail = Models\InvoiceDetail::find($iddetailinvoice);
            $invoicedetail->delete();

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Container Dikeluarkan!'
        ]);
    }

    public function view($idinvoice,$idtrip,$idcustomer){
        $mode = 'VIEW';
        $invoice = Models\Invoice::find($idinvoice);
        $datainvoice = Models\SuratJalan::select('suratjalan.id_suratjalan','suratjalan.no_container','suratjalan.no_suratjalan','proposal.no_proposal','suratjalan.keterangan_total', 'proposaldetail.nominal_total','invoicedetail.id_invoice','invoicedetail.id_invoicedetail','invoicedetail.biaya','invoicedetail.biaya_tambahan_1','invoicedetail.nominal_biaya_tambahan_1','invoicedetail.biaya_tambahan_2','invoicedetail.nominal_biaya_tambahan_2','invoicedetail.biaya_tambahan_3','invoicedetail.nominal_biaya_tambahan_3','invoicedetail.total','invoicedetail.deleted_at',DB::raw('GROUP_CONCAT(bapb.no_bapb SEPARATOR ", ") AS no_bapb'))
                                         ->leftJoin('do','do.id_suratjalan','suratjalan.id_suratjalan')
                                         ->leftJoin('bapb','bapb.id_bapb','do.id_bapb')
                                         ->leftJoin('order','suratjalan.id_order','order.id_order')
                                         ->leftJoin('proposaldetail','proposaldetail.id_proposaldetail','order.id_proposaldetail')
                                         ->leftJoin('proposal','proposal.id_proposal','proposaldetail.id_proposal')
                                         ->leftJoin('invoicedetail','invoicedetail.id_suratjalan','suratjalan.id_suratjalan')
                                         ->where('suratjalan.id_kodetrip','=',$idtrip)
                                         ->where('order.id_customer','=',$idcustomer)
                                         ->whereNull('suratjalan.deleted_at')
                                         ->whereNull('do.deleted_at')
                                         ->whereNull('bapb.deleted_at')
                                         ->whereNull('order.deleted_at')
                                         ->whereNull('proposaldetail.deleted_at')
                                         ->where('invoicedetail.id_invoice','=',$idinvoice)
                                         ->whereNull('invoicedetail.deleted_at')
                                        //  ->where(function($q) {
                                        //     $q->whereNull('invoicedetail.id_invoice')
                                        //       ->orWhereNotNull('invoicedetail.deleted_at');
                                        // })
                                         ->groupBy('suratjalan.id_suratjalan','suratjalan.no_container','suratjalan.no_suratjalan','proposal.no_proposal','suratjalan.keterangan_total', 'proposaldetail.nominal_total','invoicedetail.id_invoice','invoicedetail.id_invoicedetail','invoicedetail.biaya','invoicedetail.biaya_tambahan_1','invoicedetail.nominal_biaya_tambahan_1','invoicedetail.biaya_tambahan_2','invoicedetail.nominal_biaya_tambahan_2','invoicedetail.biaya_tambahan_3','invoicedetail.nominal_biaya_tambahan_3','invoicedetail.total','invoicedetail.deleted_at')
                                         ->get();
                                        //  dd($datainvoice);
        $customer = Models\Customer::find($idcustomer);
        $ppn = Models\ppn::find(1);
        $trip = Models\Trip::find($idtrip);
        return view('transaction.invoiceadd',compact('mode','invoice','datainvoice','customer','trip','ppn'));
    }

    public function generatePDFInvoice()
    {
        // $listbapb = Models\BAPB::select('dooring.nama as nama_dooring','dooring.alamat','city.nama_kota','bapb.no_bapb','suratjalan.no_container','trip.kode_trip','trip.nama_kapal','trip.tanggal_berangkat','do.id_consignee','consignee.nama as nama_consignee','dodetail.nama_barang',DB::raw('SUM(dodetail.kolli) as total_kolli'))
        //                         ->leftJoin('dooring','dooring.id_dooring','bapb.id_dooring')
        //                         ->leftJoin('city','city.id_kota','dooring.id_kota')
        //                         ->leftJoin('trip','trip.id_trip','bapb.id_trip')
        //                         ->leftJoin('do','do.id_bapb','bapb.id_bapb')
        //                         ->leftJoin('dodetail','dodetail.id_do','do.id_do')
        //                         ->leftJoin('suratjalan','suratjalan.id_suratjalan','do.id_suratjalan')
        //                         ->leftJoin('consignee','consignee.id_consignee','do.id_consignee')
        //                         ->whereNull('bapb.tanggal_kembali')
        //                         ->where('bapb.id_dooring','=',$iddooring)
        //                         ->groupBy('dooring.nama','dooring.alamat','city.nama_kota','bapb.no_bapb','suratjalan.no_container','trip.kode_trip','trip.nama_kapal','trip.tanggal_berangkat','do.id_consignee','consignee.nama','dodetail.nama_barang')
        //                         ->get();

        // $data = ['iddooring' => $iddooring, 'listbapb' => $listbapb];
        // dd($listbapb[0]);
        setLocale(LC_TIME,'id');
        // $pdf = PDF::loadView('transaction.generate-bapb-not-back', $data)->setPaper('A4', 'portrait');
        $pdf = PDF::loadView('transaction.generate-invoice')->setPaper(array(0,0,595,421));
  
        return $pdf->download('invoice.pdf');
    }
    
}
