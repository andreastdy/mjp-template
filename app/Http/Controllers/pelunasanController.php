<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models;
use Carbon\Carbon;
use Auth;

class pelunasanController extends Controller
{
    public function index(){
   		return view('transaction.pelunasan');
    }

    // public function getData(){
    //     $datalcl = Models\Pelunasan::select('pelunasan.id_pelunasan','pelunasan.tgl_pelunasan','invoice_lcl.id_invoice_lcl as id_invoice','invoice_lcl.no_invoice_lcl as no_invoice','invoice_lcl.tgl_invoice_lcl','trip.kode_trip','invoice_lcl.id_customer','invoice_lcl.bukti_bayar','invoice_lcl.total','invoice_lcl.kode_penagihan',DB::Raw("'LCL' as type_invoice"))
    //                         ->leftJoin('invoice_lcl','invoice_lcl.id_invoice_lcl','pelunasan.id_invoice')
    //                         ->leftJoin('trip','trip.id_trip','invoice_lcl.id_trip')
    //                         ->where('pelunasan.tipe_invoice','=','LCL');

    //     $data = Models\Pelunasan::select('pelunasan.id_pelunasan','pelunasan.tgl_pelunasan','invoice.id_invoice','invoice.no_invoice','invoice.tgl_invoice','trip.kode_trip','invoice.id_customer','invoice.bukti_bayar','invoice.total','invoice.kode_penagihan',DB::Raw("'CONTAINER' as type_invoice"))
    //                         ->leftJoin('invoice','invoice.id_invoice','pelunasan.id_invoice')
    //                         ->leftJoin('trip','trip.id_trip','invoice.id_trip')
    //                         ->where('pelunasan.tipe_invoice','=','CONTAINER')
    //                         ->union($datalcl)
    //                         ->get();

    //     $count = $data->count();
        
    
    //     return response()->json([
    //             'total' => $count,
    //             'rows' => $data
    //         ]);
    // }

    public function getData(Request $request){
        $data = Models\MPelunasan::leftJoin('customer','m_pelunasan.id_customer','customer.id_customer');


        $data = $data->select([
            'm_pelunasan.*',
            'customer.nama_pt as nama_customer',

        ]);

        if ($request->input('search')) {
            $data = $data->where('m_pelunasan.kode_pelunasan','LIKE','%'.$request->input('search').'%')
                ->orWhere('m_pelunasan.tgl_pelunasan','LIKE','%'.$request->input('search').'%')
                ->orWhere('customer.nama_pt','LIKE','%'.$request->input('search').'%')
                ->orWhere('m_pelunasan.total_bayar','LIKE','%'.$request->input('search').'%')
                ->orWhere('m_pelunasan.bukti_bayar','LIKE','%'.$request->input('search').'%');;
        }
        
        $count = $data->count();

        if ($request->input('sort')) {
            $data = $data->orderBy($request->input('sort'),$request->input('order'))
                ->orderBy('m_pelunasan.kode_pelunasan',$request->input('order'));
            
        } else {
            $data = $data->orderBy('m_pelunasan.created_at','desc');
        }
        if($request->input('limit')) {
            $data = $data->take($request->input('limit'))
            ->skip($request->input('offset'));
        }

        $data = $data->get();
        
    
        return response()->json([
                'total' => $count,
                'rows' => $data
            ]);
    }

    public function getDataCustomer(){
        $data = Models\Customer::get();

        $count = $data->count();
        
    
        return response()->json([
                'total' => $count,
                'rows' => $data
            ]);
    }

    public function getDataInvoice(){
        $datalcl = Models\InvoiceLCL::select('invoice_lcl.id_invoice_lcl','invoice_lcl.no_invoice_lcl','invoice_lcl.tgl_invoice_lcl','trip.kode_trip','invoice_lcl.id_customer','customer.nama_pt as nama_customer','invoice_lcl.bukti_bayar','invoice_lcl.total','invoice_lcl.kode_penagihan',DB::Raw("'LCL' as type_invoice"))
                            ->leftJoin('trip','trip.id_trip','invoice_lcl.id_trip')
                            ->leftJoin('customer','customer.id_customer','invoice_lcl.id_customer')
                            ->whereNotNull('invoice_lcl.kode_penagihan')
                            ->where('invoice_lcl.status','=','1');

        $data = Models\Invoice::select('invoice.id_invoice','invoice.no_invoice','invoice.tgl_invoice','trip.kode_trip','invoice.id_customer','customer.nama_pt as nama_customer','invoice.bukti_bayar','invoice.total','invoice.kode_penagihan',DB::Raw("'CONTAINER' as type_invoice"))
                            ->leftJoin('trip','trip.id_trip','invoice.id_trip')
                            ->leftJoin('customer','customer.id_customer','invoice.id_customer')
                            ->whereNotNull('invoice.kode_penagihan')
                            ->where('invoice.status','=','1')
                            ->union($datalcl)
                            ->get();

        $count = $data->count();


        
    
        return response()->json([
                'total' => $count,
                'rows' => $data
            ]);
    }
    
    public function add($idcustomer){
        $mode='ADD';
        // $type_invoice = $typeinvoice;
        $customer = Models\Customer::find($idcustomer);
         $invoicelcl = Models\InvoiceLCL::select('invoice_lcl.id_invoice_lcl as id_invoice','invoice_lcl.no_invoice_lcl as no_invoice','invoice_lcl.tgl_invoice_lcl','invoice_lcl.tgl_penagihan','invoice_lcl.total as nominal_biaya','invoice_lcl.nominal_dibayar',DB::Raw("'LCL' as type_invoice"))
                                          ->whereNotNull('invoice_lcl.kode_penagihan')
                                          ->whereNull('invoice_lcl.deleted_at')
                                          ->where('invoice_lcl.status','=','1')
                                          ->where('invoice_lcl.id_customer','=',$idcustomer);
        $custominvoice = Models\CustomInvoice::select('custominvoice.id_custominvoice as id_invoice','custominvoice.no_custominvoice as no_invoice','custominvoice.tgl_custominvoice as tgl_invoice','custominvoice.tgl_penagihan','custominvoice.nominal_biaya','custominvoice.nominal_dibayar',DB::Raw("'CUSTOM' as type_invoice"))
                                        ->whereNotNull('custominvoice.kode_penagihan')
                                          ->whereNull('custominvoice.deleted_at')
                                          ->where('custominvoice.status','=','0')
                                          ->where('custominvoice.id_customer','=',$idcustomer);
        $invoice = Models\Invoice::select('invoice.id_invoice','invoice.no_invoice','invoice.tgl_invoice','invoice.tgl_penagihan','invoice.total as nominal_biaya','invoice.nominal_dibayar',DB::Raw("'CONTAINER' as type_invoice"))
                                    ->whereNotNull('invoice.kode_penagihan')
                                    ->whereNull('invoice.deleted_at')
                                    ->where('invoice.status','=','1')
                                    ->where('invoice.id_customer','=',$idcustomer)
                                    ->union($invoicelcl)
                                    ->union($custominvoice)
                                    ->orderBy('tgl_penagihan','asc')
                                    ->get();
        // dd($invoice);
        $extracostclaim = Models\ExtraCostClaim::where('id_customer',$idcustomer)->where('status',0)->orderBy('tgl_biaya','asc')->get();
        return view('transaction.pelunasanadd',compact('mode','invoice','customer','extracostclaim'));
    }

    public function create(Request $request){
        // dd($idproposal);
        $validator = Validator::make($request->all(), [
            'tgl_pelunasan'  => 'required',
            'pph23' => 'required',
            'admin_bank' => 'required',
            'lain_lain' => 'required',
            'total_bayar' => 'required'

        ],['tgl_pelunasan.required'=>'tanggal pelunasan harus diisi',     
            'pph23.required' => 'pph 23 harus diisi',
            'admin_bank.required' => 'biaya admin bank harus diisi',
            'lain_lain.required' => 'potongan lain-lain harus diisi',
            'total_bayar.required' => 'nominal dibayar harus diisi',
        ])->validate();

        $year = DB::select('select distinct SUBSTRING(EXTRACT(YEAR FROM CURRENT_DATE),3,4) as year from dual');
        $month = DB::select('select MONTH(NOW()) as month');
        $cekkodepelunasan = DB::select('select count(*) as ada from m_pelunasan where kode_pelunasan like "PEL'.$year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).'%"');
        // dd($ceknoinvoice[0]->ada);
        if($cekkodepelunasan[0]->ada>0){
            $kodepelunasan = DB::select('select max(kode_pelunasan) as kodepelunasan from m_pelunasan where kode_pelunasan like "PEL'.$year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).'%"');
            $nobaru = (int)(ltrim(substr($kodepelunasan[0]->kodepelunasan,7), '0'));
            $nobaru++;
            $kodepelunasanbaru = "PEL".$year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).str_pad($nobaru,4,"0",STR_PAD_LEFT);
        }else{
            $nobaru = 1;
            $kodepelunasanbaru = "PEL".$year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).str_pad($nobaru,4,"0",STR_PAD_LEFT);
        }

        // dd($kodepelunasanbaru);

        DB::beginTransaction();
        try {

            $pelunasan = new Models\MPelunasan;
            $pelunasan->kode_pelunasan = $kodepelunasanbaru;
            $pelunasan->id_customer = $request->input('id_customer');
            $pelunasan->tgl_pelunasan = Carbon::parse($request->input('tgl_pelunasan'))->format('Y/m/d H:i:s');
            $pelunasan->keterangan = $request->input('keterangan');
            $pelunasan->bukti_bayar = $request->input('bukti_bayar');
            $pelunasan->pph23 = $request->input('pph23');
            $pelunasan->admin_bank = $request->input('admin_bank');
            $pelunasan->lain_lain = $request->input('lain_lain');
            $pelunasan->total_bayar = $request->input('total_bayar');
            $pelunasan->status = 0;
            if($pelunasan->save()){
                $detail = $request->input('detail');
                foreach ($detail as $item) {
                    $dpelunasan = new Models\DPelunasan;
                    $dpelunasan->id_m_pelunasan = $pelunasan->id_m_pelunasan;
                    $dpelunasan->id_detail = $item['id_detail'];
                    $dpelunasan->no_detail = $item['no_detail'];
                    $dpelunasan->tipe_detail = $item['tipe_detail'];
                    $dpelunasan->nominal_detail = $item['nominal_detail'];
                    $dpelunasan->save();
                }
            }

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        $request->session()->flash('success', 'Pelunasan Tersimpan!');
        return response()->json([
            'status' => 'success',
            'description' => 'Pelunasan Tersimpan!'
        ]);
    }


    public function edit($idpelunasan,$idcustomer){
        $mode='EDIT';
        $pelunasan = Models\MPelunasan::find($idpelunasan);
        $dpelunasan = Models\DPelunasan::where('id_m_pelunasan','=',$idpelunasan)->pluck("no_detail")->toArray();
        // dd($dpelunasan);
        $customer = Models\Customer::find($idcustomer);
         $invoicelcl = Models\InvoiceLCL::select('invoice_lcl.id_invoice_lcl as id_invoice','invoice_lcl.no_invoice_lcl as no_invoice','invoice_lcl.tgl_invoice_lcl','invoice_lcl.tgl_penagihan','invoice_lcl.total as nominal_biaya','invoice_lcl.nominal_dibayar',DB::Raw("'LCL' as type_invoice"))
                                          ->whereNotNull('invoice_lcl.kode_penagihan')
                                          ->whereNull('invoice_lcl.deleted_at')
                                          ->where('invoice_lcl.status','=','1')
                                          ->where('invoice_lcl.id_customer','=',$idcustomer);
        $custominvoice = Models\CustomInvoice::select('custominvoice.id_custominvoice as id_invoice','custominvoice.no_custominvoice as no_invoice','custominvoice.tgl_custominvoice as tgl_invoice','custominvoice.tgl_penagihan','custominvoice.nominal_biaya','custominvoice.nominal_dibayar',DB::Raw("'CUSTOM' as type_invoice"))
                                        ->whereNotNull('custominvoice.kode_penagihan')
                                          ->whereNull('custominvoice.deleted_at')
                                          ->where('custominvoice.status','=','0')
                                          ->where('custominvoice.id_customer','=',$idcustomer);
        $invoice = Models\Invoice::select('invoice.id_invoice','invoice.no_invoice','invoice.tgl_invoice','invoice.tgl_penagihan','invoice.total as nominal_biaya','invoice.nominal_dibayar',DB::Raw("'CONTAINER' as type_invoice"))
                                    ->whereNotNull('invoice.kode_penagihan')
                                    ->whereNull('invoice.deleted_at')
                                    ->where('invoice.status','=','1')
                                    ->where('invoice.id_customer','=',$idcustomer)
                                    ->union($invoicelcl)
                                    ->union($custominvoice)
                                    ->orderBy('tgl_penagihan','asc')
                                    ->get();
        // dd($invoice);
        $extracostclaim = Models\ExtraCostClaim::where('id_customer',$idcustomer)->where('status',0)->orderBy('tgl_biaya','asc')->get();
        return view('transaction.pelunasanadd',compact('mode','invoice','customer','extracostclaim','pelunasan','dpelunasan'));
    }

    public function deleteInvoice(Request $request,$kodebiaya){
        DB::beginTransaction();
        try {
            $dpelunasan = Models\DPelunasan::where('no_detail','=',$kodebiaya)->first();
            // $dpelunasan->deleted_at = Carbon::now();
            // $dpelunasan->deleted_by = Auth::user()->username;
            $dpelunasan->delete();

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Invoice Dikeluarkan!'
        ]);
    }

    public function update(Request $request){
        // dd($idproposal);
        $validator = Validator::make($request->all(), [
            'tgl_pelunasan'  => 'required',
            'pph23' => 'required',
            'admin_bank' => 'required',
            'lain_lain' => 'required',
            'total_bayar' => 'required'

        ],['tgl_pelunasan.required'=>'tanggal pelunasan harus diisi',     
            'pph23.required' => 'pph 23 harus diisi',
            'admin_bank.required' => 'biaya admin bank harus diisi',
            'lain_lain.required' => 'potongan lain-lain harus diisi',
            'total_bayar.required' => 'nominal dibayar harus diisi',
        ])->validate();

        // dd($kodepelunasanbaru);

        DB::beginTransaction();
        try {

            $pelunasan = Models\MPelunasan::find($request->input('id_pelunasan'));            
            $pelunasan->tgl_pelunasan = Carbon::parse($request->input('tgl_pelunasan'))->format('Y/m/d H:i:s');
            $pelunasan->keterangan = $request->input('keterangan');
            $pelunasan->bukti_bayar  = $request->input('bukti_bayar');
            $pelunasan->pph23 = $request->input('pph23');
            $pelunasan->admin_bank = $request->input('admin_bank');
            $pelunasan->lain_lain = $request->input('lain_lain');
            $pelunasan->total_bayar = $request->input('total_bayar');
            if($pelunasan->save()){
                $detail = $request->input('detail');
                foreach ($detail as $item) {
                    $cekdetailpelunasan = DB::select('select count(*) as ada from d_pelunasan where deleted_at is not null and id_m_pelunasan='.$pelunasan->id_m_pelunasan.' and no_detail=\''.$item['no_detail'].'\'');
                        if($cekdetailpelunasan[0]->ada>0){
                            $iddetailpelunasan = DB::select('select id_d_pelunasan from d_pelunasan where deleted_at is not null and id_m_pelunasan='.$pelunasan->id_m_pelunasan.' and no_detail=\''.$item['no_detail'].'\'');
                            // dd($cekinvoicedetail[0]->id_invoicedetail);
                            $dpelunasan = Models\DPelunasan::withTrashed()->find($iddetailpelunasan[0]->id_d_pelunasan);
                            $dpelunasan->deleted_at = null;
                            $dpelunasan->deleted_by = null;
                            $dpelunasan->save();
                            
                        }else{
                            $cekpernahdetailpelunasan = DB::select('select count(*) as ada from d_pelunasan where deleted_at is null and id_m_pelunasan='.$pelunasan->id_m_pelunasan.' and no_detail=\''.$item['no_detail'].'\'');
                            if($cekpernahdetailpelunasan[0]->ada==0){
                                $dpelunasan = new Models\DPelunasan;
                                $dpelunasan->id_m_pelunasan = $pelunasan->id_m_pelunasan;
                                $dpelunasan->id_detail = $item['id_detail'];
                                $dpelunasan->no_detail = $item['no_detail'];
                                $dpelunasan->tipe_detail = $item['tipe_detail'];
                                $dpelunasan->nominal_detail = $item['nominal_detail'];
                                $dpelunasan->save();
                            }
                        }
                    
                    
                }
            }

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        $request->session()->flash('success', 'Pelunasan Tersimpan!');
        return response()->json([
            'status' => 'success',
            'description' => 'Pelunasan Tersimpan!'
        ]);
    }

    public function confirm(Request $request){
        // dd($idproposal);

        // dd($kodepelunasanbaru);

        DB::beginTransaction();
        try {

            $totalbayar = (int)$request->input('total_bayar') + (int)$request->input('deposit');

            $customer = Models\Customer::find($request->input('id_customer'));
            $customer->nominal_deposit = $request->input('sisa_deposit');
            $customer->save();

            $detail = $request->input('detail');
                foreach ($detail as $item) {
                    if($item['tipe_detail']=='EXTRACOST'){
                        $totalbayar = $totalbayar - (int)$item['nominal_detail'];
                        $extracost = Models\ExtraCostClaim::find($item['id_detail']);
                        $extracost->bukti_bayar = $extracost->bukti_bayar.$request->input('bukti_bayar').",";
                        $extracost->status=1;
                        $extracost->save();
                    }else if($item['tipe_detail']=='CLAIM'){
                        $totalbayar = $totalbayar + (int)$item['nominal_detail'];
                        $claim = Models\ExtraCostClaim::find($item['id_detail']);
                        $claim->bukti_bayar = $claim->bukti_bayar.$request->input('bukti_bayar').",";
                        $claim->status=1;
                        $claim->save();
                    }
                }

                foreach ($detail as $item) {
                    if($item['tipe_detail']=='CONTAINER'){

                        if($totalbayar>$item['nominal_detail']){
                            $totalbayar = $totalbayar - (int)$item['nominal_detail'];
                            $container = Models\Invoice::find($item['id_detail']);
                            $container->nominal_dibayar = $container->total; 
                            $container->bukti_bayar = $container->bukti_bayar.$request->input('bukti_bayar').",";
                            $container->status=2;
                            $container->save();
                        }else{
                            $container = Models\Invoice::find($item['id_detail']);
                            $container->nominal_dibayar = $container->nominal_dibayar + $totalbayar; 
                            $container->bukti_bayar = $container->bukti_bayar.$request->input('bukti_bayar').",";
                            $container->save();
                            $totalbayar = 0;
                        }
                        
                    }else if($item['tipe_detail']=='LCL'){

                        if($totalbayar>$item['nominal_detail']){
                            $totalbayar = $totalbayar - (int)$item['nominal_detail'];
                            $lcl = Models\InvoiceLCL::find($item['id_detail']);
                            $lcl->nominal_dibayar = $lcl->total; 
                            $lcl->bukti_bayar = $lcl->bukti_bayar.$request->input('bukti_bayar').",";
                            $lcl->status=2;
                            $lcl->save();
                        }else{
                            $lcl = Models\InvoiceLCL::find($item['id_detail']);
                            $lcl->nominal_dibayar = $lcl->nominal_dibayar + $totalbayar ; 
                            $lcl->bukti_bayar = $lcl->bukti_bayar.$request->input('bukti_bayar').",";
                            $lcl->save();
                            $totalbayar = 0;
                        }

                    }else if($item['tipe_detail']=='CUSTOM'){

                        if($totalbayar>$item['nominal_detail']){
                            $totalbayar = $totalbayar - (int)$item['nominal_detail'];
                            $custom = Models\CustomInvoice::find($item['id_detail']);
                            $custom->nominal_dibayar = $custom->total; 
                            $custom->bukti_bayar = $custom->bukti_bayar.$request->input('bukti_bayar').",";
                            $custom->status=1;
                            $custom->save();
                        }else{
                            $custom = Models\CustomInvoice::find($item['id_detail']);
                            $custom->nominal_dibayar = $custom->nominal_dibayar + $totalbayar; 
                            $custom->bukti_bayar = $custom->bukti_bayar.$request->input('bukti_bayar').",";
                            $custom->save();
                            $totalbayar = 0;
                        }
                        
                    }
                }
            
            $pelunasan = Models\MPelunasan::find($request->input('id_pelunasan')); 
            $pelunasan->status=1;
            $pelunasan->save();


        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        $request->session()->flash('success', 'Pelunasan Terkonfirmasi!');
        return response()->json([
            'status' => 'success',
            'description' => 'Pelunasan Terkonfirmasi!'
        ]);
    }
}
