<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models;

class sectorController extends Controller
{
    public function index(){
   		return view('master.sector');
    }

    public function getData(Request $request){
        $data = new Models\Sector;

        if ($request->input('search')) {
            $data = $data->where('sector','LIKE','%'.$request->input('search').'%')
                ->orWhere('alamat','LIKE','%'.$request->input('search').'%');
        }

        $count = $data->count();
        
        if ($request->input('sort')) {
            $data = $data->orderBy($request->input('sort'),$request->input('order'))
                ->orderBy('id_sector',$request->input('order'));
            
        } else {
            $data = $data->orderBy('sector','asc');
        }
        if($request->input('limit')) {
            $data = $data->take($request->input('limit'))
            ->skip($request->input('offset'));
        }

        $data = $data->get();
        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }
    
    public function add(){
        $mode = 'ADD';
        return view('master.sectoradd',compact('mode'));
    }

    public function create(Request $request){
        Validator::make($request->all(), [
            'txtSector' => 'required',
            'txtAlamat' => 'required',

        ])->validate();

        DB::beginTransaction();
        try {

            $cekada = DB::select("select COUNT(1) AS ada FROM sector WHERE sector=?",
                                    [$request->input('txtSector')]);

            if ($cekada[0]->ada>0) {
                throw new \Exception("Sector yang sama sudah pernah diinputkan");
            }

            $sector = new Models\Sector;
            $sector->sector = $request->input('txtSector');
            $sector->alamat = $request->input('txtAlamat');
            $sector->keterangan = $request->input('txtKeterangan');
            $sector->save();

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput($request->all);
        }
        DB::commit();
        return redirect('master/sector')
            ->with('success',__('Data Saved'));
    }

    public function edit($idSector){
        $mode = 'EDIT';
        $sector = Models\Sector::find($idSector);
        return view('master.sectoradd',compact('mode','sector'));
    }

    public function update(Request $request,$idSector){
        Validator::make($request->all(), [
            'txtSector' => 'required',
            'txtAlamat' => 'required',

        ])->validate();

        DB::beginTransaction();
        try {
            $sector = Models\Sector::find($idSector);
            $sector->sector = $request->input('txtSector');
            $sector->alamat = $request->input('txtAlamat');
            $sector->keterangan = $request->input('txtKeterangan');
            $sector->save();

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->back()
            ->with('success',__('Data Updated'));
    }

    public function delete($idSector, Request $request)
    {   
        DB::beginTransaction();
        try {
            $sector = Models\Sector::find($idSector);
            $sector->delete();
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Sector Deleted.'
        ]);
    }

}
