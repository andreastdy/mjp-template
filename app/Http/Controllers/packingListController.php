<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models;
use Carbon\Carbon;
use Auth;

class packingListController extends Controller
{
    public function index(){
   		return view('transaction.packinglist');
    }

    public function getData(){
        // $data = DB::select('SELECT consignee.nama, trip.kode_trip,trip.nama_kapal,trip.tanggal_Berangkat COUNT(bapb.id_trip) AS total FROM bapb bapb
        //                     LEFT JOIN consignee consignee ON consignee.id_consignee=bapb.id_consignee
        //                     LEFT JOIN trip trip ON trip.id_trip=bapb.id_trip
        //                     GROUP BY consignee.nama, trip.kode_trip');
        $data = Models\BAPB::select('bapb.id_trip','bapb.id_dooring','order.id_pelayaran','pelayaran.name_full','trip.kode_trip','dooring.nama','trip.nama_kapal','trip.tanggal_berangkat', DB::raw('count(bapb.id_trip) as total'))
                            ->leftJoin('trip','bapb.id_trip','trip.id_trip')
                            ->leftJoin('dooring','bapb.id_dooring','dooring.id_dooring')
                            ->leftJoin('consignee','bapb.id_consignee','consignee.id_consignee')
                            ->leftJoin('order','bapb.id_order','order.id_order')
                            ->leftJoin('pelayaran','order.id_pelayaran','pelayaran.id_pelayaran')
                            ->whereNull('bapb.deleted_at')
                            ->whereNull('trip.deleted_at')
                            ->whereNull('dooring.deleted_at')
                            ->whereNull('consignee.deleted_at')
                            ->whereNull('order.deleted_at')
                            ->whereNull('pelayaran.deleted_at')
                            ->orderBy('trip.tanggal_berangkat', 'asc')
                            ->groupBy('bapb.id_trip','bapb.id_dooring','order.id_pelayaran','pelayaran.name_full','trip.kode_trip','dooring.nama','trip.nama_kapal','trip.tanggal_berangkat');

        $count = $data->count();


        $data = $data->get();
        
    
        return response()->json([
                'total' => $count,
                'rows' => $data
            ]);
    }

    public function getDataDetail($idtrip,$iddooring,$idpelayaran){

        $data = Models\BAPB::leftJoin('do','do.id_bapb','bapb.id_bapb')
                                    ->leftJoin('suratjalan','suratjalan.id_suratjalan','do.id_suratjalan')
                                    ->leftJoin('consignee','consignee.id_consignee','do.id_consignee')
                                    ->leftJoin('order','bapb.id_order','order.id_order')
                                    ->where('order.id_pelayaran','=',$idpelayaran)
                                    ->where('bapb.id_trip','=',$idtrip)
                                    ->where('bapb.id_dooring','=',$iddooring)
                                    ->orderBy('bapb.no_bapb', 'ASC');
        $count = $data->count();
        
        $data = $data->select([
            'bapb.no_bapb',
            'suratjalan.no_suratjalan',
            'do.no_do',
            'suratjalan.no_container',
            'suratjalan.seal',
            'consignee.nama as nama_consignee'
        ]);


        // if ($request->input('sort')) {
        //     $data = $data->orderBy($request->input('sort'),$request->input('order'))
        //         ->orderBy('consignee.id_consignee',$request->input('order'));
            
        // } else {
        //     $data = $data->orderBy('consignee.nama');
        // }
        // if($request->input('limit')) {
        //     $data = $data->take($request->input('limit'))
        //     ->skip($request->input('offset'));
        // }
        
        $data = $data->get();

        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }
    
    public function add(){
        return view('transaction.packinglistadd');
    }
    public function edit(){
        return view('transaction.packinglistadd');
    }
}
