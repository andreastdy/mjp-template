<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models;
use Carbon\Carbon;
use Auth;

class custominvoiceController extends Controller
{
    public function index(){
   	return view('transaction.custominvoice');
    }

    public function getData(Request $request){
        $data = Models\CustomInvoice::leftJoin('customer','custominvoice.id_customer','customer.id_customer');


        $data = $data->select([
            'custominvoice.*',
            'customer.nama_pt as nama_customer',

        ]);

        if ($request->input('search')) {
            $data = $data->where('custominvoice.no_custominvoice','LIKE','%'.$request->input('search').'%')
                ->orWhere('custominvoice.tgl_custominvoice','LIKE','%'.$request->input('search').'%')
                ->orWhere('customer.nama_pt','LIKE','%'.$request->input('search').'%')
                ->orWhere('custominvoice.keterangan_biaya','LIKE','%'.$request->input('search').'%')
                ->orWhere('custominvoice.nominal_biaya','LIKE','%'.$request->input('search').'%');
        }
        
        $count = $data->count();

        if ($request->input('sort')) {
            $data = $data->orderBy($request->input('sort'),$request->input('order'))
                ->orderBy('custominvoice.id_custominvoice',$request->input('order'));
            
        } else {
            $data = $data->orderBy('custominvoice.created_at','desc');
        }
        if($request->input('limit')) {
            $data = $data->take($request->input('limit'))
            ->skip($request->input('offset'));
        }

        $data = $data->get();
        
    
        return response()->json([
                'total' => $count,
                'rows' => $data
            ]);
    }
    
    public function add(){
        $mode = 'ADD';
        $customer = Models\Customer::Get();
        return view('transaction.custominvoiceadd',compact('mode','customer'));
    }

    public function create(Request $request){
        // dd($idproposal);
        Validator::make($request->all(), [
            'cboCustomer' => 'required|not_in:0',
            'txtTglNota' => 'required',
            'txtNominalBiaya' => 'required',

        ],[
            'cboCustomer.not_in'=>'customer harus dipilih',        
        ])->validate();


        DB::beginTransaction();
        try {
            $year = DB::select('select distinct SUBSTRING(EXTRACT(YEAR FROM CURRENT_DATE),3,4) as year from dual');
            $month = DB::select('select MONTH(NOW()) as month');
            $ceknocustominvoice = DB::select('select count(*) as ada from custominvoice where no_custominvoice like "CSTM'.$year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).'%" and deleted_at is null');
            // dd($ceknobapb[0]->ada);
            if($ceknocustominvoice[0]->ada>0){
                $nocustominvoice = DB::select('select max(no_custominvoice) as nocustominvoice from custominvoice where no_custominvoice like "CSTM'.$year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).'%" and deleted_at is null');
                $nobaru = (int)(ltrim(substr($nocustominvoice[0]->nocustominvoice,8), '0'));
                $nobaru++;
                $nocustominvoicebaru = 'CSTM'.$year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).str_pad($nobaru,4,"0",STR_PAD_LEFT);
            }else{
                $nobaru = 1;
                $nocustominvoicebaru = 'CSTM'.$year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).str_pad($nobaru,4,"0",STR_PAD_LEFT);
            }

            $custominvoice = new Models\CustomInvoice;
            $custominvoice->no_custominvoice = $nocustominvoicebaru;
            $custominvoice->tgl_custominvoice = Carbon::parse($request->input('txtTglNota'))->format('Y/m/d H:i:s');
            $custominvoice->id_customer = $request->input('cboCustomer');
            $custominvoice->keterangan_biaya = $request->input('txtKeteranganBiaya');
            $custominvoice->nominal_biaya = str_replace( ',', '',$request->input('txtNominalBiaya'));
            $custominvoice->nominal_dibayar = 0;
            $custominvoice->status = 0;
            $custominvoice->save();

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->route('transaction.custominvoice')
            ->with('success',('Data Saved'));
    }

    public function edit($idcustominvoice){
        $mode='EDIT';
        $custominvoice = Models\CustomInvoice::find($idcustominvoice);
        $customer = Models\Customer::Get();
        return view('transaction.custominvoiceadd',compact('mode','custominvoice','customer'));
    }

    public function update(Request $request,$idcustominvoice){
        // dd($idproposal);
        Validator::make($request->all(), [
            'cboCustomer' => 'required',
            'txtTglNota' => 'required',
            'txtNominalBiaya' => 'required',

        ],[
            'cboCustomer.not_in'=>'customer harus dipilih',        
        ])->validate();


        DB::beginTransaction();
        try {
            

            $custominvoice = Models\CustomInvoice::find($idcustominvoice);
            $custominvoice->tgl_custominvoice = Carbon::parse($request->input('txtTglNota'))->format('Y/m/d H:i:s');
            $custominvoice->id_customer = $request->input('cboCustomer');
            $custominvoice->keterangan_biaya = $request->input('txtKeteranganBiaya');
            $custominvoice->nominal_biaya = str_replace( ',', '',$request->input('txtNominalBiaya'));
            $custominvoice->save();

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->back()
            ->with('success',('Data Updated'));
    }

    public function delete($idcustominvoice, Request $request)
    {   
        DB::beginTransaction();
        try {
            $custominvoice = Models\CustomInvoice::find($idcustominvoice);
            $custominvoice->delete();
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        // $request->session()->flash('success', 'Custom Invoice Dihapus!');
        return response()->json([
            'status' => 'success',
            'description' => 'Custom Invoice Dihapus!'
        ]);
    }
}
