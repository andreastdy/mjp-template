<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models;

class pelabuhanController extends Controller
{
    public function index(){
   	return view('setting.pelabuhan');
    }

    public function getData(Request $request){
        $data = Models\Pelabuhan::leftjoin('city','pelabuhan.id_kota','=','city.id_kota')->orderBy('city.nama_kota','asc');

        if ($request->input('search')) {
            $data = $data->where('pelabuhan.nama_pelabuhan','LIKE','%'.$request->input('search').'%')
                ->orWhere('city.nama_kota','LIKE','%'.$request->input('search').'%');
        }

        if ($request->input('sort')) {
            $data = $data->orderBy($request->input('sort'),$request->input('order'))
                ->orderBy('pelabuhan.id_pelabuhan',$request->input('order'));
            
        } else {
            $data = $data->orderBy('pelabuhan.nama_pelabuhan','asc');
        }
        if($request->input('limit')) {
            $data = $data->take($request->input('limit'))
            ->skip($request->input('offset'));
        }

        $data = $data->get();
        $count = $data->count();
        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }
    
    public function add(){
        $mode = 'ADD';
        $city = Models\City::orderBy('nama_kota','asc')->Get();
        return view('setting.pelabuhanadd',compact('city','mode'));
    }

    public function create(Request $request){
        Validator::make($request->all(), [
            'cboKota' => 'required|not_in:0',
            'txtNamaPelabuhan' => 'required',

        ],[
            'cboKota.not_in' => 'kota harus dipilih',
            ])->validate();

        DB::beginTransaction();
        try {
            $pelabuhan = new Models\Pelabuhan;
            $pelabuhan->id_kota = $request->input('cboKota');
            $pelabuhan->nama_pelabuhan = $request->input('txtNamaPelabuhan');
            $pelabuhan->save();


        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect('setting/pelabuhan')
            ->with('success',__('Data Saved'));
    }

    public function edit($idPelabuhan){
        $mode = 'EDIT';
        $city = Models\City::orderBy('nama_kota','asc')->Get();
        $pelabuhan = Models\Pelabuhan::find($idPelabuhan);
        return view('setting.pelabuhanadd', compact('mode','city','pelabuhan'));
    }

    public function update(Request $request, $idPelabuhan){
        Validator::make($request->all(), [
            'cboKota' => 'required|not_in:0',
            'txtNamaPelabuhan' => 'required',

        ],[
            'cboKota.not_in' => 'kota harus dipilih',
            ])->validate();

        DB::beginTransaction();
        try {
            $pelabuhan = Models\Pelabuhan::find($idPelabuhan);
            $pelabuhan->id_kota = $request->input('cboKota');
            $pelabuhan->nama_pelabuhan = $request->input('txtNamaPelabuhan');
            $pelabuhan->save();


        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->back()
            ->with('success',__('Data Updated'));
    }

    public function delete($idPelabuhan, Request $request)
    {   
        DB::beginTransaction();
        try {
            $pelabuhan = Models\Pelabuhan::find($idPelabuhan);
            $pelabuhan->delete();
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Pelabuhan Deleted.'
        ]);
    }
}
