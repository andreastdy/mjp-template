<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models;
use Carbon\Carbon;
use Auth;

class customerOrderController extends Controller
{
    public function index(){
   		return view('transaction.customerorder');
    }

    public function getData(Request $request){
        
        $data = Models\Order::leftJoin('pelabuhan as pelabuhan_asal','pelabuhan_asal.id_pelabuhan','order.id_pelabuhan_asal')
                                    ->leftJoin('pelabuhan as pelabuhan_tujuan','pelabuhan_tujuan.id_pelabuhan','order.id_pelabuhan_tujuan')
                                    ->leftJoin('city as city_pelabuhan_asal','city_pelabuhan_asal.id_kota','pelabuhan_asal.id_kota')
                                    ->leftJoin('city as city_pelabuhan_tujuan','city_pelabuhan_tujuan.id_kota','pelabuhan_tujuan.id_kota')
                                    ->leftJoin('city as city_asal','city_asal.id_kota','order.id_kota_asal')
                                    ->leftJoin('city as city_tujuan','city_tujuan.id_kota','order.id_kota_tujuan')
                                    ->leftJoin('pelayaran','pelayaran.id_pelayaran','order.id_pelayaran')
                                    ->leftJoin('customer','customer.id_customer','order.id_customer');
        
        $data = $data->select([
            'order.*',
            'pelabuhan_asal.nama_pelabuhan as nama_pelabuhan_asal',
            'pelabuhan_tujuan.nama_pelabuhan as nama_pelabuhan_tujuan',
            'city_pelabuhan_asal.nama_kota as nama_kotapelabuhan_asal',
            'city_pelabuhan_tujuan.nama_kota as nama_kotapelabuhan_tujuan',
            'city_asal.nama_kota as nama_kota_asal',
            'city_tujuan.nama_kota as nama_kota_tujuan',
            'pelayaran.name_full as nama_pelayaran',
            'customer.nama_pt as nama_customer'
        ]);

        if ($request->input('search')) {
            $data = $data->where('customer.nama_pt','LIKE','%'.$request->input('search').'%')
                ->orWhere('city_asal.nama_kota','LIKE','%'.$request->input('search').'%')
                ->orWhere('city_pelabuhan_asal.nama_kota','LIKE','%'.$request->input('search').'%')
                ->orWhere('pelabuhan_asal.nama_pelabuhan','LIKE','%'.$request->input('search').'%')
                ->orWhere('city_tujuan.nama_kota','LIKE','%'.$request->input('search').'%')
                ->orWhere('city_pelabuhan_tujuan.nama_kota','LIKE','%'.$request->input('search').'%')
                ->orWhere('pelabuhan_tujuan.nama_pelabuhan','LIKE','%'.$request->input('search').'%')
                ->orWhere('order.type_order','LIKE','%'.$request->input('search').'%')
                ->orWhere('order.created_at','LIKE','%'.$request->input('search').'%')
                ->orWhere('order.created_by','LIKE','%'.$request->input('search').'%')
                ->orWhere('order.executed_at','LIKE','%'.$request->input('search').'%')
                ->orWhere('order.executed_by','LIKE','%'.$request->input('search').'%');
        }
        $count = $data->count();

        if ($request->input('sort')) {
            $data = $data->orderBy($request->input('sort'),$request->input('order'))
                ->orderBy('order.id_order',$request->input('order'));
            
        } else {
            $data = $data->orderBy('order.created_at','desc');
        }
        if($request->input('limit')) {
            $data = $data->take($request->input('limit'))
            ->skip($request->input('offset'));
        }
        
        $data = $data->get();
        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }

    public function getDataProposal(){
        // $data = Models\PelayaranDetail::with('DetailBongkar')->with('DetailAsal')
        $data = Models\ProposalDetail::rightJoin('proposal','proposal.id_proposal','proposaldetail.id_proposal')
                                    ->leftJoin('pelabuhan as pelabuhan_asal','pelabuhan_asal.id_pelabuhan','proposaldetail.id_pelabuhan_asal')
                                    ->leftJoin('pelabuhan as pelabuhan_tujuan','pelabuhan_tujuan.id_pelabuhan','proposaldetail.id_pelabuhan_tujuan')
                                    ->leftJoin('city as city_asal','city_asal.id_kota','proposaldetail.id_kota_asal')
                                    ->leftJoin('city as city_tujuan','city_tujuan.id_kota','proposaldetail.id_kota_tujuan')
                                    ->leftJoin('customer','customer.id_customer','proposal.id_customer')
                                    ->leftJoin('consignee','proposaldetail.id_consignee','consignee.id_consignee')
                                    ->where('proposal.status','=','3')
                                    ->whereRaw('proposaldetail.valid_to > DATE(SYSDATE())');
        
        $data = $data->select([
            'proposaldetail.*',
            'proposal.*',
            'pelabuhan_asal.nama_pelabuhan as nama_pelabuhan_asal',
            'pelabuhan_tujuan.nama_pelabuhan as nama_pelabuhan_tujuan',
            'city_asal.nama_kota as nama_kota_asal',
            'city_tujuan.nama_kota as nama_kota_tujuan',
            'customer.nama_pt as nama_customer',
            'consignee.nama as nama_consignee'
            // DB::raw( "STR_TO_DATE(proposaldetail.valid_to,'%d/%m/%Y') as valid_to")
            // DB::raw("CASE WHEN DATE(SYSDATE())<=proposal.valid_to THEN '<font color=green>ACTIVE</font>' ELSE '<font color=red>EXPIRED</font>' END AS validity")
        ]);

        $count = $data->count();

        // if ($request->input('sort')) {
        //     $data = $data->orderBy($request->input('sort'),$request->input('order'))
        //         ->orderBy('consignee.id_consignee',$request->input('order'));
            
        // } else {
        //     $data = $data->orderBy('consignee.nama');
        // }
        // if($request->input('limit')) {
        //     $data = $data->take($request->input('limit'))
        //     ->skip($request->input('offset'));
        // }
        
        $data = $data->get();
        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }
    
    public function add($idproposaldetail){
        $mode = 'ADD';
        $proposaldetail = Models\ProposalDetail::leftJoin('proposal','proposal.id_proposal','proposaldetail.id_proposal')
                                                ->leftJoin('pelayaran','pelayaran.id_pelayaran','proposaldetail.id_pelayaran')
                                                ->leftJoin('city as city_asal','city_asal.id_kota','proposaldetail.id_kota_asal')
                                                ->leftJoin('city as city_tujuan','city_tujuan.id_kota','proposaldetail.id_kota_tujuan')
                                                ->leftJoin('pelabuhan as pelabuhan_asal','pelabuhan_asal.id_pelabuhan','proposaldetail.id_pelabuhan_asal')
                                                ->leftJoin('pelabuhan as pelabuhan_tujuan','pelabuhan_tujuan.id_pelabuhan','proposaldetail.id_pelabuhan_tujuan')
                                                ->where('id_proposaldetail','=',$idproposaldetail);
        $proposaldetail = $proposaldetail->select([
            'proposaldetail.*',
            'proposal.*',
            'pelayaran.name_full as nama_pelayaran',
            'city_asal.id_kota as id_kota_asal',
            'city_tujuan.id_kota as id_kota_tujuan'
        ]);                                 
        $proposaldetail =   $proposaldetail->Get();               
        // dd($proposaldetail);                                                                                   
        $pelayaran = Models\Pelayaran::Get();
        $customer = Models\Customer::Get();
        $pelabuhan = Models\Pelabuhan::leftJoin('city', 'city.id_kota', '=', 'pelabuhan.id_kota')->orderBy('city.nama_kota','asc')->get();
        $city = Models\City::orderBy('nama_kota','asc')->Get();
        return view('transaction.customerorderadd',compact('mode','proposaldetail','pelayaran','pelabuhan','customer','city'));
    }

    public function create(Request $request, $idproposaldetail ){
        // dd($idproposal);
        Validator::make($request->all(), [
            'rdoType' => 'required',
            'cboCustomer' => 'required|not_in:0',
            'cboKotaAsal' => 'required|not_in:0',
            'cboPortAsal' => 'required|not_in:0',
            'cboKotaTujuan' => 'required|not_in:0',
            'cboPortTujuan' => 'required|not_in:0',
            'cboPelayaran' => 'required|not_in:0',
            'cboJenisBarang' => 'required|not_in:0',
            'txtAlamatAsal' => 'required',
            'txtAlamatTujuan' => 'required',
            'txtHargaPelayaran' => 'required',
            'txtJadwal' => 'required',

        ],[
            'rdoType.required' => 'tipe proposal harus dipilih',
            'cboCustomer.not_in' => 'customer harus dipilih',
            'cboKotaAsal.not_in' => 'kota asal harus dipilih',
            'cboPortAsal.not_in' => 'pelabuhan asal harus dipilih',
            'cboKotaTujuan.not_in' => 'kota tujuan harus dipilih',
            'cboPortTujuan.not_in' => 'pelabuhan tujuan harus dipilih',
            'cboPelayaran.not_in' => 'pelayaran harus dipilih',
            'cboJenisBarang.not_in' => 'tipe muatan harus dipilih',
            ])->validate();


        DB::beginTransaction();
        try {
            $typeorder = $request->input('rdoType');

            


            // if($typeproposal=="CONTAINER"){
            //     $cekada = DB::select("select COUNT(1) AS ada FROM proposaldetail WHERE id_proposal=? and DATE(valid_from)=STR_TO_DATE(?,'%d/%m/%Y') AND DATE(valid_to)=STR_TO_DATE(?,'%d/%m/%Y') AND id_pelabuhan_asal=? AND id_pelabuhan_tujuan=? AND type_proposal=? AND  freight_size=?",
            //                         [$idproposal,Carbon::parse($request->input('txtValidFrom'))->format('d/m/Y'),Carbon::parse($request->input('txtValidTo'))->format('d/m/Y'),$request->input('cboPortAsal'),$request->input('cboPortTujuan'),$typeproposal,$request->input('rdoFtsize')]);
            // }else{
            //     $cekada = DB::select("select COUNT(1) AS ada FROM proposaldetail WHERE id_proposal=? and DATE(valid_from)=STR_TO_DATE(?,'%d/%m/%Y') AND DATE(valid_to)=STR_TO_DATE(?,'%d/%m/%Y') AND id_pelabuhan_asal=? AND id_pelabuhan_tujuan=? AND type_proposal=? AND  lcl_size=?",
            //                         [$idproposal, Carbon::parse($request->input('txtValidFrom'))->format('d/m/Y'),Carbon::parse($request->input('txtValidTo'))->format('d/m/Y'),$request->input('cboPortAsal'),$request->input('cboPortTujuan'),$typeproposal,$request->input('rdolclsize')]);
            // }

            // dd($cekada[0]->ada);

            // if ($cekada[0]->ada>0) {
            //     throw new \Exception("Detail Proposal yang sama sudah pernah diinputkan");
            // }


            $order = new Models\Order;
            $order->id_proposaldetail = $idproposaldetail;
            $order->type_order = $typeorder;
            $order->id_customer = $request->input('cboCustomer');
            $order->id_kota_asal = $request->input('cboKotaAsal');
            $order->id_pelabuhan_asal = $request->input('cboPortAsal');
            $order->id_kota_tujuan = $request->input('cboKotaTujuan');
            $order->id_pelabuhan_tujuan = $request->input('cboPortTujuan');
            $order->id_pelayaran = $request->input('cboPelayaran');
            $order->jenis_barang = $request->input('cboJenisBarang');
            $order->alamat_asal = $request->input('txtAlamatAsal');
            $order->alamat_tujuan = $request->input('txtAlamatTujuan');
            $order->nominal_pelayaran_order = str_replace( ',', '',$request->input('txtHargaPelayaran'));
            
            $order->jadwal_pengambilan = Carbon::parse($request->input('txtJadwal'))->format('Y/m/d H:i:s');

            if($typeorder=="CONTAINER"){
                $order->freight_size = $request->input('rdoFtsize');
                $order->jumlah_order = $request->input('txtJmlOrder');
                $order->nominal_optsurabaya = str_replace( ',', '',$request->input('txtHargaOptSby'));
                $order->nominal_thcp = str_replace( ',', '',$request->input('txtHargaTHCP')); 
                $order->nominal_thcd = str_replace( ',', '',$request->input('txtHargaTHCD'));
            }else{
                $order->lcl_size = $request->input('rdolclsize');
                if($request->input('rdolclsize')=="M3"){
                    $order->nominal_m3 = str_replace( ',', '',$request->input('txtHargam3'));
                }else{
                    $order->nominal_ton = str_replace( ',', '',$request->input('txtHargaton'));
                }
            }

            $order->status=0;
            $order->save();

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->route('transaction.customerorder')
            ->with('success',('Data Saved'));
    }

    public function delete($idorder, Request $request)
    {   
        DB::beginTransaction();
        try {
            $order = Models\Order::find($idorder);
            $order->delete();
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Order Deleted.'
        ]);
    }

    public function execute($idorder)
    {   
        DB::beginTransaction();
        try {
            $order = Models\Order::find($idorder);
            $order->status = 1;
            $order->executed_by = Auth::user()->username;
            $order->executed_at = Carbon::now();
            $order->save();
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Order Executed!'
        ]);
    }

    public function edit($idorder){
        $mode = 'EDIT';
        $order = Models\Order::find($idorder);
        // $proposaldetail = Models\ProposalDetail::leftJoin('proposal','proposal.id_proposal','proposaldetail.id_proposal')
        //                                         ->leftJoin('pelayaran','pelayaran.id_pelayaran','proposaldetail.id_pelayaran')
        //                                         ->where('proposaldetail.id_proposaldetail','=',$order->id_proposaldetail);
        // $proposaldetail = $proposaldetail->select([
        //     'proposaldetail.*',
        //     'proposal.*',
        //     'pelayaran.name_full as nama_pelayaran'
        // ]);                                 
        // $proposaldetail =   $proposaldetail->Get();    
        $pelayaran = Models\Pelayaran::Get();
        $customer = Models\Customer::Get();
        $pelabuhan = Models\Pelabuhan::leftJoin('city', 'city.id_kota', '=', 'pelabuhan.id_kota')->orderBy('city.nama_kota','asc')->get();
        $city = Models\City::orderBy('nama_kota','asc')->Get();
        return view('transaction.customerorderadd',compact('mode','order','proposaldetail','pelayaran','customer','pelabuhan','city'));
    }

    public function update(Request $request, $idorder ){
        // dd($idproposal);
        Validator::make($request->all(), [
            'rdoType' => 'required',
            'cboCustomer' => 'required|not_in:0',
            'cboKotaAsal' => 'required|not_in:0',
            'cboPortAsal' => 'required|not_in:0',
            'cboKotaTujuan' => 'required|not_in:0',
            'cboPortTujuan' => 'required|not_in:0',
            'cboPelayaran' => 'required|not_in:0',
            'cboJenisBarang' => 'required|not_in:0',
            'txtAlamatAsal' => 'required',
            'txtAlamatTujuan' => 'required',
            'txtHargaPelayaran' => 'required',
            'txtJadwal' => 'required',

        ],[
            'rdoType.required' => 'tipe proposal harus dipilih',
            'cboCustomer.not_in' => 'customer harus dipilih',
            'cboKotaAsal.not_in' => 'kota asal harus dipilih',
            'cboPortAsal.not_in' => 'pelabuhan asal harus dipilih',
            'cboKotaTujuan.not_in' => 'kota tujuan harus dipilih',
            'cboPortTujuan.not_in' => 'pelabuhan tujuan harus dipilih',
            'cboPelayaran.not_in' => 'pelayaran harus dipilih',
            'cboJenisBarang.not_in' => 'tipe muatan harus dipilih',
            ])->validate();


        DB::beginTransaction();
        try {
            $typeorder = $request->input('rdoType');

            


            // if($typeproposal=="CONTAINER"){
            //     $cekada = DB::select("select COUNT(1) AS ada FROM proposaldetail WHERE id_proposal=? and DATE(valid_from)=STR_TO_DATE(?,'%d/%m/%Y') AND DATE(valid_to)=STR_TO_DATE(?,'%d/%m/%Y') AND id_pelabuhan_asal=? AND id_pelabuhan_tujuan=? AND type_proposal=? AND  freight_size=?",
            //                         [$idproposal,Carbon::parse($request->input('txtValidFrom'))->format('d/m/Y'),Carbon::parse($request->input('txtValidTo'))->format('d/m/Y'),$request->input('cboPortAsal'),$request->input('cboPortTujuan'),$typeproposal,$request->input('rdoFtsize')]);
            // }else{
            //     $cekada = DB::select("select COUNT(1) AS ada FROM proposaldetail WHERE id_proposal=? and DATE(valid_from)=STR_TO_DATE(?,'%d/%m/%Y') AND DATE(valid_to)=STR_TO_DATE(?,'%d/%m/%Y') AND id_pelabuhan_asal=? AND id_pelabuhan_tujuan=? AND type_proposal=? AND  lcl_size=?",
            //                         [$idproposal, Carbon::parse($request->input('txtValidFrom'))->format('d/m/Y'),Carbon::parse($request->input('txtValidTo'))->format('d/m/Y'),$request->input('cboPortAsal'),$request->input('cboPortTujuan'),$typeproposal,$request->input('rdolclsize')]);
            // }

            // dd($cekada[0]->ada);

            // if ($cekada[0]->ada>0) {
            //     throw new \Exception("Detail Proposal yang sama sudah pernah diinputkan");
            // }


            $order = Models\Order::find($idorder);
            
            // $order->id_proposal = $idproposal;
            $order->type_order = $typeorder;
            $order->id_customer = $request->input('cboCustomer');
            $order->id_kota_asal = $request->input('cboKotaAsal');
            $order->id_pelabuhan_asal = $request->input('cboPortAsal');
            $order->id_kota_tujuan = $request->input('cboKotaTujuan');
            $order->id_pelabuhan_tujuan = $request->input('cboPortTujuan');
            $order->id_pelayaran = $request->input('cboPelayaran');
            $order->jenis_barang = $request->input('cboJenisBarang');
            $order->alamat_asal = $request->input('txtAlamatAsal');
            $order->alamat_tujuan = $request->input('txtAlamatTujuan');
            $order->nominal_pelayaran_order = str_replace( ',', '',$request->input('txtHargaPelayaran'));            
            $order->jadwal_pengambilan = Carbon::parse($request->input('txtJadwal'))->format('Y/m/d H:i:s');
            if($typeorder=="CONTAINER"){
                $order->freight_size = $request->input('rdoFtsize');
                $order->jumlah_order = $request->input('txtJmlOrder');
                $order->nominal_optsurabaya = str_replace( ',', '',$request->input('txtHargaOptSby'));
                $order->nominal_thcp = str_replace( ',', '',$request->input('txtHargaTHCP'));
                $order->nominal_thcd = str_replace( ',', '',$request->input('txtHargaTHCD'));
            }else{
                $order->lcl_size = $request->input('rdolclsize');
                if($request->input('rdolclsize')=="M3"){
                    $order->nominal_m3 = str_replace( ',', '',$request->input('txtHargam3'));
                }else{
                    $order->nominal_ton = str_replace( ',', '',$request->input('txtHargaton'));
                }
            }
            
            $order->save();

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->back()
            ->with('success',('Data Updated'));
    }

    public function detail($idorder){
        $mode = 'VIEW';
        $order = Models\Order::find($idorder);
        $pelayaran = Models\Pelayaran::Get();
        $customer = Models\Customer::Get();
        $pelabuhan = Models\Pelabuhan::leftJoin('city', 'city.id_kota', '=', 'pelabuhan.id_kota')->orderBy('city.nama_kota','asc')->get();
        $city = Models\City::orderBy('nama_kota','asc')->Get();
        return view('transaction.customerorderadd',compact('mode','order','pelayaran','customer','pelabuhan','city'));
    }
    
}
