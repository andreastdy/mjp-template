<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models;
use Carbon\Carbon;

class tripController extends Controller
{
    public function index(){
   		return view('master.trip');
    }

    public function getData(Request $request){
        $data = Models\Trip::leftJoin('pelabuhan','pelabuhan.id_pelabuhan','trip.id_pelabuhan_tujuan')
                                ->leftJoin('city','city.id_kota','pelabuhan.id_kota');
        
        $data = $data->select([
            'trip.*',
            'pelabuhan.nama_pelabuhan as nama_pelabuhan_tujuan',
            'city.nama_kota as nama_kota_tujuan'
        ]);

        if ($request->input('search')) {
            $data = $data->where('pelabuhan.nama_pelabuhan','LIKE','%'.$request->input('search').'%')
                ->orWhere('city.nama_kota','LIKE','%'.$request->input('search').'%')
                ->orWhere('trip.kode_trip','LIKE','%'.$request->input('search').'%')
                ->orWhere('trip.nama_kapal','LIKE','%'.$request->input('search').'%')
                ->orWhere('trip.tanggal_berangkat','LIKE','%'.$request->input('search').'%');
        }
        $count = $data->count();

        if ($request->input('sort')) {
            $data = $data->orderBy($request->input('sort'),$request->input('order'))
                ->orderBy('trip.id_trip',$request->input('order'));
            
        } else {
            $data = $data->orderBy('trip.created_at','desc');
        }
        if($request->input('limit')) {
            $data = $data->take($request->input('limit'))
            ->skip($request->input('offset'));
        }
        
        $data = $data->get();
        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }
    
    public function add(){
        $mode = 'ADD';
        $city = Models\City::orderBy('nama_kota','asc')->Get();
        $pelabuhan = Models\Pelabuhan::leftJoin('city', 'city.id_kota', '=', 'pelabuhan.id_kota')
        ->orderBy('city.nama_kota','asc')->get();
        return view('master.tripadd',compact('mode','city','pelabuhan'));
    }

    public function create(Request $request){
        Validator::make($request->all(), [
            'cboKodeTripKota' => 'required|not_in:0',
            'txtTglBerangkat' => 'required',
            'cboPelabuhanTujuan' => 'required|not_in:0',

        ],[
            'cboKodeTripKota.not_in' => 'kode trip harus dipilih',
            'cboPelabuhanTujuan.not_in' => 'kote pelabuhan tujuan harus dipilih',
            ])->validate();

        $year = DB::select('select distinct SUBSTRING(EXTRACT(YEAR FROM CURRENT_DATE),3,4) as year from dual');
        $lastnomortrip = DB::select('select IFNULL(MAX(kode_trip_nomor),0)+1 as notrip from trip where kode_trip_kota=?',[$request->input('cboKodeTripKota')]);
        // dd($year[0]->year);
        // dd($lastnomortrip[0]->notrip);
        DB::beginTransaction();
        try {
            $trip = new Models\Trip;
            $trip->kode_trip = $request->input('cboKodeTripKota') . '-'.$year[0]->year.'-'.str_pad($lastnomortrip[0]->notrip,4,"0",STR_PAD_LEFT);
            $trip->kode_trip_kota = $request->input('cboKodeTripKota');
            $trip->kode_trip_nomor = $lastnomortrip[0]->notrip;
            $trip->nama_kapal = $request->input('txtNamaKapal');
            $trip->tanggal_berangkat = Carbon::parse($request->input('txtTglBerangkat'))->format('Y/m/d H:i:s');
            $trip->id_pelabuhan_tujuan = $request->input('cboPelabuhanTujuan');
            $trip->status = 0;
            $trip->save();


        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->route('master.trip')
            ->with('success',__('Data Saved'));
    }


    public function edit($idtrip){
        $mode = 'EDIT';
        $trip = Models\Trip::find($idtrip);
        $city = Models\City::orderBy('nama_kota','asc')->Get();
        $pelabuhan = Models\Pelabuhan::leftJoin('city', 'city.id_kota', '=', 'pelabuhan.id_kota')
        ->orderBy('city.nama_kota','asc')->get();
        return view('master.tripadd',compact('mode','trip','pelabuhan','city'));
    }

    public function update($idtrip, Request $request){
        Validator::make($request->all(), [
            'txtTglBerangkat' => 'required',
            'cboPelabuhanTujuan' => 'required|not_in:0',

        ],[
            'cboPelabuhanTujuan.not_in' => 'kote pelabuhan tujuan harus dipilih',
            ])->validate();

        DB::beginTransaction();
        try {
            $trip = Models\Trip::find($idtrip);
            $trip->nama_kapal = $request->input('txtNamaKapal');
            $trip->tanggal_berangkat = Carbon::parse($request->input('txtTglBerangkat'))->format('Y/m/d H:i:s');
            $trip->id_pelabuhan_tujuan = $request->input('cboPelabuhanTujuan');
            $trip->save();


        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->back()
            ->with('success',__('Data Updated'));
    }

    public function complete($idtrip, Request $request){

        DB::beginTransaction();
        try {
            $trip = Models\Trip::find($idtrip);
            $trip->status=1;
            $trip->save();


        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        // $request->session()->flash('success', 'DO Tersimpan!');
        return response()->json([
            'status' => 'success',
            'description' => 'Trip Complete!'
        ]);
    }

    public function reopen($idtrip, Request $request){

        DB::beginTransaction();
        try {
            $trip = Models\Trip::find($idtrip);
            $trip->status=0;
            $trip->save();


        }  catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        // $request->session()->flash('success', 'DO Tersimpan!');
        return response()->json([
            'status' => 'success',
            'description' => 'Trip Re-open!'
        ]);
    }
}
