<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models;
use Carbon\Carbon;
use Auth;

class suratjalanController extends Controller
{
    public function index(){
   		return view('transaction.suratjalan');
    }

    public function getData(Request $request){

        $data = Models\SuratJalan::leftJoin('trip','trip.id_trip','suratjalan.id_kodetrip')
                                    ->leftJoin('consignee','consignee.id_consignee','suratjalan.id_consignee');
        
        $data = $data->select([
            'suratjalan.*',
            'trip.kode_trip',
            'consignee.nama as nama_consignee'
        ]);

        if ($request->input('search')) {
            $data = $data->where('trip.kode_trip','LIKE','%'.$request->input('search').'%')
                ->orWhere('suratjalan.no_suratjalan','LIKE','%'.$request->input('search').'%')
                ->orWhere('suratjalan.alamat_pengambilan','LIKE','%'.$request->input('search').'%')
                ->orWhere('consignee.nama','LIKE','%'.$request->input('search').'%')
                ->orWhere('suratjalan.no_container','LIKE','%'.$request->input('search').'%')
                ->orWhere('suratjalan.seal','LIKE','%'.$request->input('search').'%')
                ->orWhere('suratjalan.keterangan_total','LIKE','%'.$request->input('search').'%');
        }
        $count = $data->count();

        if ($request->input('sort')) {
            $data = $data->orderBy($request->input('sort'),$request->input('order'))
                ->orderBy('suratjalan.id_suratjalan',$request->input('order'));
            
        } else {
            $data = $data->orderBy('suratjalan.created_at','desc');
        }
        if($request->input('limit')) {
            $data = $data->take($request->input('limit'))
            ->skip($request->input('offset'));
        }
        
        $data = $data->get();

        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }

    public function getDataOrder(){

        $data = Models\Order::leftJoin('pelabuhan as pelabuhan_asal','pelabuhan_asal.id_pelabuhan','order.id_pelabuhan_asal')
                                    ->leftJoin('pelabuhan as pelabuhan_tujuan','pelabuhan_tujuan.id_pelabuhan','order.id_pelabuhan_tujuan')
                                    ->leftJoin('city as city_pelabuhan_asal','city_pelabuhan_asal.id_kota','pelabuhan_asal.id_kota')
                                    ->leftJoin('city as city_pelabuhan_tujuan','city_pelabuhan_tujuan.id_kota','pelabuhan_tujuan.id_kota')
                                    ->leftJoin('city as city_asal','city_asal.id_kota','order.id_kota_asal')
                                    ->leftJoin('city as city_tujuan','city_tujuan.id_kota','order.id_kota_tujuan')
                                    ->leftJoin('pelayaran','pelayaran.id_pelayaran','order.id_pelayaran')
                                    ->leftJoin('customer','customer.id_customer','order.id_customer')
                                    ->where('order.status','=','1');
        
        $data = $data->select([
            'order.*',
            'pelabuhan_asal.nama_pelabuhan as nama_pelabuhan_asal',
            'pelabuhan_tujuan.nama_pelabuhan as nama_pelabuhan_tujuan',
            'city_pelabuhan_asal.nama_kota as nama_kotapelabuhan_asal',
            'city_pelabuhan_tujuan.nama_kota as nama_kotapelabuhan_tujuan',
            'city_asal.nama_kota as nama_kota_asal',
            'city_tujuan.nama_kota as nama_kota_tujuan',
            'pelayaran.name_full as nama_pelayaran',
            'customer.nama_pt as nama_customer'
        ]);

        $count = $data->count();


        // if ($request->input('sort')) {
        //     $data = $data->orderBy($request->input('sort'),$request->input('order'))
        //         ->orderBy('consignee.id_consignee',$request->input('order'));
            
        // } else {
        //     $data = $data->orderBy('consignee.nama');
        // }
        // if($request->input('limit')) {
        //     $data = $data->take($request->input('limit'))
        //     ->skip($request->input('offset'));
        // }
        
        $data = $data->get();

        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }
    
    public function add($idorder){
        $mode = 'ADD';
        $order = Models\Order::find($idorder);             
        // dd($proposaldetail);                                                                                   
        $trip = Models\Trip::leftJoin('pelabuhan','pelabuhan.id_pelabuhan','trip.id_pelabuhan_tujuan')
                                ->leftJoin('city','city.id_kota','pelabuhan.id_kota')
                                ->where('trip.status','=','0')
                                ->Get();
        $consignee = Models\Consignee::Get();
        return view('transaction.suratjalanadd',compact('mode','order','trip','consignee'));
    }

    public function create(Request $request, $idorder){
        // dd($idproposal);
        Validator::make($request->all(), [
            'cboKodeTrip' => 'required|not_in:0',
            'txtNoSuratJalan' => 'required',
            'txtAlamatPengambilan' => 'required',
            'cboConsignee' => 'required|not_in:0',
            'txtNoPlatTruck' => 'required',

        ],[
        'cboKodeTrip.not_in' => 'kode trip harus dipilih',
        'cboConsignee.not_in' => 'consignee harus dipilih',
        ])->validate();

        $idsuratjalan = 0;

        DB::beginTransaction();
        try {
            $typeorder = $request->input('typeorder');
            $trip = explode('|',$request->input('cboKodeTrip'));

            // if($typeproposal=="CONTAINER"){
            //     $cekada = DB::select("select COUNT(1) AS ada FROM proposaldetail WHERE id_proposal=? and DATE(valid_from)=STR_TO_DATE(?,'%d/%m/%Y') AND DATE(valid_to)=STR_TO_DATE(?,'%d/%m/%Y') AND id_pelabuhan_asal=? AND id_pelabuhan_tujuan=? AND type_proposal=? AND  freight_size=?",
            //                         [$idproposal,Carbon::parse($request->input('txtValidFrom'))->format('d/m/Y'),Carbon::parse($request->input('txtValidTo'))->format('d/m/Y'),$request->input('cboPortAsal'),$request->input('cboPortTujuan'),$typeproposal,$request->input('rdoFtsize')]);
            // }else{
            //     $cekada = DB::select("select COUNT(1) AS ada FROM proposaldetail WHERE id_proposal=? and DATE(valid_from)=STR_TO_DATE(?,'%d/%m/%Y') AND DATE(valid_to)=STR_TO_DATE(?,'%d/%m/%Y') AND id_pelabuhan_asal=? AND id_pelabuhan_tujuan=? AND type_proposal=? AND  lcl_size=?",
            //                         [$idproposal, Carbon::parse($request->input('txtValidFrom'))->format('d/m/Y'),Carbon::parse($request->input('txtValidTo'))->format('d/m/Y'),$request->input('cboPortAsal'),$request->input('cboPortTujuan'),$typeproposal,$request->input('rdolclsize')]);
            // }

            // dd($cekada[0]->ada);

            // if ($cekada[0]->ada>0) {
            //     throw new \Exception("Detail Proposal yang sama sudah pernah diinputkan");
            // }


            $suratjalan = new Models\SuratJalan;
            $suratjalan->id_order = $idorder;
            $suratjalan->id_kodetrip = $trip[0];
            $suratjalan->no_suratjalan = $request->input('txtNoSuratJalan');
            $suratjalan->alamat_pengambilan = $request->input('txtAlamatPengambilan');
            $suratjalan->id_consignee = $request->input('cboConsignee');
            $suratjalan->no_plattruck = $request->input('txtNoPlatTruck');
            $suratjalan->keterangan_total = 0;
            if($typeorder=='CONTAINER'){
                $suratjalan->freight_size = $request->input('freightsize');
                $suratjalan->no_container = $request->input('txtNoContainer');
                $suratjalan->seal = $request->input('txtSeal');
            }else{
                $suratjalan->lcl_size = $request->input('lclsize');
            }
            $suratjalan->type_suratjalan=$typeorder;
            $suratjalan->status=0;
            if($suratjalan->save()){
                $idsuratjalan = $suratjalan->id_suratjalan;
            }

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->route('transaction.suratjalan.edit',$idsuratjalan)
            ->with('success',('Data Saved'));
    }

    public function edit($idsuratjalan){
        $mode = 'EDIT';
        $suratjalan = Models\SuratJalan::find($idsuratjalan);             
        // dd($proposaldetail);                                                                                   
        $trip = Models\Trip::leftJoin('pelabuhan','pelabuhan.id_pelabuhan','trip.id_pelabuhan_tujuan')
                                ->leftJoin('city','city.id_kota','pelabuhan.id_kota')
                                ->where('trip.status','=','0')
                                ->Get();
        $consignee = Models\Consignee::Get();
        return view('transaction.suratjalanadd',compact('mode','suratjalan','trip','consignee'));
    }

    public function update(Request $request, $idsuratjalan){
        // dd($idproposal);
        Validator::make($request->all(), [
            'cboKodeTrip' => 'required|not_in:0',
            'txtNoSuratJalan' => 'required',
            'txtAlamatPengambilan' => 'required',
            'cboConsignee' => 'required|not_in:0',
            'txtNoPlatTruck' => 'required',

        ],[
        'cboKodeTrip.not_in' => 'kode trip harus dipilih',
        'cboConsignee.not_in' => 'consignee harus dipilih',
        ])->validate();


        DB::beginTransaction();
        try {
            $typeorder = $request->input('typeorder');
            $trip = explode('|',$request->input('cboKodeTrip'));

            // if($typeproposal=="CONTAINER"){
            //     $cekada = DB::select("select COUNT(1) AS ada FROM proposaldetail WHERE id_proposal=? and DATE(valid_from)=STR_TO_DATE(?,'%d/%m/%Y') AND DATE(valid_to)=STR_TO_DATE(?,'%d/%m/%Y') AND id_pelabuhan_asal=? AND id_pelabuhan_tujuan=? AND type_proposal=? AND  freight_size=?",
            //                         [$idproposal,Carbon::parse($request->input('txtValidFrom'))->format('d/m/Y'),Carbon::parse($request->input('txtValidTo'))->format('d/m/Y'),$request->input('cboPortAsal'),$request->input('cboPortTujuan'),$typeproposal,$request->input('rdoFtsize')]);
            // }else{
            //     $cekada = DB::select("select COUNT(1) AS ada FROM proposaldetail WHERE id_proposal=? and DATE(valid_from)=STR_TO_DATE(?,'%d/%m/%Y') AND DATE(valid_to)=STR_TO_DATE(?,'%d/%m/%Y') AND id_pelabuhan_asal=? AND id_pelabuhan_tujuan=? AND type_proposal=? AND  lcl_size=?",
            //                         [$idproposal, Carbon::parse($request->input('txtValidFrom'))->format('d/m/Y'),Carbon::parse($request->input('txtValidTo'))->format('d/m/Y'),$request->input('cboPortAsal'),$request->input('cboPortTujuan'),$typeproposal,$request->input('rdolclsize')]);
            // }

            // dd($cekada[0]->ada);

            // if ($cekada[0]->ada>0) {
            //     throw new \Exception("Detail Proposal yang sama sudah pernah diinputkan");
            // }


            $suratjalan = Models\SuratJalan::find($idsuratjalan);
            $suratjalan->id_kodetrip = $trip[0];
            $suratjalan->no_suratjalan = $request->input('txtNoSuratJalan');
            $suratjalan->alamat_pengambilan = $request->input('txtAlamatPengambilan');
            $suratjalan->id_consignee = $request->input('cboConsignee');
            $suratjalan->no_plattruck = $request->input('txtNoPlatTruck');
            $suratjalan->keterangan_total = $request->input('txtKeteranganTotal');
            if($typeorder=='CONTAINER'){
                $suratjalan->no_container = $request->input('txtNoContainer');
                $suratjalan->seal = $request->input('txtSeal');
            }
            $suratjalan->save();

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->back()
            ->with('success',('Data Updated'));
    }

    public function view($idsuratjalan){
        $mode = 'VIEW';
        $suratjalan = Models\SuratJalan::find($idsuratjalan);             
        // dd($proposaldetail);                                                                                   
        $trip = Models\Trip::leftJoin('pelabuhan','pelabuhan.id_pelabuhan','trip.id_pelabuhan_tujuan')
                                ->leftJoin('city','city.id_kota','pelabuhan.id_kota')
                                ->where('trip.status','=','0')
                                ->Get();
        $consignee = Models\Consignee::Get();
        return view('transaction.suratjalanadd',compact('mode','suratjalan','trip','consignee'));
    }

    public function delete($idsuratjalan, Request $request)
    {   
        DB::beginTransaction();
        try {
            $suratjalan = Models\SuratJalan::find($idsuratjalan);
            $suratjalan->delete();
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Surat Jalan Dihapus.'
        ]);
    }

    public function detailsuratjalanadd(){
        return view('transaction.detailsuratjalanadd');
    }

    public function doadd($idsuratjalan){
        $mode = 'ADD';
        $suratjalan = Models\SuratJalan::find($idsuratjalan);             
        // dd($proposaldetail);                                                                                   
        $consignee = Models\Consignee::Get();
        return view('transaction.doadd',compact('mode','suratjalan','consignee'));
    }

    public function getDataBarang($kodebarang){
            $data = Models\Barang::where('kode_barang','=',$kodebarang);

            $data = $data->select([
                'barang.*'
            ]);
            $count = $data->count();

            $data = $data->get();
            
        
      return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }

    public function createDO(Request $request, $idsuratjalan){
        $validator = Validator::make($request->all(), [
            'nodo'  => 'required',
            'idconsignee' => 'required|not_in:0',

        ],['nodo.required'=>' No DO harus diisi',
        'idconsignee.not_in' => ' consignee harus dipilih',
        ])->validate();

        // dd($idproposal);
        $typesuratjalan = $request->input('typesuratjalan');
        if($typesuratjalan == 'LCL'){
            $lclsize = $request->input('lclsize');
        }
        $cektotal = DB::select("select keterangan_total FROM suratjalan WHERE id_suratjalan=?",
                                    [$idsuratjalan]);
        $total = $cektotal[0]->keterangan_total;
        $totalDO = 0;
        DB::beginTransaction();
        try {
            $deliveryorder = new Models\DeliveryOrder;
            $deliveryorder->id_suratjalan = $idsuratjalan;
            $deliveryorder->no_do = $request->input('nodo');
            $deliveryorder->id_consignee = $request->input('idconsignee');
            $deliveryorder->alamat = $request->input('alamat');
            $deliveryorder->total = 0;
            if($deliveryorder->save()){
                $detail = $request->input('detail');
                foreach ($detail as $item) {
                    $dodetail = new Models\DeliveryOrderDetail;
                    $dodetail->id_do = $deliveryorder->id_do;
                    $dodetail->kode_barang = $item['kodebarang'];
                    $dodetail->nama_barang = $item['namabarang'];
                    if($typesuratjalan == 'LCL'){
                        $dodetail->panjang = $item['panjang'];
                        $dodetail->lebar = $item['lebar'];
                        $dodetail->tinggi = $item['tinggi'];
                        if($lclsize=='M3'){
                            $dodetail->m3 = $item['m3'];
                            $total += $item['m3'];
                            $totalDO += $item['m3'];
                        }else{
                            $dodetail->ton = $item['ton'];
                            $total += $item['ton'];
                            $totalDO += $item['ton'];
                        }
                    }else{
                        $dodetail->kolli = $item['kolli'];
                        $total += $item['kolli'];
                        $totalDO += $item['kolli'];
                    }
                    $dodetail->satuan = $item['satuan'];
                    $dodetail->save();
                }
            }
            $deliveryordernew = Models\DeliveryOrder::find($deliveryorder->id_do);
            $deliveryordernew->total = $totalDO;
            $deliveryordernew->save();

            $suratjalan = Models\SuratJalan::find($idsuratjalan);
            $suratjalan->keterangan_total = $total;
            $suratjalan->save();

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        $request->session()->flash('success', 'DO Tersimpan!');
        return response()->json([
            'status' => 'success',
            'description' => 'DO Tersimpan!'
        ]);
    }

    public function getDataDO($idsuratjalan){

        $data = Models\DeliveryOrder::leftJoin('consignee','consignee.id_consignee','do.id_consignee')
                                        ->where('id_suratjalan','=',$idsuratjalan)
                                        ->orderBy('do.created_at', 'DESC');
        
        $data = $data->select([
            'do.*',
            'consignee.nama as nama_consignee'
        ]);

        $count = $data->count();

        // if ($request->input('sort')) {
        //     $data = $data->orderBy($request->input('sort'),$request->input('order'))
        //         ->orderBy('consignee.id_consignee',$request->input('order'));
            
        // } else {
        //     $data = $data->orderBy('consignee.nama');
        // }
        // if($request->input('limit')) {
        //     $data = $data->take($request->input('limit'))
        //     ->skip($request->input('offset'));
        // }
        
        $data = $data->get();

        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }

    public function doedit($iddo){
        $mode = 'EDIT';
        $deliveryorderdetail = Models\DeliveryOrderDetail::leftJoin('do','do.id_do','dodetail.id_do')
                                                ->leftJoin('suratjalan','suratjalan.id_suratjalan','do.id_suratjalan')
                                                ->where('dodetail.id_do','=',$iddo)->Get();             
        // dd($proposaldetail);                                                                                   
        $consignee = Models\Consignee::Get();
        return view('transaction.doadd',compact('mode','deliveryorderdetail','consignee'));
    }

    public function deletedo($iddo, Request $request)
    {   
        DB::beginTransaction();
        try {
            $deliveryorder = Models\DeliveryOrder::find($iddo);
            $deliveryorder->delete();

            $deliveryorderdetail = Models\DeliveryOrderDetail::where('id_do', $iddo);
            $deliveryorderdetail->delete();
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'DO Dihapus.'
        ]);
    }

    public function updateDO(Request $request, $iddo){
        // dd($iddo);
        $validator = Validator::make($request->all(), [
            'nodo'  => 'required',
            'idconsignee' => 'required|not_in:0',

        ],['nodo.required'=>' No DO harus diisi',
        'idconsignee.not_in' => ' consignee harus dipilih',
        ])->validate();

            $typesuratjalan = $request->input('typesuratjalan');
            if($typesuratjalan == 'LCL'){
                $lclsize = $request->input('lclsize');
            }
            $idsuratjalan = $request->input('idsuratjalan');
            $cektotal = DB::select("select keterangan_total FROM suratjalan WHERE id_suratjalan=?",
                                        [$idsuratjalan]);
            $total = $cektotal[0]->keterangan_total;
            $totalDO = 0;
            DB::beginTransaction();
            try {
                $deliveryorder = Models\DeliveryOrder::find($iddo);
                // $deliveryorder->id_suratjalan = $idsuratjalan;
                $deliveryorder->no_do = $request->input('nodo');
                $deliveryorder->id_consignee = $request->input('idconsignee');
                $deliveryorder->alamat = $request->input('alamat');

                    $detail = $request->input('detail');
                    foreach ($detail as $item) {
                        
                        // dd($dodetail->count());
                        if($item['id_dodetail']!='0'){
                            $dodetail = Models\DeliveryOrderDetail::find($item['id_dodetail']);
                            // $dodetail->id_do = $deliveryorder->id_do;
                            $dodetail->kode_barang = $item['kodebarang'];
                            $dodetail->nama_barang = $item['namabarang'];
                            if($typesuratjalan == 'LCL'){
                                $dodetail->panjang = $item['panjang'];
                                $dodetail->lebar = $item['lebar'];
                                $dodetail->tinggi = $item['tinggi'];
                                if($lclsize=='M3'){
                                    $total += ($item['m3'] - $dodetail->m3);
                                    $totalDO += $item['m3'];
                                    $dodetail->m3 = $item['m3'];
                                }else{
                                    $total += ($item['ton'] - $dodetail->ton);
                                    $totalDO += $item['ton'];
                                    $dodetail->ton = $item['ton'];
                                    
                                }
                            }else{
                                $total += ($item['kolli'] - $dodetail->kolli);
                                $totalDO += $item['kolli'];
                                $dodetail->kolli = $item['kolli'];
                                
                            }
                            $dodetail->satuan = $item['satuan'];
                            $dodetail->save();
                        }else{
                            $dodetail = new Models\DeliveryOrderDetail;
                            $dodetail->id_do = $iddo;
                            $dodetail->kode_barang = $item['kodebarang'];
                            $dodetail->nama_barang = $item['namabarang'];
                            if($typesuratjalan == 'LCL'){
                                $dodetail->panjang = $item['panjang'];
                                $dodetail->lebar = $item['lebar'];
                                $dodetail->tinggi = $item['tinggi'];
                                if($lclsize=='M3'){
                                    $dodetail->m3 = $item['m3'];
                                    $total += $item['m3'];
                                    $totalDO += $item['m3'];
                                }else{
                                    $dodetail->ton = $item['ton'];
                                    $total += $item['ton'];
                                    $totalDO += $item['ton'];
                                }
                            }else{
                                $dodetail->kolli = $item['kolli'];
                                $total += $item['kolli'];
                                $totalDO += $item['kolli'];
                            }
                            $dodetail->satuan = $item['satuan'];
                            $dodetail->save();
                        }
                        
                    }
                    // dd($total);
                    
                    $deliveryorder->total = $totalDO;
                    $deliveryorder->save();

                    $suratjalan = Models\SuratJalan::find($idsuratjalan);
                    // dd($request->input('typesuratjalan'));
                    $suratjalan->keterangan_total = $total;
                    // dd($total);
                    $suratjalan->save();

            } catch (\Exception $e) {
                DB::rollback();
                return response()->json([
                    'status' => 'error',
                    'description' => $e->getMessage()
                ]);
            }
            DB::commit();
            $request->session()->flash('success', 'DO Updated!');
            return response()->json([
                'status' => 'success',
                'description' => 'DO Updated!'
            ]);
        
    }

    public function deletedodetail($iddodetail, Request $request)
    {   
        DB::beginTransaction();
        try {
            $deliveryorderdetail = Models\DeliveryOrderDetail::find($iddodetail);
            $ceksuratjalan = DB::select("select id_suratjalan FROM do WHERE id_do=?",
                                    [$deliveryorderdetail->id_do]);
            $suratjalan = Models\SuratJalan::find($ceksuratjalan[0]->id_suratjalan);
            $do = Models\DeliveryOrder::find($deliveryorderdetail->id_do);

            $totalawaldo = $do->total;
            $totalawal = $suratjalan->keterangan_total;
            if($suratjalan->type_suratjalan=='CONTAINER'){
                $total = $deliveryorderdetail->kolli;
            }else{
                if($suratjalan->lcl_size=='M3'){
                    $total = $deliveryorderdetail->m3;
                }else{
                    $total = $deliveryorderdetail->ton;
                }
            }
            $totalakhir = $totalawal - $total;
            $totalakhirdo = $totalawaldo - $total;

            $do->total = $totalakhirdo;
            if($do->save()){
                $suratjalan->keterangan_total = $totalakhir;
                
                if($suratjalan->save()){
                    $deliveryorderdetail->delete();
                }
            }
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Barang Dihapus.'
        ]);
    }

    public function doview($iddo){
        $mode = 'VIEW';
        $deliveryorderdetail = Models\DeliveryOrderDetail::leftJoin('do','do.id_do','dodetail.id_do')
                                                    ->leftJoin('suratjalan','suratjalan.id_suratjalan','do.id_suratjalan')
                                                ->where('dodetail.id_do','=',$iddo)->Get();             
        // dd($proposaldetail);                                                                                   
        $consignee = Models\Consignee::Get();
        return view('transaction.doadd',compact('mode','deliveryorderdetail','consignee'));
    }

    public function doviewfromview($iddo){
        $mode = 'VIEWFROMVIEW';
        $deliveryorderdetail = Models\DeliveryOrderDetail::leftJoin('do','do.id_do','dodetail.id_do')
                                                    ->leftJoin('suratjalan','suratjalan.id_suratjalan','do.id_suratjalan')
                                                ->where('dodetail.id_do','=',$iddo)->Get();             
        // dd($proposaldetail);                                                                                   
        $consignee = Models\Consignee::Get();
        return view('transaction.doadd',compact('mode','deliveryorderdetail','consignee'));
    }

    public function getListBarang($iddo){

        $data = Models\DeliveryOrderDetail::where('id_do',$iddo);
        
        $data = $data->select([
            'dodetail.*',
        ]);
        $count = $data->count();


        // if ($request->input('sort')) {
        //     $data = $data->orderBy($request->input('sort'),$request->input('order'))
        //         ->orderBy('consignee.id_consignee',$request->input('order'));
            
        // } else {
        //     $data = $data->orderBy('consignee.nama');
        // }
        // if($request->input('limit')) {
        //     $data = $data->take($request->input('limit'))
        //     ->skip($request->input('offset'));
        // }
        
        $data = $data->get();

        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }

    public function getListDataBarang(Request $request){
        $data = new Models\Barang;

        if ($request->input('search')) {
            $data = $data->where('kode_barang','LIKE','%'.$request->input('search').'%')
                ->orWhere('nama_barang','LIKE','%'.$request->input('search').'%')
                ->orWhere('satuan','LIKE','%'.$request->input('search').'%');
        }

        $count = $data->count();

        if ($request->input('sort')) {
            $data = $data->orderBy($request->input('sort'),$request->input('order'))
                ->orderBy('id_barang',$request->input('order'));
            
        } else {
            $data = $data->orderBy('kode_barang','asc');
        }
        if($request->input('limit')) {
            $data = $data->take($request->input('limit'))
            ->skip($request->input('offset'));
        }

        $data = $data->get();
        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }
}
