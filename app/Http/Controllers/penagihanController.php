<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models;
use Carbon\Carbon;
use Auth;

class penagihanController extends Controller
{
    public function index(){
   	return view('transaction.penagihan');
    }

    public function getData(Request $request){
        $data = Models\Penagihan::leftJoin('customer','penagihan.id_customer','customer.id_customer');

        $data = $data->select([
            'penagihan.*',
            'customer.nama_pt as nama_customer',

        ]);

        if ($request->input('search')) {
            $data = $data->where('penagihan.kode_penagihan','LIKE','%'.$request->input('search').'%')
                ->orWhere('penagihan.tgl_penagihan','LIKE','%'.$request->input('search').'%')
                ->orWhere('customer.nama_pt','LIKE','%'.$request->input('search').'%');
        }
        $count = $data->count();

        if ($request->input('sort')) {
            $data = $data->orderBy($request->input('sort'),$request->input('order'))
                ->orderBy('penagihan.id_penagihan',$request->input('order'));
            
        } else {
            $data = $data->orderBy('penagihan.kode_penagihan','asc');
        }
        if($request->input('limit')) {
            $data = $data->take($request->input('limit'))
            ->skip($request->input('offset'));
        }

        $data = $data->get();
        
    
        return response()->json([
                'total' => $count,
                'rows' => $data
            ]);
    }
    
    public function add(){
        $mode='ADD';
        $customer = Models\Customer::get();
        return view('transaction.penagihanadd',compact('mode','customer'));
    }

    public function getDataInvoice($idcustomer){
        $datalcl = Models\InvoiceLCL::select('invoice_lcl.id_invoice_lcl','invoice_lcl.no_invoice_lcl','invoice_lcl.tgl_invoice_lcl','trip.kode_trip','invoice_lcl.id_customer','customer.nama_pt as nama_customer','invoice_lcl.bukti_bayar','invoice_lcl.total','invoice_lcl.kode_penagihan',DB::Raw("'LCL' as type_invoice"))
                            ->leftJoin('trip','trip.id_trip','invoice_lcl.id_trip')
                            ->leftJoin('customer','customer.id_customer','invoice_lcl.id_customer')
                            ->where('invoice_lcl.status','=','1')
                            ->where('invoice_lcl.id_customer','=',$idcustomer);

        $datacustom = Models\CustomInvoice::select('custominvoice.id_custominvoice','custominvoice.no_custominvoice','custominvoice.tgl_custominvoice',DB::Raw("'-' as kode_trip"),'custominvoice.id_customer','customer.nama_pt as nama_customer',DB::Raw("'-' as bukti_bayar"),'custominvoice.nominal_biaya','custominvoice.kode_penagihan',DB::Raw("'CUSTOM' as type_invoice"))
                            ->leftJoin('customer','customer.id_customer','custominvoice.id_customer')
                            ->where('custominvoice.id_customer','=',$idcustomer);

        $data = Models\Invoice::select('invoice.id_invoice','invoice.no_invoice','invoice.tgl_invoice','trip.kode_trip','invoice.id_customer','customer.nama_pt as nama_customer','invoice.bukti_bayar','invoice.total','invoice.kode_penagihan',DB::Raw("'CONTAINER' as type_invoice"))
                            ->leftJoin('trip','trip.id_trip','invoice.id_trip')
                            ->leftJoin('customer','customer.id_customer','invoice.id_customer')
                            ->where('invoice.status','=','1')
                            ->where('invoice.id_customer','=',$idcustomer)
                            ->union($datalcl)
                            ->union($datacustom)
                            ->get();
                            // ->whereNull('do.id_bapb');

        $count = $data->count();
        
    
        return response()->json([
                'total' => $count,
                'rows' => $data
            ]);
    }

    public function create(Request $request){
        // dd($idproposal);
        $validator = Validator::make($request->all(), [
            'tgl_penagihan'  => 'required',
            'id_customer' => 'required|not_in:0',

        ],['tgl_penagihan.required'=>'tanggal penagihan harus diisi',
        'id_customer.not_in' => ' customer harus dipilih',
        ])->validate();

        $year = DB::select('select distinct SUBSTRING(EXTRACT(YEAR FROM CURRENT_DATE),3,4) as year from dual');
        $month = DB::select('select MONTH(NOW()) as month');
        $cekkodepenagihan = DB::select('select count(*) as ada from penagihan where kode_penagihan like "PEN'.$year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).'%"');
        // dd($ceknobapb[0]->ada);
        if($cekkodepenagihan[0]->ada>0){
            $kodepenagihan = DB::select('select max(kode_penagihan) as kodepenagihan from penagihan where kode_penagihan like "PEN'.$year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).'%"');
            $nobaru = (int)(ltrim(substr($kodepenagihan[0]->kodepenagihan,7), '0'));
            $nobaru++;
            $kodepenagihanbaru = "PEN".$year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).str_pad($nobaru,4,"0",STR_PAD_LEFT);
        }else{
            $nobaru = 1;
            $kodepenagihanbaru = "PEN".$year[0]->year.str_pad($month[0]->month,2,"0",STR_PAD_LEFT).str_pad($nobaru,4,"0",STR_PAD_LEFT);
        }
        // dd($nobapbbaru);
        
        DB::beginTransaction();
        try {
            $penagihan = new Models\Penagihan;
            $penagihan->kode_penagihan = $kodepenagihanbaru;
            $penagihan->tgl_penagihan = Carbon::parse($request->input('tgl_penagihan'))->format('Y/m/d H:i:s');
            $penagihan->id_customer = $request->input('id_customer');
            if($penagihan->save()){
                $detail = $request->input('detail');
                foreach ($detail as $item) {
                    if($item['type_invoice']=="CONTAINER"){
                        $invoice = Models\Invoice::find($item['id_invoice']);
                        $invoice->kode_penagihan = $penagihan->kode_penagihan;
                        $invoice->tgl_penagihan =Carbon::parse($request->input('tgl_penagihan'))->format('Y/m/d H:i:s');
                        $invoice->save();
                    }else if($item['type_invoice']=="LCL"){
                        $invoicelcl = Models\InvoiceLCL::find($item['id_invoice']);
                        $invoicelcl->kode_penagihan = $penagihan->kode_penagihan;
                        $invoicelcl->tgl_penagihan = Carbon::parse($request->input('tgl_penagihan'))->format('Y/m/d H:i:s');
                        $invoicelcl->save();
                    }else{
                        $custominvoice = Models\CustomInvoice::find($item['id_invoice']);
                        $custominvoice->kode_penagihan = $penagihan->kode_penagihan;
                        $custominvoice->tgl_penagihan =Carbon::parse($request->input('tgl_penagihan'))->format('Y/m/d H:i:s');
                        $custominvoice->save();
                    }
                }
            }

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        $request->session()->flash('success', 'Penagihan Tersimpan!');
        return response()->json([
            'status' => 'success',
            'description' => 'Penagihan Tersimpan!'
        ]);
    }

    public function edit($idpenagihan){
        $mode='EDIT';
        $customer = Models\Customer::get();
        $penagihan = Models\Penagihan::find($idpenagihan);
        return view('transaction.penagihanadd',compact('mode','customer','penagihan'));
    }

    public function update(Request $request,$idpenagihan){
        $validator = Validator::make($request->all(), [
            'tgl_penagihan'  => 'required',
            'id_customer' => 'required|not_in:0',

        ],['tgl_penagihan.required'=>'tanggal penagihan harus diisi',
        'id_customer.not_in' => ' customer harus dipilih',
        ])->validate();
        
        DB::beginTransaction();
        try {
            $penagihan = Models\Penagihan::find($idpenagihan);
            $penagihan->tgl_penagihan = Carbon::parse($request->input('tgl_penagihan'))->format('Y/m/d H:i:s');
            if($penagihan->save()){
                $detail = $request->input('detail');
                foreach ($detail as $item) {
                    if($item['type_invoice']=="CONTAINER"){
                        $invoice = Models\Invoice::find($item['id_invoice']);
                        if($invoice->kode_penagihan==null){
                            $invoice->kode_penagihan = $penagihan->kode_penagihan;
                        }
                        $invoice->tgl_penagihan =Carbon::parse($request->input('tgl_penagihan'))->format('Y/m/d H:i:s');
                        $invoice->save();
                    }else if($item['type_invoice']=="LCL"){
                        $invoicelcl = Models\InvoiceLCL::find($item['id_invoice']);
                        if($invoicelcl->kode_penagihan==null){
                            $invoicelcl->kode_penagihan = $penagihan->kode_penagihan;
                        }
                        $invoicelcl->tgl_penagihan = Carbon::parse($request->input('tgl_penagihan'))->format('Y/m/d H:i:s');
                        $invoicelcl->save();
                    }else{
                        $custominvoice = Models\CustomInvoice::find($item['id_invoice']);
                        if($custominvoice->kode_penagihan==null){
                            $custominvoice->kode_penagihan = $penagihan->kode_penagihan;
                        }
                        $custominvoice->tgl_penagihan =Carbon::parse($request->input('tgl_penagihan'))->format('Y/m/d H:i:s');
                        $custominvoice->save();
                    }
                }
            }

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        $request->session()->flash('success', 'Penagihan Terupdate!');
        return response()->json([
            'status' => 'success',
            'description' => 'Penagihan Terupdate!'
        ]);
    }

    public function deleteInvoice(Request $request,$idinvoice,$type_invoice){
        DB::beginTransaction();
        try {
            if($type_invoice=='CONTAINER'){
                $invoice = Models\Invoice::find($idinvoice);
                $invoice->kode_penagihan = null;
                $invoice->tgl_penagihan = null;
                $invoice->save();
            }else{
                $invoicelcl = Models\InvoiceLCL::find($idinvoice);
                $invoicelcl->kode_penagihan = null;
                $invoicelcl->tgl_penagihan = null;
                $invoicelcl->save();
            }
            

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'Invoice Dikeluarkan!'
        ]);
    }

}
