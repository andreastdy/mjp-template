<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models;

class cityController extends Controller
{
    public function index(){
   	return view('setting.city');
    }

    public function getData(Request $request){
        $data = new Models\City;

        if ($request->input('search')) {
            $data = $data->where('kode_kota','LIKE','%'.$request->input('search').'%')
                ->orWhere('nama_kota','LIKE','%'.$request->input('search').'%');
        }
        
        $count = $data->count();

        if ($request->input('sort')) {
            $data = $data->orderBy($request->input('sort'),$request->input('order'))
                ->orderBy('nama_kota',$request->input('order'));
            
        } else {
            $data = $data->orderBy('nama_kota','asc');
        }
        if($request->input('limit')) {
            $data = $data->take($request->input('limit'))
            ->skip($request->input('offset'));
        }

        $data = $data->get();
        
        return response()->json([
            'total' => $count,
            'rows' => $data
        ]);
    }
    
    public function add(){
        $mode='ADD';
        return view('setting.cityadd',compact('mode'));
    }

    public function create(Request $request){
        Validator::make($request->all(), [
            'txtKodeKota' => 'required',
            'txtNamaKota' => 'required',

        ])->validate();

        DB::beginTransaction();
        try {
            $city = new Models\City;
            $city->kode_kota = strtoupper($request->input('txtKodeKota'));
            $city->nama_kota = $request->input('txtNamaKota');
            $city->save();


        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect('setting/city')
            ->with('success',__('Data Saved'));
    }

    public function edit($idCity){
        $mode = 'EDIT';
        $city = Models\City::find($idCity);
        return view('setting.cityadd',compact('mode','city'));
    }

    public function update(Request $request, $idCity){
        Validator::make($request->all(), [
            'txtKodeKota' => 'required',
            'txtNamaKota' => 'required',

        ])->validate();

        DB::beginTransaction();
        try {
            $city = Models\City::find($idCity);
            $city->kode_kota = strtoupper($request->input('txtKodeKota'));
            $city->nama_kota = $request->input('txtNamaKota');
            $city->save();


        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error',$e->getMessage())
                ->withInput();
        }
        DB::commit();
        return redirect()->back()
            ->with('success',__('Data Updated'));
    }

    public function delete($idCity, Request $request)
    {   
        DB::beginTransaction();
        try {
            $city = Models\City::find($idCity);
            $city->delete();
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 'success',
            'description' => 'City Deleted.'
        ]);
    }
}
