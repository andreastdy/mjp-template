<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models;
use Carbon\Carbon;
use Auth;

class ppnController extends Controller
{
   public function index(){
       $ppn = Models\ppn::Get();
    //    dd($ppn[0]->ppn);
   		return view('setting.ppn',compact('ppn'));
    }

    public function update(Request $request)
    {   
        DB::beginTransaction();
        try {
            $ppn = Models\ppn::find(1);
            $ppn->ppn = $request->input('txtPPN');;
            $ppn->save();
            
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ]);
        }
        DB::commit();
        return redirect()->back()
            ->with('success',__('PPN Diubah!'));
        // $request->session()->flash('success', 'PPN Diubah!');
        // return response()->json([
        //     'status' => 'success',
        //     'description' => 'PPN Diubah!'
        // ]);
    }
}
