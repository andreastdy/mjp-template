<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use App\Models;
use App\Observers;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        config(['app.locale' => 'id']);
        Schema::defaultStringLength(191);
        Models\Barang::observe(Observers\GeneralObserver::class);
        Models\Biaya::observe(Observers\GeneralObserver::class);
        Models\ppn::observe(Observers\GeneralObserver::class);
        Models\City::observe(Observers\GeneralObserver::class);
        Models\Consignee::observe(Observers\GeneralObserver::class);
        Models\Dooring::observe(Observers\GeneralObserver::class);
        Models\DooringDetail::observe(Observers\GeneralObserver::class);
        Models\Pelabuhan::observe(Observers\GeneralObserver::class);
        Models\Pelayaran::observe(Observers\GeneralObserver::class);
        Models\PelayaranDetail::observe(Observers\GeneralObserver::class);
        Models\Customer::observe(Observers\GeneralObserver::class);
        Models\Sector::observe(Observers\GeneralObserver::class);
        Models\Trip::observe(Observers\GeneralObserver::class);
        Models\Trucking::observe(Observers\GeneralObserver::class);
        Models\TruckingDetail::observe(Observers\GeneralObserver::class);
        Models\Proposal::observe(Observers\GeneralObserver::class);
        Models\ProposalDetail::observe(Observers\GeneralObserver::class);
        Models\Order::observe(Observers\GeneralObserver::class);
        Models\SuratJalan::observe(Observers\GeneralObserver::class);
        Models\DeliveryOrder::observe(Observers\GeneralObserver::class);
        Models\DeliveryOrderDetail::observe(Observers\GeneralObserver::class);
        Models\HistoryUpdateOrder::observe(Observers\GeneralObserver::class);
        Models\BAPB::observe(Observers\GeneralObserver::class);
        Models\CustomInvoice::observe(Observers\GeneralObserver::class);
        Models\Invoice::observe(Observers\GeneralObserver::class);
        Models\InvoiceDetail::observe(Observers\GeneralObserver::class);
        Models\InvoiceLCL::observe(Observers\GeneralObserver::class);
        Models\InvoiceDetailLCL::observe(Observers\GeneralObserver::class);
        Models\Penagihan::observe(Observers\GeneralObserver::class);
        Models\Pelunasan::observe(Observers\GeneralObserver::class);
        Models\ExtraCostClaim::observe(Observers\GeneralObserver::class);
        Models\MPelunasan::observe(Observers\GeneralObserver::class);
        Models\DPelunasan::observe(Observers\GeneralObserver::class);
        Models\BiayaLain::observe(Observers\GeneralObserver::class);

        
        \Carbon\Carbon::setLocale('id');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
