@extends('layouts.default')

@section('content_header')
<h1>
@if ($mode == 'ADD')
    ADD USER
@else
    EDIT USER
@endif
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Master</a></li>
    <li class="active">User</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
<form  class="form-horizontal">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="form-group">
    <label class="control-label col-xs-2" for="txtNama">Nama</label> 
    <div class="col-xs-5">
      <input required="required" class="form-control" name="txtNama" id="txtNama" type="text" @if($mode == 'EDIT') value="{{$user->name}}" @endif>
      <span class="text-danger" id="errornama"></span>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtUsername">Username</label> 
    <div class="col-xs-5">
      <input required="required" class="form-control" name="txtUsername" id="txtUsername" type="text" @if($mode == 'EDIT') value="{{$user->username}}" @endif>
      <span class="text-danger" id="errorusername"></span>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtEmail">Email</label> 
    <div class="col-xs-5">
      <input required="required" class="form-control" name="txtEmail" id="txtEmail" type="email" @if($mode == 'EDIT') value="{{$user->email}}" @endif>
      <span class="text-danger" id="erroremail"></span>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtPassword">Password</label> 
    <div class="col-xs-5">
      <input required="required" class="form-control" name="txtPassword" id="txtPassword" type="password" >
      <span class="text-danger" id="errorpassword"></span>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtConfirmpassword">Konfirmasi Password</label> 
    <div class="col-xs-5">
      <input required="required" class="form-control" name="txtConfirmpassword" id="txtConfirmpassword" type="password" >
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="cboRole">Hak Akses</label> 
    <div class="col-xs-4">
      <select class="select form-control cboRole" name="cboRole" id="cboRole" onchange="changeRole();">
      <option value="0" >--PILIH HAK AKSES--</option>
      @foreach($roles as $item)
      <option value="{{ $item->id }}" >{{ $item->name }}</option>
    @endforeach
      </select>
    </div>
  </div>

  <div class="form-group">
  <label class="control-label col-xs-2" for="listMenu">List Menu</label> 
  <div class="col-xs-4">
    <div class="box box-solid">
    <div class="box-body" id="listMenu">
    @foreach($permissions as $item)
      @if($mode=='ADD')
        <input type="checkbox" id="chkPermission_{{$item->id}}" value="{{$item->id}}" />&nbsp;{{$item->display_name}}<br/>
      @else
          @if(in_array ($item->id,$alluserpermission))
                <input type="checkbox" id="chkPermission_{{$item->id}}" value="{{$item->id}}" checked />&nbsp;{{$item->display_name}}<br/>
          @else
               <input type="checkbox" id="chkPermission_{{$item->id}}" value="{{$item->id}}" />&nbsp;{{$item->display_name}}<br/>
          @endif
      @endif
    @endforeach
    </div>
    </div>
    </div>
  </div>

  
  <div class="form-group row">
    <div class="col-xs-offset-2 col-xs-5">
    @if($mode=='ADD')
      <button class="btn btn-primary" type="button" name="submit" onclick="saveUser()"><i class="fas fa-save"></i> Simpan</button>
      @else
      <button class="btn btn-primary" type="button" name="submit" onclick="updateUser({{$user->id}})"><i class="fas fa-save"></i> Simpan</button>
      @endif
      <a href="{{route('setting.user')}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
    </div>
  </div>
</form>



@endsection

@section('footer_script')
<script>
    
    $('.cboRole').select2();


function changeRole(){
    var namerole=$('#cboRole>option:selected').text();
    // var namerole= 'SUPERADMIN';
    var newlist = "";
    // alert(namerole);
    $.ajax({
        url: route('setting.user.getpermission', {
          name: namerole
        }),
        method: 'get'
      })
      .then(function(response) {
        if (response.total == 0) {
            @foreach($permissions as $item)
            newlist += '<input type="checkbox" id="chkPermission_{{$item->id}}" value="{{$item->id}}" />&nbsp;{{$item->display_name}}<br/>';
            @endforeach

            $("#listMenu").html(newlist);
        } else {
          var data = response.rows;
        //   alert(data);
          @foreach($permissions as $item)
            if(inArray({{$item->id}},data)){
                newlist += '<input type="checkbox" id="chkPermission_{{$item->id}}" value="{{$item->id}}" checked />&nbsp;{{$item->display_name}}<br/>';
            }else{
                newlist += '<input type="checkbox" id="chkPermission_{{$item->id}}" value="{{$item->id}}" />&nbsp;{{$item->display_name}}<br/>';
            }
            
            @endforeach
          $("#listMenu").html(newlist);
        }

      });
}

function saveUser(){
    var obj = {_token:$('meta[name="csrf_token"]').attr('content'),nama:$("#txtNama").val(),username:$("#txtUsername").val(),email:$("#txtEmail").val(),password:$("#txtPassword").val(),
                confirm_password:$("#txtConfirmpassword").val(),detail:[]};
    var detail = [];
    $("#listMenu input:checked").each(function (i, el) {
      var $idpermission = $(this).val();
      detail.push($idpermission);
    });
    // alert(detail);
    // var count = detail.length;
  // alert(count);
    obj.detail = detail;
      $.ajax({
        url: route('setting.user.create'),
        data:obj,
        method:'post'
      })
      .then(function (response){
        if (response.status == 'error') {
          alert(response.description);
        } else if( response.status == 'success') {
          alert(response.description);
          window.location = "{{route('setting.user')}}";
          // refresh();
        }
      })
      .catch(function (response) {
        console.log(response);
        if(response.status == '422') {
          let error = response.responseJSON.errors;
          $.each(error, function(index,item) {
            $('#error'+index).html(item.join(", "));
          });
          alert("Please check the form errors.");

        } else {
        alert(response.responseJSON.message);
        }
        
      }); 

  
}

function updateUser($iduser){
    var obj = {_token:$('meta[name="csrf_token"]').attr('content'),nama:$("#txtNama").val(),username:$("#txtUsername").val(),email:$("#txtEmail").val(),password:$("#txtPassword").val(),
                confirm_password:$("#txtConfirmpassword").val(),detail:[]};
    var detail = [];
    $("#listMenu input:checked").each(function (i, el) {
      var $idpermission = $(this).val();
      detail.push($idpermission);
    });
    // alert(detail);
    // var count = detail.length;
  // alert(count);
    obj.detail = detail;
      $.ajax({
        url: route('setting.user.update',$iduser),
        data:obj,
        method:'post'
      })
      .then(function (response){
        if (response.status == 'error') {
          alert(response.description);
        } else if( response.status == 'success') {
          alert(response.description);
          window.location = "{{route('setting.user')}}";
          // refresh();
        }
      })
      .catch(function (response) {
        console.log(response);
        if(response.status == '422') {
          let error = response.responseJSON.errors;
          $.each(error, function(index,item) {
            $('#error'+index).html(item.join(", "));
          });
          alert("Please check the form errors.");

        } else {
        alert(response.responseJSON.message);
        }
        
      }); 

  
}
    
</script>
@endsection
