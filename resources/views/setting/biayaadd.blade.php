@extends('layouts.default')

@section('content_header')
<h1>
@if($mode == 'ADD')
    ADD BIAYA
@else
    EDIT BIAYA
@endif
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Master</a></li>
    <li class="active">Biaya</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@if($mode == 'ADD')
<form action="{{ route('setting.biaya.create') }}" method="post" class="form-horizontal">
@else
<form action="{{ route('setting.biaya.update',$biaya->id_biaya) }}" method="post" class="form-horizontal">
@endif
<input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtKodeBiaya">Kode Biaya</label> 
    <div class="col-xs-2">
      <input required="required" class="form-control" name="txtKodeBiaya" id="txtKodeBiaya" type="text" @if($mode == 'EDIT') value="{{$biaya->kode_biaya}}" @endif>
      @if($errors->has('txtKodeBiaya'))
      <span class="text-danger">{{$errors->first('txtKodeBiaya')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtNamaBiaya">Nama Biaya</label> 
    <div class="col-xs-5">
      <input required="required" class="form-control" name="txtNamaBiaya" id="txtNamaBiaya" type="text" @if($mode == 'EDIT') value="{{$biaya->nama_biaya}}" @endif>
      @if($errors->has('txtNamaBiaya'))
      <span class="text-danger">{{$errors->first('txtNamaBiaya')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtNominalBiaya">Nominal Biaya</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input required="required" class="form-control input-element" name="txtNominalBiaya" id="txtNominalBiaya" type="text" @if($mode == 'EDIT') value="{{$biaya->nominal_biaya}}" @else value="0" @endif style="text-align: right">
        @if($errors->has('txtNominalBiaya'))
      <span class="text-danger">{{$errors->first('txtNominalBiaya')}}
      @endif
      </div>
    </div>
  </div>
  
  <div class="form-group row">
    <div class="col-xs-offset-2 col-xs-5">
      <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
      <a href="{{route('setting.biaya')}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
    </div>
  </div>
</form>



@endsection

@section('footer_script')
<script>
   var cleave = new Cleave('.input-element', {
    numeral: true,
    numeralThousandsGroupStyle: 'thousand',
    autoUnmask : true
});

    
</script>
@endsection
