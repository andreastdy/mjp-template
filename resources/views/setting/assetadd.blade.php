@extends('layouts.default')

@section('content_header')
<h1>
    ADD ASSET
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Setting</a></li>
    <li class="active">Asset</li>
</ol>
@endsection

@section('content')
<hr>
<form class="form-horizontal">
  
    <div class="form-group">
    <label class="control-label col-xs-2" for="cboPortAsal">Nama Asset</label> 
    <div class="col-xs-5">
      <input required="required" class="form-control" name="txtShortname" id="txtShortname" type="text">
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="cboPortAsal">Type</label> 
    <div class="col-xs-4">
      <input required="required" class="form-control" name="txtShortname" id="txtShortname" type="text">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtShortname">Penanggung Jawab</label> 
    
    <div class="col-xs-4">
      <select class="select form-control" name="cboPortAsal" id="cboPortAsal">
        <option value="sby">Si A</option>
        <option value="jkt">Si B</option>
      </select>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="cboPortAsal">Tanggal Beli</label> 
    <div class="col-xs-4">
      <input required="required" class="form-control" name="txtShortname" id="txtShortname" type="text">
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="cboPortAsal">Parent</label> 
    <div class="col-xs-4">
      <select class="select form-control" name="cboPortAsal" id="cboPortAsal">
        <option value="sby">Komputer</option>
      </select>
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="cboPortAsal">Keterangan</label> 
    <div class="col-xs-4">
      <textarea class="form-control" rows="3" cols="40" name="txtAlamat" id="txtAlamat"></textarea>
    </div>
  </div>
    
  <div class="form-group row">
    <div class="col-xs-offset-2 col-xs-5">
      <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
      <button class="btn btn-danger" name="cancel">Cancel</button>
    </div>
  </div>
</form>



@endsection

@section('footer_script')
<script>
    
   
    
    function editPelayaran(){
       // window.open(' {{route('master.pelayaran.edit')}} ', "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, width=1400, height=800");
        //openPopup({{route('master.pelayaran.edit')}}, 'Title', 1400, 800, 'scrollbars,resizable', true, false);
    }
</script>
@endsection
