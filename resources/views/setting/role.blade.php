@extends('layouts.default')

@section('content_header')
<h1>
    ROLE
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Master</a></li>
    <li class="active">Role</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@can('add role')
<a href="{{route('setting.role.add')}}" data-toggle="tooltip" title="Tambah Role"><button type='button' class='btn btn-danger'><i class='fa fa-plus'></i> Buat Baru</button></a>
@endcan

<div class="portlet-body">
    <table class="table table-bordered" id="roleTable" style='background-color: white'>
        
        

    </table>
</div>



@endsection

@section('footer_script')
<script>

$(document).ready(function () {
    function refresh() {
    $('#roleTable').bootstrapTable('refresh',{
      url:route('setting.role.data')
    });
    
  }

    $('#roleTable').bootstrapTable({
        url: route('setting.role.data'),
    pagination: true,
    sidePagination: 'server',
  search: true,
  columns: [{
    title: 'NO',
    formatter: function (value, row, index) {
        return index+1;
      }
  }, {
    field: 'name',
    title: 'NAMA ROLE',
    sortable: true
  }, {
      title:'AKSI',
      formatter: function(value, row, index) {
        var btnString = "";
        @can('add role')
        btnString = '<a href="'+route('setting.role.ubah',{name:row.name})+'" data-toggle="tooltip" title="Edit Role"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>'
         btnString +='<button type="button" class="btn btn-delete btn-xs btn-danger" title="delete role"><i class="fa fa-trash"></i></button></a>';
         @endcan
         
        return btnString;
      },
      events:{
        'click .btn-delete': function(e, value, row, index) {
          if (confirm('Are you sure you want to delete this record?')) {
            $.ajax({
              url: route('setting.role.delete',{name:row.name}),
              data:{_token:$('meta[name="csrf_token"]').attr('content')},
              method:'post'
            })
            .then(function (response){
              if (response.status == 'error') {
                alert(response.description);
              } else if( response.status == 'success') {
                alert(response.description);
                refresh();
              }
            }); 
          }
        }
      }
    }]
})
});

</script>
@endsection
