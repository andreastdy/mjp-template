@extends('layouts.default')

@section('content_header')
<h1>
    KARYAWAN
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Setting</a></li>
    <li class="active">Karyawan</li>
</ol>
@endsection

@section('content')
<hr>
<a href="{{route('setting.karyawan.add')}}" data-toggle="tooltip" title="Tambah Karyawan"><button type='button' class='btn btn-danger'><i class='fa fa-plus'></i> Buat Baru</button></a>
<input type="text" placeholder="search.." style="float: right; margin-top:1%"></input>

<div class="portlet-body">
    <table class="table table-bordered" id="assetTable" style='background-color: white'>
        <tr>
            <th style="width:5%">ID</th>
            <th style="width:20%">NAMA KARYAWAN</th>
            <th style="width:10%">ALAMAT</th>
            <th style="width:20%">JABATAN</th>
            <th style="width:20%">TANGGAL JOIN</th>
            <th style="width:10%">STATUS KARYAWAN</th>
            <th style="width:15%">AKSI</th>
        </tr>
        <tr>
            <td>1</td>
            <td>Ralph</td>
            <td>Jl. Halan</td>
            <td>Admin Document</td>
            <td>01/01/2018</td>
            <td>Active</td>
            <td>
                <a href="{{route('setting.karyawan.edit')}}" data-toggle="tooltip" title="Edit Karyawan" onclick="editPelayaran()"><button type='button' class='btn btn-primary'><i class='fa fa-edit'></i></button></a>
                <a href="#" data-toggle="tooltip" title="View Detail"><button type='button' class='btn btn-primary' data-toggle="modal" data-target="#DetailModal"><i class='fa fa-eye'></i></button></a>
                <a href="#" data-toggle="tooltip" title="Delete Karyawan "><button type='button' class='btn btn-danger'><i class='fa fa-times'></i></button></a>
            </td>
        </tr>
         <tr>
            <td>2</td>
            <td>Dey</td>
            <td>Jl. Selasa</td>
            <td>Kasir</td>
            <td>01/01/2017</td>
            <td><font color="red">Inactive</font></td>
            <td>
                <a href="{{route('setting.karyawan.edit')}}" data-toggle="tooltip" title="Edit Karyawan" onclick="editPelayaran()"><button type='button' class='btn btn-primary'><i class='fa fa-edit'></i></button></a>
                <a href="#" data-toggle="tooltip" title="View Detail"><button type='button' class='btn btn-primary' data-toggle="modal" data-target="#DetailModal"><i class='fa fa-eye'></i></button></a>
                <a href="#" data-toggle="tooltip" title="Delete Karyawan "><button type='button' class='btn btn-danger'><i class='fa fa-times'></i></button></a>
            </td>
        </tr>
        
       
        

    </table>
</div>

<div id="DetailModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Detail </h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-xs-4" for="chkPortToDoor">ID</label> 
                        <div class="col-xs-4">
                            <label class="checkbox-inline">
                                1
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-4" for="chkPortToDoor">Nama Karyawan</label> 
                        <div class="col-xs-4">
                            <label class="checkbox-inline">
                                Dey
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-4" for="chkPortToDoor">Alamat</label> 
                        <div class="col-xs-4">
                            <label class="checkbox-inline">
                                Jl. Selasa
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-4" for="chkPortToDoor">Kelurahan</label> 
                        <div class="col-xs-4">
                            <label class="checkbox-inline">
                                Minggu
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-4" for="chkPortToDoor">Kecamatan</label> 
                        <div class="col-xs-4">
                            <label class="checkbox-inline">
                                Desember
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-4" for="chkPortToDoor">Kota</label> 
                        <div class="col-xs-4">
                            <label class="checkbox-inline">
                                Surabaya
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-4" for="chkPortToDoor">Tanggal Lahir</label> 
                        <div class="col-xs-4">
                            <label class="checkbox-inline">
                                01-01-1988
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-4" for="chkPortToDoor">Uang Makan</label> 
                        <div class="col-xs-4">
                            <label class="checkbox-inline">
                                Rp. 20.000
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-4" for="chkPortToDoor">Tunjangan Jabatan</label> 
                        <div class="col-xs-4">
                            <label class="checkbox-inline">
                                Rp. 500.000
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-4" for="chkPortToDoor">Gaji Pokok</label> 
                        <div class="col-xs-4">
                            <label class="checkbox-inline">
                                Rp. 3.700.000
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-4" for="chkPortToDoor">Jabatan</label> 
                        <div class="col-xs-4">
                            <label class="checkbox-inline">
                                Kasir
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-4" for="chkPortToDoor">Divisi</label> 
                        <div class="col-xs-4">
                            <label class="checkbox-inline">
                                Finance
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-4" for="chkPortToDoor">No Identitas</label> 
                        <div class="col-xs-4">
                            <label class="checkbox-inline">
                                3578290101980003
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-4" for="chkPortToDoor">Tanggal Join</label> 
                        <div class="col-xs-4">
                            <label class="checkbox-inline">
                                01-01-2017
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-4" for="chkPortToDoor">Tanggal Keluar</label> 
                        <div class="col-xs-4">
                            <label class="checkbox-inline">
                                01-12-2018
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-4" for="chkPortToDoor">Status Karyawan</label> 
                        <div class="col-xs-4">
                            <label class="checkbox-inline">
                                Inactive
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-4" for="chkPortToDoor">Cuti Per Tahun</label> 
                        <div class="col-xs-4">
                            <label class="checkbox-inline">
                                12
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-4" for="chkPortToDoor">Contact No</label> 
                        <div class="col-xs-4">
                            <label class="checkbox-inline">
                                0811234568
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-4" for="chkPortToDoor">Status BPJS</label> 
                        <div class="col-xs-4">
                            <label class="checkbox-inline">
                                Active
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-4" for="chkPortToDoor">NPWP</label> 
                        <div class="col-xs-4">
                            <label class="checkbox-inline">
                                72.403.2012.4290.192
                            </label>
                        </div>
                    </div>
                    
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>



@endsection

@section('footer_script')
<script>

    $('#karyawanTable').bootstrapTable({});

    function editPelayaran() {
        // window.open(' {{route('master.pelayaran.edit')}} ', "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, width=1400, height=800");
        //openPopup({{route('master.pelayaran.edit')}}, 'Title', 1400, 800, 'scrollbars,resizable', true, false);
    }
</script>
@endsection
