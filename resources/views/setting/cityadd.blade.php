@extends('layouts.default')

@section('content_header')
<h1>
@if($mode == 'ADD')
    ADD CITY
@else
    EDIT CITY
@endif
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Master</a></li>
    <li class="active">City</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@if ($mode == 'ADD')
<form action="{{ route('setting.city.create') }}" method="post" class="form-horizontal">
@else
<form action="{{ route('setting.city.update',$city->id_kota) }}" method="post" class="form-horizontal">
@endif
<input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtKodeKota">Kode Kota</label> 
    <div class="col-xs-2">
      <input required="required" class="form-control" style="text-transform: uppercase" name="txtKodeKota" id="txtKodeKota" type="text" @if($mode == 'EDIT') value="{{$city->kode_kota}}" @endif>
      @if($errors->has('txtKodeKota'))
      <span class="text-danger">{{$errors->first('txtKodeKota')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtNamaKota">Nama Kota</label> 
    <div class="col-xs-5">
      <input required="required" class="form-control" name="txtNamaKota" id="txtNamaKota" type="text" @if($mode == 'EDIT') value="{{$city->nama_kota}}" @endif>
      @if($errors->has('txtNamaKota'))
      <span class="text-danger">{{$errors->first('txtNamaKota')}}
      @endif
    </div>
  </div>
  
  
  <div class="form-group row">
    <div class="col-xs-offset-2 col-xs-5">
      <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
      <a href="{{route('setting.city')}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
    </div>
  </div>
</form>



@endsection

@section('footer_script')
<script>
    
</script>
@endsection
