@extends('layouts.default')

@section('content_header')
<h1>
    ADD KARYAWAN
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Setting</a></li>
    <li class="active">Karyawan</li>
</ol>
@endsection

@section('content')
<hr>
<form class="form-horizontal">
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtShortname">Nama Karyawan</label> 
    <div class="col-xs-6">
      <input required="required" class="form-control" name="txtShortname" id="txtShortname" type="text">
    </div>
  </div>
  
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtAlamat">Alamat</label> 
    <div class="col-xs-4">
      <textarea class="form-control" rows="3" cols="40" name="txtAlamat" id="txtAlamat"></textarea>
    </div>
  </div>
     <div class="form-group">
    <label class="control-label col-xs-2" for="txtShortname">Kelurahan</label> 
    <div class="col-xs-3">
      <input required="required" class="form-control" name="txtShortname" id="txtShortname" type="text">
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtShortname">Kecamatan</label> 
    <div class="col-xs-3">
      <input required="required" class="form-control" name="txtShortname" id="txtShortname" type="text">
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtShortname">Kota</label> 
    <div class="col-xs-3">
      <input required="required" class="form-control" name="txtShortname" id="txtShortname" type="text">
    </div>
    </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtShortname">Tanggal Lahir</label> 
    <div class="col-xs-3">
      <input required="required" class="form-control" name="txtShortname" id="txtShortname" type="text">
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txt20ft">Uang Makan</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input class="form-control" name="txt20ft" id="txt20ft" type="text" value="0" style="text-align: right">
      </div>
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txt20ft">Tunjangan Jabatan</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input class="form-control" name="txt20ft" id="txt20ft" type="text" value="0" style="text-align: right">
      </div>
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txt20ft">Gaji Pokok</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input class="form-control" name="txt20ft" id="txt20ft" type="text" value="0" style="text-align: right">
      </div>
    </div>
  </div> 
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">Jabatan</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtKota" id="txtKota" type="text">
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">Divisi</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtKota" id="txtKota" type="text">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtKodepos">No Identitas</label> 
    <div class="col-xs-4">
      <input class="form-control" name="txtKodepos" id="txtKodepos" type="text">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtTelepon">Tanggal Join</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtTelepon" id="txtTelepon" type="text">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtNamaCP">Tanggal Keluar</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtNamaCP" id="txtNamaCP" type="text">
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtShortname">Status Karyawan</label> 
    
    <div class="col-xs-4">
      <select class="select form-control" name="cboPortAsal" id="cboPortAsal">
        <option value="sby">Active</option>
        <option value="jkt">Inactive</option>
      </select>
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtTeleponCP">Cuti Per Tahun</label> 
    <div class="col-xs-1">
      <input class="form-control" name="txtTeleponCP" id="txtTeleponCP" type="text">
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtTeleponCP">Contact No</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtTeleponCP" id="txtTeleponCP" type="text">
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtShortname">Status BPJS</label> 
    
    <div class="col-xs-4">
      <select class="select form-control" name="cboPortAsal" id="cboPortAsal">
        <option value="sby">Active</option>
        <option value="jkt">Inactive</option>
      </select>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtTeleponCP">NPWP</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtTeleponCP" id="txtTeleponCP" type="text">
    </div>
  </div>
  
    
  <div class="form-group row">
    <div class="col-xs-offset-2 col-xs-3">
      <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
      <button class="btn btn-danger" name="cancel">Cancel</button>
    </div>
  </div>
</form>



@endsection

@section('footer_script')
<script>
    
   
    
    function editPelayaran(){
       // window.open(' {{route('master.pelayaran.edit')}} ', "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, width=1400, height=800");
        //openPopup({{route('master.pelayaran.edit')}}, 'Title', 1400, 800, 'scrollbars,resizable', true, false);
    }
</script>
@endsection
