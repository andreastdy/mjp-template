@extends('layouts.default')

@section('content_header')
<h1>
    ASSET
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Setting</a></li>
    <li class="active">Asset</li>
</ol>
@endsection

@section('content')
<hr>
<a href="{{route('setting.asset.add')}}" data-toggle="tooltip" title="Tambah Asset"><button type='button' class='btn btn-danger'><i class='fa fa-plus'></i> Buat Baru</button></a>
<input type="text" placeholder="search.." style="float: right; margin-top:1%"></input>

<div class="portlet-body">
    <table class="table table-bordered" id="assetTable" style='background-color: white'>
        <tr>
            <th style="width:5%">ID</th>
            <th style="width:20%">NAMA ASSET</th>
            <th style="width:10%">TYPE</th>
            <th style="width:20%">PENANGGUNG JAWAB</th>
            <th style="width:20%">TANGGAL BELI</th>
            <th style="width:10%">PARENT</th>
            <th style="width:10%">AKSI</th>
        </tr>
        <tr>
            <td>1</td>
            <td>Komputer</td>
            <td>Elektronik</td>
            <td>Si A</td>
            <td>01/01/2018</td>
            <td>Komputer</td>
            <td>
                <a href="{{route('setting.asset.edit')}}" data-toggle="tooltip" title="Edit Asset" onclick="editPelayaran()"><button type='button' class='btn btn-primary'><i class='fa fa-edit'></i></button></a>
                <a href="#" data-toggle="tooltip" title="Delete Asset "><button type='button' class='btn btn-danger'><i class='fa fa-times'></i></button></a>
            </td>
        </tr>
        <tr>
            <td>2</td>
            <td>Ipad</td>
            <td>Elektronik</td>
            <td>Si B</td>
            <td>01/06/2018</td>
            <td>Komputer</td>
            <td>
                <a href="{{route('setting.asset.edit')}}" data-toggle="tooltip" title="Edit Asset" onclick="editPelayaran()"><button type='button' class='btn btn-primary'><i class='fa fa-edit'></i></button></a>
                <a href="#" data-toggle="tooltip" title="Delete Asset "><button type='button' class='btn btn-danger'><i class='fa fa-times'></i></button></a>
            </td>
        </tr>
        
        <tr>
            <td>3</td>
            <td>Tablet Phone</td>
            <td>Elektronik</td>
            <td>Si C</td>
            <td>01/09/2018</td>
            <td>Komputer</td>
            <td>
                <a href="{{route('setting.asset.edit')}}" data-toggle="tooltip" title="Edit Asset" onclick="editPelayaran()"><button type='button' class='btn btn-primary'><i class='fa fa-edit'></i></button></a>
                <a href="#" data-toggle="tooltip" title="Delete Asset "><button type='button' class='btn btn-danger'><i class='fa fa-times'></i></button></a>
            </td>
        </tr>
        

    </table>
</div>



@endsection

@section('footer_script')
<script>

    $('#assetTable').bootstrapTable({});

    function editPelayaran() {
        // window.open(' {{route('master.pelayaran.edit')}} ', "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, width=1400, height=800");
        //openPopup({{route('master.pelayaran.edit')}}, 'Title', 1400, 800, 'scrollbars,resizable', true, false);
    }
</script>
@endsection
