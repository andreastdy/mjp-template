@extends('layouts.default')

@section('content_header')
<h1>
    USER
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Master</a></li>
    <li class="active">User</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@can('edit user')
<a href="{{route('setting.user.add')}}" data-toggle="tooltip" title="Tambah User"><button type='button' class='btn btn-danger'><i class='fa fa-plus'></i> Buat Baru</button></a>
@endcan

<div class="portlet-body">
    <table class="table table-bordered" id="userTable" style='background-color: white'>
        
        

    </table>
</div>



@endsection

@section('footer_script')
<script>

$(document).ready(function () {
    function refresh() {
    $('#userTable').bootstrapTable('refresh',{
      url:route('setting.user.data')
    });
    
  }

    $('#userTable').bootstrapTable({
        url: route('setting.user.data'),
    pagination: true,
    sidePagination: 'server',
  search: true,
  columns: [{
    title: 'NO',
    formatter: function (value, row, index) {
        return index+1;
      }
  }, {
    field: 'username',
    title: 'USERNAME',
    sortable: true
  }, {
    field: 'name',
    title: 'NAMA',
    sortable: true
  },{
    field: 'email',
    title: 'EMAIL',
    sortable: true
  },{
      title:'AKSI',
      formatter: function(value, row, index) {
        var btnString = "";
        @can('edit user')
        btnString = '<a href="'+route('setting.user.ubah',{id:row.id})+'" data-toggle="tooltip" title="Edit User"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>'
         btnString +='<button type="button" class="btn btn-delete btn-xs btn-danger" title="delete user"><i class="fa fa-trash"></i></button></a>';
         @endcan
         
        return btnString;
      },
      events:{
        'click .btn-delete': function(e, value, row, index) {
          if (confirm('Are you sure you want to delete this record?')) {
            $.ajax({
              url: route('setting.user.delete',{id:row.id}),
              data:{_token:$('meta[name="csrf_token"]').attr('content')},
              method:'post'
            })
            .then(function (response){
              if (response.status == 'error') {
                alert(response.description);
              } else if( response.status == 'success') {
                alert(response.description);
                refresh();
              }
            }); 
          }
        }
      }
    }]
})
});

</script>
@endsection
