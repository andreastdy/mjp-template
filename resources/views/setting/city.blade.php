@extends('layouts.default')

@section('content_header')
<h1>
  CITY
</h1>
<ol class="breadcrumb">
  <li><a href="#"> Master</a></li>
  <li class="active">City</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@can('edit city')
<a href="{{route('setting.city.add')}}" data-toggle="tooltip" title="Tambah Kota"><button type='button' class='btn btn-danger'><i class='fa fa-plus'></i> Buat Baru</button></a>
@endcan
<div class="portlet-body">
  <table class="table table-bordered" id="cityTable" style='background-color: white'>


  </table>
</div>

@endsection

@section('footer_script')
<script>
  $(document).ready(function() {
    function refresh() {
      $('#cityTable').bootstrapTable('refresh', {
        url: route('setting.city.data')
      });

    }

    $('#cityTable').bootstrapTable({
      url: route('setting.city.data'),
      pagination: true,
      sidePagination: 'server',
      search: true,
      columns: [{
        title: 'NO',
        formatter: function(value, row, index) {
          return index + 1;
        }
      }, {
        field: 'kode_kota',
        title: 'KODE KOTA',
        sortable: true
      }, {
        field: 'nama_kota',
        title: 'NAMA KOTA',
        sortable: true
      }, {
        title: 'AKSI',
        formatter: function(value, row, index) {
          var btnString = '';

          @can('edit city')
           btnString = '<a href="' + route('setting.city.ubah', {
            id: row.id_kota
          }) + '" data-toggle="tooltip" title="Edit City"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>'
          btnString += '<button type="button" class="btn btn-delete btn-xs btn-danger" title="Delete City"><i class="fa fa-trash"></i></button></a>'
          @endcan

          return btnString;
        },
        events: {
          'click .btn-delete': function(e, value, row, index) {
            if (confirm('Are you sure you want to delete this record?')) {
              $.ajax({
                  url: route('setting.city.delete', {
                    id: row.id_kota
                  }),
                  data: {
                    _token: $('meta[name="csrf_token"]').attr('content')
                  },
                  method: 'post'
                })
                .then(function(response) {
                  if (response.status == 'error') {
                    alert(response.description);
                  } else if (response.status == 'success') {
                    alert(response.description);
                    refresh();
                  }
                });
            }
          }
        }
      }]
    })
  });
</script>
@endsection