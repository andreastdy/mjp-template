@extends('layouts.default')

@section('content_header')
<h1>
    PELABUHAN
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Master</a></li>
    <li class="active">Pelabuhan</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@can('edit pelabuhan')
<a href="{{route('setting.pelabuhan.add')}}" data-toggle="tooltip" title="Tambah Pelabuhan"><button type='button' class='btn btn-danger'><i class='fa fa-plus'></i> Buat Baru</button></a>
@endcan

<div class="portlet-body">
    <table class="table table-bordered" id="pelabuhanTable" style='background-color: white'>
        
        

    </table>
</div>



@endsection

@section('footer_script')
<script>

$(document).ready(function () {
    function refresh() {
    $('#pelabuhanTable').bootstrapTable('refresh',{
      url:route('setting.pelabuhan.data')
    });
    
  }

    $('#pelabuhanTable').bootstrapTable({
        url: route('setting.pelabuhan.data'),
    pagination: true,
    sidePagination: 'server',
  search: true,
  columns: [{
    title: 'NO',
    formatter: function (value, row, index) {
        return index+1;
      }
  }, {
    field: 'nama_kota',
    title: 'NAMA KOTA',
    sortable: true
  }, {
    field: 'nama_pelabuhan',
    title: 'NAMA PELABUHAN',
    sortable: true
  },{
      title:'AKSI',
      formatter: function(value, row, index) {
        var btnString = '';

        @can('edit pelabuhan')
         btnString = '<a href="'+route('setting.pelabuhan.ubah',{id:row.id_pelabuhan})+'" data-toggle="tooltip" title="Edit Pelabuhan"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>'
         btnString +='<button type="button" class="btn btn-delete btn-xs btn-danger" title="delete pelabuhan"><i class="fa fa-trash"></i></button></a>'
         @endcan
         
        return btnString;
      },
      events:{
        'click .btn-delete': function(e, value, row, index) {
          if (confirm('Are you sure you want to delete this record?')) {
            $.ajax({
              url: route('setting.pelabuhan.delete',{id:row.id_pelabuhan}),
              data:{_token:$('meta[name="csrf_token"]').attr('content')},
              method:'post'
            })
            .then(function (response){
              if (response.status == 'error') {
                alert(response.description);
              } else if( response.status == 'success') {
                alert(response.description);
                refresh();
              }
            }); 
          }
        }
      }
    }]
})
});

</script>
@endsection
