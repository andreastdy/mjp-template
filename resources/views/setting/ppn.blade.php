@extends('layouts.default')

@section('content_header')
<h1>
    PPN
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Setting</a></li>
    <li class="active">PPN</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<form action="{{ route('setting.ppn.update') }}" method="post" class="form-horizontal">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
  <div class="form-group">
    <label for="txtPPN" class="control-label col-xs-1">PPN</label> 
    <div class="col-xs-2">
      <div class="input-group">
        <input id="txtPPN" name="txtPPN" placeholder="10" type="text" class="form-control" style="text-align:right;" required="required" value="{{$ppn[0]->ppn}}"> 
        <div class="input-group-addon">
          <i class="fa fa-percent"></i>
        </div>
      </div>
    </div>
  </div> 
  <div class="form-group row">
    <div class="col-xs-offset-1 col-xs-2">
      <button name="submit" type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
    </div>
  </div>
</form>

@endsection
