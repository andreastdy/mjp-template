@extends('layouts.default')

@section('content_header')
<h1>
@if ($mode == 'ADD')
    ADD PELABUHAN
@else
    EDIT PELABUHAN
@endif
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Master</a></li>
    <li class="active">Pelabuhan</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@if ($mode == 'ADD')
<form action="{{ route('setting.pelabuhan.create') }}" method="post" class="form-horizontal">
@else
<form action="{{ route('setting.pelabuhan.update',$pelabuhan->id_pelabuhan) }}" method="post" class="form-horizontal">
@endif
<input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
    <label class="control-label col-xs-2" for="cboKota">Nama Kota</label> 
    <div class="col-xs-4">
      <select class="select form-control cboKota" name="cboKota" id="cboKota">
      <option value="0">-- PILIH KOTA --</option>
      @foreach($city as $item)
      @if($mode == 'EDIT') 
        @if($pelabuhan->id_kota==$item->id_kota) 
        <option value="{{ $item->id_kota }}" selected>{{ $item->nama_kota }}</option>
        @else
        <option value="{{ $item->id_kota }}" >{{ $item->nama_kota }}</option>
        @endif
      @else
      <option value="{{ $item->id_kota }}" >{{ $item->nama_kota }}</option>
      @endif
    @endforeach
      </select>
      @if($errors->has('cboKota'))
      <span class="text-danger">{{$errors->first('cboKota')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtNamaPelabuhan">Nama Pelabuhan</label> 
    <div class="col-xs-5">
      <input required="required" class="form-control" name="txtNamaPelabuhan" id="txtNamaPelabuhan" type="text" @if($mode == 'EDIT') value="{{$pelabuhan->nama_pelabuhan}}" @endif>
      @if($errors->has('txtNamaPelabuhan'))
      <span class="text-danger">{{$errors->first('txtNamaPelabuhan')}}
      @endif
    </div>
  </div>
  
  <div class="form-group row">
    <div class="col-xs-offset-2 col-xs-5">
      <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
      <a href="{{route('setting.pelabuhan')}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
    </div>
  </div>
</form>



@endsection

@section('footer_script')
<script>
    
    $('.cboKota').select2();
    
</script>
@endsection
