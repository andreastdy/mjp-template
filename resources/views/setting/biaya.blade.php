@extends('layouts.default')

@section('content_header')
<h1>
    BIAYA
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Master</a></li>
    <li class="active">Biaya</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
<!-- <a href="{{route('setting.biaya.add')}}" data-toggle="tooltip" title="Tambah Biaya"><button type='button' class='btn btn-danger'><i class='fa fa-plus'></i> Buat Baru</button></a> -->

<div class="portlet-body">
    <table class="table table-bordered" id="biayaTable" style='background-color: white'>
        
        

    </table>
</div>



@endsection

@section('footer_script')
<script>

$(document).ready(function () {
    function refresh() {
    $('#biayaTable').bootstrapTable('refresh',{
      url:route('setting.biaya.data')
    });
    
  }

    $('#biayaTable').bootstrapTable({
        url: route('setting.biaya.data'),
    pagination: true,
  search: true,
  sidePagination:'server',
  columns: [{
    title: 'NO',
    formatter: function (value, row, index) {
        return index+1;
      }
  }, {
    field: 'kode_biaya',
    title: 'KODE BIAYA',
    sortable : true
  }, {
    field: 'nama_biaya',
    title: 'NAMA BIAYA',
    sortable : true
  },{
    
    title: 'NOMINAL BIAYA (IDR)',
    field: 'nominal_biaya',
    align:'right',
    sortable : true,
    formatter: function (value, row, index) {
        return addCommas(row.nominal_biaya.toString());
    }
  },{
      title:'AKSI',
      formatter: function(value, row, index) {
        var btnString = "";
        @can('edit biaya')
        btnString = '<a href="'+route('setting.biaya.ubah',{id:row.id_biaya})+'" data-toggle="tooltip" title="Edit Biaya"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>'
        @endcan
        //  btnString +='<button type="button" class="btn btn-delete btn-xs btn-danger" title="delete biaya"><i class="fa fa-trash"></i></button></a>'
         
        
        return btnString;
      }
      // events:{
      //   'click .btn-delete': function(e, value, row, index) {
      //     if (confirm('Are you sure you want to delete this record?')) {
      //       $.ajax({
      //         url: route('setting.biaya.delete',{id:row.id_biaya}),
      //         data:{_token:$('meta[name="csrf_token"]').attr('content')},
      //         method:'post'
      //       })
      //       .then(function (response){
      //         if (response.status == 'error') {
      //           alert(response.description);
      //         } else if( response.status == 'success') {
      //           alert(response.description);
      //           refresh();
      //         }
      //       }); 
      //     }
      //   }
      // }
    }]
})
});
    

</script>
@endsection
