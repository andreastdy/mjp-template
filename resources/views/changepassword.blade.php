@extends('layouts.default')

@section('content_header')
<h1>
    UBAH PASSWORD
</h1>
@endsection

@section('content')
@include('includes/notifications')
<hr>
<form action="{{ route('change_password') }}" method="post" class="form-horizontal">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="form-group">
    <label class="control-label col-xs-2" for="txtPassword">Old Password</label> 
    <div class="col-xs-2">
      <input required="required" class="form-control" name="txtPassword" id="txtPassword" type="password" >
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="NewPassword">New Password</label> 
    <div class="col-xs-2">
      <input required="required" class="form-control" name="NewPassword" id="NewPassword" type="password" >
    </div>
  </div>
 <div class="form-group">
    <label class="control-label col-xs-2" for="NewPassword_confirmation">Confirm Password</label> 
    <div class="col-xs-2">
    <input required="required" class="form-control" name="NewPassword_confirmation" id="NewPassword_confirmation" type="password" >
    </div>
  </div>
    
  <div class="form-group row">
    <div class="col-xs-offset-2 col-xs-6">
      <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
      <a href="/"><button type="button" class="btn btn-danger" name="cancel">Cancel</button>
    </div>
  </div>
</form>

@endsection

@section('footer_script')
<script>
    
  
</script>
@endsection
