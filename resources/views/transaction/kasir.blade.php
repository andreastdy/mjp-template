@extends('layouts.default')

@section('content_header')
<h1>
    KASIR
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">Kasir</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
<a href="{{route('transaction.kasir.add')}}" data-toggle="tooltip" title="Add Kas"><button type='button' class='btn btn-danger'><i class='fa fa-plus'></i> Buat Baru</button></a>
<input type="text" placeholder="search.." style="float: right; margin-top:1%"></input>

<div class="portlet-body">
    <table class="table table-bordered" id="customerOrderTable" style='background-color: white'>
        <tr>
            <th>NO</th>
            <th>KETERANGAN</th>
            <th>CASH IN</th>
            <th>CASH OUT</th>
            <!-- <th>ATTACHMENT</th> -->
            <th>CREATED</th>
        </tr>
        <tr>
            <td>1</td>
            <td>Kas Kecil</td>
            <td>1.200.000</td>
            <td>0</td>
            <!-- <td>-</td> -->
            <td>Kasir <br/>(01-12-2018 12:34:02)</td>
        </tr>
        <tr>
             <td>2</td>
            <td>Pembelian Alat Tulis</td>
            <td>0</td>
            <td>200.000</td>
            <!-- <td>-</td> -->
            <td>Kasir <br/>(03-12-2018 09:34:02)</td>
        </tr>
        

    </table>
</div>






@endsection

@section('footer_script')
<script>

    $('#pelayaranTable').bootstrapTable({});


</script>
@endsection
