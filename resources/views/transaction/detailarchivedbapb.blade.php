@extends('layouts.default')

@section('content_header')
<h1>
    DETAIL ARCHIVED BAPB
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">Archived BAPB</li>
</ol>
@endsection

@section('content')
<hr>
<form class="form-horizontal">
<div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">No ORDER</label> 
    <div class="col-xs-3">
      
      <label class="checkbox-inline">
        ORD290191
      </label>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">No BAPB</label> 
    <div class="col-xs-3">
      
      <label class="checkbox-inline">
        290191BA
      </label>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="chkPortToDoor">Kode Trip</label> 
    <div class="col-xs-4">
      <label class="checkbox-inline">
        -
      </label>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtAlamat">Alamat</label> 
    <div class="col-xs-4">
      
      <label class="checkbox-inline">
        Jl. Alamat
      </label>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">Penerima</label> 
    <div class="col-xs-3">
      <label class="checkbox-inline">
        CONSIGNEE
      </label>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="chkPortToDoor">Pelabuhan</label> 
    <div class="col-xs-4">
      <label class="checkbox-inline">
        Tanjung Priok
      </label>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="chkPortToDoor">Nama Kapal</label> 
    <div class="col-xs-4">
      <label class="checkbox-inline">
        Armada Segara
      </label>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="chkPortToDoor">Tanggal Berangkat</label> 
    <div class="col-xs-4">
      <label class="checkbox-inline">
        07-12-2018 07:00:00
      </label>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="chkPortToDoor">Keterangan Total</label> 
    <div class="col-xs-4">
      <label class="checkbox-inline">
        1000
      </label>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">No Container</label> 
    <div class="col-xs-3">
      <label class="checkbox-inline">
        MRTU290198
      </label>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">Seal</label> 
    <div class="col-xs-3">
      <label class="checkbox-inline">
        29019829
      </label>
    </div>
  </div>
  
  
</form>

<div id="detailRute">
<input type="text" placeholder="search.." style="float: right; margin-top:1%"></input>

<div class="portlet-body">
    <table class="table table-bordered" id="detailCustomerProposalTable" style='background-color: white'>
        <tr>
            <th>NO</th>
            <th>SURAT JALAN</th>
            <th>ALAMAT</th>
            <th>PENERIMA</th>
            <th>KETERANGAN TOTAL</th>
            <th >AKSI</th>
        </tr>
        <tr>
            <td>1</td>
            <td>SJ09109</td>
            <td>Jl. Kebon Kacang</td>
            <td>CONSIGNEE A</td>            
            <td>1200</td>
            <td>
                 <a href="#" data-toggle="tooltip" title="View Detail"><button type='button' class='btn btn-primary' data-toggle="modal" data-target="#DetailModal"><i class='fa fa-eye'></i></button></a>
            </td>
        </tr>
        <tr>
            <td>2</td>
            <td>SJ12093</td>
            <td>Jl. Kelapa Gading</td>
            <td>CONSIGNEE B</td>            
            <td>1000</td>
            <td>

                 <a href="#" data-toggle="tooltip" title="View Detail"><button type='button' class='btn btn-primary' data-toggle="modal" data-target="#DetailModal"><i class='fa fa-eye'></i></button></a>
            </td>
        </tr>
        

    </table>
</div>

</div>

<div id="DetailModal" class="modal fade" role="dialog" >
    <div class="modal-dialog" >

        <!-- Modal content-->
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Detail Surat Jalan</h4>
            </div>
            <div class="modal-body">
            <table class="table table-bordered" id="customerOrderTable" style='background-color: white'>
        <tr>
            <th>NO</th>
            <th>PARTY</th>
            <th>NAMA BARANG</th>
            <th>P</th>
            <th>L</th>
            <th>T</th>
            <th>M3/TON</th>
        </tr>
        <tr>
            <td>1</td>
            <td>PARTY A</td>
            <td>BARANG A</td>
            <td>150</td>
            <td>100</td>
            <td>50</td>
            <td>12</td>
            
        </tr>
        <tr>
            <td>2</td>
            <td>PARTY B</td>
            <td>BARANG B</td>
            <td>100</td>
            <td>50</td>
            <td>10</td>
            <td>2</td>
        </tr>
        

    </table>
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

@endsection

@section('footer_script')
<script>
    
   // $("#detailRute").hide();


    
    function editPelayaran(){
       // window.open(' {{route('master.pelayaran.edit')}} ', "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, width=1400, height=800");
        //openPopup({{route('master.pelayaran.edit')}}, 'Title', 1400, 800, 'scrollbars,resizable', true, false);
    }
</script>
@endsection
