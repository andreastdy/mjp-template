<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" type="text/css" href="../public/css/bootstrap.min.css"/>
	<title>Hi</title>
</head>
<style>
    #customer{
        font-size: 8px
    }
	table#detail{
		font-size: 6px;
	}
	body{
		font-size: 8px;
	}
	sup {
    font-size: 75%;
    line-height: 0;
    position: relative;
    vertical-align: baseline;
	}
	sup {
		top: -0.5em;
	}
</style>
<body>
<!-- <span class="pagenum"></span> -->
	<!-- <header>
		KODE SURAT : <b>{</b>
	</header>
	<br/>
	<br/>
	<div id="image">
		<img src="../public/img/350x150.png"/>
	</div>
	<br/>
	<div id="title">
	<h2><b>Proposal Tarif Jasa Ekspedisi</b></h2>
	</div>
	<br/> -->
	<div id="customer" class="col-md-4">
		<p>
			Kepada,<br/>
			<b>{{$listbapb[0]->nama_dooring}}</b><br/>
			Di {{$listbapb[0]->alamat}}<br/>
			{{$listbapb[0]->nama_kota}}<br/>
		</p>
	</div>
	<br/>
    <br/>
	<div id="isi">
		<p>
			Dengan hormat,<br/>
			Bersama ini kami sampaikan rekap dokumen asli (Berita acara & DO pabrik) yang sampai hari ini belum kami terima kembali dengan perincian sbb :
		</p>
		<div class="col-md-12">
		<table id="detail" class="table" style="border-top:solid">
            
			<thead>
            <!-- <tr>
            <td colspan="8"><hr/></td>
            </tr> -->
				<tr>
				<th style="width:10%">No BAPB</th>
				<th style="width:10%">No Cont</th>
				<th style="width:10%">Kode</th>
				<th style="width:15%">Kapal</th>
				<th style="width:10%">TD</th>
				<th style="width:5%">Kolli</th>
				<th style="width:10%">Jenis Barang</th>
                <th style="width:10%">Perwakilan</th>
			</tr>
            <!-- <tr>
            <td colspan="8"><hr/></td>
            </tr> -->
			</thead>
            
			<tbody>
				@foreach($listbapb as $item)
				<tr>
					<td>{{$item->no_bapb}}</td>
					<td>{{$item->no_container}}</td>
					<td>{{$item->kode_trip}}</td>
                    <td>{{$item->nama_kapal}}</td>
					<td>{{Carbon\Carbon::parse($item->tanggal_berangkat)->formatLocalized('%d/%m/%Y')}}</td>				
					<td>{{$item->total_kolli}}</td>
                    <td>{{$item->nama_barang}}</td>
                    <td>{{$item->nama_consignee}}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
        <hr/>
		</div>
        
		<br/>
		<br/>
		<p>
		NB : Harap diperiksa kembali BAPB & DO pabrik untuk kelengkapan tanda tangan, stampel & nama terang penerima barang & perwakilan.
		</p>
		<br/>
        <p>
		Demikian pemberitahuan kami, atas perhatian dan bantuannya kami sampaikan terima kasih.
		</p>
		<table class="table table-no-bordered" style="border-top:hidden">
			<tr>
				<td>Sidoarjo, {{Carbon\Carbon::now()->formatLocalized('%d %B %Y')}}<br/>Hormat kami,<br/><br/><br/><br/></td>
				<td></td>
			</tr>
		</table>
	</div>
</body>
</html>