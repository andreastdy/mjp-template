@extends('layouts.default')

@section('content_header')
<h1>
@if ($mode == 'ADD')
    ADD CUSTOM INVOICE
@else
    EDIT CUSTOM INVOICE
@endif
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">Custom Invoice</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@if($mode=='ADD')
<form action="{{ route('transaction.custominvoice.create') }}" method="post" class="form-horizontal">
@else
<form action="{{ route('transaction.custominvoice.update',$custominvoice->id_custominvoice) }}" method="post" class="form-horizontal">
@endif
<input type="hidden" name="_token" value="{{ csrf_token() }}">
@if($mode!='ADD')
<input type="hidden" name="id_custominvoice" value="{{$custominvoice->id_custominvoice}}">
@endif
<div class="form-group">
    <label class="control-label col-xs-2" for="txtNoCustomInvoice">No Custom Invoice</label> 
    <div class="col-xs-2">
      <input class="form-control" name="txtNoCustomInvoice" id="txtNoCustomInvoice" type="text" @if($mode=='EDIT') value="{{$custominvoice->no_custominvoice}}" @endif readonly>
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtTglNota">Tanggal Nota</label> 
    <div class="col-xs-2">
      <input required="required" class="form-control" name="txtTglNota" id="txtTglNota"  type="text" @if($mode=='EDIT' ) value="{{date('d-m-Y', strtotime($custominvoice->tgl_custominvoice))}}"  @endif>
      @if($errors->has('txtTglNota'))
      <span class="text-danger">{{$errors->first('txtTglNota')}}
      @endif
    </div>
  </div>
 <div class="form-group">
    <label class="control-label col-xs-2" for="cboCustomer">Nama Customer</label> 
    <div class="col-xs-4">
      <select class="select form-control cboCustomer" name="cboCustomer" id="cboCustomer">
      <option value="0">-- PILIH CUSTOMER --</option>
      @foreach($customer as $item)
        @if($mode=='ADD')
        <option value="{{ $item->id_customer }}" >{{ $item->nama_pt }}</option>
        @else
          @if($item->id_customer==$custominvoice->id_customer)
          <option value="{{ $item->id_customer }}" selected>{{ $item->nama_pt }}</option>
          @else
          <option value="{{ $item->id_customer }}" >{{ $item->nama_pt }}</option>
          @endif
        @endif
      @endforeach
      </select>
      @if($errors->has('cboCustomer'))
      <span class="text-danger">{{$errors->first('cboCustomer')}}
      @endif
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtKeteranganBiaya">Keterangan Biaya<br/><span class="opsional"><sup>(opsional)</sup></span></label> 
    <div class="col-xs-4">
      <input class="form-control" name="txtKeteranganBiaya" id="txtKeteranganBiaya" type="text" @if($mode=='EDIT') value="{{$custominvoice->keterangan_biaya}}" @endif>
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtNominalBiaya">Nominal</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input required="required" class="form-control input-element" name="txtNominalBiaya" id="txtNominalBiaya" onkeypress="return isNumberKey(event)" type="text"  style="text-align: right" @if($mode=='EDIT') value="{{$custominvoice->nominal_biaya}}" @else value="0" @endif>
        @if($errors->has('txtNominalBiaya'))
      <span class="text-danger">{{$errors->first('txtNominalBiaya')}}
      @endif
      </div>
    </div>
  </div>
  
  
    
  <div class="form-group row">
    <div class="col-xs-offset-2 col-xs-6">
      <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
      <a href="{{route('transaction.custominvoice')}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button>
    </div>
  </div>
</form>



@endsection

@section('footer_script')
<script>
    $(".cboCustomer").select2();

    var cleave = new Cleave('.input-element', {
    numeral: true,
    numeralThousandsGroupStyle: 'thousand',
    autoUnmask : true
});
   
    $('#txtTglNota').datetimepicker({
      minView: 2,
      format: 'dd-mm-yyyy',
      autoclose: true,
      todayBtn: true
    });

</script>
@endsection
