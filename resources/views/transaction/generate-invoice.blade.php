<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" type="text/css" href="../public/css/bootstrap.min.css"/>
	<title>Hi</title>
</head>
<style>
    #isi{
        border: 1px solid black;
        padding: 15px;
        
    }
    #customer{
        font-size: 10px;
        text-align:center;
    }
	table{
		font-size: 10px;
        width:650px;
	}
	table#detail{
		font-size: 10px;
        width:650px;
	}
	table#detail1{
		font-size: 10px;
        width:650px;
	}
	body{
		font-size: 10px;
		height: 50%;
	}
	.isitotal{
		width:50%;
	}
    
	sup {
		font-size: 75%;
		line-height: 0;
		position: relative;
		vertical-align: baseline;
	}
	sup {
		top: -0.5em;
	}
    hr{
        border:1px solid black;
    }
</style>
<body >
<!-- <span class="pagenum"></span> -->
	<!-- <header>
		KODE SURAT : <b>{</b>
	</header>
	<br/>
	<br/>
	<div id="image">
		<img src="../public/img/350x150.png"/>
	</div>
	<br/>
	<div id="title">
	<h2><b>Proposal Tarif Jasa Ekspedisi</b></h2>
	</div>
	<br/> -->
	<div id="customer" class="col-md-5">
		<h5><b>PT.MANDALA JASA PRATAMA</b></h5>
        <p style="margin-top:5px">JL. Raya Ir. H. Juanda No. 1 Sidoarjo - Jawa Timur Indonesia. 61253</p>
	</div>

	<div id="isi">
		
		<table id="detail"  >
            
				<tr>
				<td style="width:13%"><b>Kwitansi No</b></td>
                <td style="width:2%">:</td>
                <td style="width:80%"><b>18090243</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b style="font-size:14px;">KPG-18-020</b></td>
                
			    </tr>

                <tr>
				<td>Nama Kapal</td>
                <td>:</td>
                <td>MERATUS BANJAR 1</td>
                
			    </tr>

                <tr>
				<td>Tanggal Berangkat</td>
                <td>:</td>
                <td>26/09/2018</td>
                
			    </tr>

                <tr>
				<td>Untuk Pembayaran</td>
                <td>:</td>
                <td>Biaya Pengiriman Ke Kupang Dengan Container</td>
                
			    </tr>

                <tr>
				<td>Jenis Barang</td>
                <td>:</td>
                <td>750 Pcs Otsucath 24c X 3/4", 10 Pcs Otsu Menthol 20 / 250ml, Asering Softbag 500ml 750 Pcs Otsucath 24c X 3/4", 10 Pcs Otsu Menthol 20 / 250ml, Asering Softbag 500ml</td>
                
			    </tr>
            
			
		</table>
        <hr/>
        <table id="detail1">
            <tr>
            <td style="width:13%" class="judul">Terima Dari</td>
            <td style="width:2%">:</td>
            <td style="width:30%">PT. MERAPI UTAMA PHARMA JAKARTA</td>
            <td style="width:2%"></td>
			<td style="width:5%"></td>
			<td style="width:10%"></td>
			<td style="width:4%"></td>
			<td style="width:10%"></td>
            </tr>

            <tr>
            <td class="judul">Banyaknya Uang</td>
            <td>:</td>
            <td style="border:1px solid black;padding:2px;" class="isitotal" colspan="6">{{strtoupper( Terbilang::make(17540000) )}} RUPIAH</td>
            </tr>

			<tr>
			<td>Perincian</td>
			<td>:</td>
			<td>Soe</td>
			<td>1</td>
			<td>Unit</td>
			<td style="text-align:right">x 17.400.000 </td>
			<td> &nbsp;&nbsp; Rp.</td>
			<td style="text-align:right">17.400.000</td>
			</tr>

			<tr>
			<td style="text-align:right" colspan="6">PPN </td>
			<td> &nbsp;&nbsp; Rp.</td>
			<td style="text-align:right">174.000</td>
			</tr>

			<tr style="border-bottom: 1px solid;border-top: 1px solid;">
			<td style="text-align:right" colspan="6">Jumlah </td>
			<td> &nbsp;&nbsp; Rp.</td>
			<td style="text-align:right">17.574.000</td>
			</tr>
        </table>

			<br/>
			

        <table >
			<tr>
				<td style="text-align:right">Sidoarjo, {{Carbon\Carbon::now()->formatLocalized('%d %B %Y')}}<br/><br/><br/><br/></td>
				<td></td>
			</tr>
		</table>
		</div>
        
		<br/>
		<br/>
		<br/>
		
	</div>
</body>
</html>