@extends('layouts.default')

@section('content_header')
<h1>
    INVOICE
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">Invoice</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>

@can('add invoice')
<a href="#" data-toggle="tooltip" title="Add Invoice"><button type='button' class='btn btn-danger' data-toggle="modal" data-target="#ListModal"><i class='fa fa-plus'></i> Buat Baru</button></a>
@endcan

<div class="portlet-body">
    <table class="table table-bordered" id="invoiceTable" style='background-color: white'>
        
        

    </table>
</div>

<div id="DetailModal" class="modal fade" role="dialog" >
    <div class="modal-dialog" >

        <!-- Modal content-->
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">List Invoice Additional</h4>
            </div>
            <div class="modal-body">
            <table class="table table-bordered" id="invoiceAdditionalTable" style='background-color: white'>
        <tr>
            <th>NO</th>
            <th>NAMA BIAYA</th>
            <th>BIAYA</th>
            <th>TOTAL</th>
        </tr>
        <tr>
            <td>2</td>
            <td>Extra Cost</td>
            <td style="text-align: right">100.000</td>
            <td style="text-align: right">100.000</td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: right">GRAND TOTAL</td>
            <td style="text-align: right">100.000</td>
        </tr>
        

    </table>
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<div id="ListModal" class="modal fade" role="dialog" >
    <div class="modal-dialog" style="width:900px;">

        <!-- Modal content-->
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">List Trip</h4>
            </div>
            <div class="modal-body">
            <table class="table table-bordered" id="ListTripTable" style='background-color: white'>
        
        

    </table>
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


<div id="UpdateModal" class="modal fade" role="dialog" >
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update Tanggal Penagihan</h4>
            </div>
            <div class="modal-body">
            <form class="form-horizontal">
            <input type="hidden" name="idinvoice" id="idinvoice" value="">
  <div class="form-group">
    <label class="control-label col-xs-4" for="txtNoInvoice">No Kuitansi</label> 
    <div class="col-xs-4">
        <input type="text" class="form-control" name="txtNoInvoice" id="txtNoInvoice" value="" readonly disabled></input>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-4" for="txtTglPenagihan">Tanggal Penagihan</label> 
    <div class="col-xs-4">
      <input type="date" class="form-control" name="txtTglPenagihan" id="txtTglPenagihan"></input>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-xs-offset-6 col-xs-4">
      <a href="#"><button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Update</button></a>
    </div>
  </div>
</form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

@endsection

@section('footer_script')
<script>

function refresh(){
    $('#invoiceTable').bootstrapTable('refresh',{
                url: route('transaction.invoice.data'),
            });
}

$('#invoiceTable').bootstrapTable({
        classes:'table table-hover table-no-bordered',
        url: route('transaction.invoice.data'),
    pagination: true,
    sidePagination: 'server',
  search: true,
  columns: [{
    title: 'NO',
    formatter: function (value, row, index) {
        return index+1;
      }
  }, {
    field: 'no_invoice',
    title: 'NO KUITANSI',
    sortable: true
  },{
    title: 'TANGGAL KUITANSI',
    field: 'tgl_invoice',
    formatter : function (value, row, index) {
        return moment(value).format('DD-MM-YYYY');
      },
    sortable: true
  },{
    field: 'kode_trip',
    title: 'KODE TRIP',
    sortable: true
  },{
    title: 'CREATED',
    field: 'created_at',
    formatter : function (value, row, index) {
        return row.created_by+'<br/>('+moment(row.created_at).format('DD-MM-YYYY HH:mm:ss')+')';
      },
    sortable: true
  },{
    title: 'STATUS',
    field: 'status',
    formatter : function (value, row, index) {
      if(value==0){
          return "PENDING";
        }else{
          return "VERIFIED";
        }
      },
    sortable: true
  },{
    title: 'VERIFIED',
    field: 'verified_at',
    formatter : function (value, row, index) {
        if(row.verified_at==null){
            return "-";
        }else{
            return row.verified_by+'<br/>('+moment(row.verified_at).format('DD-MM-YYYY HH:mm:ss')+')';
        }
        
      },
    sortable: true
  },{
      title:'AKSI',
      formatter: function(value, row, index) {
        var btnString = '';
        if(row.status==0){
          @can('edit invoice')
            btnString += '<a href="'+route('transaction.invoice.edit',{id:row.id_invoice,idtrip:row.id_trip,idcustomer:row.id_customer})+'" data-toggle="tooltip" title="Edit Invoice"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>';
          @endcan
          btnString += '<a href="'+route('transaction.invoice.view',{id:row.id_invoice,idtrip:row.id_trip,idcustomer:row.id_customer})+'" data-toggle="tooltip" title="View Detail"><button type="button" class="btn btn-xs btn-primary" ><i class="fa fa-eye"></i></button></a>';
        //   btnString +='<a href="#" data-toggle="tooltip" title="Update Tanggal Penagihan"><button type="button" class="btn btn-update btn-success" data-toggle="modal" data-target="#UpdateModal"><i class="fa fa-calendar"></i></button></a>';

          @can('verify invoice')
          btnString += '<button type="button" class="btn btn-verify btn-xs btn-success" title="Verify Invoice"><i class="fa fa-check"></i></button></a>';
          @endcan
          
          @can('edit invoice')
          btnString +='<button type="button" class="btn btn-delete btn-xs btn-danger" title="Delete Invoice"><i class="fa fa-times"></i></button></a>';
          @endcan

          @can('print invoice')
          btnString += '<a href="'+route('transaction.invoice.print')+'" data-toggle="tooltip" title="Print Invoice"><button type="button" class="btn btn-xs btn-primary"><i class="fa fa-print"></i></button></a>'
          @endcan
        }else{
            btnString += '<a href="'+route('transaction.invoice.view',{id:row.id_invoice,idtrip:row.id_trip,idcustomer:row.id_customer})+'" data-toggle="tooltip" title="View Detail"><button type="button" class="btn btn-xs btn-primary" ><i class="fa fa-eye"></i></button></a>';
            
            @can('print invoice')
            btnString += '<a href="'+route('transaction.invoice.print')+'" data-toggle="tooltip" title="Print Invoice"><button type="button" class="btn btn-xs btn-primary"><i class="fa fa-print"></i></button></a>'
            @endcan
        }
          
        
        return btnString;
      },
      events:{
        'click .btn-update': function(e, value, row, index) {
            $("#txtNoInvoice").val(row.no_invoice);
            if(row.tgl_penagihan!="null"||row.tgl_penagihan!=null){
              $('#txtTglPenagihan').val("");
            }else{
              $('#txtTglPenagihan').val(moment(row.tgl_penagihan).format('DD-MM-YYYY HH:mm:ss'));
            }
            $("#idinvoice").val(row.id_bapb);
        },
        'click .btn-delete': function(e, value, row, index) {
          if (confirm('Anda yakin akan menghapus invoice ini?')) {
            $.ajax({
              url: route('transaction.invoice.delete',{id:row.id_invoice}),
              data:{_token:$('meta[name="csrf_token"]').attr('content')},
              method:'post'
            })
            .then(function (response){
              if (response.status == 'error') {
                alert(response.description);
              } else if( response.status == 'success') {
                alert(response.description);
                refresh();
              }
            }); 
          }
        },'click .btn-verify': function(e, value, row, index) {
          if (confirm('Anda yakin akan melakukan verifikasi terhadap invoice ini?')) {
            $.ajax({
              url: route('transaction.invoice.verify',{id:row.id_invoice}),
              data:{_token:$('meta[name="csrf_token"]').attr('content')},
              method:'post'
            })
            .then(function (response){
              if (response.status == 'error') {
                alert(response.description);
              } else if( response.status == 'success') {
                alert(response.description);
                refresh();
              }
            }); 
          }
        }
      }
    }]
});

$('#ListTripTable').bootstrapTable({
  classes:'table table-hover table-no-bordered',
  url: route('transaction.invoice.datatrip'),
  pagination : true,
  search : true,
columns: [{
title: 'NO',
formatter: function (value, row, index) {
  return index+1;
}
}, {
    field: 'kode_trip',
    title: 'KODE TRIP',
    sortable: true
  }, {
    field: 'nama_pt',
    title: 'NAMA CUSTOMER',
    sortable: true
  },{
title:'AKSI',
formatter: function(value, row, index) {
  var btnString = '<a href="'+route('transaction.invoice.add',{idtrip:row.id_kodetrip,idcustomer:row.id_customer})+'" data-toggle="tooltip" title="Add Invoice"><button type="button" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i></button></a>'
  return btnString;
}
}]
})

</script>
@endsection
