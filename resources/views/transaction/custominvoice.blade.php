@extends('layouts.default')

@section('content_header')
<h1>
    CUSTOM INVOICE
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">Custom Invoice</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>

@can('edit custom invoice')
<a href="{{route('transaction.custominvoice.add')}}" data-toggle="tooltip" title="Tambah Custom Invoice"><button type='button' class='btn btn-danger'><i class='fa fa-plus'></i> Buat Baru</button></a>
@endcan

<div class="portlet-body">
    <table class="table table-bordered" id="customInvoiceTable" style='background-color: white'>
        
        

    </table>
</div>



@endsection

@section('footer_script')
<script>
function refresh() {
    $('#customInvoiceTable').bootstrapTable('refresh',{
      url:route('transaction.custominvoice.data')
    });
}

$('#customInvoiceTable').bootstrapTable({
        classes:'table table-hover table-no-bordered',
        url: route('transaction.custominvoice.data'),
    pagination: true,
    sidePagination: true,
  search: true,
  columns: [{
    title: 'NO',
    formatter: function (value, row, index) {
        return index+1;
      }
  },  {
    field: 'no_custominvoice',
    title: 'NO CUSTOM INVOICE',
    sortable: true
  },{
    field: 'tgl_custominvoice',
    title: 'TANGGAL NOTA',
    formatter : function (value, row, index) {
      if(value==null){
          return "-";
        }else{
          return moment(value).format('DD-MM-YYYY');
        }
      },
    sortable: true
  },{
    field: 'nama_customer',
    title: 'NAMA CUSTOMER',
    sortable: true
  },{
    field: 'keterangan_biaya',
    title: 'KETERANGAN BIAYA',
    sortable: true
  },{
    title: 'NOMINAL BIAYA (IDR)',
    field: 'nominal_biaya',
    align:'right',
    formatter: function (value, row, index) {
        return addCommas(row.nominal_biaya.toString());
    },
    sortable: true
  },{
      title:'AKSI',
      formatter: function(value, row, index) {
        var btnString = '';
        @can('edit custom invoice')
          btnString += '<a href="'+route('transaction.custominvoice.edit',{id:row.id_custominvoice})+'" data-toggle="tooltip" title="Edit Custom Invoice"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>';
           btnString +='<button type="button" class="btn btn-delete btn-xs btn-danger" title="Delete Custom Invoice"><i class="fa fa-trash"></i></button></a>'
        @endcan
         
        return btnString;
      },
      events:{
        'click .btn-delete': function(e, value, row, index) {
          if (confirm('Anda yakin untuk menghapus custom invoice ini?')) {
            $.ajax({
              url: route('transaction.custominvoice.delete',{id:row.id_custominvoice}),
              data:{_token:$('meta[name="csrf_token"]').attr('content')},
              method:'post'
            })
            .then(function (response){
              if (response.status == 'error') {
                alert(response.description);
              } else if( response.status == 'success') {
                alert(response.description);
                refresh();
              }
            }); 
          }
        }
      }
    }]
});


</script>
@endsection
