@extends('layouts.default')

@section('content_header')
<h1>
@if ($mode == 'ADD')
    ADD DETAIL CUSTOMER PROPOSAL 
@elseif ($mode == 'EDIT')
    EDIT DETAIL CUSTOMER PROPOSAL 
@else
    DETAIL CUSTOMER PROPOSAL 
@endif
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">Customer Proposal</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@if($mode=='ADD')
<form action="{{ route('transaction.customerproposal.detailcreate',$proposal->id_proposal) }}" method="post" class="form-horizontal">
@else
<form action="{{ route('transaction.customerproposal.detailupdate',$proposaldetail->id_proposaldetail) }}" method="post" class="form-horizontal">
@endif
<input type="hidden" name="_token" value="{{ csrf_token() }}">
@if($mode=='ADD')
<input name="txtIdProposal" id="txtIdProposal" type="hidden" value="{{$proposal->id_proposal}}">
@else
<input name="txtIdProposal" id="txtIdProposal" type="hidden" value="{{$proposaldetail->id_proposal}}">
@endif
    <div class="col-md-6">
    <div class="form-group">
    <label class="control-label col-xs-3" for="chkPortToDoor">No Proposal</label> 
    <div class="col-xs-6">
      <label class="checkbox-inline">
      @if($mode=='ADD')
        {{$proposal->no_proposal}}
      @else
        {{$proposaldetail->no_proposal}}
      @endif
      </label>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" for="chkPortToDoor">Customer</label> 
    <div class="col-xs-6">
      <label class="checkbox-inline">
      @if($mode=='ADD')
        {{$proposal->nama_pt}}
      @else
        {{$proposaldetail->nama_pt}}
      @endif
      </label>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" for="txtValidFrom">Periode Awal</label> 
    <div class="col-xs-6">
      <input required="required" class="form-control" name="txtValidFrom" id="txtValidFrom" type="text" @if($mode!='ADD' ) value="{{date('d-m-Y', strtotime($proposaldetail->valid_from))}}"  @endif @if($mode=='VIEW') readonly @endif>
      @if($errors->has('txtValidFrom'))
      <span class="text-danger">{{$errors->first('txtValidFrom')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" for="txtValidTo">Periode Akhir<br/><span class="opsional"><sup>(opsional)</sup></span></label> 
    <div class="col-xs-6">
      <input class="form-control" name="txtValidTo" id="txtValidTo" type="text" @if($mode!='ADD') @if($proposaldetail->valid_to!=null) value="{{date('d-m-Y', strtotime($proposaldetail->valid_to))}}" @else value="" @endif @endif @if($mode=='VIEW') readonly disabled @elseif($mode=='ADD') @if($proposal->tipe_proposal=='NON-CONTRACT') readonly disabled @endif @else @if($proposaldetail->valid_to==null) readonly disabled @endif @endif>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" for="cboKotaAsal">Kota Asal</label> 
    <div class="col-xs-6">
    <select class="select form-control cboKota" name="cboKotaAsal" id="cboKotaAsal" @if($mode=='VIEW') disabled readonly @endif>
  <option value="0">-- PILIH KOTA ASAL --</option>
    @foreach($city as $item)
      @if($mode == 'EDIT') 
        @if($proposaldetail->id_kota_asal==$item->id_kota) 
        <option value="{{ $item->id_kota }}" selected>{{ $item->nama_kota }}</option>
        @else
        <option value="{{ $item->id_kota }}" >{{ $item->nama_kota }}</option>
        @endif
      @else
      <option value="{{ $item->id_kota }}" >{{ $item->nama_kota }}</option>
      @endif
    @endforeach
      </select>
      @if($errors->has('cboKotaAsal'))
      <span class="text-danger">{{$errors->first('cboKotaAsal')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" for="cboPortAsal">Pelabuhan Asal</label> 
    <div class="col-xs-6">
      <select class="select form-control cboPortAsal" name="cboPortAsal" id="cboPortAsal" @if($mode=='VIEW') disabled readonly @endif>
      <option value="0">-- PILIH PELABUHAN ASAL --</option>
      @foreach($pelabuhan as $item)
      @if($mode != 'ADD') 
        @if($proposaldetail->id_pelabuhan_asal==$item->id_pelabuhan) 
        <option value="{{ $item->id_pelabuhan }}" selected>{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}</option>
        @else
        <option value="{{ $item->id_pelabuhan }}" >{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}</option>
        @endif
      @else
      <option value="{{ $item->id_pelabuhan }}" >{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}</option>
      @endif
    @endforeach
      </select>
      @if($errors->has('cboPortAsal'))
      <span class="text-danger">{{$errors->first('cboPortAsal')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" for="cboKotaTujuan">Kota Tujuan</label> 
    <div class="col-xs-6">
    <select class="select form-control cboKota" name="cboKotaTujuan" id="cboKotaTujuan" @if($mode=='VIEW') disabled readonly @endif>
  <option value="0">-- PILIH KOTA Tujuan --</option>
    @foreach($city as $item)
      @if($mode == 'EDIT') 
        @if($proposaldetail->id_kota_tujuan==$item->id_kota) 
        <option value="{{ $item->id_kota }}" selected>{{ $item->nama_kota }}</option>
        @else
        <option value="{{ $item->id_kota }}" >{{ $item->nama_kota }}</option>
        @endif
      @else
      <option value="{{ $item->id_kota }}" >{{ $item->nama_kota }}</option>
      @endif
    @endforeach
      </select>
      @if($errors->has('cboKotaTujuan'))
      <span class="text-danger">{{$errors->first('cboKotaTujuan')}}
      @endif
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-3" for="cboPortTujuan">Pelabuhan Tujuan</label> 
    <div class="col-xs-6">
      <select class="select form-control cboPortTujuan" name="cboPortTujuan" id="cboPortTujuan" @if($mode=='VIEW') disabled readonly @endif>
      <option value="0">-- PILIH PELABUHAN TUJUAN --</option>
      @foreach($pelabuhan as $item)
      @if($mode != 'ADD') 
        @if($proposaldetail->id_pelabuhan_tujuan==$item->id_pelabuhan) 
        <option value="{{ $item->id_pelabuhan }}" selected>{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}</option>
        @else
        <option value="{{ $item->id_pelabuhan }}" >{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}</option>
        @endif
      @else
      <option value="{{ $item->id_pelabuhan }}" >{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}</option>
      @endif
    @endforeach
      </select>
      @if($errors->has('cboPortTujuan'))
      <span class="text-danger">{{$errors->first('cboPortTujuan')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" for="cboConsignee">Consignee<br/><span class="opsional"><sup>(opsional)</sup></span></label> 
    <div class="col-xs-6">
    <select class="select form-control cboConsignee" name="cboConsignee" id="cboConsignee" @if($mode=='VIEW') disabled readonly @endif>
  <option value="0">-- PILIH CONSIGNEE --</option>
    @foreach($consignee as $item)
      @if($mode == 'EDIT') 
        @if($proposaldetail->id_consignee==$item->id_consignee) 
        <option value="{{ $item->id_consignee }}" selected>{{ $item->nama }}</option>
        @else
        <option value="{{ $item->id_consignee }}" >{{ $item->nama }}</option>
        @endif
      @else
      <option value="{{ $item->id_consignee }}" >{{ $item->nama }}</option>
      @endif
    @endforeach
      </select>
    </div>
  </div>
    <div class="form-group">
    <label for="rdoType" class="control-label col-xs-3">Tipe Proposal</label> 
    <div class="col-xs-6">
      <label class="radio-inline">
      @if($mode != 'ADD') 
        <input type="radio" name="rdoType" id="rdoType" value="CONTAINER" @if($proposaldetail->type_proposal=='CONTAINER') checked @endif @if($mode=='VIEW') disabled readonly @endif>
      @else
      <input type="radio" name="rdoType" id="rdoType" value="CONTAINER" >
      @endif
              Container
      </label>
      <label class="radio-inline">
      @if($mode != 'ADD') 
        <input type="radio" name="rdoType" id="rdoType" value="LCL" @if($proposaldetail->type_proposal=='LCL') checked @endif @if($mode=='VIEW') disabled readonly @endif>
      @else
        <input type="radio" name="rdoType" id="rdoType" value="LCL">
      @endif
              LCL
      </label>
      @if($errors->has('rdoType'))
      <span class="text-danger">{{$errors->first('rdoType')}}
      @endif
      <!-- <label class="radio-inline">
        <input type="radio" name="rdoType" id="rdoType" value="Bongkaran">
              Bongkaran
      </label> -->
    </div>
  </div> 
  <div class="form-group">
    <label class="control-label col-xs-3" for="cboProposalType">Jenis Proposal</label> 
    <div class="col-xs-6">
      <select class="select form-control" name="cboProposalType" id="cboProposalType" onchange="changeJenisProposal()" @if($mode=='VIEW') disabled readonly @endif>
      <option value="0">-- PILIH JENIS PROPOSAL --</option>
      @if($mode != 'ADD') 
        <option value="PORT TO PORT" @if($proposaldetail->jenis_proposal=='PORT TO PORT') selected @endif>Port to Port</option>
        <option value="PORT TO DOOR" @if($proposaldetail->jenis_proposal=='PORT TO DOOR') selected @endif>Port to Door</option>
        <option value="DOOR TO DOOR" @if($proposaldetail->jenis_proposal=='DOOR TO DOOR') selected @endif>Door to Door</option>        
        <option value="DOOR TO PORT" @if($proposaldetail->jenis_proposal=='DOOR TO PORT') selected @endif>Door to Port</option> 
      @else
        <option value="PORT TO PORT">Port to Port</option>
        <option value="PORT TO DOOR">Port to Door</option>
        <option value="DOOR TO DOOR">Door to Door</option>    
        <option value="DOOR TO PORT">Door to Port</option>    
      @endif
      </select>
      @if($errors->has('cboProposalType'))
      <span class="text-danger">{{$errors->first('cboProposalType')}}
      @endif
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-3" for="cboJenisBarang">Jenis Barang</label> 
    <div class="col-xs-6">
      <select class="select form-control" name="cboJenisBarang" id="cboJenisBarang" @if($mode=='VIEW') disabled readonly @endif>
      <option value="0">-- PILIH JENIS BARANG --</option>
      @if($mode != 'ADD') 
      <option value="GENERAL" @if($proposaldetail->jenis_barang=='GENERAL') selected @endif>General Cargo</option>
        <option value="DG" @if($proposaldetail->jenis_barang=='DG') selected @endif>Dangerous Goods</option>
      @else
        <option value="GENERAL">General Cargo</option>
        <option value="DG">Dangerous Goods</option>
        @endif
      </select>
      @if($errors->has('cboJenisBarang'))
      <span class="text-danger">{{$errors->first('cboJenisBarang')}}
      @endif
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-3" for="txtAlamatAsal">Alamat Asal</label> 
    <div class="col-xs-6">
      <textarea required="required" class="form-control" rows="3" cols="40" name="txtAlamatAsal" id="txtAlamatAsal" @if($mode=='VIEW')  readonly @endif>@if($mode != 'ADD') {{$proposaldetail->alamat_asal}} @endif</textarea>
      @if($errors->has('txtAlamatAsal'))
      <span class="text-danger">{{$errors->first('txtAlamatAsal')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" for="txtAlamatTujuan">Alamat Tujuan</label> 
    <div class="col-xs-6">
      <textarea required="required" class="form-control" rows="3" cols="40" name="txtAlamatTujuan" id="txtAlamatTujuan" @if($mode=='VIEW')  readonly @endif>@if($mode != 'ADD') {{$proposaldetail->alamat_tujuan}} @endif</textarea>
      @if($errors->has('txtAlamatTujuan'))
      <span class="text-danger">{{$errors->first('txtAlamatTujuan')}}
      @endif
    </div>
  </div>
  @if($mode!='ADD')
    @if($proposaldetail->type_proposal=='CONTAINER')
    <div class="form-group" id="freightsize">
    @else
    <div class="form-group" id="freightsize" hidden>
    @endif
  @else
  <div class="form-group" id="freightsize">
  @endif
    <label class="control-label col-xs-3">Freight Size</label> 
    <div class="col-xs-6">
      <label class="radio-inline">
        <input type="radio" name="rdoFtsize" id="rdoFtSize1" value="20 FT" @if($mode!='ADD') @if($proposaldetail->freight_size=='20 FT') checked @endif @else checked @endif  @if($mode=='VIEW') disabled readonly @endif>
              20 FT
      </label>
      <label class="radio-inline">
        <input type="radio" name="rdoFtsize" id="rdoFtSize2" value="40 FT" @if($mode!='ADD') @if($proposaldetail->freight_size=='40 FT') checked @endif @endif @if($mode=='VIEW') disabled readonly @endif>
              40 FT
      </label>
      <label class="radio-inline">
        <input type="radio" name="rdoFtsize" id="rdoFtSize3" value="40 HC" @if($mode!='ADD') @if($proposaldetail->freight_size=='40 HC') checked @endif @endif @if($mode=='VIEW') disabled readonly @endif>
              40 HC
      </label>
    </div>
  </div>

  @if($mode!='ADD')
    @if($proposaldetail->type_proposal=='CONTAINER')
    <div class="form-group" id="lclsize" hidden>
    @else
    <div class="form-group" id="lclsize">
    @endif
  @else
  <div class="form-group" id="lclsize" hidden>
  @endif
    <label for="rdolclsize" class="control-label col-xs-3">LCL Size</label> 
    <div class="col-xs-6">
      <label class="radio-inline">
        <input type="radio" name="rdolclsize" id="rdolclsize1" value="M3" @if($mode!='ADD') @if($proposaldetail->lcl_size=='M3') checked @endif @else checked @endif @if($mode=='VIEW') disabled readonly @endif>
              M3
      </label>
      <label class="radio-inline">
        <input type="radio" name="rdolclsize" id="rdolclsize2" value="TON" @if($mode!='ADD') @if($proposaldetail->lcl_size=='TON') checked @endif @endif @if($mode=='VIEW') disabled readonly @endif>
              TON
      </label>
    </div>
  </div> 
  @if($mode!='ADD')
    @if($proposaldetail->type_proposal=='LCL')
      @if($proposaldetail->lcl_size=='M3')
      <div class="form-group" id="hargam3">
      @else
      <div class="form-group" id="hargam3" hidden>
      @endif
    @else
    <div class="form-group" id="hargam3" hidden>
    @endif
  @else
    <div class="form-group" id="hargam3" hidden>
  @endif
    <label class="control-label col-xs-3" for="txtHargam3">Harga M3</label> 
    <div class="col-xs-6">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input class="form-control hargalcl input-element" name="txtHargam3" id="txtHargam3" type="text" @if($mode!='ADD') value="{{$proposaldetail->nominal_m3}}" @else value="0" @endif style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)" @if($mode=='VIEW') readonly @endif>
      </div>
    </div>
    </div>

  @if($mode!='ADD')
    @if($proposaldetail->type_proposal=='LCL')
      @if($proposaldetail->lcl_size=='TON')
      <div class="form-group" id="hargaton">
      @else
      <div class="form-group" id="hargaton" hidden>
      @endif
    @else
      <div class="form-group" id="hargaton" hidden>
    @endif
  @else 
    <div class="form-group" id="hargaton" hidden>
  @endif
    <label class="control-label col-xs-3" for="txtHargaton">Harga Ton</label> 
      <div class="col-xs-6">
        <div class="input-group">
          <div class="input-group-addon">Rp.</div> 
          <input class="form-control hargalcl input-element" name="txtHargaton" id="txtHargaton" type="text" @if($mode!='ADD') value="{{$proposaldetail->nominal_ton}}" @else value="0" @endif style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)" @if($mode=='VIEW') readonly @endif>
        </div>
      </div>
    </div>

    


</div>
<div class="col-md-6">
  <div class="box box-solid">
  <div class="box-header with-border">
      <h4>PELAYARAN</h4>
  </div>
  <div class="box-body">
  <div class="form-group">
    <label class="control-label col-xs-3" for="cboPelayaran">Nama Pelayaran</label> 
    <div class="col-xs-6">
      <select class="select form-control cboPelayaran" name="cboPelayaran" id="cboPelayaran" @if($mode=='VIEW') disabled readonly @endif>
      <option value="0">-- PILIH PELAYARAN --</option>
      @foreach($pelayaran as $item)
      @if($mode != 'ADD') 
        @if($proposaldetail->id_pelayaran==$item->id_pelayaran) 
        <option value="{{ $item->id_pelayaran }}" selected>{{ $item->name_full }} </option>
        @else
        <option value="{{ $item->id_pelayaran }}">{{ $item->name_full }} </option>
        @endif
      @else
      <option value="{{ $item->id_pelayaran }}">{{ $item->name_full }} </option>
      @endif
    @endforeach
      </select>
      @if($errors->has('cboPelayaran'))
      <span class="text-danger">{{$errors->first('cboPelayaran')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" for="txtHargaPelayaran">Harga Pelayaran</label> 
    <div class="col-xs-6">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input required="required" class="form-control input-element" name="txtHargaPelayaran" id="txtHargaPelayaran" type="text" @if($mode!='ADD') value="{{$proposaldetail->nominal_pelayaran}}" @else value="0" @endif style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)" @if($mode=='VIEW') readonly @endif>
        @if($errors->has('txtHargaPelayaran'))
      <span class="text-danger">{{$errors->first('txtHargaPelayaran')}}
      @endif
      </div>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" for="txtHargaExtraPelayaran">Extra Pelayaran<br/><span class="opsional"><sup>(opsional)</sup></span></label> 
    <div class="col-xs-6">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input class="form-control input-element" name="txtHargaExtraPelayaran" id="txtHargaExtraPelayaran" type="text" @if($mode!='ADD') value="{{$proposaldetail->nominal_extrapelayaran}}" @else value="0" @endif style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)" @if($mode=='VIEW') readonly @endif>
      </div>
    </div>
  </div> 
  <div class="form-group">
    <label class="control-label col-xs-3" for="txtExtraPelayaran">Extra Pelayaran Keterangan<br/><span class="opsional"><sup>(opsional)</sup></span></label> 
    <div class="col-xs-6">
      <textarea class="form-control" rows="3" cols="20" name="txtExtraPelayaran" id="txtExtraPelayaran" @if($mode=='VIEW') readonly @endif>@if($mode!='ADD') {{$proposaldetail->keterangan_extrapelayaran}}@endif</textarea>
    </div>
  </div>
      <div class="form-group">
    <label class="control-label col-xs-3" for="txtNotesPelayaran">Notes Pelayaran<br/><span class="opsional"><sup>(opsional)</sup></span></label> 
    <div class="col-xs-6">
      <textarea class="form-control" rows="3" cols="20" name="txtNotesPelayaran" id="txtNotesPelayaran" @if($mode=='VIEW') readonly @endif>@if($mode!='ADD') {{$proposaldetail->notes_pelayaran}} @endif</textarea>
    </div>
  </div>
  </div>
  </div>
  
    <div class="box box-solid">
        <div class="box-header with-border">
      <h4>TRUCKING</h4>
  </div>
  <div class="form-group">
  <label class="control-label col-xs-3" for="cboSector">Sektor</label> 
    <div class="col-xs-6" >
    <select class="select form-control cboSector" name="cboSector" id="cboSector" onchange="changeSector()" @if($mode=='VIEW') disabled readonly @endif>
    <option value="0">-- PILIH SECTOR --</option>
      @foreach($sector as $item)
      @if($mode != 'ADD') 
        @if($proposaldetail->id_sector==$item->id_sector) 
        <option value="{{ $item->id_sector }}" selected>{{ $item->sector }} : {{ $item->alamat }} </option>
        @else
        <option value="{{ $item->id_sector }}">{{ $item->sector }} : {{ $item->alamat }}</option>
        @endif
      @else
      <option value="{{ $item->id_sector }}">{{ $item->sector }} : {{ $item->alamat }}</option>
      @endif
    @endforeach
      </select>
      @if($errors->has('cboSector'))
      <span class="text-danger">{{$errors->first('cboSector')}}
      @endif
    </div>
  </div>
  <div class="box-body">
  <div class="form-group">
    <label class="control-label col-xs-3" for="cboTrucking">Nama Trucking</label> 
    <div class="col-xs-6" id="divcboTrucking">
      
    </div>
    
  </div>
   
    <div class="form-group">
    <label class="control-label col-xs-3" for="txtHargaTrucking">Harga Trucking</label> 
    <div class="col-xs-6">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input required="required" class="form-control input-element" name="txtHargaTrucking" id="txtHargaTrucking" type="text" @if($mode!='ADD') value="{{$proposaldetail->nominal_trucking}}" @else value="0" @endif style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)" @if($mode=='VIEW') readonly @endif>
        @if($errors->has('txtHargaTrucking'))
      <span class="text-danger">{{$errors->first('txtHargaTrucking')}}
      @endif
      </div>
    </div>
  </div>
  </div>
    </div>
    <div class="box box-solid">
        <div class="box-header with-border">
      <h4>DOORING</h4>
  </div>
  <div class="box-body">
  <div class="form-group">
    <label class="control-label col-xs-3" for="cboDooring">Nama Dooring</label> 
    <div class="col-xs-6">
      <select class="select form-control cboDooring" name="cboDooring" id="cboDooring" onchange="changeDooring()" @if($mode=='VIEW') disabled readonly @endif>
      <option value="0">-- PILIH DOORING --</option>
      @foreach($dooring as $item)
      @if($mode != 'ADD') 
        @if($proposaldetail->id_dooring==$item->id_dooring) 
        <option value="{{ $item->id_dooring }}" selected>{{ $item->nama }} </option>
        @else
        <option value="{{ $item->id_dooring }}">{{ $item->nama }} </option>
        @endif
      @else
      <option value="{{ $item->id_dooring }}">{{ $item->nama }} </option>
      @endif
    @endforeach
      </select>
      @if($errors->has('cboDooring'))
      <span class="text-danger">{{$errors->first('cboDooring')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" for="txtHargaDooring">Harga Dooring</label> 
    <div class="col-xs-6">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input required="required" class="form-control input-element" name="txtHargaDooring" id="txtHargaDooring" type="text" @if($mode!='ADD') value="{{$proposaldetail->nominal_dooring}}" @else value="0" disabled="true" @endif style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)" @if($mode=='VIEW') readonly @endif>
        @if($errors->has('txtHargaDooring'))
      <span class="text-danger">{{$errors->first('txtHargaDooring')}}
      @endif
      </div>
    </div>
  </div>
  
  <div class="form-group">
    <label class="control-label col-xs-3" for="txtHargaExtraDooring">Extra Dooring</label> 
    <div class="col-xs-6">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input required="required" class="form-control input-element" name="txtHargaExtraDooring" id="txtHargaExtraDooring" type="text" @if($mode!='ADD') value="{{$proposaldetail->nominal_extradooring}}" @else value="0" disabled="true" @endif  style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)" @if($mode=='VIEW') readonly @endif>
        @if($errors->has('txtHargaExtraDooring'))
      <span class="text-danger">{{$errors->first('txtHargaExtraDooring')}}
      @endif
      </div>
    </div>
  </div> 

  </div>
</div>
</div>
<!--  <div class="form-group">
    <label for="chkCustom" class="control-label col-xs-2">Custom Price</label> 
    <div class="col-xs-4">
      <label class="checkbox-inline">
        <input type="checkbox" name="chkCustom" style="margin-top: -7%" disabled readonly>
      </label>
    </div>
  </div> 
    <div class="form-group">
    <label class="control-label col-xs-2" for="txt20ft">Harga Custom Price</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input class="form-control" name="txt20ft" id="txt20ft" type="text" value="0" style="text-align: right" disabled readonly>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtAlamat">Keterangan</label> 
    <div class="col-xs-4">
      <textarea class="form-control" rows="3" cols="40" name="txtAlamat" id="txtAlamat" disabled readonly></textarea>
    </div>
  </div>-->
  @if($mode!='ADD')
    @if($proposaldetail->type_proposal=='CONTAINER')
    <div class="col-md-12" id="listBiaya">
    @else
    <div class="col-md-12" id="listBiaya" hidden>
    @endif
  @else
  <div class="col-md-12" id="listBiaya">
  @endif
      <table class="table table-bordered" id="listBiayaTable" style='background-color: white'>
        <tr>
            <th>NO</th>
            <th>NAMA BIAYA</th>
            <th style="text-align: right">NOMINAL BIAYA</th>
        </tr>
        <tr>
            <td><input type="checkbox" value="1" checked disabled/>&nbsp;1</td>
            <td>OPT SURABAYA</td>    
       <td> <input class="form-control hargacont input-element" name="txtHargaOptSby" id="txtHargaOptSby" type="text" @if($mode!='ADD') value="{{$proposaldetail->nominal_optsurabaya}}" @else value="{{$biaya[4]->nominal_biaya}}" @endif style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)" @if($mode=='VIEW') readonly @endif></td>
        </tr>
        <tr>
            <td><input type="checkbox" value="1" checked disabled/>&nbsp;2</td>
            <td>THC P</td>
            <td> <input class="form-control hargacont input-element" name="txtHargaTHCP" id="txtHargaTHCP" type="text" @if($mode!='ADD') value="{{$proposaldetail->nominal_thcp}}" @else value="{{$biaya[5]->nominal_biaya}}" @endif style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)" @if($mode=='VIEW') readonly @endif></td>
        </tr>
        <tr>
            <td><input type="checkbox" value="1" checked disabled/>&nbsp;3</td>
            <td>THC D</td>
            <td> <input class="form-control hargacont input-element" name="txtHargaTHCD" id="txtHargaTHCD" type="text" @if($mode!='ADD') value="{{$proposaldetail->nominal_thcd}}" @else value="{{$biaya[6]->nominal_biaya}}" @endif style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)" @if($mode=='VIEW') readonly @endif></td>
        </tr>
        <tr>
            <td><input type="checkbox" value="1" checked disabled/>&nbsp;4</td>
            <td>SURCHARGE</td>
            <td> <input class="form-control hargacont input-element" name="txtHargaSurcharge" id="txtHargaSurcharge" type="text" @if($mode!='ADD') value="{{$proposaldetail->nominal_surcharge}}" @else value="{{$biaya[0]->nominal_biaya}}" @endif style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)" @if($mode=='VIEW') readonly @endif></td>
        </tr>
        <tr>
            <td><input type="checkbox" value="1" checked disabled/>&nbsp;5</td>
            <td>OPT DOORING</td>
            <td> <input class="form-control hargacont input-element" name="txtHargaOptDooring" id="txtHargaOptDooring" type="text" @if($mode!='ADD') value="{{$proposaldetail->nominal_optdooring}}" @else value="{{$biaya[2]->nominal_biaya}}" @endif style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)" @if($mode=='VIEW') readonly @endif></td>
        </tr>
        <tr>
            <td><input type="checkbox" value="1" checked disabled/>&nbsp;6</td>
            <td>OPT MJP</td>
            <td> <input class="form-control hargacont input-element" name="txtHargaOptMjp" id="txtHargaOptMjp" type="text" @if($mode!='ADD') value="{{$proposaldetail->nominal_optmjp}}" @else value="{{$biaya[11]->nominal_biaya}}" @endif style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)" @if($mode=='VIEW') readonly @endif></td>
        </tr>
    </table>
  </div>
  <div class="form-group row">
  
    <label class="control-label col-xs-6" for="txtHargaTotal">Harga Total</label> 
    <div class="col-xs-6">
        <div class="input-group">
          <div class="input-group-addon">Rp.</div> 
          <input class="form-control " name="txtValHargaTotal" id="txtValHargaTotal" type="text" @if($mode!='ADD') value="{{$proposaldetail->nominal_total}}" @else value="0" @endif style="text-align: right" readonly>
         <input  name="txtHargaTotal" id="txtHargaTotal" type="hidden" @if($mode!='ADD') value="{{$proposaldetail->nominal_total}}" @else value="0" @endif >
        </div>
      </div>
    </div>

<div id="detailBiaya">
@if($mode!='VIEW')
<button type='button' class='btn btn-danger' onclick="insRow()" ><i class='fa fa-plus'></i> ADD BIAYA LAIN</button></a><br/><br/>
@endif
    <div class="portlet-body">
    <table class="table table-bordered" id="ListBiayaLain" style='background-color: white'>
      <thead>
        <tr>
            <th></th>
            <th >NAMA BIAYA</th>
            <th style="text-align: right">NOMINAL BIAYA</th>
            <th>AKSI</th>
        </tr>
        </thead>
        <tbody>
        @if($mode!='ADD')
          @foreach($biayalain as $item)
          <tr>
          <td><input class="form-control" name="txtBiayaLainHidden[]" id="txtBiayaLainHidden[]" type="hidden" value="{{$item->id_biayalain}}" ></td>
            <td><input class="form-control" name="txtNamaBiayaLain[]" id="txtNamaBiayaLain[]" type="text" value="{{$item->namabiayalain}}" ></td>
            <td><input class="form-control" name="txtNominalBiayaLain[]" id="txtNominalBiayaLain[]" type="text" value="{{$item->nominalbiayalain}}"  style="text-align: right" ></td>
            @if($mode!='VIEW')<td><a href="#" data-toggle="tooltip" title="Delete Detail " onclick="deleteRowDatabase(this)"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></a></td>@endif
            </tr>
            @endforeach
          @endif
        </tbody>
    </table>
</div>
</div>

<div class="form-group">
    <label class="control-label col-xs-3" for="txtLampiran">Lampiran</label> 
    <div class="col-xs-6">
      <textarea class="form-control" rows="20" cols="100" name="txtLampiran" id="txtLampiran" @if($mode=='VIEW')  readonly @endif></textarea>
    </div>
  </div>

  <div class="form-group row">
    <div class="col-xs-offset-5 col-xs-4">
    @if($mode!='VIEW') 
      @can('edit proposal')
      <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
      @endcan
    @endif
    @if($from == 'EDITOR')
      @if($mode=='ADD')
      <a href="{{route('transaction.customerproposal.edit',$proposal->id_proposal)}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
      @else
      <a href="{{route('transaction.customerproposal.edit',$proposaldetail->id_proposal)}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
      @endif
    @else
    <a href="{{route('transaction.proposalapproval.view',$proposaldetail->id_proposal)}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
    @endif
    </div>
  </div>
</form>

@endsection

@section('footer_script')
<script >

@if($mode!='ADD')
    $('#txtLampiran').summernote('code', '{!! $proposaldetail->lampiran !!}');
    @else
    $('#txtLampiran').summernote({
        placeholder: "Tambahkan lampiran",
        tabsize: 2,
        height: 100
      });
    @endif

$('.input-element').toArray().forEach(function(field){
new Cleave(field, {
    numeral: true,
    numeralThousandsGroupStyle: 'thousand',
    autoUnmask : true
})
});

$('#txtValidFrom').datetimepicker({
      minView: 2,
      format: 'dd-mm-yyyy',
      autoclose: true,
      todayBtn: true
    });

    $('#txtValidTo').datetimepicker({
      minView: 2,
      format: 'dd-mm-yyyy',
      autoclose: true,
      todayBtn: true
    });

var total=$("#txtHargaTotal").val();
$("#txtValHargaTotal").val(addCommas(total.toString()));


function changeDooring(){
  if($("#cboDooring").val=="0"){
    $("#txtHargaDooring").attr("disabled","true");
    $("#txtHargaExtraDooring").attr("disabled","true");
    $("#txtHargaDooring").val("0");
    $("#txtHargaExtraDooring").val("0");
  }else{
    $("#txtHargaDooring").removeAttr("disabled");
    $("#txtHargaExtraDooring").removeAttr("disabled");
  }
}

function changeJenisProposal(){
  if($("input[name='rdoType']").val()=="CONTAINER"){
    if($("input[name='rdoFtsize']").val()=="20 FT"){
      if($("#cboProposalType").val()=='PORT TO PORT' || $("#cboProposalType").val()=='PORT TO DOOR'){
        $("#txtHargaTHCP").val("{{$biaya[5]->nominal_biaya}}");
        $("#txtHargaTHCD").val("0");
      }else{
        $("#txtHargaTHCP").val("0");
        $("#txtHargaTHCD").val("{{$biaya[6]->nominal_biaya}}");
      }
    }else{
      if($("#cboProposalType").val()=='PORT TO PORT' || $("#cboProposalType").val()=='PORT TO DOOR'){
        $("#txtHargaTHCP").val("{{$biaya[7]->nominal_biaya}}");
        $("#txtHargaTHCD").val("0");
      }else{
        $("#txtHargaTHCP").val("0");
        $("#txtHargaTHCD").val("{{$biaya[8]->nominal_biaya}}");
      }
    }
  }
}


  function changeHarga(){
    var total=0;
    // alert(total);
    total = total + parseInt($("#txtHargam3").val().replace(/,/g, ''));
    total = total + parseInt($("#txtHargaton").val().replace(/,/g, ''));
    total = total + parseInt($("#txtHargaPelayaran").val().replace(/,/g, ''));
    total = total + parseInt($("#txtHargaExtraPelayaran").val().replace(/,/g, ''));
    total = total + parseInt($("#txtHargaTrucking").val().replace(/,/g, ''));
    total = total + parseInt($("#txtHargaDooring").val().replace(/,/g, ''));
    total = total + parseInt($("#txtHargaExtraDooring").val().replace(/,/g, ''));
    total = total + parseInt($("#txtHargaOptSby").val().replace(/,/g, ''));
    total = total + parseInt($("#txtHargaTHCP").val().replace(/,/g, ''));
    total = total + parseInt($("#txtHargaTHCD").val().replace(/,/g, ''));
    total = total + parseInt($("#txtHargaSurcharge").val().replace(/,/g, ''));
    total = total + parseInt($("#txtHargaOptDooring").val().replace(/,/g, ''));
    total = total + parseInt($("#txtHargaOptMjp").val().replace(/,/g, ''));
    // alert(total);
    $("#txtHargaTotal").val(total);
    $("#txtValHargaTotal").val(addCommas(total.toString()));
    // alert(parseInt($("#txtHargaExtraPelayaran").val().replace(/,/g, '')));
  }

//     $('#txtValidFrom').datepicker({
//     format: 'dd-mm-yyyy',
//     autoclose : true,
//     todayBtn : true,
//     timepicker: false
// });

// $('#txtValidTo').datepicker({
//     format: 'dd-mm-yyyy',
//     autoclose : true,
//     todayBtn : true,
//     timepicker: false
// });

    $("input[name='rdoType']").click(function() {
        var type = $(this).val();
        // alert(test);
       if(type=="LCL"){
           $("#freightsize").hide();
           $("#listBiaya").hide();
           $("#lclsize").show();
           $("#hargam3").show();
           $("#txtHargam3").val("{{$biaya[9]->nominal_biaya}}");
           $(".hargacont").val("0");
           document.getElementById("rdolclsize1").checked=true;
           changeHarga();
       }else{
          $("#listBiaya").show();
           $("#freightsize").show();
           $("#lclsize").hide();
           $("#hargam3").hide();
           $("#hargaton").hide();
           $(".hargalcl").val("0");
           $("#txtHargaOptSby").val("{{$biaya[4]->nominal_biaya}}");
           if($("#cboProposalType").val()=='PORT TO PORT' || $("#cboProposalType").val()=='PORT TO DOOR'){
              $("#txtHargaTHCP").val("{{$biaya[5]->nominal_biaya}}");
              $("#txtHargaTHCD").val("0");
           }else{
              $("#txtHargaTHCP").val("0");
              $("#txtHargaTHCD").val("{{$biaya[6]->nominal_biaya}}");
           }
        $("#txtHargaSurcharge").val("{{$biaya[0]->nominal_biaya}}");
        $("#txtHargaOptDooring").val("{{$biaya[2]->nominal_biaya}}");
        $("#txtHargaOptMjp").val("{{$biaya[11]->nominal_biaya}}");
           document.getElementById("rdoFtSize1").checked=true;
           changeHarga();
       }
        // $("#" + test).show();
    });

    $("input[name='rdolclsize']").click(function() {
        var typelcl = $(this).val();
        // alert(typelcl);
       if(typelcl=="TON"){
           $("#hargam3").hide();
           $("#txtHargaton").val("{{$biaya[10]->nominal_biaya}}");
           $("#txtHargam3").val("0");
           $("#hargaton").show();
           changeHarga();
       }else{
           $("#hargam3").show();
           $("#hargaton").hide();
           $("#txtHargam3").val("{{$biaya[9]->nominal_biaya}}");
           $("#txtHargaton").val("0");
           changeHarga();
       }
        // $("#" + test).show();
    });

    $("input[name='rdoFtsize']").click(function() {
        var typecont = $(this).val();
        // alert(typelcl);
       if(typecont=="40 FT" || typecont=="40 HC"){
        
        if($("#cboProposalType").val()=='PORT TO PORT' || $("#cboProposalType").val()=='PORT TO DOOR'){
              $("#txtHargaTHCP").val("{{$biaya[7]->nominal_biaya}}");
              $("#txtHargaTHCD").val("0");
           }else{
              $("#txtHargaTHCP").val("0");
              $("#txtHargaTHCD").val("{{$biaya[8]->nominal_biaya}}");
           }
        $("#txtHargaSurcharge").val("{{$biaya[1]->nominal_biaya}}");
        $("#txtHargaOptDooring").val("{{$biaya[3]->nominal_biaya}}");
        $("#txtHargaOptMjp").val("{{$biaya[11]->nominal_biaya}}");
        $(".hargalcl").val("0");
        changeSector();
           changeHarga();
       }else{
        if($("#cboProposalType").val()=='PORT TO PORT' || $("#cboProposalType").val()=='PORT TO DOOR'){
              $("#txtHargaTHCP").val("{{$biaya[5]->nominal_biaya}}");
              $("#txtHargaTHCD").val("0");
           }else{
              $("#txtHargaTHCP").val("0");
              $("#txtHargaTHCD").val("{{$biaya[6]->nominal_biaya}}");
           }
        $("#txtHargaSurcharge").val("{{$biaya[0]->nominal_biaya}}");
        $("#txtHargaOptDooring").val("{{$biaya[2]->nominal_biaya}}");
        $("#txtHargaOptMjp").val("{{$biaya[11]->nominal_biaya}}");
        $(".hargalcl").val("0");
        changeSector();
           changeHarga();
       }
        // $("#" + test).show();
    });
    
    
    // function changeSector(){
    //     var sector = $('#cboSector').val().split(";");
    //     var harga = sector[1];
    //     $("#txtHargaTrucking").val(harga);
    //     changeHarga();
      
    // }

    function changeSector(){
        

        var idsector = $("#cboSector").val();
      var type = $("input[name='rdoType']:checked").val();
      var size = "";
      if(type=='LCL'){
        size='LCL';
      }else{
        if($("input[name='rdoFtsize']:checked").val()=="20 FT"){
          size='20';
        }else if($("input[name='rdoFtsize']:checked").val()=="40 FT" || $("input[name='rdoFtsize']:checked").val()=="40 HC"){
          size='40';
        }else{
          size='0';
        }
      }
      var html = "";
      
      $.ajax({
              url: route('transaction.customerproposal.datatrucking',{id:idsector,size:size}),
              method:'get'
            })
            .then(function (response){
              var data = response.rows;
              html += '<select class="select form-control cboTrucking" name="cboTrucking" id="cboTrucking" onchange="changeTrucking();" @if($mode=='VIEW') disabled readonly @endif>';
              html += '<option value="0">-- PILIH TRUCKING --</option>'
              $.each( data, function( i, val ) {
                  // alert( "sector: " + val.sector + ", alamat: " + val.alamat );
                  @if($mode!='ADD')
                  if( {{$proposaldetail->id_truckingdetail}} == val.id_truckingdetail ){
                    html += '<option value="'+val.id_truckingdetail+';'+val.harga+'" selected>'+val.nama+'</option>';
                  }else{
                    html += '<option value="'+val.id_truckingdetail+';'+val.harga+'">'+val.nama+'</option>';
                  }
                  @else
                  html += '<option value="'+val.id_truckingdetail+';'+val.harga+'">'+val.nama+'</option>';
                  @endif
              });
              html +='</select>';
              @if($errors->has('cboTrucking'))
              html += '<span class="text-danger">'{{$errors->first('txtCustCredit')}};
              @endif
              $("#divcboTrucking").html(html);
              $(".cboTrucking").select2();
              $("#txtHargaTrucking").val("0");
            }); 
            
    }

    function changeTrucking(){
      var trucking = $('#cboTrucking').val().split(";");
        var harga = trucking[1];
        $("#txtHargaTrucking").val(harga);
        changeHarga();
            
              
    }

    // function changeTrucking(){
    //   var idtrucking = $("#cboTrucking").val();
    //   var type = $("input[name='rdoType']:checked").val();
    //   var size = "";
    //   if(type=='LCL'){
    //     size='LCL';
    //   }else{
    //     if($("input[name='rdoType']:checked").val()==undefined){
    //       size='0';
    //     }else if($("input[name='rdoType']:checked").val()=='40 FT'){
    //       size='40';
    //     }else{
    //       size='20';
    //     }
    //   }
    //   var html = "";
      
    //   $.ajax({
    //           url: route('transaction.customerproposal.datatrucking',{id:idtrucking,size:size}),
    //           method:'get'
    //         })
    //         .then(function (response){
    //           var data = response.rows;
    //           html += '<select class="select form-control cboSector" name="cboSector" id="cboSector" onchange="changeSector();" @if($mode=='VIEW') disabled readonly @endif>';
    //           html += '<option value="0;0">-- PILIH SECTOR --</option>'
    //           $.each( data, function( i, val ) {
    //               // alert( "sector: " + val.sector + ", alamat: " + val.alamat );
    //               @if($mode!='ADD')
    //               if({{$proposaldetail->id_truckingdetail}}== val.id_truckingdetail ){
    //                 html += '<option value="'+val.id_truckingdetail+';'+val.harga+'" selected>'+val.sector+': '+val.alamat+'</option>';
    //               }else{
    //                 html += '<option value="'+val.id_truckingdetail+';'+val.harga+'">'+val.sector+': '+val.alamat+'</option>';
    //               }
    //               @else
    //               html += '<option value="'+val.id_truckingdetail+';'+val.harga+'">'+val.sector+': '+val.alamat+'</option>';
    //               @endif
    //           });
    //           html +='</select>';
    //           $("#divcboSector").html(html);
    //           $(".cboSector").select2();
    //         }); 
            
              
    // }


    //tambahan table biaya lain
function insRow() {
  var x = document.getElementById('ListBiayaLain');
  var new_row = '';
  var i = 0;
  if(x.rows.length==1){
    i = 1;
  }else{
    i = x.rows.length;
  }
  new_row += '<tr>'
  + '<td><input class="form-control" name="txtBiayaLainHidden[]" id="txtBiayaLainHidden[]" type="hidden" value="0" ></td>'
            + '<td><input class="form-control" name="txtNamaBiayaLain[]'+i+'" id="txtNamaBiayaLain[]'+i+'" type="text" ></td>'
            +'<td><input class="form-control" name="txtNominalBiayaLain[]'+i+'" id="txtNominalBiayaLain[]'+i+'" type="text"  style="text-align: right" ></td>'
            + '<td>'
            + '<a href="#" data-toggle="tooltip" title="Delete Detail " onclick="deleteRow(this)"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></a>'  
            + '</td>'
            + '</tr>';
 $("table#ListBiayaLain").append(new_row);

}

function deleteRow(row) {
  var i = row.parentNode.parentNode.rowIndex;
  document.getElementById('ListBiayaLain').deleteRow(i);
}

function deleteRowDatabase(row) {
  var i = row.parentNode.parentNode.rowIndex;
  var td = row.parentNode.parentNode;
  var iddetail = $(td).find('td').eq(0).find('input').val();
  if (confirm('Anda yakin akan menghapus biaya ini?')) {
            $.ajax({
              url: route('transaction.customerproposal.deletebiayalain',{id:iddetail}),
              data:{_token:$('meta[name="csrf_token"]').attr('content')},
              method:'post'
            })
            .then(function (response){
              if (response.status == 'error') {
                alert(response.description);
              } else if( response.status == 'success') {
                alert(response.description);
                document.getElementById('ListBiayaLain').deleteRow(i);
              }
            }); 
          }
}


    $(document).ready(function () {
      $(".cboSector").select2({
    minimumInputLength: 2,
    placeholder: "-- PILIH SEKTOR --"});
      $(".cboDooring").select2();
      $(".cboPelayaran").select2();
      $(".cboPortAsal").select2();
      $(".cboPortTujuan").select2();
      $(".cboConsignee").select2();
    changeSector();
    @if($mode!='ADD')
    changeTrucking();
    @endif

    
    
    });

    @if($mode=='ADD')
    changeHarga();
    @endif
    
    
</script>
@endsection
