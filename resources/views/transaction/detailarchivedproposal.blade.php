@extends('layouts.default')

@section('content_header')
<h1>
    ARCHIVED PROPOSAL DETAIL
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">Archived Proposal</li>
</ol>
@endsection

@section('content')
<hr>
<form class="form-horizontal">
<div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">ID Penawaran</label> 
    <div class="col-xs-3">
      <label class="control-label">: PEN00002</label>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtShortname">Customer</label> 
    <div class="col-xs-6">
      <label class="control-label">: CUSTOMER B</label>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">Validity</label> 
    <div class="col-xs-3">
      <label class="control-label">: 01-10-2018 s/d 31-10-2018</label>
    </div>
  </div>
  
</form>

<div id="detailRute">
<input type="text" placeholder="search.." style="float: right; margin-top:1%"></input>

<div class="portlet-body">
    <table class="table table-bordered" id="detailCustomerProposalTable" style='background-color: white'>
        <tr>
            <th>NO</th>
            <th>NAMA PELAYARAN</th>
            <th>KOTA ASAL</th>
            <th>KOTA PELABUHAN ASAL</th>
            <th>NAMA PELABUHAN ASAL</th>
            <th>KOTA TUJUAN</th>
            <th>KOTA PELABUHAN TUJUAN</th>
            <th>NAMA PELABUHAN TUJUAN</th>
            <th>TIPE PROPOSAL</th>
            <th>HARGA JUAL</th>
            <th >AKSI</th>
        </tr>
        <tr>
            <td>1</td>
            <td>PT. TEST 1</td>
            <td>SURABAYA</td>
            <td>SURABAYA</td>
            <td>PORT JAMRUD</td>
            <td>JAKARTA</td>
            <td>JAKARTA</td>
            <td>TANJUNG PRIOK</td>
            <td>CONTAINER</td>
            <td>1.200.000</td>
            <td>
                 <a href="#" data-toggle="tooltip" title="View Detail"><button type='button' class='btn btn-primary' data-toggle="modal" data-target="#DetailModal"><i class='fa fa-eye'></i></button></a>
            </td>
        </tr>
        <tr>
            <td>2</td>
            <td>PT. TEST 1</td>
            <td>SURABAYA</td>
            <td>SURABAYA</td>
            <td>PORT JAMRUD</td>
            <td>JAKARTA</td>
            <td>JAKARTA</td>
            <td>TANJUNG PRIOK</td>
            <td>LCL</td>
            <td>1.000.000</td>
            <td>

                 <a href="#" data-toggle="tooltip" title="View Detail"><button type='button' class='btn btn-primary' data-toggle="modal" data-target="#DetailModal"><i class='fa fa-eye'></i></button></a>
            </td>
        </tr>
        

    </table>
</div>

</div>

<div id="DetailModal" class="modal fade" role="dialog" >
    <div class="modal-dialog" >

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Detail Proposal </h4>
            </div>
            <div class="modal-body">
            <label class="control-label">PENAWARAN ID : PEN00003 </label>
            <br/>
            <label class="control-label">CUSTOMER : CUSTOMER C </label>
            <br/>
            <label class="control-label">VALIDITY : 05-12-2018 s/d 31-01-2019 </label>
            <br/>
            <label class="control-label">NAMA PELAYARAN : PT. TEST 1</label>
            <br/>   
            <label class="control-label">KOTA ASAL : SURABAYA </label>
            <br/>
            <label class="control-label">KOTA PELABUHAN ASAL : SURABAYA </label>
            <br/>
            <label class="control-label">NAMA PELABUHAN ASAL : PORT JAMRUD </label>
            <br/>
            <label class="control-label">KOTA TUJUAN : JAKARTA</label>
            <br/>
            <label class="control-label">KOTA PELABUHAN TUJUAN : JAKARTA</label>
            <br/>
            <label class="control-label">NAMA PELABUHAN TUJUAN : TANJUNG PRIOK </label>
            <br/>
            <label class="control-label">TIPE PROPOSAL : CONTAINER </label>
            <br/>
            <label class="control-label">JENIS PROPOSAL : PORT TO PORT </label>
            <br/>
            <label class="control-label">ALAMAT ASAL : JL. Pertama </label>
            <br/>
            <label class="control-label">ALAMAT TUJUAN : JL. Kedua </label>
            <br/>
            <label class="control-label">FREIGHT SIZE : 20 FT</label>
            <br/>
            <label class="control-label">PELAYARAN : PT. BERLAYAR</label>
            <br/>
            <label class="control-label">HARGA PELAYARAN : Rp. 1.500.000</label>
            <br/>
            <label class="control-label">EXTRA PELAYARAN : Rp. 0 </label>
            <br/>
            <label class="control-label">KETERANGAN EXTRA PELAYARAN : -  </label>
            <br/>
            <label class="control-label">THC TYPE : THC 20</label>
            <br/>
            <label class="control-label">HARGA THC : Rp. 200.000</label>
            <br/>
            <label class="control-label">HARGA SURCHARGE : Rp. 0 </label>
            <br/>
            <label class="control-label">BIAYA ADMIN : Rp. 150.000 </label>
            <br/>
            <label class="control-label">JASA TRUCKING : PT. TRUCKING 1 </label>
            <br/>
            <label class="control-label">SEKTOR TRUCKING : 1 </label>
            <br/>
            <label class="control-label">HARGA TRUCKING : Rp. 800.000 </label>
            <br/>
            <label class="control-label">JASA DOORING : PT. ABC</label>
            <br/>
            <label class="control-label">HARGA DOORING : Rp. 0 </label>
            <br/>
            <label class="control-label">CUSTOM PRICE : Rp. 0 </label>
            <br/>
            <label class="control-label">KETERANGAN : - </label>
            <br/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

@endsection

@section('footer_script')
<script>
    
   // $("#detailRute").hide();


    
    function editPelayaran(){
       // window.open(' {{route('master.pelayaran.edit')}} ', "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, width=1400, height=800");
        //openPopup({{route('master.pelayaran.edit')}}, 'Title', 1400, 800, 'scrollbars,resizable', true, false);
    }
</script>
@endsection
