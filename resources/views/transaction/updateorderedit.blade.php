@extends('layouts.default')

@section('content_header')
<h1>
  MONITORING ORDER DETAIL
</h1>
<ol class="breadcrumb">
  <li><a href="#"> Transaction</a></li>
  <li class="active">Monitoring Order</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
<form action="#" method="post" class="form-horizontal">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <div class="form-group">
    <label for="rdoType" class="control-label col-xs-2">Type Order</label>
    <div class="col-xs-6">
      <label class="radio-inline">
        <input type="radio" name="rdoType" id="rdoType" value="CONTAINER" @if($order->type_order=='CONTAINER') checked @endif disabled readonly >
        Container
      </label>
      <label class="radio-inline">
        <input type="radio" name="rdoType" id="rdoType" value="LCL" @if($order->type_order=='LCL') checked @endif disabled readonly >
        LCL
      </label>
      <!-- <label class="radio-inline">
        <input type="radio" name="rdoType" id="rdoType" value="Bongkaran">
              Bongkaran
      </label> -->
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="cboCustomer">Customer</label>
    <div class="col-xs-4">
      <select class="select form-control" name="cboCustomer" id="cboCustomer" readonly disabled>
        <option value="0">-- PILIH CUSTOMER --</option>
        @foreach($customer as $item)
        @if($order->id_customer==$item->id_customer)
        <option value="{{ $item->id_customer }}" selected>{{ $item->nama_pt }}</option>
        @else
        <option value="{{ $item->id_customer }}">{{ $item->nama_pt }}</option>
        @endif
        @endforeach
      </select>
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-xs-2" for="cboKotaAsal">Kota Asal</label>
    <div class="col-xs-4">
      <select class="select form-control" name="cboKotaAsal" id="cboKotaAsal" readonly disabled>
        @foreach($city as $item)
        @if($order->id_kota_asal==$item->id_kota)
        <option value="{{ $item->id_kota }}" selected>{{ $item->nama_kota }}</option>
        @else
        <option value="{{ $item->id_kota }}">{{ $item->nama_kota }}</option>
        @endif
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="cboPortAsal">Pelabuhan Asal</label>
    <div class="col-xs-4">
      <select class="select form-control" name="cboPortAsal" id="cboPortAsal" readonly disabled>
        @foreach($pelabuhan as $item)
        @if($order->id_pelabuhan_asal==$item->id_pelabuhan)
        <option value="{{ $item->id_pelabuhan }}" selected>{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}</option>
        @else
        <option value="{{ $item->id_pelabuhan }}">{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}</option>
        @endif
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="cboKotaTujuan">Kota Tujuan</label>
    <div class="col-xs-4">
      <select class="select form-control" name="cboKotaTujuan" id="cboKotaTujuan" readonly disabled>
        @foreach($city as $item)
        @if($order->id_kota_tujuan==$item->id_kota)
        <option value="{{ $item->id_kota }}" selected>{{ $item->nama_kota }}</option>
        @else
        <option value="{{ $item->id_kota }}">{{ $item->nama_kota }}</option>
        @endif
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="cboPortTujuan">Pelabuhan Tujuan</label>
    <div class="col-xs-4">
      <select class="select form-control" name="cboPortTujuan" id="cboPortTujuan" readonly disabled>
        @foreach($pelabuhan as $item)
        @if($order->id_pelabuhan_tujuan==$item->id_pelabuhan)
        <option value="{{ $item->id_pelabuhan }}" selected>{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}</option>
        @else
        <option value="{{ $item->id_pelabuhan }}">{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}</option>
        @endif
        @endforeach
      </select>
    </div>
  </div>


  <div class="form-group">
    <label class="control-label col-xs-2" for="cboPelayaran">Pelayaran</label>
    <div class="col-xs-4">
      <select class="select form-control" name="cboPelayaran" id="cboPelayaran" readonly disabled>
        @foreach($pelayaran as $item)
        @if($order->id_pelayaran==$item->id_pelayaran)
        <option value="{{ $item->id_pelayaran }}" selected>{{ $item->name_full }} </option>
        @else
        <option value="{{ $item->id_pelayaran }}">{{ $item->name_full }} </option>
        @endif
        @endforeach
      </select>
    </div>
  </div>
  @if($order->type_order=='CONTAINER')
  <div class="form-group" id="freightsize">
    @else
    <div class="form-group" id="freightsize" hidden>
      @endif
      <label class="control-label col-xs-2">Freight Size</label>
      <div class="col-xs-6">
        <label class="radio-inline">
          <input type="radio" name="rdoFtsize" id="rdoFtSize1" value="20 FT" @if($order->freight_size=='20 FT') checked @endif disabled readonly >
          20 FT
        </label>
        <label class="radio-inline">
          <input type="radio" name="rdoFtsize" id="rdoFtSize2" value="40 FT" @if($order->freight_size=='40 FT') checked @endif disabled readonly >
          40 FT
        </label>
      </div>
    </div>

    @if($order->type_order=='CONTAINER')
    <div class="form-group" id="lclsize" hidden>
      @else
      <div class="form-group" id="lclsize">
        @endif
        <label for="rdolclsize" class="control-label col-xs-2">LCL Size</label>
        <div class="col-xs-6">
          <label class="radio-inline">
            <input type="radio" name="rdolclsize" id="rdolclsize1" value="M3" @if($order->lcl_size=='M3') checked @endif disabled readonly >
            M3
          </label>
          <label class="radio-inline">
            <input type="radio" name="rdolclsize" id="rdolclsize2" value="TON" @if($order->lcl_size=='TON') checked @endif disabled readonly>
            TON
          </label>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-xs-2" for="cboJenisBarang">Tipe Muatan</label>
        <div class="col-xs-4">
          <select class="select form-control" name="cboJenisBarang" id="cboJenisBarang" readonly disabled>
            <option value="GENERAL" @if($order->jenis_barang=='GENERAL') selected @endif>General Cargo</option>
            <option value="DG" @if($order->jenis_barang=='DG') selected @endif>Dangerous Goods</option>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-xs-2" for="txtAlamatAsal">Alamat Asal</label>
        <div class="col-xs-4">
          <textarea class="form-control" rows="3" cols="40" name="txtAlamatAsal" id="txtAlamatAsal" readonly>{{$order->alamat_asal}} </textarea>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-xs-2" for="txtAlamatTujuan">Alamat Tujuan</label>
        <div class="col-xs-4">
          <textarea class="form-control" rows="3" cols="40" name="txtAlamatTujuan" id="txtAlamatTujuan" readonly>{{$order->alamat_tujuan}} </textarea>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-xs-2" for="chkPortToDoor">Harga Pelayaran Saat Ini</label>
        <div class="col-xs-4">
          <div class="input-group">
            <div class="input-group-addon">Rp.</div>
            <input class="form-control" name="txtHargaPelayaran" id="txtHargaPelayaran" type="text" value="{{$order->nominal_pelayaran_order}}" style="text-align: right" readonly>
          </div>
        </div>
      </div>

      @if($order->type_order=='LCL')
      @if($order->lcl_size=='M3')
      <div class="form-group" id="hargam3">
        @else
        <div class="form-group" id="hargam3" hidden>
          @endif
          @else
          <div class="form-group" id="hargam3" hidden>
            @endif
            <label class="control-label col-xs-2" for="txtHargam3">Harga M3</label>
            <div class="col-xs-4">
              <div class="input-group">
                <div class="input-group-addon">Rp.</div>
                <input class="form-control hargalcl" name="txtHargam3" id="txtHargam3" type="text" value="{{$order->nominal_m3}}" style="text-align: right" readonly>
              </div>
            </div>
          </div>

          @if($order->type_order=='LCL')
          @if($order->lcl_size=='TON')
          <div class="form-group" id="hargaton">
            @else
            <div class="form-group" id="hargaton" hidden>
              @endif
              @else
              <div class="form-group" id="hargaton" hidden>
                @endif
                <label class="control-label col-xs-2" for="txtHargaton">Harga Ton</label>
                <div class="col-xs-4">
                  <div class="input-group">
                    <div class="input-group-addon">Rp.</div>
                    <input class="form-control hargalcl" name="txtHargaton" id="txtHargaton" type="text" value="{{$order->nominal_ton}}" style="text-align: right" readonly>
                  </div>
                </div>
              </div>

              @if($order->type_order=='CONTAINER')
              <div id="listBiaya">
                @else
                <div id="listBiaya" hidden>
                  @endif
                  <div class="form-group">
                    <label class="control-label col-xs-2" for="txtHargaOPTSurabaya">Harga OPT Surabaya</label>
                    <div class="col-xs-4">
                      <div class="input-group">
                        <div class="input-group-addon">Rp.</div>
                        <input class="form-control hargacont" name="txtHargaOptSby" id="txtHargaOptSby" type="text" value="{{$order->nominal_optsurabaya}}" style="text-align: right" readonly>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-xs-2" for="txtHargaTHCP">Harga THC P</label>
                    <div class="col-xs-4">
                      <div class="input-group">
                        <div class="input-group-addon">Rp.</div>
                        <td> <input class="form-control hargacont" name="txtHargaTHCP" id="txtHargaTHCP" type="text" value="{{$order->nominal_thcp}}" style="text-align: right" readonly>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-xs-2" for="txtHargaTHCD">Harga THC D</label>
                    <div class="col-xs-4">
                      <div class="input-group">
                        <div class="input-group-addon">Rp.</div>
                        <input class="form-control hargacont" name="txtHargaTHCD" id="txtHargaTHCD" type="text" value="{{$order->nominal_thcd}}" style="text-align: right" readonly>
                      </div>
                    </div>
                  </div>
                </div>

                @if($order->type_order=='CONTAINER')
                <div class="form-group" id="jumlahorder">
                  @else
                  <div class="form-group" id="jumlahorder" hidden>
                    @endif
                    <label class="control-label col-xs-2" for="txtJmlOrder">Jumlah Order</label>
                    <div class="col-xs-4">
                      <div class="input-group">
                        <input class="form-control" name="txtJmlOrder" id="txtJmlOrder" value="{{$order->jumlah_order}}" type="text" readonly>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-xs-2" for="txtJadwal">Jadwal Pengambilan</label>
                    <div class="col-xs-4">
                      <div class="input-group">
                        <input class="form-control" name="txtJadwal" id="txtJadwal" type="text" value="{{Carbon\Carbon::parse($order->jadwal_pengambilan)->format('d-m-Y H:i:s')}}" readonly>
                      </div>
                    </div>
                  </div>

                  <div id="detailRute">
                    <div class="portlet-body">
                      <table class="table table-bordered" id="ListContainer" style='background-color: white'>
                        <thead>
                          <tr>
                            <th style='width:4%;'></th>
                            <th style='width:15%;'>NO SURAT JALAN</th>
                            <th style='width:15%;'>NO CONTAINER</th>
                            <th style='width:15%;'>SEAL</th>
                            <th style='width:25%;'>STATUS</th>
                            <th style='width:30%;'>KETERANGAN STATUS</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($suratjalan as $item)
                          <tr>
                            <td><input class="form-control" name="txtIdSuratJalan_{{$item->id_suratjalan}}" id="txtIdSuratJalan_{{$item->id_suratjalan}}" type="hidden" value="{{$item->id_suratjalan}}"><input type="checkbox" id="chkBarang_{{$item->id_suratjalan}}" checked readonly disabled></td>
                            <td> {{$item->no_suratjalan }} </td>
                            <td>{{$item->no_container }} </td>
                            <td> {{$item->seal }} </td>
                            <td>
                              <select class="select form-control" name="cboStatus" id="cboStatus">
                                <option value="0" @if($item->status==0) selected @endif>Barang Diterima</option>
                                <option value="1" @if($item->status==1) selected @endif>Kapal Berangkat</option>
                                <option value="2" @if($item->status==2) selected @endif>Kapal Tiba</option>
                                <option value="3" @if($item->status==3) selected @endif>Kapal Sandar</option>
                                <option value="4" @if($item->status==4) selected @endif>Kapal Bongkar</option>
                                <option value="5" @if($item->status==5) selected @endif>Barang Diterima Dooring</option>
                                <option value="6" @if($item->status==6) selected @endif>Barang Diterima Customer</option>
                              </select>
                            </td>
                            <td><input class="form-control" name="txtKeterangan_{{$item->id_suratjalan}}" id="txtKeterangan_{{$item->id_suratjalan}}" type="text"></td>
                          </tr>
                          @endforeach
                        </tbody>

                      </table>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-xs-offset-2 col-xs-4">
                      <button type="button" class="btn btn-success" type="submit" name="complete" onclick="updateOrder({{$order->id_order}})"><i class="fas fa-save"></i> Update</button>
                      <a href="{{route('transaction.updateorder')}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
                    </div>
                  </div>
</form>

@endsection

@section('footer_script')
<script>
  function updateOrder(idorder) {
    // alert(idorder);
    if (confirm('Ubah Status Seluruh Kontainer di Order ini?')) {
      var objDO = {
        _token: $('meta[name="csrf_token"]').attr('content'),
        detail: []
      };
      var detail = [];
      $("#ListContainer > tbody > tr").each(function(i, el) {
        var $tds = $(this).find('td');
        // alert($tds.eq(4).find('select').val());
        var objDetail = {
          idsuratjalan: $tds.eq(0).find('input').val(),
          status: $tds.eq(4).find('select').val(),
          keterangan_status: $tds.eq(5).find('input').val()
        };
        detail.push(objDetail);
      });
      //   var count = detail.length;
      // alert(count);
      //   if(count!=0){
      objDO.detail = detail;
      $.ajax({
          url: route('transaction.updateorder.updatestatus', {
            id: idorder
          }),
          data: objDO,
          method: 'post'
        })
        .then(function(response) {
          if (response.status == 'error') {
            alert(response.description);
          } else if (response.status == 'success') {
            alert(response.description);
            window.location = "{{route('transaction.updateorder')}}";
            // refresh();
          }
        });
    }
    //   }else{
    //     alert("Detail barang tidak boleh kosong!");
    //   }
  }
</script>
@endsection