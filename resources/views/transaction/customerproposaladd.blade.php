@extends('layouts.default')

@section('content_header')
<h1>
  @if ($mode == 'ADD')
  ADD CUSTOMER PROPOSAL
  @else
  EDIT CUSTOMER PROPOSAL
  @endif
</h1>
<ol class="breadcrumb">
  <li><a href="#"> Transaction</a></li>
  <li class="active">Customer Proposal</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@if($mode=='ADD')
<form action="{{ route('transaction.customerproposal.create') }}" method="post" class="form-horizontal">
  @else
  <form action="{{ route('transaction.customerproposal.update',$proposal->id_proposal) }}" method="post" class="form-horizontal">
    @endif
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    @if($mode=='ADD')
    <input type="hidden" name="idproposal" id="idproposal" value="0">
    @else
    <input type="hidden" name="idproposal" id="idproposal" value="{{ $proposal->id_proposal }}">
    @endif
    <div class="form-group">
      <label class="control-label col-xs-2" for="txtIdProposal">No Proposal</label>
      <div class="col-xs-3">
        <input class="form-control" name="txtIdProposal" id="txtIdProposal" type="text" @if($mode=='EDIT' ) value="{{$proposal->no_proposal}}" @endif readonly>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-xs-2" for="cboCustomer">Customer</label>
      <div class="col-xs-4">
        <select class="select form-control cboCustomer" name="cboCustomer" id="cboCustomer" onchange="changeCustomer()" @if($mode=='EDIT' ) disabled readonly @endif>
          <option value="0">-- PILIH CUSTOMER --</option>
          @foreach($customer as $item)
          @if($mode == 'EDIT')
          @if($proposal->id_customer==$item->id_customer)
          <option value="{{ $item->id_customer }};{{ $item->lama_kredit }}" selected>{{ $item->nama_pt }}</option>
          @else
          <option value="{{ $item->id_customer }};{{ $item->lama_kredit }}">{{ $item->nama_pt }}</option>
          @endif
          @else
          <option value="{{ $item->id_customer }};{{ $item->lama_kredit }}">{{ $item->nama_pt }}</option>
          @endif
          @endforeach
        </select>
        @if($errors->has('cboCustomer'))
      <span class="text-danger">{{$errors->first('cboCustomer')}}
      @endif
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-xs-2" for="txtCustCredit">Customer Kredit</label>
      <div class="col-xs-2">
        <input required="required" class="form-control" name="txtCustCredit" id="txtCustCredit" type="text" style="width:40%" @if($mode!='ADD' ) value="{{$proposal->customer_credit}}" @if($proposal->status!=1) readonly @endif @endif @if($mode=='VIEW') readonly @endif>
        @if($errors->has('txtCustCredit'))
      <span class="text-danger">{{$errors->first('txtCustCredit')}}
      @endif
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-xs-2">Tipe Proposal</label>
      <div class="col-xs-4">
        <label class="radio-inline">
          @if($mode != 'ADD')
          @if($proposal->tipe_proposal=="CONTRACT")
          <input type="radio" name="rdoType" value="CONTRACT" checked @if($proposal->status!=1) disabled readonly @endif @if($mode=='VIEW') readonly @endif>
          @else
          <input type="radio" name="rdoType" value="CONTRACT" @if($proposal->status!=1) disabled readonly @endif @if($mode=='VIEW') readonly @endif>
          @endif
          @else
          <input type="radio" name="rdoType" value="CONTRACT" checked>
          @endif
          Kontrak
        </label>
        <label class="radio-inline">
          @if($mode != 'ADD')
          @if($proposal->tipe_proposal=="NON-CONTRACT")
          <input type="radio" name="rdoType" value="NON-CONTRACT" checked @if($proposal->status!=1) disabled readonly @endif @if($mode=='VIEW') readonly @endif>
          @else
          <input type="radio" name="rdoType" value="NON-CONTRACT" @if($proposal->status!=1) disabled readonly @endif @if($mode=='VIEW') readonly @endif>
          @endif
          @else
          <input type="radio" name="rdoType" value="NON-CONTRACT">
          @endif
          Non-Kontrak
        </label>

      </div>
    </div>


    <div class="form-group row">
      <div class="col-xs-offset-2 col-xs-6">
      @can('edit proposal')
        @if ($mode == 'ADD')
        <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
        @else
        @if($proposal->status==1)
        <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Update</button>
        @endif
        @endif
      @endcan
        <a href="{{route('transaction.customerproposal')}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
      </div>
    </div>
  </form>

  @if ($mode != 'ADD')
  <div id="detailRute">
    @if($proposal->status==1)
    @can('edit proposal')
    <a href="{{route('transaction.customerproposal.detailadd',$proposal->id_proposal)}}" data-toggle="tooltip" title="Add Detail"><button type='button' class='btn btn-danger'><i class='fa fa-plus'></i> ADD DETAIL PROPOSAL</button></a>
    @endcan
    @endif

    <div class="portlet-body">
      <table class="table table-bordered" id="detailCustomerProposalTable" style='background-color: white'>

      </table>
    </div>
    <div class="form-group row">
      <div class="col-xs-offset-10 col-xs-6">
        @if($mode=='EDIT')
          @if($proposal->status==1)

            @can('edit proposal')
            <button type="button" class="btn btn-lg btn-success" type="submit" name="complete" onclick="completeProposal()"><i class="fas fa-check-double"></i> COMPLETE</button>
            @endcan

          @endif
        @endif

      </div>
    </div>

  </div>


  @endif

  @endsection

  @section('footer_script')
  <script>
    function refreshPage() {
      location.reload();
    }

    function completeProposal() {
      if (confirm('Kirim proposal ini untuk diapprove?')) {
        $.ajax({
            url: route('transaction.customerproposal.completeproposal', $("#idproposal").val()),
            data: {
              _token: $('meta[name="csrf_token"]').attr('content')
            },
            method: 'post'
          })
          .then(function(response) {
            if (response.status == 'error') {
              alert(response.description);
            } else if (response.status == 'success') {
              alert(response.description);
              refreshPage();
            }
          });
      }
    }

    function changeCustomer() {
      var customer = $('#cboCustomer').val().split(";");
      var creditdays = customer[1];
      $('#txtCustCredit').val(creditdays);
    }

    $(document).ready(function() {
      $(".cboCustomer").select2();


      function refresh() {
        $('#detailCustomerProposalTable').bootstrapTable('refresh', {
          url: route('transaction.customerproposal.datadetail', $("#idproposal").val()),
        });
      }

      $('#detailCustomerProposalTable').bootstrapTable({
        classes: 'table table-hover table-no-bordered',
        url: route('transaction.customerproposal.datadetail', $("#idproposal").val()),
        columns: [{
          title: 'NO',
          formatter: function(value, row, index) {
            return index + 1;
          }
        }, {
          field: 'nama_pelayaran',
          title: 'NAMA PELAYARAN',
          sortable: true
        }, {
          field: 'nama_kota_asal',
          title: 'KOTA PELABUHAN ASAL',
          sortable: true
        }, {
          field: 'nama_pelabuhan_asal',
          title: 'NAMA PELABUHAN ASAL',
          sortable: true
        }, {
          field: 'nama_kota_tujuan',
          title: 'KOTA PELABUHAN TUJUAN',
          sortable: true
        }, {
          field: 'nama_pelabuhan_tujuan',
          title: 'NAMA PELABUHAN TUJUAN',
          sortable: true
        }, {
          field: 'nama_consignee',
          title: 'CONSIGNEE',
          sortable: true
        },{
          title: 'TIPE PROPOSAL',
          field: 'type_proposal',
          formatter: function(value, row, index) {
            if (row.type_proposal == "CONTAINER") {
              return row.type_proposal + '<br/>(' + row.freight_size + ')';
            } else {
              return row.type_proposal + '<br/>(' + row.lcl_size + ')';
            }
          },
          sortable: true
        }, {
          title: 'PERIODE AWAL',
          field: 'valid_from',
          formatter: function(value, row, index) {
            return moment(value).format('DD-MM-YYYY');
          },
          sortable: true
        }, {
          title: 'PERIODE AKHIR',
          field: 'valid_to',
          formatter: function(value, row, index) {
            if(value==null||value==""){
              return "-";
            }else{
              return moment(value).format('DD-MM-YYYY');
            }
          },
          sortable: true
        }, {
          title: 'HARGA JUAL (IDR)',
          field: 'nominal_total',
          align: 'right',
          formatter: function(value, row, index) {
            return addCommas(row.nominal_total.toString());
          },
          sortable: true
        }, {
          title: 'AKSI',
          formatter: function(value, row, index) {
            var btnString = '';
            
            @can('edit proposal')
             btnString = '<a href="' + route('transaction.customerproposal.detailedit', {
              id: row.id_proposaldetail
            }) + '" data-toggle="tooltip" title="Edit Proposal"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>';
            btnString += '<button type="button" class="btn btn-delete btn-xs btn-danger" title="delete detail proposal"><i class="fa fa-trash"></i></button></a>';
            @endcan

            @if($mode != 'ADD')
            @if($proposal->status == 1)
            return btnString;
            @else
              @can('view detail proposal')
              return '<a href="' + route('transaction.customerproposal.detailview', {
                id: row.id_proposaldetail
              }) + '" data-toggle="tooltip" title="View Proposal"><button type="button" class="btn  btn-primary btn-xs"><i class="fa fa-eye"></i></button></a>';
              @endcan
            @endif
            @endif

          },
          events: {
            'click .btn-delete': function(e, value, row, index) {
              if (confirm('Anda yakin akan menghapus detail proposal ini?')) {
                $.ajax({
                    url: route('transaction.customerproposal.detaildelete', {
                      id: row.id_proposaldetail
                    }),
                    data: {
                      _token: $('meta[name="csrf_token"]').attr('content')
                    },
                    method: 'post'
                  })
                  .then(function(response) {
                    if (response.status == 'error') {
                      alert(response.description);
                    } else if (response.status == 'success') {
                      alert(response.description);
                      refresh();
                    }
                  });
              }
            }
          }
        }]
      })
    });
  </script>
  @endsection