<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" type="text/css" href="../public/css/bootstrap.min.css"/>
	<title>Hi</title>
</head>
<style>
   .pagenum:before {
        content: counter(page);
	}
	header {
                position: fixed;
				top: -30px;
				bottom : 30px;
                left: 0px;
                right: 0px;
                height: 50px;
			}
	table#detail{
		font-size: 8px;
	}
	body{
		font-size: 12px;
	}
	sup {
    font-size: 75%;
    line-height: 0;
    position: relative;
    vertical-align: baseline;
	}
	sup {
		top: -0.5em;
	}
	.page_break { page-break-before: always; }
</style>
<body>
<!-- <span class="pagenum"></span> -->
	<header>
		KODE SURAT : <b>{{$proposal[0]->no_proposal}}</b>
	</header>
	<br/>
	<br/>
	<div id="image">
		<img src="../public/img/350x150.png"/>
	</div>
	<br/>
	<div id="title">
	<h2><b>Proposal Tarif Jasa Ekspedisi</b></h2>
	</div>
	<br/>
	<div id="customer" class="col-md-6">
		<p>
			Kepada Yth,<br/>
			Bapak/Ibu {{$proposal[0]->name_cp}}<br/>
			<b>{{$proposal[0]->nama_pt}}</b><br/>
			{{$proposal[0]->alamat_pt}}<br/>
			Email &nbsp;&nbsp;&nbsp;{{$proposal[0]->email_1}}<br/>
			Telp &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$proposal[0]->telp}}<br/>
		</p>
	</div>
	<br/>
	<div id="isi">
		<p>
			Dengan hormat,<br/>
			Berikut ini kami ajukan proposal tarif pengiriman sebagai berikut :
		</p>
		<div class="col-md-8">
		<table id="detail" class="table table-bordered" cellpadding="0">
			<thead>
				<tr>
				<th style="width:15%">Rute</th>
				<th style="width:8%">Kondisi Kirim</th>
				<th style="width:8%">Tipe</th>
				<th style="width:8%">Muatan</th>
				<th style="width:10%">Total Harga</th>
				<th style="width:15%">Validity</th>
				<th style="width:15%">Biaya Lain</th>
			</tr>
			</thead>
			<tbody>
				@foreach($proposal as $item)
				<tr>
					<td>{{$item->nama_kota_asal}} - {{$item->nama_kota_tujuan}}</td>
					<td>{{$item->jenis_proposal}}</td>
					@if($item->type_proposal=='CONTAINER')
					<td>{{$item->type_proposal}}<br/>{{$item->freight_size}}</td>
					@else
					<td>{{$item->type_proposal}}</td>
					@endif
					@if($item->jenis_barang=='DG')
					<td>DANGEROUS GOODS</td>
					@elseif($item->jenis_barang=='GENERAL')
					<td>GENERAL CARGO</td>
					@endif
					<td>Rp. {{number_format($item->nominal_total,2,",",".")}}</td>
					@if($item->tipe_proposal=='CONTRACT')
					<td>{{Carbon\Carbon::parse($item->valid_from)->formatLocalized('%d %B %Y')}}<br> s/d <br/>{{Carbon\Carbon::parse($item->valid_to)->formatLocalized('%d %B %Y')}}</td>
					@else
					<td>Per {{Carbon\Carbon::parse($item->valid_from)->formatLocalized('%d %B %Y')}}</td>
					@endif
					<td>
					@foreach($item->biayalain as $biayalain)
					{{$biayalain->namabiayalain}} : Rp. {{number_format($biayalain->nominalbiayalain,2,",",".")}} ; 
					
					@endforeach
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		</div>
		@if($proposal[0]->keterangan_muat != null)
			<br/>
			Keterangan Muatan : {{$proposal[0]->keterangan_muat}}
			<br/>
		@endif
		<br/>
		{!!$proposal[0]->termcondition!!}
		<br/>
		<p>
		Kami mohon agar Bapak / Ibu dapat menandatangani surat proposal ini dan mengirimkan kembali ke <font color="blue">marketing@mandalajasapratama.com</font> sebagai tanda persetujuan atas proposal kami. Untuk informasi lebih lanjut silahkan hubungi kami.
		</p>
		<br/>
		<table class="table table-no-bordered" style="border-top:none">
			<tr>
				<td>Sidoarjo, {{Carbon\Carbon::now()->formatLocalized('%d %B %Y')}}<br/>Hormat kami,<br/><br/><br/><br/><hr/><b>Kino M.</b></td>
				<td></td>
				<td>Disetujui oleh,<br/><br/><br/><br/><br/><hr/></td>
			</tr>
		</table>
	</div>
	<div class="page_break">
	<p><b>LAMPIRAN : </b></p>
	@foreach($proposal as $item)
		@if($item->lampiran==null)
			COBA <br/>
		@endif
		TES 12341241231231<br/>
	@endforeach
	</div>
</body>
</html>