@extends('layouts.default')

@section('content_header')
<h1>
@if($mode=='ADD')
    ADD PENAGIHAN
@else
    EDIT PENAGIHAN
@endif
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">Penagihan</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
<form class="form-horizontal">
<div class="form-group">
    <label class="control-label col-xs-2" for="txtKodePenagihan">Kode Penagihan</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtKodePenagihan" id="txtKodePenagihan" type="text" @if($mode!='ADD') value="{{$penagihan->kode_penagihan}}" @endif readonly>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtTglPenagihan">Tanggal Penagihan</label> 
    <div class="col-xs-2">
      <input class="form-control" name="txtTglPenagihan" id="txtTglPenagihan" type="text" @if($mode!='ADD') value="{{date('d-m-Y', strtotime($penagihan->tgl_penagihan))}}" @endif >
      <span class="text-danger" id="errortgl_penagihan"></span>
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="cboCustomer">Nama Customer</label> 
    <div class="col-xs-3">
    <select class="select form-control cboCustomer" name="cboCustomer" id="cboCustomer" onchange="changeCustomer()" @if($mode=='EDIT') readonly disabled @endif>
      <option value="0" >-- PILIH CUSTOMER --</option>
      @foreach($customer as $item)
      @if($mode == 'ADD') 
        <option value="{{ $item->id_customer }}" >{{ $item->nama_pt }}</option>
      @else
        @if($penagihan->id_customer==$item->id_customer) 
        <option value="{{ $item->id_customer }}" selected>{{ $item->nama_pt }}</option>
        @else
        <option value="{{ $item->id_customer }}" >{{ $item->nama_pt }}</option>
        @endif
      @endif
    @endforeach
      </select>
      <span class="text-danger" id="errorid_customer"></span>
    </div>
  </div>

<div id="detailRute">

<br/>
<div class="portlet-body">
    <table class="table table-bordered" id="detailInvoiceTable" style='background-color: white'>
    <thead>
        <tr>
            <th>NO</th>
            <th>NO KUITANSI</th>
            <th>TGL KUITANSI</th>
            <th>KODE TRIP</th>
            <th>NAMA CUSTOMER</th>
           <th>TOTAL (IDR)</th>
           <th>TIPE INVOICE</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
        
        

    </table>
</div>
  <div class="form-group row">
    <div class="col-xs-offset-2 col-xs-6">
    @can('edit penagihan')
      @if($mode=='ADD')
      <button class="btn btn-primary" type="button" onclick="save()" name="submit"><i class="fas fa-save"></i> Simpan</button>
      @else
      <button class="btn btn-primary" type="button" onclick="update({{$penagihan->id_penagihan}})" name="submit"><i class="fas fa-save"></i> Simpan</button>
      @endif
    @endcan
      <a href="{{route('transaction.penagihan')}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
    </div>
  </div>
</form>

</div>
    



@endsection

@section('footer_script')
<script>
    $(".cboCustomer").select2();

    changeCustomer();

    $('#txtTglPenagihan').datetimepicker({
      minView: 2,
      format: 'dd-mm-yyyy',
      autoclose: true,
      todayBtn: true
    });

    function changeCustomer(){
     var idcustomer = $("#cboCustomer").val();
     var x = document.getElementById('detailInvoiceTable');
     $('#detailInvoiceTable tbody').html('');
    var new_row = '';
    var rownum = 0;
    $.ajax({
          url: route('transaction.penagihan.datainvoice',{id:idcustomer}),
          method:'get'
        })
        .then(function (response){
          if(response.total==0){
            new_row += '<tr>'
                    +  '<td colspan=7 style="text-align:center"><font color=red><b>TIDAK ADA INVOICE</b></font></td>'
                    + '</tr>';
            $("table#detailInvoiceTable").append(new_row);        
          }else{
            var data = response.rows;
            @php
            $a = 0;
            @endphp
            $.each( data, function( i, val ) {
              rownum  += 1;
              
                @if($mode=='ADD')
                if(val.kode_penagihan == null){
                  new_row += '<tr>'
                        + '<td><input type="checkbox" value="'+val.id_invoice+'"/>&nbsp;'+(i+1)+'</td>'
                        + '<td>'+val.no_invoice+'</td>'
                        + '<td>'+moment(val.tgl_invoice).format('DD-MM-YYYY')+'</td>'
                        + '<td>'+val.kode_trip+'</td>'
                        + '<td>'+val.nama_customer+'</td>'
                        + '<td>'+addCommas(val.total.toString())+'</td>'
                        + '<td><input type="hidden" name="typeinvoice" id="typeinvoice" value="'+val.type_invoice+'">'+val.type_invoice+'</td>'
                        + '</tr>';
                  }else{
                  new_row += '<tr>'
                        + '<td>&nbsp;'+(i+1)+'</td>'
                        + '<td>'+val.no_invoice+'</td>'
                        + '<td>'+moment(val.tgl_invoice).format('DD-MM-YYYY')+'</td>'
                        + '<td>'+val.kode_trip+'</td>'
                        + '<td>'+val.nama_customer+'</td>'
                        + '<td>'+addCommas(val.total.toString())+'</td>'
                        + '<td><input type="hidden" name="typeinvoice" id="typeinvoice" value="'+val.type_invoice+'">'+val.type_invoice+'</td>'
                        + '</tr>';
                  }
                @else
                  if(val.kode_penagihan != null && val.kode_penagihan=="{{$penagihan->kode_penagihan}}"){
                    new_row += '<tr>'
                          + "<td><a href='#' onclick='removeInvoice("+val.id_invoice+",\""+val.type_invoice+"\");'><i class='fa fa-times'  style='color:red'></i></a><input type='checkbox' value='"+val.id_invoice+"' checked hidden/>&nbsp;&nbsp;"+(i+1)+"</td>"
                          + '<td>'+val.no_invoice+'</td>'
                          + '<td>'+moment(val.tgl_invoice).format('DD-MM-YYYY')+'</td>'
                          + '<td>'+val.kode_trip+'</td>'
                          + '<td>'+val.nama_customer+'</td>'
                          + '<td>'+addCommas(val.total.toString())+'</td>'
                          + '<td><input type="hidden" name="typeinvoice" id="typeinvoice" value="'+val.type_invoice+'">'+val.type_invoice+'</td>'
                          + '</tr>';
                  }else if(val.kode_penagihan != null && val.kode_penagihan!="{{$penagihan->kode_penagihan}}"){
                    new_row += '<tr>'
                        + '<td>&nbsp;'+(i+1)+'</td>'
                        + '<td>'+val.no_invoice+'</td>'
                        + '<td>'+moment(val.tgl_invoice).format('DD-MM-YYYY')+'</td>'
                        + '<td>'+val.kode_trip+'</td>'
                        + '<td>'+val.nama_customer+'</td>'
                        + '<td>'+addCommas(val.total.toString())+'</td>'
                        + '<td><input type="hidden" name="typeinvoice" id="typeinvoice" value="'+val.type_invoice+'">'+val.type_invoice+'</td>'
                        + '</tr>';
                  }else{
                  new_row += '<tr>'
                        + '<td><input type="checkbox" value="'+val.id_invoice+'"/>&nbsp;'+(i+1)+'</td>'
                        + '<td>'+val.no_invoice+'</td>'
                        + '<td>'+moment(val.tgl_invoice).format('DD-MM-YYYY')+'</td>'
                        + '<td>'+val.kode_trip+'</td>'
                        + '<td>'+val.nama_customer+'</td>'
                        + '<td>'+addCommas(val.total.toString())+'</td>'
                        + '<td><input type="hidden" name="typeinvoice" id="typeinvoice" value="'+val.type_invoice+'">'+val.type_invoice+'</td>'
                        + '</tr>';
                  }
                @endif
                
                @php
                $a=$a+1;
                @endphp
                
            });
            $("table#detailInvoiceTable").append(new_row);
        }
              
    }); 
   }

   function removeInvoice($idnvoice,$type_invoice){
     if(confirm("Apakah anda yakin akan mengeluarkan Invoice ini dari Penagihan?")){
      //  alert("DO dikeluarkan");
      $.ajax({
              url: route('transaction.penagihan.deleteinvoice',{id:$idnvoice,tipe_invoice:$type_invoice}),
              data:{_token:$('meta[name="csrf_token"]').attr('content')},
              method:'post'
            })
            .then(function (response){
              if (response.status == 'error') {
                alert(response.description);
              } else if( response.status == 'success') {
                alert(response.description);
                location.reload();
              }
            }); 
      
     }
   }

   function save(){
    var obj = {_token:$('meta[name="csrf_token"]').attr('content'),tgl_penagihan:$("#txtTglPenagihan").val(),id_customer:$("#cboCustomer").val(),detail:[]};
    var detail = [];
    $("#detailInvoiceTable > tbody > tr").each(function (i, el) {
      var $tds = $(this).find('td');
      if($tds.eq(0).find('input').prop('checked') == true){
          var objDetail = {id_invoice:$tds.eq(0).find('input').val(),type_invoice:$tds.eq(6).find('input').val()};
          // alert($tds.eq(0).find('input').val());
          detail.push(objDetail);
      }
      
    });
    var count = detail.length;
    // alert(count);
      obj.detail = detail;
      if(count!=0){
        $.ajax({
          url: route('transaction.penagihan.create'),
          data:obj,
          method:'post'
        })
        .then(function (response){
          if (response.status == 'error') {
            alert(response.description);
          } else if( response.status == 'success') {
            alert(response.description);
            window.location = "{{route('transaction.penagihan')}}";
            // refresh();
          }
        })
        .catch(function (response) {
        console.log(response);
        if(response.status == '422') {
          let error = response.responseJSON.errors;
          $.each(error, function(index,item) {
            $('#error'+index).html(item.join(", "));
          });
          alert("Please check the form errors.");

        } else {
        alert(response.responseJSON.message);
        }
        
      }); 
      }else{
        alert("Tidak ada Invoice yang dipilih!");
      }
    

  // alert(objDO.alamat);
}

function update($idpenagihan){
    var obj = {_token:$('meta[name="csrf_token"]').attr('content'),tgl_penagihan:$("#txtTglPenagihan").val(),detail:[]};
    var detail = [];
    $("#detailInvoiceTable > tbody > tr").each(function (i, el) {
      var $tds = $(this).find('td');
      if($tds.eq(0).find('input').prop('checked') == true){
          var objDetail = {id_invoice:$tds.eq(0).find('input').val(),type_invoice:$tds.eq(6).find('input').val()};
          // alert($tds.eq(0).find('input').val());
          detail.push(objDetail);
      }
      
    });
    var count = detail.length;
    // alert(count);
      obj.detail = detail;
      if(count!=0){
        $.ajax({
          url: route('transaction.penagihan.update',{id:$idpenagihan}),
          data:obj,
          method:'post'
        })
        .then(function (response){
          if (response.status == 'error') {
            alert(response.description);
          } else if( response.status == 'success') {
            alert(response.description);
            window.location = "{{route('transaction.penagihan')}}";
            // refresh();
          }
        })
        .catch(function (response) {
        console.log(response);
        if(response.status == '422') {
          let error = response.responseJSON.errors;
          $.each(error, function(index,item) {
            $('#error'+index).html(item.join(", "));
          });
          alert("Please check the form errors.");

        } else {
        alert(response.responseJSON.message);
        }
        
      }); 
      }else{
        alert("Tidak ada Invoice yang dipilih!");
      }
    

  // alert(objDO.alamat);
}
    

</script>
@endsection
