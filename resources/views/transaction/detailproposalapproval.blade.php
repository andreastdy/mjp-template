@extends('layouts.default')

@section('content_header')
<h1>
    PROPOSAL APPROVAL DETAIL
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">Proposal Approval</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>

<form action="#" method="post" class="form-horizontal">
<input type="hidden" name="idproposal" id="idproposal" value="{{ $proposal->id_proposal }}">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="form-group">
    <label class="control-label col-xs-2" for="txtIdProposal">No Proposal</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtIdProposal" id="txtIdProposal" type="text" value="{{$proposal->no_proposal}}"  readonly>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="cboCustomer">Customer</label> 
    <div class="col-xs-4">
      <select class="select form-control" name="cboCustomer" id="cboCustomer"   disabled readonly >
      <option value="0;0" >-- PILIH CUSTOMER --</option>
      @foreach($customer as $item)
      
        @if($proposal->id_customer==$item->id_customer) 
        <option value="{{ $item->id_customer }};{{ $item->lama_kredit }}" selected>{{ $item->nama_pt }}</option>
        @else
        <option value="{{ $item->id_customer }};{{ $item->lama_kredit }}" >{{ $item->nama_pt }}</option>
        @endif
      
    @endforeach
      </select>
    </div>
  </div>
  
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtCustCredit">Customer Kredit</label> 
    <div class="col-xs-2">
      <input class="form-control" name="txtCustCredit" id="txtCustCredit" type="text" style="width:40%"  value="{{$proposal->customer_credit}}"   readonly > 
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2">Tipe Proposal</label> 
    <div class="col-xs-4">
      <label class="radio-inline">
        @if($proposal->tipe_proposal=="CONTRACT") 
        <input type="radio" name="rdoType" value="CONTRACT"  checked  disabled readonly >
        @else
        <input type="radio" name="rdoType" value="CONTRACT" disabled readonly>
        @endif
              Kontrak
      </label>
      <label class="radio-inline">

        @if($proposal->tipe_proposal=="NON-CONTRACT") 
        <input type="radio" name="rdoType" value="NON-CONTRACT"  checked disabled readonly >
        @else
        <input type="radio" name="rdoType" value="NON-CONTRACT" disabled readonly >
        @endif
              Non-Kontrak
      </label>
     
    </div>
  </div>
    
    
  
</form>


<div id="detailRute">



<div class="portlet-body">
    <table class="table table-bordered" id="detailCustomerProposalTable" style='background-color: white'>

    </table>
</div>
    <div class="form-group row">
    <div class="col-xs-offset-1 col-xs-6">
      @if($proposal->status==2)
      <button class="btn btn-primary" type="submit" onclick='approveProposal()' name="submit"><i class="far fa-thumbs-up"></i> Approve</button>
      <button class="btn btn-danger" name="cancel" onclick='rejectProposal()'><i class="far fa-thumbs-down"></i> Reject</button>
      @endif
      <a href="{{route('transaction.proposalapproval')}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
    </div>
  </div>
</div>

</div>



@endsection

@section('footer_script')
<script>
    function approveProposal(){
        if (confirm('Are you sure you want to approve this proposal?')) {
            $.ajax({
              url: route('transaction.proposalapproval.approveproposal',$("#idproposal").val()),
              data:{_token:$('meta[name="csrf_token"]').attr('content')},
              method:'post'
            })
            .then(function (response){
              if (response.status == 'error') {
                alert(response.description);
              } else if( response.status == 'success') {
                alert(response.description);
                refreshPage();
              }
            }); 
          }
      }

      function rejectProposal(){
        if (confirm('Are you sure you want to reject this proposal?')) {
            $.ajax({
              url: route('transaction.proposalapproval.rejectproposal',$("#idproposal").val()),
              data:{_token:$('meta[name="csrf_token"]').attr('content')},
              method:'post'
            })
            .then(function (response){
              if (response.status == 'error') {
                alert(response.description);
              } else if( response.status == 'success') {
                alert(response.description);
                refreshPage();
              }
            }); 
          }
      }

    function refreshPage(){
        location.reload();
      }

      


    $(document).ready(function () {

      

      function refresh(){
        $('#detailCustomerProposalTable').bootstrapTable('refresh',{
                  url: route('transaction.customerproposal.datadetail',$("#idproposal").val()),
                });
      }

    $('#detailCustomerProposalTable').bootstrapTable({
        classes:'table table-hover table-no-bordered',
        url: route('transaction.customerproposal.datadetail',$("#idproposal").val()),
    pagination: true,
  search: true,
  columns: [{
    title: 'NO',
    formatter: function (value, row, index) {
        return index+1;
      }
  }, {
    field: 'nama_pelayaran',
    title: 'NAMA PELAYARAN'
  },{
          field: 'nama_kota_asal',
          title: 'KOTA PELABUHAN ASAL',
          sortable: true
        }, {
          field: 'nama_pelabuhan_asal',
          title: 'NAMA PELABUHAN ASAL',
          sortable: true
        }, {
          field: 'nama_kota_tujuan',
          title: 'KOTA PELABUHAN TUJUAN',
          sortable: true
        }, {
          field: 'nama_pelabuhan_tujuan',
          title: 'NAMA PELABUHAN TUJUAN',
          sortable: true
        },
  {
    field: 'nama_consignee',
    title: 'CONSIGNEE',
    sortable: true
  },{
    title: 'TIPE PROPOSAL',
    formatter : function (value, row, index) {
        if(row.type_proposal=="CONTAINER"){
          return row.type_proposal+'<br/>('+row.freight_size+')';
        }else{
          return row.type_proposal+'<br/>('+row.lcl_size+')';
        }
      }
  },{
    title: 'PERIODE AWAL',
    field: 'valid_from',
    formatter : function (value, row, index) {
        return moment(value).format('DD-MM-YYYY');
      }
  },{
          title: 'PERIODE AKHIR',
          field: 'valid_to',
          formatter: function(value, row, index) {
            if(value==null||value==""){
              return "-";
            }else{
              return moment(value).format('DD-MM-YYYY');
            }
          },
          sortable: true
        }, {
    title: 'HARGA JUAL (IDR)',
    align:'right',
    formatter: function (value, row, index) {
      return addCommas(row.nominal_total.toString());
    }
  },{
      title:'AKSI',
      formatter: function(value, row, index) {
          var btnstring='';
          @can('view detail proposal')
          btnstring = '<a href="'+route('transaction.proposalapproval.detailview',{id:row.id_proposaldetail})+'" data-toggle="tooltip" title="View Detail Proposal"><button type="button" class="btn  btn-primary btn-xs"><i class="fa fa-eye"></i></button></a>';
          @endcan
          return btnstring;
        
      }
    }]
  })
});


    

</script>
@endsection
