@extends('layouts.default')

@section('content_header')
<h1>
    EXTRA COST / CLAIM
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">Extra Cost/Claim</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>

@can('add extra cost claim')
<a href="#" data-toggle="tooltip" title="Tambah Biaya"><button type='button' class='btn btn-danger' data-toggle="modal" data-target="#ListModal"><i class='fa fa-plus'></i> Buat Baru</button></a>
@endcan

<div class="portlet-body">
    <table class="table table-bordered" id="extraCostClaimTable" style='background-color: white'>
        
        

    </table>
</div>

<div id="ListModal" class="modal fade" role="dialog" >
    <div class="modal-dialog" style="width:900px;">

        <!-- Modal content-->
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">List Customer</h4>
            </div>
            <div class="modal-body">
            <table class="table table-bordered" id="ListCustomerTable" style='background-color: white'>
        
        

    </table>
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


@endsection

@section('footer_script')
<script>
function refresh() {
    $('#extraCostClaimTable').bootstrapTable('refresh',{
      url:route('transaction.extracostclaim.data')
    });
}

$('#extraCostClaimTable').bootstrapTable({
        classes:'table table-hover table-no-bordered',
        url: route('transaction.extracostclaim.data'),
    pagination: true,
    sidePagination: true,
  search: true,
  columns: [{
    title: 'NO',
    formatter: function (value, row, index) {
        return index+1;
      }
  },  {
    field: 'kode_biaya',
    title: 'KODE BIAYA',
    sortable: true
  },{
    field: 'tipe_biaya',
    title: 'TIPE BIAYA',
    sortable: true
  },{
    field: 'tgl_biaya',
    title: 'TANGGAL BIAYA',
    formatter : function (value, row, index) {
      if(value==null){
          return "-";
        }else{
          return moment(value).format('DD-MM-YYYY');
        }
      },
    sortable: true
  },{
    field: 'nama_customer',
    title: 'NAMA CUSTOMER',
    sortable: true
  },{
    field: 'no_invoice',
    title: 'NO INVOICE',
    sortable: true
  },{
    field: 'deskripsi',
    title: 'DESKRIPSI BIAYA',
    sortable: true
  },{
    title: 'NOMINAL BIAYA (IDR)',
    field: 'nominal_biaya',
    align:'right',
    formatter: function (value, row, index) {
        return addCommas(row.nominal_biaya.toString());
    },
    sortable: true
  },{
      title:'AKSI',
      formatter: function(value, row, index) {
        var btnString = '';
        @can('add extra cost claim')
          btnString += '<a href="'+route('transaction.extracostclaim.edit',{id:row.id_extracostclaim})+'" data-toggle="tooltip" title="Edit Extra Cost / Claim"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>';
           btnString +='<button type="button" class="btn btn-delete btn-xs btn-danger" title="Delete Extra Cost / Claim"><i class="fa fa-trash"></i></button></a>'
        @endcan
         
        return btnString;
      },
      events:{
        'click .btn-delete': function(e, value, row, index) {
          if (confirm('Anda yakin untuk menghapus biaya ini?')) {
            $.ajax({
              url: route('transaction.extracostclaim.delete',{id:row.id_extracostclaim}),
              data:{_token:$('meta[name="csrf_token"]').attr('content')},
              method:'post'
            })
            .then(function (response){
              if (response.status == 'error') {
                alert(response.description);
              } else if( response.status == 'success') {
                alert(response.description);
                refresh();
              }
            }); 
          }
        }
      }
    }]
});

$('#ListCustomerTable').bootstrapTable({
  classes:'table table-hover table-no-bordered',
  url: route('transaction.extracostclaim.datacustomer'),
  pagination : true,
  search : true,
columns: [{
title: 'NO',
formatter: function (value, row, index) {
  return index+1;
}
}, {
    field: 'nama_pt',
    title: 'NAMA CUSTOMER',
    sortable: true
  },{
title:'AKSI',
formatter: function(value, row, index) {
  var btnString = '<a href="'+route('transaction.extracostclaim.add',{idcustomer:row.id_customer})+'" data-toggle="tooltip" title="Tambah Biaya"><button type="button" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i></button></a>'
  return btnString;
}
}]
})

</script>
@endsection
