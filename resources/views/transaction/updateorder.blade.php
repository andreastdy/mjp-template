@extends('layouts.default')

@section('content_header')
<h1>
  MONITORING ORDER
</h1>
<ol class="breadcrumb">
  <li><a href="#"> Transaction</a></li>
  <li class="active">Monitoring Order</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<br />
<div class="portlet-body">
  <table class="table table-bordered" id="customerOrderTable" style='background-color: white'>
  </table>
</div>

<div id="DetailModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width:900px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">List Detail Order</h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered" id="listDetailTable" style='background-color: white'>
        </table>


        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



@endsection

@section('footer_script')
<script>
  $(document).ready(function() {

    function refresh() {
      $('#customerOrderTable').bootstrapTable('refresh', {
        url: route('transaction.updateorder.data'),
      });
    }

    $('#customerOrderTable').bootstrapTable({
      classes: 'table table-hover table-no-bordered',
      url: route('transaction.updateorder.data'),
      pagination: true,
      search: true,
      columns: [{
        title: 'NO',
        formatter: function(value, row, index) {
          return index + 1;
        }
      }, {
        field: 'nama_customer',
        title: 'NAMA CUSTOMER',
        sortable:true
      }, {
        field: 'nama_kota_asal',
        title: 'KOTA ASAL',
        sortable:true
      }, {
        title: 'PELABUHAN ASAL',
        field: 'nama_pelabuhan_asal',
        formatter: function(value, row, index) {
          return row.nama_kotapelabuhan_asal + ' - ' + row.nama_pelabuhan_asal;
        },
        sortable:true
      }, {
        field: 'nama_kota_tujuan',
        title: 'KOTA TUJUAN',
        sortable:true
      }, {
        title: 'PELABUHAN TUJUAN',
        field: 'nama_pelabuhan_tujuan',
        formatter: function(value, row, index) {
          return row.nama_kotapelabuhan_tujuan + ' - ' + row.nama_pelabuhan_tujuan;
        },
        sortable:true
      }, {
        title: 'TIPE ORDER',
        field: 'type_order',
        formatter: function(value, row, index) {
          if (row.type_order == "CONTAINER") {
            return row.type_order + '<br/>(' + row.freight_size + ')';
          } else {
            return row.type_order + '<br/>(' + row.lcl_size + ')';
          }
        },
        sortable:true
      }, {
        title: 'EXECUTED',
        field: 'executed_at',
        formatter: function(value, row, index) {
          if (row.executed_at == null) {
            return "-";
          } else {
            return row.executed_by + '<br/>(' + moment(row.executed_at).format('DD-MM-YYYY HH:mm:ss') + ')';
          }

        },
        sortable:true
      }, {
        title: 'AKSI',
        formatter: function(value, row, index) {
          var btnString = '';
          @can('update status order')
          btnString += '<a href="' + route('transaction.updateorder.edit', {
            id: row.id_order
          }) + '" data-toggle="tooltip" title="Update Order"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>';
          @endcan
          btnString += '<button type="button" class="btn btn-view btn-primary btn-xs" data-toggle="modal" data-target="#DetailModal"><i class="fa fa-eye"></i></button></a>';
          return btnString;
        },
        events: {
          'click .btn-view': function(e, value, row, index) {
            $('#listDetailTable').bootstrapTable('refresh', {
              url: route('transaction.updateorder.listbarang', {
                id: row.id_order
              }),
            });
            $('#listDetailTable').bootstrapTable({
              classes: 'table table-hover table-no-bordered',
              url: route('transaction.updateorder.listbarang', {
                id: row.id_order
              }),
              columns: [{
                title: 'NO',
                formatter: function(value, row, index) {
                  return index + 1;
                }
              }, {
                field: 'no_suratjalan',
                title: 'NO SURAT JALAN',
                sortable:true
              }, {
                field: 'no_container',
                title: 'NO CONTAINER',
                sortable:true
              }, {
                field: 'seal',
                title: 'SEAL',
                sortable:true
              }, {
                title: 'STATUS',
                field: 'status',
                formatter: function(value, row, index) {
                  if (row.status == 0) {
                    return '<font color=black>BARANG DITERIMA</font>';
                  } else if (row.status == 1) {
                    return '<font color=black>KAPAL BERANGKAT</font>';
                  } else if (row.status == 2) {
                    return '<font color=black>KAPAL TIBA</font>';
                  } else if (row.status == 3) {
                    return '<font color=black>KAPAL SANDAR</font>';
                  } else if (row.status == 4) {
                    return '<font color=black>KAPAL BONGKAR</font>';
                  } else if (row.status == 5) {
                    return '<font color=black>BARANG DITERIMA DOORING</font>';
                  } else if (row.status == 6) {
                    return '<font color=black>BARANG DITERIMA CUSTOMER</font>';
                  }
                },
                sortable:true
              }, {
                field: 'keterangan_status',
                title: 'KETERANGAN',
                sortable:true
              }, {
                title: 'TANGGAL UPDATE STATUS',
                field: 'updated_at',
                formatter: function(value, row, index) {
                  if (row.updated_at == null) {
                    return "-";
                  } else {
                    return row.updated_by + '<br/>(' + moment(row.updated_at).format('DD-MM-YYYY HH:mm:ss') + ')';
                  }

                },
                sortable:true
              }]
            });
          }
        }
      }]
    });


  });
</script>
@endsection