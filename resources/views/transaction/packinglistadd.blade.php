@extends('layouts.default')

@section('content_header')
<h1>
  ADD PACKING LIST
</h1>
<ol class="breadcrumb">
  <li><a href="#"> Transaction</a></li>
  <li class="active">Packing List</li>
</ol>
@endsection

@section('content')
<hr>
<form class="form-horizontal">
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">No Packing List</label>
    <div class="col-xs-3">
      <input class="form-control" name="txtKota" id="txtKota" type="text" value="PL29081" readonly>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="chkPortToDoor">Kode Trip</label>
    <div class="col-xs-4">
      <input class="form-control" name="txtKota" id="txtKota" type="text">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">Perwakilan</label>
    <div class="col-xs-3">
      <input class="form-control" name="txtKota" id="txtKota" type="text">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">Nama Kapal</label>
    <div class="col-xs-3">
      <input class="form-control" name="txtKota" id="txtKota" type="text">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">Tanggal Berangkat</label>
    <div class="col-xs-3">
      <input class="form-control" name="txtKota" id="txtKota" type="text">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="cboPortName">Kota Pelabuhan Bongkar</label>
    <div class="col-xs-4">
      <select class="select form-control" name="cboPortName" id="cboPortName">
        <option value="1">SURABAYA</option>
        <option value="2" selected>JAKARTA</option>
        <option value="2">MEDAN</option>
      </select>
    </div>
  </div>

  <div id="detailRute">
    <input type="text" placeholder="search.." style="float: right; margin-top:1%">
    <div class="portlet-body">
      <table class="table table-bordered" id="detailCustomerProposalTable" style='background-color: white'>
        <tr>
          <th>NO BAPB</th>
          <th>NO ORDER</th>
          <th>NO SURAT JALAN</th>
          <th>NO CONTAINER</th>
          <th>NO SEAL</th>
          <th>NAMA BARANG</th>
          <th>NAMA PENERIMA</th>
          <th>KOTA PELABUHAN</th>
          <th>AKSI</th>
        </tr>
        <tr>
          <td>290891BA</td>
          <td>ORD290891</td>
          <td>SJ29091</td>
          <td>MRTU29098</td>
          <td>29098</td>
          <td>INDOMIE</td>
          <td>CONSIGNEE A</td>
          <td>JAKARTA</td>
          <td>
            <input type="checkbox" name="chk1" id="chk1">
          </td>
        </tr>
        <tr>
          <td>290891BA</td>
          <td>ORD290891</td>
          <td>SJ29091</td>
          <td>MRTU29098</td>
          <td>29098</td>
          <td>SUSU UHT</td>
          <td>CONSIGNEE A</td>
          <td>JAKARTA</td>
          <td>
            <input type="checkbox" name="chk1" id="chk1">
          </td>
        </tr>
        <tr>
          <td>302801BA</td>
          <td>ORD302801</td>
          <td>SJ30981</td>
          <td>MRTU302801</td>
          <td>302801</td>
          <td>INDOMIE</td>
          <td>CONSIGNEE B</td>
          <td>JAKARTA</td>
          <td>
            <input type="checkbox" name="chk1" id="chk1" checked>
          </td>
        </tr>
        <tr>
          <td>302801BA</td>
          <td>ORD302801</td>
          <td>SJ30981</td>
          <td>MRTU302801</td>
          <td>302801</td>
          <td>SUSU UHT</td>
          <td>CONSIGNEE B</td>
          <td>JAKARTA</td>
          <td>
            <input type="checkbox" name="chk1" id="chk1" checked>
          </td>
        </tr>


      </table>
    </div>

  </div>

  <div class="form-group row">
    <div class="col-xs-offset-2 col-xs-6">
      <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
      <button class="btn btn-danger" name="cancel">Cancel</button>
    </div>
  </div>
</form>



@endsection

@section('footer_script')
<script>
  $("#detailRute").hide();
</script>
@endsection