@extends('layouts.default')

@section('content_header')
<h1>
@if($mode=='ADD')
    ADD PELUNASAN
@else
    EDIT PELUNASAN
@endif
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">Pelunasan</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
<form action="#" method="post" class="form-horizontal">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="form-group">
    <label class="control-label col-xs-3" for="txtKodePelunasan">Kode Pelunasan</label> 
    <div class="col-xs-4">
      <input class="form-control" name="txtKodePelunasan" id="txtKodePelunasan" type="text" @if($mode=='EDIT') value="{{$pelunasan->kode_pelunasan}}" @endif readonly>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" for="txtNamaCustomer">Nama Customer</label> 
    <div class="col-xs-4">
      <input class="form-control" name="txtNamaCustomer" id="txtNamaCustomer" type="text"  value="{{$customer->nama_pt}}" readonly>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" for="txtTglPelunasan">Tanggal Pelunasan</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtTglPelunasan" id="txtTglPelunasan" type="text" @if($mode!='ADD') value="{{date('d-m-Y', strtotime($pelunasan->tgl_pelunasan))}}" @endif  >
      <span class="text-danger" id="errortgl_pelunasan"></span>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" for="txtBuktiBayar">Bukti Bayar<br/><span class="opsional"><sup>(opsional)</sup></span></label>
    <div class="col-xs-3">
      <input class="form-control" name="txtBuktiBayar" id="txtBuktiBayar" type="text" @if($mode!='ADD') value="{{$pelunasan->bukti_bayar}}" @endif @if($mode=='VIEW') readonly @endif>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" for="txtKeterangan">Keterangan<br/><span class="opsional"><sup>(opsional)</sup></span></label>
    <div class="col-xs-4">
       <textarea class="form-control" rows="3" cols="40" name="txtKeterangan" id="txtKeterangan">@if($mode!='ADD') {{$pelunasan->keterangan}} @endif</textarea>
    </div>
  </div>
<table class="table table-bordered" id="detailInvoiceTable" style='background-color: white'>
     <tr>
      <th>NO</th>
      <th>NO INVOICE</th>
      <th>TIPE INVOICE</th>
      <th>TGL INVOICE</th>
      <th>TGL PENAGIHAN</th>
      <th style="text-align:right">NOMINAL DITAGIH</th>
      <th style="text-align: right">NOMINAL BELUM DIBAYAR</th>
    </tr>
    @php
    $i = 0;
    @endphp

    @foreach($invoice as $item)
    @php
    $i++;
    $nominalbelumdibayar = $item->nominal_biaya - $item->nominal_dibayar;
    @endphp
          <tr>
          @if($mode!='ADD')
            @if(in_array($item->no_invoice, $dpelunasan)) 
            <td><a href="#" onclick="removeInvoice('{{$item->no_invoice}}');"><i class="fa fa-times"  style="color:red"></i></a><input type="checkbox" id="chkInvoice_{{$item->no_invoice}}" value="{{$item->id_invoice}}|{{$item->type_invoice}}|{{$item->no_invoice}}" onchange="changeHarga();" style='display:none;' checked/>&nbsp;{{$i}}</td>
            @else
              <td><input type="checkbox" id="chkInvoice_{{$item->no_invoice}}" value="{{$item->id_invoice}}|{{$item->type_invoice}}|{{$item->no_invoice}}" onchange="changeHarga();"/>&nbsp;{{$i}}</td>
            @endif
          @else
          <td><input type="checkbox" id="chkInvoice_{{$item->no_invoice}}" value="{{$item->id_invoice}}|{{$item->type_invoice}}|{{$item->no_invoice}}" onchange="changeHarga();"/>&nbsp;{{$i}}</td>
          @endif
              
              <td>{{$item->no_invoice}}</td>
              <td>{{$item->type_invoice}}</td>
              <td>{{date('d-m-Y', strtotime($item->tgl_invoice))}}</td>
              <td>{{date('d-m-Y', strtotime($item->tgl_penagihan))}}</td>
              <td style="text-align: right">{{number_format($item->nominal_biaya)}}</td>
              <td style="text-align: right">{{number_format($nominalbelumdibayar)}}</td>
              <input class="form-control" name="txtNominalInvoice_{{$item->no_invoice}}" id="txtNominalInvoice_{{$item->no_invoice}}" type="hidden"  value="{{$nominalbelumdibayar}}"  >
          </tr>
    @endforeach
        <tr>
          <td style="text-align: right" colspan="6">SUB TOTAL</td>
         <td style="text-align: right" id="lbltotalinvoice">0</td>
        </tr>
      
    </table>

    <div class="col-xs-2">
    <label><b>EXTRA COST / CLAIM</b></label>
    </div>
    <table class="table table-bordered" id="detailExtraTable" style='background-color: white'>
     <tr>
      <th>NO</th>
      <th>KODE BIAYA</th>
      <th>TIPE BIAYA</th>
      <th>DESKRIPSI</th>
      <th>TGL INVOICE</th>
      <th style="text-align:right">NOMINAL</th>
    </tr>
    @php
    $i = 0;
    @endphp

    @foreach($extracostclaim as $item)
    @php
    $i++;
    @endphp
          <tr>
              @if($mode!='ADD')
                @if(in_array($item->kode_biaya, $dpelunasan)) 
                  <td><a href="#" onclick="removeInvoice('{{$item->kode_biaya}}');"><i class="fa fa-times"  style="color:red"></i></a><input type="checkbox" id="chkInvoice_{{$item->kode_biaya}}" value="{{$item->id_extracostclaim}}|{{$item->tipe_biaya}}|{{$item->kode_biaya}}" onchange="changeHarga();" style='display:none;' checked/>&nbsp;{{$i}}</td>
                @else
                  <td><input type="checkbox" id="chkInvoice_{{$item->kode_biaya}}" value="{{$item->id_extracostclaim}}|{{$item->tipe_biaya}}|{{$item->kode_biaya}}" onchange="changeHarga();"/>&nbsp;{{$i}}</td>
                @endif
              @else
              <td><input type="checkbox" id="chkInvoice_{{$item->kode_biaya}}" value="{{$item->id_extracostclaim}}|{{$item->tipe_biaya}}|{{$item->kode_biaya}}" onchange="changeHarga();"/>&nbsp;{{$i}}</td>
              @endif
              <td>{{$item->kode_biaya}}</td>
              <td>{{$item->tipe_biaya}}</td>
              <td>{{$item->deskripsi}}</td>
              <td>{{date('d-m-Y', strtotime($item->tgl_biaya))}}</td>
              @if($item->tipe_biaya=="CLAIM")
              <td style="text-align: right">-{{number_format($item->nominal_biaya)}}</td>
              @else
              <td style="text-align: right">{{number_format($item->nominal_biaya)}}</td>
              @endif
              <input class="form-control" name="txtNominalExtra_{{$item->kode_biaya}}" id="txtNominalExtra_{{$item->kode_biaya}}" type="hidden"  value="{{$item->nominal_biaya}}"  >
          </tr>
    @endforeach
        <tr>
          <td style="text-align: right" colspan="5">SUB TOTAL</td>
         <td style="text-align: right" id="lbltotalextraclaim">0</td>
        </tr>
        <tr>
          <td style="text-align: right" colspan="5">TOTAL</td>
         <td style="text-align: right" id="lbltotal">0</td>
        </tr>

    </table>
<div class="form-group">
    <label class="control-label col-xs-3" for="txtpph23">PPH 23</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input class="form-control input-element" name="txtpph23" id="txtpph23" type="text" @if($mode=='ADD' )  value="0" @else value="{{number_format($pelunasan->pph23)}}" @endif style="text-align: right" onkeyup="changeHarga();" onkeypress="return isNumberKey(event)">
        <span class="text-danger" id="errorpph23"></span>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" for="txtAdminBank">Biaya Admin Bank</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input class="form-control input-element" name="txtAdminBank" id="txtAdminBank" type="text" @if($mode=='ADD' )  value="0" @else value="{{number_format($pelunasan->admin_bank)}}" @endif style="text-align: right" onkeyup="changeHarga();" onkeypress="return isNumberKey(event)">
        <span class="text-danger" id="erroradmin_bank"></span>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" for="txtLainlain">Potongan Lain-lain</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input class="form-control input-element" name="txtLainlain" id="txtLainlain" type="text" @if($mode=='ADD' )  value="0" @else value="{{number_format($pelunasan->lain_lain)}}" @endif style="text-align: right" onkeyup="changeHarga();" onkeypress="return isNumberKey(event)">
        <span class="text-danger" id="errorlain_lain"></span>
      </div>
    </div>
  </div>
<div class="form-group">
    <label class="control-label col-xs-3" for="txtDeposit">Deposit</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input class="form-control input-element" name="txtDeposit" id="txtDeposit" type="text"  value="{{number_format($customer->nominal_deposit)}}"  style="text-align: right" readonly>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" for="txtNominalDibayar">Nominal Dibayar</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input class="form-control input-element" name="txtNominalDibayar" id="txtNominalDibayar" type="text" @if($mode=='ADD' )  value="0" @else value="{{number_format($pelunasan->total_bayar)}}" @endif style="text-align: right" onkeyup="changeHarga();" onkeypress="return isNumberKey(event)">
        <span class="text-danger" id="errortotal_bayar"></span>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" for="txtSisaDeposit">Sisa Deposit</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        @php
        
        @endphp
        <input class="form-control input-element" name="txtSisaDeposit" id="txtSisaDeposit" type="text"  value=""  style="text-align: right" readonly>
      </div>
    </div>
  </div>
  

<div class="form-group row">
    <div class="col-xs-offset-3 col-xs-6">
    @can('edit pelunasan')
      @if($mode=='ADD')
      <button class="btn btn-primary" type="button" onclick="save({{$customer->id_customer}})" name="submit"><i class="fas fa-save"></i> Simpan</button>
      @elseif($mode=='EDIT')
      <button class="btn btn-primary" type="button" onclick="update({{$customer->id_customer}},{{$pelunasan->id_m_pelunasan}})" name="submit"><i class="fas fa-save"></i> Update</button>
      <button class="btn btn-success" type="button" onclick="konfirmasi({{$customer->id_customer}},{{$pelunasan->id_m_pelunasan}})" name="submit"><i class="fas fa-check"></i> Confirm</button>
      @endif
    @endcan
      <a href="{{route('transaction.pelunasan')}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
    </div>
  </div>
</form>

</div>

@endsection

@section('footer_script')
<script>
    
    $('.input-element').toArray().forEach(function(field){
new Cleave(field, {
    numeral: true,
    numeralThousandsGroupStyle: 'thousand',
    autoUnmask : true
})
});

  $('#txtTglPelunasan').datetimepicker({
      minView: 2,
      format: 'dd-mm-yyyy',
      autoclose: true,
      todayBtn: true
    });

    function save($idcustomer){
      var obj = {_token:$('meta[name="csrf_token"]').attr('content'),id_customer:$idcustomer,tgl_pelunasan:$("#txtTglPelunasan").val(),keterangan:$("#txtKeterangan").val(),bukti_bayar:$("#txtBuktiBayar").val(),
                pph23:$("#txtpph23").val().replace(/,/g, ''),admin_bank:$("#txtAdminBank").val().replace(/,/g, ''),lain_lain:$("#txtLainlain").val().replace(/,/g, ''),total_bayar:$("#txtNominalDibayar").val().replace(/,/g, ''),sisa_deposit:$("#txtSisaDeposit").val().replace(/,/g, ''),detail:[]};
      var detail = [];

      $("#detailInvoiceTable > tbody > tr").each(function (i, el) {
        var $tds = $(this).find('td');
        
        if($tds.eq(0).find('input').prop('checked') == true){
          var $td = $tds.eq(0).find('input').val().split("|");
          var $idinvoice = $td[0];
          var $tipeinvoice = $td[1];
          var $noinvoice = $td[2];
          var $nominalinvoice = $("#txtNominalInvoice_"+$noinvoice).val().replace(/,/g, '');
          var objDetail = {id_detail:$idinvoice,no_detail:$noinvoice,tipe_detail:$tipeinvoice,nominal_detail:$nominalinvoice};
          // alert($tds.eq(0).find('input').val());
          detail.push(objDetail);
        }
      });

      $("#detailExtraTable > tbody > tr").each(function (i, el) {
        var $tds = $(this).find('td');
        
        if($tds.eq(0).find('input').prop('checked') == true){
          var $td = $tds.eq(0).find('input').val().split("|");
          var $idbiaya = $td[0];
          var $tipebiaya = $td[1];
          var $kodebiaya = $td[2];
          var $nominalbiaya = $("#txtNominalExtra_"+$kodebiaya).val().replace(/,/g, '');
          var objDetail = {id_detail:$idbiaya,no_detail:$kodebiaya,tipe_detail:$tipebiaya,nominal_detail:$nominalbiaya};
          // alert($tds.eq(0).find('input').val());
          detail.push(objDetail);
          
        }
      });

      var count = detail.length;
  // alert(count);
      obj.detail = detail;
      if(count!=0){
        $.ajax({
          url: route('transaction.pelunasan.create'),
          data:obj,
          method:'post'
        })
        .then(function (response){
          if (response.status == 'error') {
            alert(response.description);
          } else if( response.status == 'success') {
            alert(response.description);
            window.location = "{{route('transaction.pelunasan')}}";
            // refresh();
          }
        })
        .catch(function (response) {
          console.log(response);
          if(response.status == '422') {
            let error = response.responseJSON.errors;
            $.each(error, function(index,item) {
              $('#error'+index).html(item.join(", "));
            });
            alert("Please check the form errors.");

          } else {
          alert(response.responseJSON.message);
          }
          
        }); 
      }else{
        alert("Tidak ada Invoice/Biaya yang dipilih!");
      }

    }

    function changeHarga(){
      // alert("ytes");
      var $totalinvoice = 0;
      var $totalextraclaim = 0;
      var $deposit = $("#txtDeposit").val().replace(/,/g, '');
      var $total = 0;
      var $sisadeposit = 0;
      var $totalbayar = $("#txtNominalDibayar").val().replace(/,/g, '');
      var $totalpotongan = 0;
      var $pph23 = $("#txtpph23").val().replace(/,/g, '');
      var $adminbank = $("#txtAdminBank").val().replace(/,/g, '');
      var $lainlain = $("#txtLainlain").val().replace(/,/g, '');

      $totalpotongan = parseInt($pph23) + parseInt($adminbank) + parseInt($lainlain);
      
      $("#detailInvoiceTable > tbody > tr").each(function (i, el) {
        var $tds = $(this).find('td');
        
        if($tds.eq(0).find('input').prop('checked') == true){
          var $td = $tds.eq(0).find('input').val().split("|");
          var $idinvoice = $td[0];
          var $tipeinvoice = $td[1];
          var $noinvoice = $td[2];
          var $nominalinvoice = $("#txtNominalInvoice_"+$noinvoice).val().replace(/,/g, '');
          $totalinvoice = parseInt($totalinvoice) + parseInt($nominalinvoice);
        }
      });

      $("#lbltotalinvoice").html(addCommas( String($totalinvoice) ));

      $("#detailExtraTable > tbody > tr").each(function (i, el) {
        var $tds = $(this).find('td');
        
        if($tds.eq(0).find('input').prop('checked') == true){
          var $td = $tds.eq(0).find('input').val().split("|");
          var $idbiaya = $td[0];
          var $tipebiaya = $td[1];
          var $kodebiaya = $td[2];
          var $nominalbiaya = $("#txtNominalExtra_"+$kodebiaya).val().replace(/,/g, '');
          if($tipebiaya=="EXTRACOST"){
            $totalextraclaim = parseInt($totalextraclaim) + parseInt($nominalbiaya);
          }else{
            $totalextraclaim = parseInt($totalextraclaim) - parseInt($nominalbiaya);
          }
          
        }
      });

      $("#lbltotalextraclaim").html(addCommas( String($totalextraclaim) ));

      $total = parseInt($totalinvoice) + parseInt($totalextraclaim);

      $("#lbltotal").html(addCommas( String($total) ));

      $sisadeposit = ( parseInt($deposit) + parseInt($totalbayar) +  parseInt($totalpotongan) ) - parseInt($total)  ;

      if($sisadeposit>0){
        $("#txtSisaDeposit").val(addCommas(String($sisadeposit)));
      }else{
        $("#txtSisaDeposit").val("0");
      }

      // alert($totalinvoice);
    }

    function removeInvoice($kodebiaya){
        if(confirm("Apakah anda yakin akan mengeluarkan invoice ini?")){
        $.ajax({
                url: route('transaction.pelunasan.deleteinvoice',{kodebiaya:$kodebiaya}),
                data:{_token:$('meta[name="csrf_token"]').attr('content')},
                method:'post'
              })
              .then(function (response){
                if (response.status == 'error') {
                  alert(response.description);
                } else if( response.status == 'success') {
                  alert(response.description);
                  location.reload();
                }
              }); 
      }
    }

    function update($idcustomer,$idpelunasan){
      var obj = {_token:$('meta[name="csrf_token"]').attr('content'),id_customer:$idcustomer,id_pelunasan:$idpelunasan,tgl_pelunasan:$("#txtTglPelunasan").val(),keterangan:$("#txtKeterangan").val(),bukti_bayar:$("#txtBuktiBayar").val(),
        pph23:$("#txtpph23").val().replace(/,/g, ''),admin_bank:$("#txtAdminBank").val().replace(/,/g, ''),lain_lain:$("#txtLainlain").val().replace(/,/g, ''),total_bayar:$("#txtNominalDibayar").val().replace(/,/g, ''),sisa_deposit:$("#txtSisaDeposit").val().replace(/,/g, ''),detail:[]};
      var detail = [];

      $("#detailInvoiceTable > tbody > tr").each(function (i, el) {
        var $tds = $(this).find('td');
        
        if($tds.eq(0).find('input').prop('checked') == true){
          var $td = $tds.eq(0).find('input').val().split("|");
          var $idinvoice = $td[0];
          var $tipeinvoice = $td[1];
          var $noinvoice = $td[2];
          var $nominalinvoice = $("#txtNominalInvoice_"+$noinvoice).val().replace(/,/g, '');
          var objDetail = {id_detail:$idinvoice,no_detail:$noinvoice,tipe_detail:$tipeinvoice,nominal_detail:$nominalinvoice};
          // alert($tds.eq(0).find('input').val());
          detail.push(objDetail);
        }
      });

      $("#detailExtraTable > tbody > tr").each(function (i, el) {
        var $tds = $(this).find('td');
        
        if($tds.eq(0).find('input').prop('checked') == true){
          var $td = $tds.eq(0).find('input').val().split("|");
          var $idbiaya = $td[0];
          var $tipebiaya = $td[1];
          var $kodebiaya = $td[2];
          var $nominalbiaya = $("#txtNominalExtra_"+$kodebiaya).val().replace(/,/g, '');
          var objDetail = {id_detail:$idbiaya,no_detail:$kodebiaya,tipe_detail:$tipebiaya,nominal_detail:$nominalbiaya};
          // alert($tds.eq(0).find('input').val());
          detail.push(objDetail);
          
        }
      });

      var count = detail.length;
  // alert(count);
      obj.detail = detail;
      if(count!=0){
        $.ajax({
          url: route('transaction.pelunasan.update'),
          data:obj,
          method:'post'
        })
        .then(function (response){
          if (response.status == 'error') {
            alert(response.description);
          } else if( response.status == 'success') {
            alert(response.description);
            window.location = "{{route('transaction.pelunasan')}}";
            // refresh();
          }
        })
        .catch(function (response) {
          console.log(response);
          if(response.status == '422') {
            let error = response.responseJSON.errors;
            $.each(error, function(index,item) {
              $('#error'+index).html(item.join(", "));
            });
            alert("Please check the form errors.");

          } else {
          alert(response.responseJSON.message);
          }
          
        }); 
      }else{
        alert("Tidak ada Invoice/Biaya yang dipilih!");
      }

    }


    function konfirmasi($idcustomer,$idpelunasan){
      if(confirm("Apakah anda yakin akan mengkonfirmasi pelunasan ini?")){
        var obj = {_token:$('meta[name="csrf_token"]').attr('content'),id_customer:$idcustomer,id_pelunasan:$idpelunasan,tgl_pelunasan:$("#txtTglPelunasan").val(),keterangan:$("#txtKeterangan").val(),bukti_bayar:$("#txtBuktiBayar").val(),
          pph23:$("#txtpph23").val().replace(/,/g, ''),admin_bank:$("#txtAdminBank").val().replace(/,/g, ''),lain_lain:$("#txtLainlain").val().replace(/,/g, ''),total_bayar:$("#txtNominalDibayar").val().replace(/,/g, ''),sisa_deposit:$("#txtSisaDeposit").val().replace(/,/g, ''),detail:[]};
        var detail = [];

        $("#detailInvoiceTable > tbody > tr").each(function (i, el) {
          var $tds = $(this).find('td');
          
          if($tds.eq(0).find('input').prop('checked') == true){
            var $td = $tds.eq(0).find('input').val().split("|");
            var $idinvoice = $td[0];
            var $tipeinvoice = $td[1];
            var $noinvoice = $td[2];
            var $nominalinvoice = $("#txtNominalInvoice_"+$noinvoice).val().replace(/,/g, '');
            var objDetail = {id_detail:$idinvoice,no_detail:$noinvoice,tipe_detail:$tipeinvoice,nominal_detail:$nominalinvoice};
            // alert($tds.eq(0).find('input').val());
            detail.push(objDetail);
          }
        });

        $("#detailExtraTable > tbody > tr").each(function (i, el) {
          var $tds = $(this).find('td');
          
          if($tds.eq(0).find('input').prop('checked') == true){
            var $td = $tds.eq(0).find('input').val().split("|");
            var $idbiaya = $td[0];
            var $tipebiaya = $td[1];
            var $kodebiaya = $td[2];
            var $nominalbiaya = $("#txtNominalExtra_"+$kodebiaya).val().replace(/,/g, '');
            var objDetail = {id_detail:$idbiaya,no_detail:$kodebiaya,tipe_detail:$tipebiaya,nominal_detail:$nominalbiaya};
            // alert($tds.eq(0).find('input').val());
            detail.push(objDetail);
            
          }
        });

        var count = detail.length;
    // alert(count);
        obj.detail = detail;
        if(count!=0){
          $.ajax({
            url: route('transaction.pelunasan.confirm'),
            data:obj,
            method:'post'
          })
          .then(function (response){
            if (response.status == 'error') {
              alert(response.description);
            } else if( response.status == 'success') {
              alert(response.description);
              window.location = "{{route('transaction.pelunasan')}}";
              // refresh();
            }
          })
          .catch(function (response) {
            console.log(response);
            if(response.status == '422') {
              let error = response.responseJSON.errors;
              $.each(error, function(index,item) {
                $('#error'+index).html(item.join(", "));
              });
              alert("Please check the form errors.");

            } else {
            alert(response.responseJSON.message);
            }
            
          }); 
        }else{
          alert("Tidak ada Invoice/Biaya yang dipilih!");
        }
      }
    }
    
    changeHarga();

</script>
@endsection
