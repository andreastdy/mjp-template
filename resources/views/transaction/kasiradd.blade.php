@extends('layouts.default')

@section('content_header')
<h1>
    ADD KASIR
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">Kasir</li>
</ol>
@endsection

@section('content')
<hr>
<form class="form-horizontal">
  <div class="form-group">
    <label class="control-label col-xs-2" for="cboPortAsal">Keterangan</label> 
    <div class="col-xs-4">
      <textarea class="form-control" rows="3" cols="40" name="txtAlamat" id="txtAlamat"></textarea>
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txt20ft">Cash In</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input class="form-control" name="txt20ft" id="txt20ft" type="text" value="0" style="text-align: right">
      </div>
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txt20ft">Cash Out</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input class="form-control" name="txt20ft" id="txt20ft" type="text" value="0" style="text-align: right">
      </div>
    </div>
  </div>
    <!-- <div class="form-group">
    <label class="control-label col-xs-2" for="cboPortAsal">Attachment</label> 
    <div class="col-xs-5">
      <input required="required" class="form-control" name="txtShortname" id="txtShortname" type="file">
    </div>
  </div> -->
    
    
  <div class="form-group row">
    <div class="col-xs-offset-2 col-xs-5">
      <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
      <button class="btn btn-danger" name="cancel">Cancel</button>
    </div>
  </div>
</form>



@endsection

@section('footer_script')
<script>
    
   
    

</script>
@endsection
