@extends('layouts.default')

@section('content_header')
<h1>
  SURAT JALAN
</h1>
<ol class="breadcrumb">
  <li><a href="#"> Transaction</a></li>
  <li class="active">Surat Jalan</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>

@can('edit surat jalan')
<button type='button' class='btn btn-danger' data-toggle="modal" data-target="#AddModal"><i class='fa fa-plus'></i> Buat Baru</button></a>
@endcan

<div class="portlet-body">
  <table class="table table-bordered" id="suratJalanTable" style='background-color: white'>
  </table>
</div>

<div id="AddModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width:900px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">List Order</h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered" id="customerOrderTable" style='background-color: white'>
        </table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


@endsection

@section('footer_script')
<script>
  $(document).ready(function() {

    function refresh() {
      $('#suratJalanTable').bootstrapTable('refresh', {
        url: route('transaction.suratjalan.data'),
      });
    }

    $('#suratJalanTable').bootstrapTable({
      classes: 'table table-hover table-no-bordered',
      url: route('transaction.suratjalan.data'),
      pagination: true,
      sidePagination: 'server',
      search: true,
      columns: [{
        title: 'NO',
        formatter: function(value, row, index) {
          return index + 1;
        }
      }, {
        field: 'kode_trip',
        title: 'KODE TRIP',
        sortable:true
      }, {
        field: 'no_suratjalan',
        title: 'SURAT JALAN',
        sortable:true
      }, {
        field: 'alamat_pengambilan',
        title: 'ALAMAT PENGAMBILAN',
        sortable:true
      }, {
        field: 'nama_consignee',
        title: 'PENERIMA',
        sortable:true
      }, {
        field: 'no_container',
        title: 'NO CONTAINER'
      }, {
        field: 'seal',
        title: 'SEAL'
      }, {
        field: 'keterangan_total',
        title: 'KETERANGAN TOTAL',
        sortable:true
      }, {
        title: 'AKSI',
        formatter: function(value, row, index) {
          var btnString = '';
          // if(row.status==0){
          @can('edit surat jalan')
          btnString += '<a href="' + route('transaction.suratjalan.edit', {
            id: row.id_suratjalan
          }) + '" data-toggle="tooltip" title="Edit Surat Jalan"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>';
          btnString += '<button type="button" class="btn btn-delete btn-xs btn-danger" title="Delete Surat Jalan"><i class="fa fa-times"></i></button></a>';
          @endcan

          btnString += '<a href="' + route('transaction.suratjalan.view', {
            id: row.id_suratjalan
          }) + '" data-toggle="tooltip" title="View Surat Jalan"><button type="button" class="btn  btn-primary btn-xs"><i class="fa fa-eye"></i></button></a>';
          // }else {
          //     btnString += '<a href="'+route('transaction.suratjalan.view',{id:row.id_suratjalan})+'" data-toggle="tooltip" title="View Surat Jalan"><button type="button" class="btn  btn-primary btn-xs"><i class="fa fa-eye"></i></button></a>';
          // }

          return btnString;
        },
        events: {
          'click .btn-delete': function(e, value, row, index) {
            if (confirm('Anda yakin akan menghapus surat jalan ini?')) {
              $.ajax({
                  url: route('transaction.suratjalan.delete', {
                    id: row.id_suratjalan
                  }),
                  data: {
                    _token: $('meta[name="csrf_token"]').attr('content')
                  },
                  method: 'post'
                })
                .then(function(response) {
                  if (response.status == 'error') {
                    alert(response.description);
                  } else if (response.status == 'success') {
                    alert(response.description);
                    refresh();
                  }
                });
            }
          }
        }
      }]
    });


    $('#customerOrderTable').bootstrapTable({
      classes: 'table table-hover table-no-bordered',
      url: route('transaction.suratjalan.dataorder'),
      pagination: true,
      search: true,
      columns: [{
        title: 'NO',
        formatter: function(value, row, index) {
          return index + 1;
        }
      }, {
        field: 'nama_customer',
        title: 'NAMA CUSTOMER',
        sortable:true
      }, {
        field: 'nama_kota_asal',
        title: 'KOTA ASAL',
        sortable:true
      }, {
        title: 'PELABUHAN ASAL',
        field: 'nama_pelabuhan_asal',
        formatter: function(value, row, index) {
          return row.nama_kotapelabuhan_asal + ' - ' + row.nama_pelabuhan_asal;
        },
        sortable:true
      }, {
        field: 'nama_kota_tujuan',
        title: 'KOTA TUJUAN'
      }, {
        title: 'PELABUHAN TUJUAN',
        field: 'nama_pelabuhan_tujuan',
        formatter: function(value, row, index) {
          return row.nama_kotapelabuhan_tujuan + ' - ' + row.nama_pelabuhan_tujuan;
        },
        sortable:true
      }, {
        title: 'TIPE ORDER',
        field: 'type_order',
        formatter: function(value, row, index) {
          if (row.type_order == "CONTAINER") {
            return row.type_order + '<br/>(' + row.freight_size + ')';
          } else {
            return row.type_order + '<br/>(' + row.lcl_size + ')';
          }
        },
        sortable:true
      }, {
        title: 'AKSI',
        formatter: function(value, row, index) {
          var btnString = '<a href="' + route('transaction.suratjalan.add', {
            id: row.id_order
          }) + '" data-toggle="tooltip" title="Add Surat Jalan"><button type="button" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i></button></a>'
          return btnString;
        }
      }]
    })

  });
</script>
@endsection