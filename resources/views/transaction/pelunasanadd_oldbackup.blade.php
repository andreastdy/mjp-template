@extends('layouts.default')

@section('content_header')
<h1>
@if($mode=='ADD')
    ADD PELUNASAN
@else
    EDIT PELUNASAN
@endif
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">Pelunasan</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@if($mode=='ADD')
<form action="{{ route('transaction.pelunasan.create',[$invoice->id_invoice,$type_invoice]) }}" method="post" class="form-horizontal">
@else
<form action="{{ route('transaction.pelunasan.update',[$pelunasan->id_pelunasan,$type_invoice]) }}" method="post" class="form-horizontal">
@endif
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="form-group">
    <label class="control-label col-xs-2" for="txtNoInvoice">No Kuitansi</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtNoInvoice" id="txtNoInvoice" type="text" value="{{$invoice->no_invoice}}" readonly>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtNoInvoice">Tanggal Kuitansi</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtNoInvoice" id="txtNoInvoice" type="text" value="{{date('d-m-Y', strtotime($invoice->tgl_invoice))}}" readonly>
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtKodeTrip">Kode Trip</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtKodeTrip" id="txtKodeTrip" type="text" value="{{$invoice->kode_trip}}" readonly>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">Nama Customer</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtKota" id="txtKota" type="text" value="{{$invoice->nama_customer}}" readonly>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtBuktiBayar">Bukti Bayar</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtBuktiBayar" id="txtBuktiBayar" type="text" value="{{$invoice->bukti_bayar}}">
    </div>
  </div>
<table class="table table-bordered" id="detailCustomerProposalTable" style='background-color: white'>
      @if($type_invoice=='CONTAINER')
        <tr>
            <th>NO</th>
            <th>NO CONTAINER</th>
            <th>NO BAPB</th>
            <th>NO SURAT JALAN</th>
            <th>TOTAL KOLLI</th>
            <th style="text-align: right">BIAYA</th>
        </tr>
        @php
        $i = 0;
        @endphp
        @foreach($datainvoice as $item)
        @php $i+=1; @endphp
          <tr>
              <td>{{$i}}</td>
              <td>{{$item->no_container}}</td>  
              <td>{{$item->no_bapb}}</td>
              <td>{{$item->no_suratjalan}}</td>
              <td>{{$item->keterangan_total}}</td>
              <td style="text-align: right">{{$item->nominal_total}}</td>
          </tr>
          @if($item->biaya_tambahan_1!=null)
          <tr>
            <td colspan=3></td>
            <td colspan=2> {{$item->biaya_tambahan_1}}</td>
            <td style="text-align: right">{{$item->nominal_biaya_tambahan_1}}</td>
          </tr>
          @endif
          @if($item->biaya_tambahan_2!=null)
          <tr>
            <td colspan=3></td>
            <td colspan=2> {{$item->biaya_tambahan_2}}</td>
            <td style="text-align: right">{{$item->nominal_biaya_tambahan_2}}</td>
          </tr>
          @endif
          @if($item->biaya_tambahan_3!=null)
          <tr>
            <td colspan=3></td>
            <td colspan=2>{{$item->biaya_tambahan_3}}</td>
            <td style="text-align: right">{{$item->nominal_biaya_tambahan_3}}</td>
          </tr>
          @endif
        @endforeach
        <tr>
          <td style="text-align: right" colspan="5">PPN</td>
         <td style="text-align: right">{{$invoice->tax}}</td>
        </tr>
        <tr>
          <td style="text-align: right" colspan="5">TOTAL</td>
         <td style="text-align: right">{{$invoice->total}}</td>
        </tr>
      @else
        <tr>
            <th>NO</th>
            <th>NO SURAT JALAN</th>
            <th>NO BAPB</th>
            <th>TOTAL</th>
            <th style="text-align: right">BIAYA</th>
        </tr>
        @php
        $i = 0;
        @endphp
        @foreach($datainvoice as $item)
        @php $i+=1; @endphp
          <tr>
              <td>{{$i}}</td>
              <td>{{$item->no_suratjalan}}</td>
              <td>{{$item->no_bapb}}</td>
              <td>{{$item->keterangan_total}}</td>
              <td style="text-align: right">{{$item->nominal_total}}</td>
          </tr>
          @if($item->biaya_tambahan_1!=null)
          <tr>
            <td colspan=2></td>
            <td colspan=2>{{$item->biaya_tambahan_1}}</td>
            <td style="text-align: right">{{$item->nominal_biaya_tambahan_1}}</td>
          </tr>
          @endif
          @if($item->biaya_tambahan_2!=null)
          <tr>
            <td colspan=2></td>
            <td colspan=2>{{$item->biaya_tambahan_2}}</td>
            <td style="text-align: right">{{$item->nominal_biaya_tambahan_2}}</td>
          </tr>
          @endif
          @if($item->biaya_tambahan_3!=null)
          <tr>
          <td colspan=2></td>
            <td colspan=2>{{$item->biaya_tambahan_3}}</td>
            <td style="text-align: right">{{$item->nominal_biaya_tambahan_3}}</td>
          </tr>
          @endif
        @endforeach
        <tr>
          <td style="text-align: right" colspan="4">PPN</td>
         <td style="text-align: right">{{$invoice->tax}}</td>
        </tr>
        <tr>
          <td style="text-align: right" colspan="4">TOTAL</td>
         <td style="text-align: right">{{$invoice->total}}</td>
        </tr>
      @endif
    </table>

<div id="detailRute">

<div class="form-group">
    <label class="control-label col-xs-3" for="txtTglPelunasan">Tanggal Pelunasan</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtTglPelunasan" id="txtTglPelunasan" type="text" @if($mode=='ADD' ) value="" @else value="{{date('d-m-Y', strtotime($pelunasan->tgl_pelunasan))}}" @endif >
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-3" for="txtPPH">PPH 23</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input class="form-control" name="txtPPH" id="txtPPH" type="text" @if($mode=='ADD' )  value="0" @else value="{{$pelunasan->pph23}}" @endif style="text-align: right" onkeypress="return isNumberKey(event)">
      </div>
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-3" for="txtPotonganAdmin">Potongan Administrasi Bank</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input class="form-control" name="txtPotonganAdmin" id="txtPotonganAdmin" type="text" @if($mode=='ADD' )  value="0" @else value="{{$pelunasan->potongan_admin}}" @endif style="text-align: right" onkeypress="return isNumberKey(event)">
      </div>
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-3" for="txtClaim">Claim</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input class="form-control" name="txtClaim" id="txtClaim" type="text" @if($mode=='ADD' )  value="0" @else value="{{$pelunasan->claim}}" @endif style="text-align: right" onkeypress="return isNumberKey(event)">
      </div>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" for="txtNominalDibayar">Nominal Dibayar</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input class="form-control" name="txtNominalDibayar" id="txtNominalDibayar" type="text" @if($mode=='ADD' )  value="0" @else value="{{$pelunasan->nominal_dibayar}}" @endif style="text-align: right" onkeypress="return isNumberKey(event)">
      </div>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" for="txtNominalDibayar">Nominal Belum Dibayar</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        @php
        $belumdibayar = $invoice->total - $invoice->nominal_dibayar;
        @endphp
        <input class="form-control" name="txtNominalDibayar" id="txtNominalDibayar" type="text"  value="{{$belumdibayar}}"  style="text-align: right" readonly>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" for="txtKeterangan">Keterangan</label> 
    <div class="col-xs-3">
       <textarea class="form-control" rows="3" cols="40" name="txtKeterangan" id="txtKeterangan">@if($mode!='ADD') {{$pelunasan->keterangan}} @endif</textarea>
    </div>
  </div>

<div class="form-group row">
    <div class="col-xs-offset-3 col-xs-6">
    @can('edit pelunasan')
      @if($mode=='ADD')
      <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
      @else
      <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Update</button>
      @endif
    @endcan
      <a href="{{route('transaction.pelunasan')}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
    </div>
  </div>
</form>

</div>

@endsection

@section('footer_script')
<script>
    

  $('#txtTglPelunasan').datetimepicker({
      minView: 2,
      format: 'dd-mm-yyyy',
      autoclose: true,
      todayBtn: true
    });
    

</script>
@endsection
