@extends('layouts.default')

@section('content_header')
<h1>
  PELUNASAN
</h1>
<ol class="breadcrumb">
  <li><a href="#"> Transaction</a></li>
  <li class="active">Pelunasan</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@can('edit pelunasan')
<a href="#" data-toggle="tooltip" title="Add Pelunasan"><button type='button' class='btn btn-danger' data-toggle="modal" data-target="#ListModal"><i class='fa fa-plus'></i> Buat Baru</button></a>
@endcan
<div class="portlet-body">
  <table class="table table-bordered" id="pelunasanTable" style='background-color: white'>

  </table>
</div>



<div id="ListModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width:900px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">List Customer</h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered" id="listCustomerTable" style='background-color: white'>


        </table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>




@endsection

@section('footer_script')
<script>
  $('#pelunasanTable').bootstrapTable({
    classes: 'table table-hover table-no-bordered',
    url: route('transaction.pelunasan.data'),
    pagination: true,
    search: true,
    columns: [{
        title: 'NO',
        formatter: function(value, row, index) {
          return index + 1;
        }
      }, {
        field: 'kode_pelunasan',
        title: 'KODE PELUNASAN',
      sortable: true
      }, {
        title: 'TANGGAL PELUNASAN',
        field: 'tgl_pelunasan',
        formatter: function(value, row, index) {
          return moment(value).format('DD-MM-YYYY');
        },
      sortable: true
      }, {
        field: 'bukti_bayar',
        title: 'BUKTI BAYAR',
      sortable: true
      },{
        field: 'nama_customer',
        title: 'NAMA CUSTOMER',
      sortable: true
      },{
    title: 'TOTAL BAYAR (IDR)',
    field: 'total_bayar',
    align:'right',
    formatter: function (value, row, index) {
        return addCommas(row.total_bayar.toString());
    },
    sortable: true
  }, {
        title: 'AKSI',
        formatter: function(value, row, index) {
          var btnString = '';
          @can('edit pelunasan')
            if(row.status==0){
              btnString += '<a href="' + route('transaction.pelunasan.edit', {
                id: row.id_m_pelunasan,idcustomer:row.id_customer
              }) + '" data-toggle="tooltip" title="Edit Pelunasan"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>';
            }
          @endcan
          return btnString;
        }
      }
    ]
  });

  $('#listCustomerTable').bootstrapTable({
    classes: 'table table-hover table-no-bordered',
    url: route('transaction.pelunasan.datacustomer'),
    pagination: true,
    search: true,
    columns: [{
      title: 'NO',
      formatter: function(value, row, index) {
        return index + 1;
      }
    }, {
    field: 'nama_pt',
    title: 'NAMA CUSTOMER',
    sortable: true
  }, {
      title: 'AKSI',
      formatter: function(value, row, index) {
        var btnString = '<a href="' + route('transaction.pelunasan.add', {idcustomer: row.id_customer}) + '" data-toggle="tooltip" title="Add Invoice"><button type="button" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i></button></a>'
        return btnString;
      }
    }]
  });
</script>
@endsection