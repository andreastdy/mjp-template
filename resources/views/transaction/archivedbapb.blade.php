@extends('layouts.default')

@section('content_header')
<h1>
    ARCHIVED BAPB
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">ARCHIVED BAPB</li>
</ol>
@endsection

@section('content')
<hr>
<input type="text" placeholder="search.." style="float: right; margin-top:1%"></input>

<div class="portlet-body">
    <table class="table table-bordered" id="customerOrderTable" style='background-color: white'>
        <tr>
            <th>NO</th>
            <th>NO BAPB</th>
            <th>NO ORDER</th>
            <th>NO CONTAINER</th>
             <th>NO SEAL</th>
            <th>NAMA PELAYARAN</th>
            <th>NAMA CUSTOMER</th>
            <th>KOTA PELABUHAN BONGKAR</th>
            <th >AKSI</th>
        </tr>
        <tr>
            <td>1</td>
            <td>290191BA</td>
            <td>ORD290191</td>
            <td>MRTU209109</td>
            <td>291892</td>
            <td>PT. TEST 1</td>
            <td>CUSTOMER A</td>
            <td>JAKARTA</td>
            <td>

                <a href="{{route('transaction.archivedbapb.detail')}}" data-toggle="tooltip" title="View Detail"><button type='button' class='btn btn-primary'><i class='fa fa-eye'></i></button></a>
            </td>
        </tr>
        <tr>
            <td>1</td>
            <td>398391BA</td>
            <td>ORD398391</td>
            <td>MRTU339109</td>
            <td>391892</td>
            <td>PT. TEST 1</td>
            <td>CUSTOMER B</td>
            <td>JAKARTA</td>
            <td>
                <a href="{{route('transaction.archivedbapb.detail')}}" data-toggle="tooltip" title="View Detail"><button type='button' class='btn btn-primary'><i class='fa fa-eye'></i></button></a>
            </td>
        </tr>
        

    </table>
</div>



@endsection

@section('footer_script')
<script>

    $('#pelayaranTable').bootstrapTable({});

    function editPelayaran() {
        // window.open(' {{route('master.pelayaran.edit')}} ', "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, width=1400, height=800");
        //openPopup({{route('master.pelayaran.edit')}}, 'Title', 1400, 800, 'scrollbars,resizable', true, false);
    }
</script>
@endsection
