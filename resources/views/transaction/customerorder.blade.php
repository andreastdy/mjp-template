@extends('layouts.default')

@section('content_header')
<h1>
  CUSTOMER ORDER
</h1>
<ol class="breadcrumb">
  <li><a href="#"> Transaction</a></li>
  <li class="active">Customer Order</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')

@can('edit order')
<button type='button' class='btn btn-danger' data-toggle="modal" data-target="#AddModal"><i class='fa fa-plus'></i> Buat Baru</button></a>
@endcan

<div class="portlet-body">
  <table class="table table-bordered" id="customerOrderTable" style='background-color: white'>



  </table>
</div>




<div id="AddModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width:1000px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">List Approved Proposal</h4>
      </div>
      <div class="modal-body">

        <table class="table table-bordered" id="listCustomerProposalTable" style='background-color: white'>



        </table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


@endsection

@section('footer_script')
<script>
  $(document).ready(function() {

    function refresh() {
      $('#customerOrderTable').bootstrapTable('refresh', {
        url: route('transaction.customerorder.data'),
      });
    }

    $('#customerOrderTable').bootstrapTable({
      classes: 'table table-hover table-no-bordered',
      url: route('transaction.customerorder.data'),
      pagination: true,
      sidePagination: 'server',
      search: true,
      columns: [{
        title: 'NO',
        formatter: function(value, row, index) {
          return index + 1;
        }
      }, {
        field: 'nama_customer',
        title: 'NAMA CUSTOMER',
        sortable:true
      }, {
        field: 'nama_kota_asal',
        title: 'KOTA ASAL',
        sortable:true
      }, {
        title: 'PELABUHAN ASAL',
        field: 'nama_kotapelabuhan_asal',
        formatter: function(value, row, index) {
          return row.nama_kotapelabuhan_asal + ' - ' + row.nama_pelabuhan_asal;
        },
        sortable:true
      }, {
        field: 'nama_kota_tujuan',
        title: 'KOTA TUJUAN',
        sortable:true
      }, {
        title: 'PELABUHAN TUJUAN',
        formatter: function(value, row, index) {
          return row.nama_kotapelabuhan_tujuan + ' - ' + row.nama_pelabuhan_tujuan;
        },
        sortable:true
      }, {
        title: 'TIPE ORDER',
        field: 'type_order',
        formatter: function(value, row, index) {
          if (row.type_order == "CONTAINER") {
            return row.type_order + '<br/>(' + row.freight_size + ')';
          } else {
            return row.type_order + '<br/>(' + row.lcl_size + ')';
          }
        },
        sortable:true
      }, {
        title: 'CREATED',
        field: 'created_at',
        formatter: function(value, row, index) {
          return row.created_by + '<br/>(' + moment(row.created_at).format('DD-MM-YYYY HH:mm:ss') + ')';
        },
        sortable:true
      }, {
        title: 'EXECUTED',
        field: 'executed_at',
        formatter: function(value, row, index) {
          if (row.executed_at == null) {
            return "-";
          } else {
            return row.executed_by + '<br/>(' + moment(row.executed_at).format('DD-MM-YYYY HH:mm:ss') + ')';
          }

        },
        sortable:true
      }, {
        title: 'AKSI',
        formatter: function(value, row, index) {
          var btnString = '';
          if (row.status == 0) {
            @can('edit order')
            btnString += '<a href="' + route('transaction.customerorder.edit', {
              id: row.id_order
            }) + '" data-toggle="tooltip" title="Edit Proposal"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>';
            btnString += '<button type="button" class="btn btn-delete btn-xs btn-danger" title="delete order"><i class="fa fa-times"></i></button></a>';
            @endcan

            @can('execute order')
            btnString += '<button type="button" class="btn btn-execute btn-xs btn-success" title="execute order"><i class="fa fa-check"></i></button></a>';
            @endcan

            btnString += '<a href="' + route('transaction.customerorder.detail', {
              id: row.id_order
            }) + '" data-toggle="tooltip" title="View Proposal"><button type="button" class="btn  btn-primary btn-xs"><i class="fa fa-eye"></i></button></a>';
          } else {
            btnString += '<a href="' + route('transaction.customerorder.detail', {
              id: row.id_order
            }) + '" data-toggle="tooltip" title="View Proposal"><button type="button" class="btn  btn-primary btn-xs"><i class="fa fa-eye"></i></button></a>';
          }

          return btnString;
        },
        events: {
          'click .btn-delete': function(e, value, row, index) {
            if (confirm('Anda yakin akan menghapus order ini?')) {
              $.ajax({
                  url: route('transaction.customerorder.delete', {
                    id: row.id_order
                  }),
                  data: {
                    _token: $('meta[name="csrf_token"]').attr('content')
                  },
                  method: 'post'
                })
                .then(function(response) {
                  if (response.status == 'error') {
                    alert(response.description);
                  } else if (response.status == 'success') {
                    alert(response.description);
                    refresh();
                  }
                });
            }
          },
          'click .btn-success': function(e, value, row, index) {
            if (confirm('Anda yakin akan mengeksekusi order ini?')) {
              $.ajax({
                  url: route('transaction.customerorder.execute', {
                    id: row.id_order
                  }),
                  data: {
                    _token: $('meta[name="csrf_token"]').attr('content')
                  },
                  method: 'post'
                })
                .then(function(response) {
                  if (response.status == 'error') {
                    alert(response.description);
                  } else if (response.status == 'success') {
                    alert(response.description);
                    refresh();
                  }
                });
            }
          }
        }
      }]
    });

    $('#listCustomerProposalTable').bootstrapTable({
      classes: 'table table-hover table-no-bordered',
      url: route('transaction.customerorder.dataproposal'),
      pagination: true,
      search: true,
      columns: [{
        title: 'NO',
        formatter: function(value, row, index) {
          return index + 1;
        }
      }, {
        field: 'no_proposal',
        title: 'NO PROPOSAL',
        sortable:true
      },  {
        field: 'keterangan',
        title: 'KETERANGAN',
        sortable:true
      }, {
        field: 'nama_customer',
        title: 'NAMA CUSTOMER',
        sortable:true
      }, {
        field: 'nama_kota_asal',
        title: 'KOTA ASAL',
        sortable:true
      },{
        field: 'nama_pelabuhan_asal',
        title: 'PELABUHAN ASAL',
        sortable:true
      }, {
        field: 'nama_kota_tujuan',
        title: 'KOTA TUJUAN',
        sortable:true
      },{
        field: 'nama_pelabuhan_tujuan',
        title: 'PELABUHAN TUJUAN',
        sortable:true
      },{
        field: 'nama_consignee',
        title: 'NAMA CONSIGNEE',
        sortable:true
      }, {
        title: 'TIPE PROPOSAL',
        field: 'type_proposal',
        formatter: function(value, row, index) {
          if (row.type_proposal == "CONTAINER") {
            return row.type_proposal + '<br/>(' + row.freight_size + ')';
          } else {
            return row.type_proposal + '<br/>(' + row.lcl_size + ')';
          }
        },
        sortable:true
      }, {
        title: 'PERIODE AWAL',
        field: 'valid_from',
        formatter: function(value, row, index) {
          return moment(value).format('DD-MM-YYYY');
        },
        sortable:true
      }, {
          title: 'PERIODE AKHIR',
          field: 'valid_to',
          formatter: function(value, row, index) {
            if(value==null||value==""){
              return "-";
            }else{
              return moment(value).format('DD-MM-YYYY');
            }
          },
          sortable: true
        },  {
        title: 'AKSI',
        formatter: function(value, row, index) {
          var btnString = '<a href="' + route('transaction.customerorder.add', {
            id: row.id_proposaldetail
          }) + '" data-toggle="tooltip" title="Add Order"><button type="button" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i></button></a>'
          return btnString;
        }
      }]
    })

  });
</script>
@endsection