@extends('layouts.default')

@section('content_header')
<h1>
    ADD SURAT JALAN
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">BAPB</li>
</ol>
@endsection

@section('content')
<hr>
<form class="form-horizontal">
<div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">No ORDER</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtKota" id="txtKota" type="text" value="ORD290191" readonly>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">No BAPB</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtKota" id="txtKota" type="text" value="290191BA" readonly>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">No Surat Jalan</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtKota" id="txtKota" type="text">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtAlamat">Alamat</label> 
    <div class="col-xs-4">
      <textarea class="form-control" rows="3" cols="40" name="txtAlamat" id="txtAlamat"></textarea>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">Penerima</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtKota" id="txtKota" type="text">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="chkPortToDoor">Pelabuhan</label> 
    <div class="col-xs-4">
      <label class="checkbox-inline">
        Tanjung Priok
      </label>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="chkPortToDoor">Nama Kapal</label> 
    <div class="col-xs-4">
      <label class="checkbox-inline">
        Armada Segara
      </label>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="chkPortToDoor">Tanggal Berangkat</label> 
    <div class="col-xs-4">
      <label class="checkbox-inline">
        07-12-2018 07:00:00
      </label>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="chkPortToDoor">Keterangan Total</label> 
    <div class="col-xs-4">
      <label class="checkbox-inline">
        1000
      </label>
    </div>
  </div>
  
    
    
  <div class="form-group row">
    <div class="col-xs-offset-2 col-xs-6">
      <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
      <button class="btn btn-danger" name="cancel">Cancel</button>
    </div>
  </div>
</form>

<div id="detailRute">
<a href="{{route('transaction.bapb.detailsuratjalanadd')}}" data-toggle="tooltip" title="Add Detail"><button type='button' class='btn btn-danger'><i class='fa fa-plus'></i> Buat Baru</button></a>
<input type="text" placeholder="search.." style="float: right; margin-top:1%"></input>

<div class="portlet-body">
    <table class="table table-bordered" id="detailCustomerProposalTable" style='background-color: white'>
        <tr>
            <th>NO</th>
            <th>PARTY</th>
            <th>NAMA BARANG</th>
            <th>P</th>
            <th>L</th>
            <th>T</th>
            <th>M3/TON</th>
        </tr>
        <tr>
            <td>1</td>
            <td>PARTY A</td>
            <td>BARANG A</td>
            <td>150</td>
            <td>100</td>
            <td>50</td>
            <td>12</td>
            <td>
                <a href="#" data-toggle="tooltip" title="Edit Detail" onclick="editPelayaran()"><button type='button' class='btn btn-primary'><i class='fa fa-edit'></i></button></a>
                <a href="#" data-toggle="tooltip" title="Delete Detail "><button type='button' class='btn btn-danger'><i class='fa fa-times'></i></button></a>
                 
            </td>
        </tr>
        <tr>
            <td>2</td>
            <td>PARTY B</td>
            <td>BARANG B</td>
            <td>100</td>
            <td>50</td>
            <td>10</td>
            <td>2</td>
            <td>
                <a href="#" data-toggle="tooltip" title="Edit Detail" onclick="editPelayaran()"><button type='button' class='btn btn-primary'><i class='fa fa-edit'></i></button></a>
              
                <a href="#" data-toggle="tooltip" title="Delete Detail "><button type='button' class='btn btn-danger'><i class='fa fa-times'></i></button></a>

                 
            </td>
        </tr>
        

    </table>
</div>

</div>



@endsection

@section('footer_script')
<script>
    
   // $("#detailRute").hide();


    
    function editPelayaran(){
       // window.open(' {{route('master.pelayaran.edit')}} ', "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, width=1400, height=800");
        //openPopup({{route('master.pelayaran.edit')}}, 'Title', 1400, 800, 'scrollbars,resizable', true, false);
    }
</script>
@endsection
