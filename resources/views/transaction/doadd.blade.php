@extends('layouts.default')

@section('content_header')
<h1>
@if($mode=='ADD')
    ADD DO
@elseif($mode=='EDIT')
    EDIT DO
@else
    VIEW DO
@endif
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">Surat Jalan</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
<form class="form-horizontal">
@if($mode=='ADD')
<input type="hidden" name="idsuratjalan" id="idsuratjalan" value="{{ $suratjalan->id_suratjalan }}">
  <input type="hidden" name="typesuratjalan" id="typesuratjalan" value="{{ $suratjalan->type_suratjalan }}">
  @if( $suratjalan->type_suratjalan=='CONTAINER')
  <input type="hidden" name="freightsize" id="freightsize"  value="{{ $suratjalan->freight_size }}">
  @else
  <input type="hidden" name="lclsize" id="lclsize" value="{{ $suratjalan->lcl_size }}">
  @endif
@else
<input type="hidden" name="idsuratjalan" id="idsuratjalan" value="{{ $deliveryorderdetail[0]->id_suratjalan }}">
  <input type="hidden" name="typesuratjalan" id="typesuratjalan" value="{{ $deliveryorderdetail[0]->type_suratjalan }}">
  @if( $deliveryorderdetail[0]->type_suratjalan=='CONTAINER')
  <input type="hidden" name="freightsize" id="freightsize" value="{{ $deliveryorderdetail[0]->freight_size }}">
  @else
  <input type="hidden" name="lclsize" id="lclsize" value="{{ $deliveryorderdetail[0]->lcl_size }}">
  @endif
@endif
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtNoSuratJalan">No Surat Jalan</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtNoSuratJalan" id="txtNoSuratJalan" type="text" @if($mode=='ADD') value="{{$suratjalan->no_suratjalan}}" @else value="{{$deliveryorderdetail[0]->no_suratjalan}}" @endif readonly>
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtNoDO">No DO</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtNoDO" id="txtNoDO" type="text" @if($mode!='ADD') value="{{$deliveryorderdetail[0]->no_do}}" @endif @if($mode=='VIEW' || $mode=='VIEWFROMVIEW')) readonly @endif>
      <span class="text-danger" id="errornodo"></span>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="cboConsignee">Penerima</label> 
    <div class="col-xs-3">
    <select class="select form-control cboConsignee" name="cboConsignee" id="cboConsignee" onchange="changeConsignee();" @if($mode=='VIEW' || $mode=='VIEWFROMVIEW') readonly disabled @endif>
    <option value="0">-- PILIH PENERIMA --</option>
      @foreach($consignee as $item)
      @if($mode == 'ADD') 
        <option value="{{ $item->id_consignee }}|{{ $item->alamat }}">{{ $item->nama }}</option>
      @else
        @if($deliveryorderdetail[0]->id_consignee==$item->id_consignee) 
        <option value="{{ $item->id_consignee }}|{{ $item->alamat }}" selected>{{ $item->nama }}</option>
        @else
        <option value="{{ $item->id_consignee }}|{{ $item->alamat }}">{{ $item->nama }}</option>
        @endif
      @endif
    @endforeach
        
      </select>
      <span class="text-danger" id="erroridconsignee"></span>
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtAlamat">Alamat</label> 
    <div class="col-xs-4">
      <textarea class="form-control" rows="3" cols="40" name="txtAlamat" id="txtAlamat"  readonly>@if($mode!='ADD') {{$deliveryorderdetail[0]->alamat}} @endif</textarea>
    </div>
  </div>
    

<div id="detailRute">
@if($mode!='VIEW' && $mode!='VIEWFROMVIEW')
<button type='button' class='btn btn-danger' data-toggle='modal' data-target='#AddModal'><i class='fa fa-plus'></i> ADD BARANG</button></a>
@endif

<div class="portlet-body">
    <table class="table table-bordered" id="ListAddDetailBarang" style='background-color: white'>
      <thead>
        <tr>
            @if($mode!='VIEW' && $mode!='VIEWFROMVIEW')
            <th style='width:4%;'></th>
            @endif
            <th style='width:12%;'>KODE BARANG</th>
            <th style='width:22%;'>NAMA BARANG</th>
            @if($mode=='ADD')
              @if($suratjalan->type_suratjalan=='LCL')
              <th style='width:7%;'>P</th>
              <th style='width:7%;'>L</th>
              <th style='width:7%;'>T</th>
                @if($suratjalan->lcl_size=='M3')
                <th style='width:7%;'>M3</th>
                @else
                <th style='width:7%;'>TON</th>
                @endif
              @endif
              @if($suratjalan->type_suratjalan=='CONTAINER')
              <th style='width:7%;'>KOLLI</th>
              @endif
            @else
              @if($deliveryorderdetail[0]->type_suratjalan=='LCL')
              <th style='width:7%;'>P</th>
              <th style='width:7%;'>L</th>
              <th style='width:7%;'>T</th>
                @if($deliveryorderdetail[0]->lcl_size=='M3')
                <th style='width:7%;'>M3</th>
                @else
                <th style='width:7%;'>TON</th>
                @endif
              @endif
              @if($deliveryorderdetail[0]->type_suratjalan=='CONTAINER')
              <th style='width:7%;'>KOLLI</th>
              @endif
            @endif
            <th style='width:10%;'>SATUAN</th>
            @if($mode!='VIEW' && $mode!='VIEWFROMVIEW')
            <th style='width:4%;'>AKSI</th>
            @endif
        </tr>
        </thead>
        <tbody>
        @if($mode!='ADD')
        @foreach($deliveryorderdetail as $item)
          <tr>
          @if($mode!='VIEW' && $mode!='VIEWFROMVIEW')
          <td><input class="form-control" name="txtKodeBarangHidden_{{$item->kode_barang}}" id="txtKodeBarangHidden_{{$item->kode_barang}}" type="hidden" value="{{$item->id_dodetail}}" ><input type="checkbox" id="chkBarang_{{$item->kode_barang}}" checked readonly disabled></td>
          @endif
          <td> <input class="form-control" name="txtKodeBarang_{{$item->kode_barang}}" id="txtKodeBarang_{{$item->kode_barang}}" type="text" value="{{$item->kode_barang}}" readonly></td>
          <td><input class="form-control" name="txtNamaBarang_{{$item->kode_barang}}" id="txtNamaBarang_{{$item->kode_barang}}" type="text" value="{{$item->nama_barang}}" readonly  ></td>
          @if($item->type_suratjalan=='LCL')
          <td><input class="form-control" name="txtPanjang_{{$item->kode_barang}}" id="txtPanjang_{{$item->kode_barang}}" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="{{$item->panjang}}" @if($mode=='VIEW' || $mode=='VIEWFROMVIEW')) readonly @endif></td>
          <td><input class="form-control" name="txtLebar_{{$item->kode_barang}}" id="txtLebar_{{$item->kode_barang}}" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="{{$item->lebar}}" @if($mode=='VIEW' || $mode=='VIEWFROMVIEW')) readonly @endif></td>
          <td><input class="form-control" name="txtTinggi_{{$item->kode_barang}}" id="txtTinggi_{{$item->kode_barang}}" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="{{$item->tinggi}}" @if($mode=='VIEW' || $mode=='VIEWFROMVIEW')) readonly @endif></td>
            @if($item->lcl_size=='M3')
            <td><input class="form-control" name="txtM3_{{$item->kode_barang}}" id="txtM3_{{$item->kode_barang}}" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="{{$item->m3}}" @if($mode=='VIEW' || $mode=='VIEWFROMVIEW')) readonly @endif></td>
            @else
            <td><input class="form-control" name="txtTon_{{$item->kode_barang}}" id="txtTon_{{$item->kode_barang}}" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="{{$item->ton}}" @if($mode=='VIEW' || $mode=='VIEWFROMVIEW')) readonly @endif></td>
            @endif
          @endif
          @if($item->type_suratjalan=='CONTAINER')
          <td><input class="form-control" name="txtKolli_{{$item->kode_barang}}" id="txtKolli_{{$item->kode_barang}}" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="{{$item->kolli}}" @if($mode=='VIEW' || $mode=='VIEWFROMVIEW') readonly @endif></td>
          @endif
          <td><input class="form-control" name="txtSatuan_{{$item->kode_barang}}" id="txtSatuan_{{$item->kode_barang}}" type="text" value="{{$item->satuan}}" readonly></td>
          @if($mode!='VIEW' && $mode!='VIEWFROMVIEW')
          <td><a href="#" data-toggle="tooltip" title="Delete Detail " onclick="deleteRowDatabase(this)"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></a></td>
          @endif          
          </tr>
        @endforeach
        @endif
        </tbody>
        
        
        

    </table>
    
    
</div>

</div>
    
<div class="form-group row">
    <div class="col-xs-offset-2 col-xs-6">
      @if($mode=='ADD')
      <button class="btn btn-primary" type="button" name="submit" onclick="adddo({{$suratjalan->id_suratjalan}});"><i class="fas fa-save"></i> Simpan</button>
      @elseif($mode=='EDIT')
      <button class="btn btn-primary" type="button" name="submit" onclick="editdo('{{$deliveryorderdetail[0]->id_do}}');"><i class="fas fa-save"></i> Update</button>
      @endif
      @if($mode=='ADD')
      <a href="{{route('transaction.suratjalan.edit',$suratjalan->id_suratjalan)}}"><button class="btn btn-danger" type="button" name="cancel">Cancel</button></a>
      @elseif($mode=='VIEWFROMVIEW')
      <a href="{{route('transaction.suratjalan.view',$deliveryorderdetail[0]->id_suratjalan)}}"><button class="btn btn-danger" type="button" name="cancel">Cancel</button></a>
      @else
      <a href="{{route('transaction.suratjalan.edit',$deliveryorderdetail[0]->id_suratjalan)}}"><button class="btn btn-danger" type="button" name="cancel">Cancel</button></a>
      @endif
    </div>
  </div>
</form>


<div id="AddModal" class="modal fade" role="dialog" >
    <div class="modal-dialog" style="width:600px;">

        <!-- Modal content-->
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">List Barang</h4>
            </div>
            <div class="modal-body">
                
            <table class="table table-bordered" id="dataBarangTable" style='background-color: white'>
        
        

    </table>
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

@endsection

@section('footer_script')
<script>
changeConsignee();
$(".cboConsignee").select2();

function changeConsignee(){
      var consignee = $('#cboConsignee').val().split("|");
      var alamat = consignee[1];
      $("#txtAlamat").val(alamat);
    }

function adddo(id_suratjalan){
  var consignee = $('#cboConsignee').val().split("|");
  @if($mode=='ADD')
    @if($suratjalan->type_suratjalan=='LCL')
    var objDO = {_token:$('meta[name="csrf_token"]').attr('content'),nodo:$("#txtNoDO").val(),idconsignee:consignee[0],alamat:$("#txtAlamat").val(),idsuratjalan:$("#idsuratjalan").val(),typesuratjalan:$("#typesuratjalan").val(),lclsize:$("#lclsize").val(),detail:[]};
    @else
    var objDO = {_token:$('meta[name="csrf_token"]').attr('content'),nodo:$("#txtNoDO").val(),idconsignee:consignee[0],alamat:$("#txtAlamat").val(),idsuratjalan:$("#idsuratjalan").val(),typesuratjalan:$("#typesuratjalan").val(),detail:[]};
    @endif
  @else
    @if($deliveryorderdetail[0]->type_suratjalan=='LCL')
    var objDO = {_token:$('meta[name="csrf_token"]').attr('content'),nodo:$("#txtNoDO").val(),idconsignee:consignee[0],alamat:$("#txtAlamat").val(),idsuratjalan:$("#idsuratjalan").val(),typesuratjalan:$("#typesuratjalan").val(),lclsize:$("#lclsize").val(),detail:[]};
    @else
    var objDO = {_token:$('meta[name="csrf_token"]').attr('content'),nodo:$("#txtNoDO").val(),idconsignee:consignee[0],alamat:$("#txtAlamat").val(),idsuratjalan:$("#idsuratjalan").val(),typesuratjalan:$("#typesuratjalan").val(),detail:[]};
    @endif
  @endif
  var detail = [];
  $("#ListAddDetailBarang > tbody > tr").each(function (i, el) {
    var $tds = $(this).find('td');
    // alert($tds.eq(2).find('input').val());
    @if($mode=='ADD')
      @if($suratjalan->type_suratjalan=='CONTAINER')
        var objDetail = {kodebarang: $tds.eq(1).find('input').val(), namabarang: $tds.eq(2).find('input').val(),
                      kolli: $tds.eq(3).find('input').val(),satuan:$tds.eq(4).find('input').val()};
      @else
          @if($suratjalan->lcl_size=='M3')
          var objDetail = {kodebarang: $tds.eq(1).find('input').val(), namabarang: $tds.eq(2).find('input').val(), panjang: $tds.eq(3).find('input').val(), 
                      lebar:$tds.eq(4).find('input').val(), tinggi:$tds.eq(5).find('input').val(), m3:$tds.eq(6).find('input').val(), 
                      satuan:$tds.eq(7).find('input').val()};
          @else
          var objDetail = {kodebarang: $tds.eq(1).find('input').val(), namabarang: $tds.eq(2).find('input').val(), panjang: $tds.eq(3).find('input').val(), 
                      lebar:$tds.eq(4).find('input').val(), tinggi:$tds.eq(5).find('input').val(), 
                      ton:$tds.eq(6).find('input').val(),satuan:$tds.eq(7).find('input').val()};
          @endif
      @endif
    @else
      @if($deliveryorderdetail[0]->type_suratjalan=='CONTAINER')
        var objDetail = {kodebarang: $tds.eq(1).find('input').val(), namabarang: $tds.eq(2).find('input').val(),
                      kolli: $tds.eq(3).find('input').val(),satuan:$tds.eq(4).find('input').val()};
      @else
          @if($deliveryorderdetail[0]->lcl_size=='M3')
          var objDetail = {kodebarang: $tds.eq(1).find('input').val(), namabarang: $tds.eq(2).find('input').val(), panjang: $tds.eq(3).find('input').val(), 
                      lebar:$tds.eq(4).find('input').val(), tinggi:$tds.eq(5).find('input').val(), m3:$tds.eq(6).find('input').val(), 
                      satuan:$tds.eq(7).find('input').val()};
          @else
          var objDetail = {kodebarang: $tds.eq(1).find('input').val(), namabarang: $tds.eq(2).find('input').val(), panjang: $tds.eq(3).find('input').val(), 
                      lebar:$tds.eq(4).find('input').val(), tinggi:$tds.eq(5).find('input').val(), 
                      ton:$tds.eq(6).find('input').val(),satuan:$tds.eq(7).find('input').val()};
          @endif
      @endif
    @endif
    detail.push(objDetail);
  });
  var count = detail.length;
  // alert(count);
  if(count!=0){
    objDO.detail = detail;
    $.ajax({
        url: route('transaction.suratjalan.createdo',{id:id_suratjalan}),
        data:objDO,
        method:'post'
      })
      .then(function (response){
        if (response.status == 'error') {
          alert(response.description);
        } else if( response.status == 'success') {
          alert(response.description);
          @if($mode=='ADD')
          window.location = "{{route('transaction.suratjalan.edit',$suratjalan->id_suratjalan)}}";
          @else
          window.location = "{{route('transaction.suratjalan.edit',$deliveryorderdetail[0]->id_suratjalan)}}";
          @endif
          // refresh();
        }
      })
      .catch(function (response) {
        console.log(response);
        if(response.status == '422') {
          let error = response.responseJSON.errors;
          $.each(error, function(index,item) {
            $('#error'+index).html(item.join(", "));
          });
          alert("Please check the form errors.");

        } else {
        alert(response.responseJSON.message);
        }
        
      }); 
  }else{
    alert("Detail barang tidak boleh kosong!");
  }

  // alert(objDO.alamat);
}

function editdo(id_do){
  var consignee = $('#cboConsignee').val().split("|");
  @if($mode=='ADD')
    @if($suratjalan->type_suratjalan=='LCL')
    var objDO = {_token:$('meta[name="csrf_token"]').attr('content'),nodo:$("#txtNoDO").val(),idconsignee:consignee[0],alamat:$("#txtAlamat").val(),idsuratjalan:$("#idsuratjalan").val(),typesuratjalan:$("#typesuratjalan").val(),lclsize:$("#lclsize").val(),detail:[]};
    @else
    var objDO = {_token:$('meta[name="csrf_token"]').attr('content'),nodo:$("#txtNoDO").val(),idconsignee:consignee[0],alamat:$("#txtAlamat").val(),idsuratjalan:$("#idsuratjalan").val(),typesuratjalan:$("#typesuratjalan").val(),detail:[]};
    @endif
  @else
    @if($deliveryorderdetail[0]->type_suratjalan=='LCL')
    var objDO = {_token:$('meta[name="csrf_token"]').attr('content'),nodo:$("#txtNoDO").val(),idconsignee:consignee[0],alamat:$("#txtAlamat").val(),idsuratjalan:$("#idsuratjalan").val(),typesuratjalan:$("#typesuratjalan").val(),lclsize:$("#lclsize").val(),detail:[]};
    @else
    var objDO = {_token:$('meta[name="csrf_token"]').attr('content'),nodo:$("#txtNoDO").val(),idconsignee:consignee[0],alamat:$("#txtAlamat").val(),idsuratjalan:$("#idsuratjalan").val(),typesuratjalan:$("#typesuratjalan").val(),detail:[]};
    @endif
  @endif
  var detail = [];
  $("#ListAddDetailBarang > tbody > tr").each(function (i, el) {
    var $tds = $(this).find('td');
    // alert($tds.eq(2).find('input').val());
    @if($mode=='ADD')
      @if($suratjalan->type_suratjalan=='CONTAINER')
        var objDetail = {id_dodetail:$tds.eq(0).find('input').val(),kodebarang: $tds.eq(1).find('input').val(), namabarang: $tds.eq(2).find('input').val(),
                      kolli: $tds.eq(3).find('input').val(),satuan:$tds.eq(4).find('input').val()};
      @else
          @if($suratjalan->lcl_size=='M3')
          var objDetail = {id_dodetail:$tds.eq(0).find('input').val(),kodebarang: $tds.eq(1).find('input').val(), namabarang: $tds.eq(2).find('input').val(), panjang: $tds.eq(3).find('input').val(), 
                      lebar:$tds.eq(4).find('input').val(), tinggi:$tds.eq(5).find('input').val(), m3:$tds.eq(6).find('input').val(), 
                      satuan:$tds.eq(7).find('input').val()};
          @else
          var objDetail = {id_dodetail:$tds.eq(0).find('input').val(),kodebarang: $tds.eq(1).find('input').val(), namabarang: $tds.eq(2).find('input').val(), panjang: $tds.eq(3).find('input').val(), 
                      lebar:$tds.eq(4).find('input').val(), tinggi:$tds.eq(5).find('input').val(), 
                      ton:$tds.eq(6).find('input').val(),satuan:$tds.eq(7).find('input').val()};
          @endif
      @endif
    @else
      @if($deliveryorderdetail[0]->type_suratjalan=='CONTAINER')
        var objDetail = {id_dodetail:$tds.eq(0).find('input').val(),kodebarang: $tds.eq(1).find('input').val(), namabarang: $tds.eq(2).find('input').val(),
                      kolli: $tds.eq(3).find('input').val(),satuan:$tds.eq(4).find('input').val()};
      @else
          @if($deliveryorderdetail[0]->lcl_size=='M3')
          var objDetail = {id_dodetail:$tds.eq(0).find('input').val(),kodebarang: $tds.eq(1).find('input').val(), namabarang: $tds.eq(2).find('input').val(), panjang: $tds.eq(3).find('input').val(), 
                      lebar:$tds.eq(4).find('input').val(), tinggi:$tds.eq(5).find('input').val(), m3:$tds.eq(6).find('input').val(), 
                      satuan:$tds.eq(7).find('input').val()};
          @else
          var objDetail = {id_dodetail:$tds.eq(0).find('input').val(),kodebarang: $tds.eq(1).find('input').val(), namabarang: $tds.eq(2).find('input').val(), panjang: $tds.eq(3).find('input').val(), 
                      lebar:$tds.eq(4).find('input').val(), tinggi:$tds.eq(5).find('input').val(), 
                      ton:$tds.eq(6).find('input').val(),satuan:$tds.eq(7).find('input').val()};
          @endif
      @endif        
    @endif
    detail.push(objDetail);
  });
  var count = detail.length;
  // alert(count);
  if(count!=0){
    objDO.detail = detail;
    $.ajax({
        url: route('transaction.suratjalan.updatedo',{id:id_do}),
        data:objDO,
        method:'post'
      })
      .then(function (response){
        if (response.status == 'error') {
            alert(response.description);
        } else if( response.status == 'success') {
          alert(response.description);
          @if($mode=='ADD')
          window.location = "{{route('transaction.suratjalan.edit',$suratjalan->id_suratjalan)}}";
          @else
          window.location = "{{route('transaction.suratjalan.edit',$deliveryorderdetail[0]->id_suratjalan)}}";
          @endif
          // refresh();
        }
      })
      .catch(function (response) {
        console.log(response);
        if(response.status == '422') {
          let error = response.responseJSON.errors;
          $.each(error, function(index,item) {
            $('#error'+index).html(item.join(", "));
          });
          alert("Please check the form errors.");

        } else {
        alert(response.responseJSON.message);
        }
        
      });
  }else{
    alert("Detail barang tidak boleh kosong!");
  }

  // alert(objDO.alamat);
}
    

    
 function deleteRow(row) {
  var i = row.parentNode.parentNode.rowIndex;
  document.getElementById('ListAddDetailBarang').deleteRow(i);
  //  $('body, html').animate({ scrollTop: $("#txtkodebarangmaster").offset().top }, 1000);
}

function deleteRowDatabase(row) {
  var i = row.parentNode.parentNode.rowIndex;
  var td = row.parentNode.parentNode;
  var iddetail = $(td).find('td').eq(0).find('input').val();
  if (confirm('Anda yakin akan menghapus barang ini?')) {
            $.ajax({
              url: route('transaction.suratjalan.deletedodetail',{id:iddetail}),
              data:{_token:$('meta[name="csrf_token"]').attr('content')},
              method:'post'
            })
            .then(function (response){
              if (response.status == 'error') {
                alert(response.description);
              } else if( response.status == 'success') {
                alert(response.description);
                document.getElementById('ListAddDetailBarang').deleteRow(i);
              }
            }); 
          }
  
  //  $('body, html').animate({ scrollTop: $("#txtkodebarangmaster").offset().top }, 1000);
}

$('#dataBarangTable').bootstrapTable({
  classes:'table table-hover table-no-bordered',
  url: route('transaction.suratjalan.listdatabarang'),
  pagination : true,
  search : true,
columns: [{
title: 'NO',
formatter: function (value, row, index) {
  return index+1;
}
}, {
    field: 'kode_barang',
    title: 'KODE BARANG',
    sortable: true
  }, {
    field: 'nama_barang',
    title: 'NAMA BARANG',
    sortable: true
  },{
    field: 'satuan',
    title: 'SATUAN',
    sortable: true
  },{
    title:'AKSI',
    formatter: function(value, row, index) {
      var btnString = "<button type='button' class='btn btn-sm  btn-success' data-dismiss='modal' onclick='insRow(\""+row.kode_barang+"\")'><i class='fa fa-plus'></i> Buat Baru</button></a>"
      return btnString;
    }
    }]
});

function insRow(kodebarang) {
  //console.log('hi');
  var x = document.getElementById('ListAddDetailBarang');
  // var kodebarang = document.getElementById('txtkodebarangmaster').value;
  var new_row = '';
  $.ajax({
        url: route('transaction.suratjalan.databarang',{id:kodebarang}),
        method:'get'
      })
      .then(function (response){
        if(response.total==0){
          alert("Kode Barang Tidak Ditemukan!");
        }else{
          var data = response.rows;
          $.each( data, function( i, val ) {
              // alert( "sector: " + val.sector + ", alamat: " + val.alamat );
              if(x.rows.length>1){


              new_row += '<tr>'
                      + '<td><input class="form-control" name="txtKodeBarangHidden_'+val.kode_barang+'" id="txtKodeBarangHidden_'+val.kode_barang+'" type="hidden" value="0" ><input type="checkbox" id="chkBarang_'+val.kode_barang+'" checked readonly disabled></td>'
                      + '<td> <input class="form-control" name="txtKodeBarang_'+val.kode_barang+'" id="txtKodeBarang_'+val.kode_barang+'" type="text" value="'+kodebarang+'" readonly></td>'
                      + '<td><input class="form-control" name="txtNamaBarang_'+val.kode_barang+'" id="txtNamaBarang_'+val.kode_barang+'" type="text" value="'+val.nama_barang+'" readonly></td>';
            @if($mode=='ADD')
                @if($suratjalan->type_suratjalan=='LCL')
                    new_row += '<td><input class="form-control" name="txtPanjang_'+val.kode_barang+'" id="txtPanjang_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0" ></td>'
                          + '<td><input class="form-control" name="txtLebar_'+val.kode_barang+'" id="txtLebar_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0"></td>'
                          + '<td><input class="form-control" name="txtTinggi_'+val.kode_barang+'" id="txtTinggi_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0"></td>';
                    @if($suratjalan->lcl_size=='M3')
                    new_row += '<td><input class="form-control" name="txtM3_'+val.kode_barang+'" id="txtM3_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0"></td>';
                    @else
                    new_row += '<td><input class="form-control" name="txtTon_'+val.kode_barang+'" id="txtTon_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0"></td>';
                    @endif
                @endif

                @if($suratjalan->type_suratjalan=='CONTAINER')
                new_row += '<td><input class="form-control" name="txtKolli_'+val.kode_barang+'" id="txtKolli_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0"></td>';
                @endif
             @else
                @if($deliveryorderdetail[0]->type_suratjalan=='LCL')
                    new_row += '<td><input class="form-control" name="txtPanjang_'+val.kode_barang+'" id="txtPanjang_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0" ></td>'
                          + '<td><input class="form-control" name="txtLebar_'+val.kode_barang+'" id="txtLebar_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0"></td>'
                          + '<td><input class="form-control" name="txtTinggi_'+val.kode_barang+'" id="txtTinggi_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0"></td>';
                    @if($deliveryorderdetail[0]->lcl_size=='M3')
                    new_row += '<td><input class="form-control" name="txtM3_'+val.kode_barang+'" id="txtM3_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0"></td>';
                    @else
                    new_row += '<td><input class="form-control" name="txtTon_'+val.kode_barang+'" id="txtTon_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0"></td>';
                    @endif
                @endif

                @if($deliveryorderdetail[0]->type_suratjalan=='CONTAINER')
                new_row += '<td><input class="form-control" name="txtKolli_'+val.kode_barang+'" id="txtKolli_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0"></td>';
                @endif
             @endif

             new_row += '<td><input class="form-control" name="txtSatuan_'+val.kode_barang+'" id="txtSatuan_'+val.kode_barang+'" type="text" value="'+val.satuan+'" readonly></td>'
                      + '<td>'
                      + '<a href="#" data-toggle="tooltip" title="Delete Detail " onclick="deleteRow(this)"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></a>'  
                      + '</td>'
                      + '</tr>';
              $("table#ListAddDetailBarang").append(new_row);
            }else{
              // alert('tabel kosong');

              // new_row += '<tr>'
              //         + '<td><input class="form-control" name="txtKodeBarangHidden_'+val.kode_barang+'" id="txtKodeBarangHidden_'+val.kode_barang+'" type="hidden" value="0" ><input type="checkbox" id="chkBarang_'+val.kode_barang+'" checked readonly disabled></td>'
              //         + '<td> <input class="form-control" name="txtKodeBarang_'+val.kode_barang+'" id="txtKodeBarang_'+val.kode_barang+'" type="text" value="'+kodebarang+'" readonly></td>'
              //         + '<td><input class="form-control" name="txtNamaBarang_'+val.kode_barang+'" id="txtNamaBarang_'+val.kode_barang+'" type="text" value="'+val.nama_barang+'" ></td>'
              //         + '<td><input class="form-control" name="txtPanjang_'+val.kode_barang+'" id="txtPanjang_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0" ></td>'
              //         + '<td><input class="form-control" name="txtLebar_'+val.kode_barang+'" id="txtLebar_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0"></td>'
              //         + '<td><input class="form-control" name="txtTinggi_'+val.kode_barang+'" id="txtTinggi_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0"></td>'
              //         + '<td><input class="form-control" name="txtM3_'+val.kode_barang+'" id="txtM3_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0"></td>'
              //         + '<td><input class="form-control" name="txtTon_'+val.kode_barang+'" id="txtTon_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0"></td>'
              //         + '<td><input class="form-control" name="txtKolli_'+val.kode_barang+'" id="txtKolli_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0"></td>'
              //         + '<td><input class="form-control" name="txtSatuan_'+val.kode_barang+'" id="txtSatuan_'+val.kode_barang+'" type="text" value="'+val.satuan+'" readonly></td>'
              //         + '<td>'
              //         + '<a href="#" data-toggle="tooltip" title="Delete Detail " onclick="deleteRow(this)"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></a>'  
              //         + '</td>'
              //         + '</tr>';
              new_row += '<tr>'
                      + '<td><input class="form-control" name="txtKodeBarangHidden_'+val.kode_barang+'" id="txtKodeBarangHidden_'+val.kode_barang+'" type="hidden" value="0" ><input type="checkbox" id="chkBarang_'+val.kode_barang+'" checked readonly disabled></td>'
                      + '<td> <input class="form-control" name="txtKodeBarang_'+val.kode_barang+'" id="txtKodeBarang_'+val.kode_barang+'" type="text" value="'+kodebarang+'" readonly></td>'
                      + '<td><input class="form-control" name="txtNamaBarang_'+val.kode_barang+'" id="txtNamaBarang_'+val.kode_barang+'" type="text" value="'+val.nama_barang+'" readonly ></td>';
            @if($mode=='ADD')
                @if($suratjalan->type_suratjalan=='LCL')
                    new_row += '<td><input class="form-control" name="txtPanjang_'+val.kode_barang+'" id="txtPanjang_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0" ></td>'
                          + '<td><input class="form-control" name="txtLebar_'+val.kode_barang+'" id="txtLebar_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0"></td>'
                          + '<td><input class="form-control" name="txtTinggi_'+val.kode_barang+'" id="txtTinggi_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0"></td>';
                    @if($suratjalan->lcl_size=='M3')
                    new_row += '<td><input class="form-control" name="txtM3_'+val.kode_barang+'" id="txtM3_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0"></td>';
                    @else
                    new_row += '<td><input class="form-control" name="txtTon_'+val.kode_barang+'" id="txtTon_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0"></td>';
                    @endif
                @endif

                @if($suratjalan->type_suratjalan=='CONTAINER')
                new_row += '<td><input class="form-control" name="txtKolli_'+val.kode_barang+'" id="txtKolli_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0"></td>';
                @endif
             @else
                @if($deliveryorderdetail[0]->type_suratjalan=='LCL')
                    new_row += '<td><input class="form-control" name="txtPanjang_'+val.kode_barang+'" id="txtPanjang_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0" ></td>'
                          + '<td><input class="form-control" name="txtLebar_'+val.kode_barang+'" id="txtLebar_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0"></td>'
                          + '<td><input class="form-control" name="txtTinggi_'+val.kode_barang+'" id="txtTinggi_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0"></td>';
                    @if($deliveryorderdetail[0]->lcl_size=='M3')
                    new_row += '<td><input class="form-control" name="txtM3_'+val.kode_barang+'" id="txtM3_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0"></td>';
                    @else
                    new_row += '<td><input class="form-control" name="txtTon_'+val.kode_barang+'" id="txtTon_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0"></td>';
                    @endif
                @endif

                @if($deliveryorderdetail[0]->type_suratjalan=='CONTAINER')
                new_row += '<td><input class="form-control" name="txtKolli_'+val.kode_barang+'" id="txtKolli_'+val.kode_barang+'" type="text" style="text-align:right" onkeypress="return isNumberKey(event)" value="0"></td>';
                @endif
             @endif

             new_row += '<td><input class="form-control" name="txtSatuan_'+val.kode_barang+'" id="txtSatuan_'+val.kode_barang+'" type="text" value="'+val.satuan+'" readonly></td>'
                      + '<td>'
                      + '<a href="#" data-toggle="tooltip" title="Delete Detail " onclick="deleteRow(this)"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></a>'  
                      + '</td>'
                      + '</tr>';
                      $("table#ListAddDetailBarang").append(new_row);
            }
              
          });
        }
              
   }); 
  
  document.getElementById('txtkodebarangmaster').value =  "";
  $('body, html').animate({ scrollTop: 1000 }, 400);

}

</script>
@endsection
