@extends('layouts.default')

@section('content_header')
<h1>
    ADD INVOICE ADDITIONAL
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">Invoice</li>
</ol>
@endsection

@section('content')
<hr>
<form class="form-horizontal">
<div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">No Kuitansi</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtKota" id="txtKota" type="text" value="KWI290191" readonly>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">No Packing List</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtKota" id="txtKota" type="text" value="PL290191" readonly>
    </div>
  </div>
<div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">Nama Biaya</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtKota" id="txtKota" type="text">
    </div>
  </div>
  
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">Biaya</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtKota" id="txtKota" type="text"  value="0">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">Total</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtKota" id="txtKota" type="text" value="0" readonly>
    </div>
  </div>
<div class="form-group row">
    <div class="col-xs-offset-2 col-xs-6">
      <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
      <button class="btn btn-danger" name="cancel">Cancel</button>
    </div>
  </div>
</form>

</div>



@endsection

@section('footer_script')
<script>
    
   // $("#detailRute").hide();


    

</script>
@endsection
