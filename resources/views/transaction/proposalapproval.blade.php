@extends('layouts.default')

@section('content_header')
<h1>
    PROPOSAL APPROVAL
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">Proposal Approval</li>
</ol>
@endsection

@section('content')
<hr>
<div class="portlet-body">
    <table class="table table-bordered" id="customerProposalTable" style='background-color: white'>
       
    </table>
</div>




@endsection

@section('footer_script')
<script>

$(document).ready(function () {
  function refresh(){
        $('#customerProposalTable').bootstrapTable('refresh',{
                  url: route('transaction.proposalapproval.data'),
                });
      }

    $('#customerProposalTable').bootstrapTable({
        classes:'table table-hover table-no-bordered',
        url: route('transaction.proposalapproval.data'),
    pagination: true,
  search: true,
  columns: [{
    title: 'NO',
    formatter: function (value, row, index) {
        return index+1;
      }
  }, {
    field: 'no_proposal',
    title: 'NO PROPOSAL',
    sortable: true
  }, {
    field: 'nama_customer',
    title: 'NAMA CUSTOMER',
    sortable: true
  },{
    title: 'CREATED',
    field: 'created_by',
    formatter : function (value, row, index) {
        return row.created_by+'<br/>('+row.created_at+')';
      },
      sortable: true
  },{
    title: 'STATUS',
    field: 'status',
    formatter : function (value, row, index) {
        if(row.status=="1"){
            return '<font color=blue>PENDING</font>';
        }else if(row.status==2){
            return '<font color=black>COMPLETED</font>';
        }else if(row.status==3){
            return '<font color=green>APPROVED</font>';
        }else if(row.status==4){
            return '<font color=red>REJECTED</font>';
        }else if(row.status==5){
            return '<font color=red>ARCHIVED</font>';
        }
      },
      sortable: true
  },{
      title:'AKSI',
      formatter: function(value, row, index) {
        var btnString = '';
          btnString += '<a href="'+route('transaction.proposalapproval.view',{id:row.id_proposal})+'" data-toggle="tooltip" title="View Proposal"><button type="button" class="btn  btn-primary btn-xs"><i class="fa fa-eye"></i></button></a>'
          btnString += '<a href="'+route('transaction.proposalapproval.previewpdf',{id:row.id_proposal,mode:'approval'})+'" data-toggle="tooltip" title="Print Proposal"><button type="button" class="btn  btn-primary btn-xs"><i class="fa fa-print"></i></button></a>'
        return btnString;
      }
    }]
});



});

</script>
@endsection
