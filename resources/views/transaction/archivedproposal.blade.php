@extends('layouts.default')

@section('content_header')
<h1>
    ARCHIVED PROPOSAL
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">Archived Proposal</li>
</ol>
@endsection

@section('content')
<hr>

<input type="text" placeholder="search.." style="float: right; margin-top:1%"></input>

<div class="portlet-body">

    <table class="table table-bordered" id="archivedProposalTable" style='background-color: white'>
        <tr>
            <th style="width:5%">NO</th>
            <th style="width:10%">ID PENAWARAN</th>
            <th style="width:10%">CUSTOMER</th>
            <th style="width:10%">PERIODE AWAL</th>
            <th style="width:10%">PERIODE AKHIR</th>
            <th style="width:10%">STATUS</th>
            <th style="width:15%">CREATED</th>
            <th style="width:15%">APPROVED</th>
            <th style="width:5%">AKSI</th>
        </tr>
        <tr>
            <td>1</td>
            <td>PEN00001</td>
            <td>CUSTOMER A</td>
            <td>01-11-2018</td>
            <td>31-12-2018</td>
            <td><font color="green">ACTIVE</td>
            <td>Admin  <br/>(28-10-2018 08:00:05)</td>
            <td>UserApprove <br/>(29-10-2018 11:20:10)</td>
            <td>
                 <a href="{{route('transaction.archivedproposal.detail')}}" data-toggle="tooltip" title="View Detail"><button type='button' class='btn btn-primary'><i class='fa fa-eye'></i></button></a>
            </td>
        </tr>
        
        <tr>
            <td>2</td>
            <td>PEN00002</td>
            <td>CUSTOMER B</td>
            <td>01-10-2018</td>
            <td>31-11-2018</td>
            <td><font color="red">EXPIRED</td>
            <td>Admin <br/>(25-09-2018 08:11:12)</td>
            <td>UserApprove <br/>(25-09-2018 19:22:10)</td>
            <td>
                 <a href="{{route('transaction.archivedproposal.detail')}}" data-toggle="tooltip" title="View Detail"><button type='button' class='btn btn-primary'><i class='fa fa-eye'></i></button></a>
            </td>
        </tr>
        

    </table>
</div>




@endsection

@section('footer_script')
<script>

    $('#pelayaranTable').bootstrapTable({});

    function editPelayaran() {
        // window.open(' {{route('master.pelayaran.edit')}} ', "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, width=1400, height=800");
        //openPopup({{route('master.pelayaran.edit')}}, 'Title', 1400, 800, 'scrollbars,resizable', true, false);
    }
</script>
@endsection
