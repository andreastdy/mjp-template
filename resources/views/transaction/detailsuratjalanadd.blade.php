@extends('layouts.default')

@section('content_header')
<h1>
    ADD DETAIL DO
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">Surat Jalan</li>
</ol>
@endsection

@section('content')
<hr>
<form class="form-horizontal">
  <!-- <div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">No Surat Jalan</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtKota" id="txtKota" type="text" value="SJ1203" readonly>
    </div>
  </div> -->
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">No DO</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtKota" id="txtKota" type="text" value="DO102919" readonly>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">Nama Barang</label> 
    <div class="col-xs-4">
      <select class="select form-control" name="cboPortAsal" id="cboPortAsal">
        <option value="sby">INDOMIE</option>
        <option value="jkt" selected>SUSU UHT</option>
      </select>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">Panjang</label> 
    <div class="col-xs-2">
      <input class="form-control" name="txtKota" id="txtKota" type="text">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">Lebar</label> 
    <div class="col-xs-2">
      <input class="form-control" name="txtKota" id="txtKota" type="text">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">Tinggi</label> 
    <div class="col-xs-2">
      <input class="form-control" name="txtKota" id="txtKota" type="text">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">M3</label> 
    <div class="col-xs-2">
      <input class="form-control" name="txtKota" id="txtKota" type="text">
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">Ton</label> 
    <div class="col-xs-2">
      <input class="form-control" name="txtKota" id="txtKota" type="text">
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtKota">Party (jumlah)</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtKota" id="txtKota" type="text">
    </div>
  </div>
    
  <div class="form-group row">
    <div class="col-xs-offset-2 col-xs-6">
      <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
      <button class="btn btn-danger" name="cancel">Cancel</button>
    </div>
  </div>
</form>





@endsection

@section('footer_script')
<script>
    
   // $("#detailRute").hide();


    

</script>
@endsection
