@extends('layouts.default')

@section('content_header')
<h1>
@if ($mode == 'ADD')
    ADD EXTRA COST / CLAIM
@else
    EDIT EXTRA COST / CLAIM
@endif
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">Extra Cost/Claim</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@if($mode=='ADD')
<form action="{{ route('transaction.extracostclaim.create',$customer->id_customer) }}" method="post" class="form-horizontal">
@else
<form action="{{ route('transaction.extracostclaim.update',$extracostclaim->id_extracostclaim) }}" method="post" class="form-horizontal">
@endif
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="form-group">
    <label class="control-label col-xs-2" for="txtKodeBiaya">Kode Biaya</label> 
    <div class="col-xs-2">
      <input class="form-control" name="txtKodeBiaya" id="txtKodeBiaya" type="text" @if($mode=='EDIT') value="{{$extracostclaim->kode_biaya}}" @endif readonly>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtNamaCustomer">Nama Customer</label> 
    <div class="col-xs-2">
      <input class="form-control" name="txtNamaCustomer" id="txtNamaCustomer" type="text" @if($mode=='EDIT') value="{{$extracostclaim->nama_pt}}" @else value="{{$customer->nama_pt}}" @endif readonly>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="cboInvoice">No Invoice</label> 
    <div class="col-xs-4">
      <select class="select form-control cboInvoice" name="cboInvoice" id="cboInvoice" @if($mode=='EDIT' ) disabled readonly @endif>
      <option value="0">-- PILIH INVOICE --</option>
      @foreach($invoice as $item)
        @if($mode=='ADD')
        <option value="{{ $item->id_invoice }}|{{ $item->type_invoice}}|{{ $item->no_invoice }}" >{{ $item->no_invoice }}</option>
        @else
          @if($item->id_invoice==$extracostclaim->id_invoice)
          <option value="{{ $item->id_invoice }}|{{ $item->type_invoice}}|{{ $item->no_invoice }}" selected>{{ $item->no_invoice }}</option>
          @else
          <option value="{{ $item->id_invoice }}|{{ $item->type_invoice}}|{{ $item->no_invoice }}" >{{ $item->no_invoice }}</option>
          @endif
        @endif
      @endforeach
      </select>
      @if($errors->has('cboInvoice'))
      <span class="text-danger">{{$errors->first('cboInvoice')}}
      @endif
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtTglBiaya">Tanggal Biaya</label> 
    <div class="col-xs-2">
      <input required="required" class="form-control" name="txtTglBiaya" id="txtTglBiaya"  type="text" @if($mode=='EDIT' ) value="{{date('d-m-Y', strtotime($extracostclaim->tgl_biaya))}}"  @endif>
      @if($errors->has('txtTglBiaya'))
      <span class="text-danger">{{$errors->first('txtTglBiaya')}}
      @endif
    </div>
  </div>
 <div class="form-group">
    <label class="control-label col-xs-2" for="cboTipeBiaya">Tipe Biaya</label> 
    <div class="col-xs-4">
      <select class="select form-control cboTipeBiaya" name="cboTipeBiaya" id="cboTipeBiaya" @if($mode=='EDIT' ) disabled readonly @endif>
      <option value="0">-- PILIH TIPE BIAYA --</option>
        @if($mode=='ADD')
        <option value="EXTRACOST" >EXTRA COST</option>
        <option value="CLAIM" >CLAIM</option>
        @else
          @if($extracostclaim->tipe_biaya=="EXTRACOST")
            <option value="EXTRACOST" selected>EXTRA COST</option>
            <option value="CLAIM" >CLAIM</option>
          @else
          <option value="EXTRACOST" >EXTRA COST</option>
        <option value="CLAIM" selected>CLAIM</option>
          @endif
        @endif
      </select>
      @if($errors->has('cboTipeBiaya'))
      <span class="text-danger">{{$errors->first('cboTipeBiaya')}}
      @endif
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtNominalBiaya">Nominal</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input required="required" class="form-control input-element" name="txtNominalBiaya" id="txtNominalBiaya" onkeypress="return isNumberKey(event)" type="text"  style="text-align: right" @if($mode=='EDIT') value="{{$extracostclaim->nominal_biaya}}" @else value="0" @endif>
        @if($errors->has('txtNominalBiaya'))
      <span class="text-danger">{{$errors->first('txtNominalBiaya')}}
      @endif
      </div>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtDeskripsi">Deskripsi</label> 
    <div class="col-xs-4">
       <textarea required="required" class="form-control" rows="3" cols="40" name="txtDeskripsi" id="txtDeskripsi">@if($mode!='ADD') {{$extracostclaim->deskripsi}} @endif</textarea>
       @if($errors->has('txtDeskripsi'))
      <span class="text-danger">{{$errors->first('txtDeskripsi')}}
      @endif
    </div>
  </div>
  
  
    
  <div class="form-group row">
    <div class="col-xs-offset-2 col-xs-6">
      <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
      <a href="{{route('transaction.extracostclaim')}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button>
    </div>
  </div>
</form>



@endsection

@section('footer_script')
<script>
    $(".cboInvoice").select2({
    minimumInputLength: 4,
    placeholder: "-- PILIH INVOICE --"});

    var cleave = new Cleave('.input-element', {
    numeral: true,
    numeralThousandsGroupStyle: 'thousand',
    autoUnmask : true
});
   
    $('#txtTglBiaya').datetimepicker({
      minView: 2,
      format: 'dd-mm-yyyy',
      autoclose: true,
      todayBtn: true
    });

</script>
@endsection
