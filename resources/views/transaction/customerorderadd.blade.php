@extends('layouts.default')

@section('content_header')
<h1>
@if($mode=='ADD')
    ADD CUSTOMER ORDER
@elseif($mode=='EDIT')
    EDIT CUSTOMER ORDER
@else
    VIEW CUSTOMER ORDER
@endif
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">Customer Order</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@if($mode=='ADD')
<form action="{{ route('transaction.customerorder.create',$proposaldetail[0]->id_proposaldetail) }}" method="post" class="form-horizontal">
@else
<form action="{{ route('transaction.customerorder.update',$order->id_order) }}" method="post" class="form-horizontal">
@endif
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="form-group">
    <label for="rdoType" class="control-label col-xs-2">Type Order</label> 
    <div class="col-xs-6">
      <label class="radio-inline">
      @if($mode == 'ADD') 
        <input type="radio" name="rdoType" id="rdoType" value="CONTAINER" @if($proposaldetail[0]->type_proposal=='CONTAINER') checked @endif @if($mode=='VIEW') disabled readonly @endif >
      @else
      <input type="radio" name="rdoType" id="rdoType" value="CONTAINER" @if($order->type_order=='CONTAINER') checked @endif @if($mode=='VIEW') disabled readonly @endif >
      @endif
              Container
      </label>
      <label class="radio-inline">
      @if($mode== 'ADD') 
        <input type="radio" name="rdoType" id="rdoType" value="LCL" @if($proposaldetail[0]->type_proposal=='LCL') checked @endif  @if($mode=='VIEW') disabled readonly @endif  >
      @else
      <input type="radio" name="rdoType" id="rdoType" value="LCL" @if($order->type_order=='LCL') checked @endif  @if($mode=='VIEW') disabled readonly @endif  >
      @endif
              LCL
      </label>
      <!-- <label class="radio-inline">
        <input type="radio" name="rdoType" id="rdoType" value="Bongkaran">
              Bongkaran
      </label> -->
    </div>
  </div> 
  <div class="form-group">
    <label class="control-label col-xs-2" for="cboCustomer">Customer</label> 
    <div class="col-xs-4">
      <select class="select form-control cboCustomer" name="cboCustomer" id="cboCustomer" @if($mode=='VIEW') readonly disabled @endif>
      <option value="0" >-- PILIH CUSTOMER --</option>
      @foreach($customer as $item)
      @if($mode == 'ADD') 
        @if($proposaldetail[0]->id_customer==$item->id_customer) 
        <option value="{{ $item->id_customer }}" selected>{{ $item->nama_pt }}</option>
        @else
        <option value="{{ $item->id_customer }}" >{{ $item->nama_pt }}</option>
        @endif
      @else
      @if($order->id_customer==$item->id_customer) 
        <option value="{{ $item->id_customer }}" selected>{{ $item->nama_pt }}</option>
        @else
        <option value="{{ $item->id_customer }}" >{{ $item->nama_pt }}</option>
        @endif
      @endif
    @endforeach
      </select>
      @if($errors->has('cboCustomer'))
      <span class="text-danger">{{$errors->first('cboCustomer')}}
      @endif
    </div>
  </div>
    
  <div class="form-group">
    <label class="control-label col-xs-2" for="cboKotaAsal">Kota Asal</label> 
    <div class="col-xs-4">
      <select class="select form-control cboKotaAsal" name="cboKotaAsal" id="cboKotaAsal" @if($mode=='VIEW') disabled readonly @endif  >
      <option value="0">-- PILIH KOTA ASAL --</option>
      @foreach($city as $item)
      @if($mode == 'ADD') 
        @if($proposaldetail[0]->id_kota_asal==$item->id_kota) 
        <option value="{{ $item->id_kota }}" selected>{{ $item->nama_kota }}</option>
        @else
        <option value="{{ $item->id_kota }}" >{{ $item->nama_kota }}</option>
        @endif
      @else
      @if($order->id_kota_asal==$item->id_kota) 
        <option value="{{ $item->id_kota }}" selected>{{ $item->nama_kota }}</option>
        @else
        <option value="{{ $item->id_kota }}" >{{ $item->nama_kota }}</option>
        @endif
      @endif
    @endforeach
      </select>
      @if($errors->has('cboKotaAsal'))
      <span class="text-danger">{{$errors->first('cboKotaAsal')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="cboPortAsal">Pelabuhan Asal</label> 
    <div class="col-xs-4">
      <select class="select form-control cboPortAsal" name="cboPortAsal" id="cboPortAsal" @if($mode=='VIEW') disabled readonly @endif >
      <option value="0">-- PILIH PELABUHAN ASAL --</option>
      @foreach($pelabuhan as $item)
      @if($mode == 'ADD') 
        @if($proposaldetail[0]->id_pelabuhan_asal==$item->id_pelabuhan) 
        <option value="{{ $item->id_pelabuhan }}" selected>{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}</option>
        @else
        <option value="{{ $item->id_pelabuhan }}" >{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}</option>
        @endif
      @else
      @if($order->id_pelabuhan_asal==$item->id_pelabuhan) 
        <option value="{{ $item->id_pelabuhan }}" selected>{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}</option>
        @else
        <option value="{{ $item->id_pelabuhan }}" >{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}</option>
        @endif
      @endif
    @endforeach
      </select>
      @if($errors->has('cboPortAsal'))
      <span class="text-danger">{{$errors->first('cboPortAsal')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="cboKotaTujuan">Kota Tujuan</label> 
    <div class="col-xs-4">
      <select class="select form-control cboKotaTujuan" name="cboKotaTujuan" id="cboKotaTujuan" @if($mode=='VIEW') disabled readonly @endif >
      <option value="0">-- PILIH KOTA TUJUAN --</option>
      @foreach($city as $item)
      @if($mode == 'ADD') 
        @if($proposaldetail[0]->id_kota_tujuan==$item->id_kota) 
        <option value="{{ $item->id_kota }}" selected>{{ $item->nama_kota }}</option>
        @else
        <option value="{{ $item->id_kota }}" >{{ $item->nama_kota }}</option>
        @endif
      @else
      @if($order->id_kota_tujuan==$item->id_kota) 
        <option value="{{ $item->id_kota }}" selected>{{ $item->nama_kota }}</option>
        @else
        <option value="{{ $item->id_kota }}" >{{ $item->nama_kota }}</option>
        @endif
      @endif
    @endforeach
      </select>
      @if($errors->has('cboKotaTujuan'))
      <span class="text-danger">{{$errors->first('cboKotaTujuan')}}
      @endif
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="cboPortTujuan">Pelabuhan Tujuan</label> 
    <div class="col-xs-4">
      <select class="select form-control cboPortTujuan" name="cboPortTujuan" id="cboPortTujuan" @if($mode=='VIEW') disabled readonly @endif  >
      <option value="0">-- PILIH PELABUHAN TUJUAN --</option>
      @foreach($pelabuhan as $item)
      @if($mode == 'ADD') 
        @if($proposaldetail[0]->id_pelabuhan_tujuan==$item->id_pelabuhan) 
        <option value="{{ $item->id_pelabuhan }}" selected>{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}</option>
        @else
        <option value="{{ $item->id_pelabuhan }}" >{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}</option>
        @endif
      @else
      @if($order->id_pelabuhan_tujuan==$item->id_pelabuhan) 
        <option value="{{ $item->id_pelabuhan }}" selected>{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}</option>
        @else
        <option value="{{ $item->id_pelabuhan }}" >{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}</option>
        @endif
      @endif
    @endforeach
      </select>
      @if($errors->has('cboPortTujuan'))
      <span class="text-danger">{{$errors->first('cboPortTujuan')}}
      @endif
    </div>
  </div>
    
    
  
  @if($mode=='ADD')
    @if($proposaldetail[0]->type_proposal=='CONTAINER')
    <div class="form-group" id="freightsize">
    @else
    <div class="form-group" id="freightsize" hidden>
    @endif
  @else
    @if($order->type_order=='CONTAINER')
    <div class="form-group" id="freightsize">
    @else
    <div class="form-group" id="freightsize" hidden>
    @endif
  @endif
    <label class="control-label col-xs-2">Freight Size</label> 
    <div class="col-xs-6">
      <label class="radio-inline">
        <input type="radio" name="rdoFtsize" id="rdoFtSize1" value="20 FT" @if($mode=='ADD') @if($proposaldetail[0]->freight_size=='20 FT') checked @endif @else @if($order->freight_size=='20 FT') checked @endif @endif @if($mode=='VIEW') disabled readonly @endif  >
              20 FT
      </label>
      <label class="radio-inline">
        <input type="radio" name="rdoFtsize" id="rdoFtSize2" value="40 FT" @if($mode=='ADD') @if($proposaldetail[0]->freight_size=='40 FT') checked @endif @else @if($order->freight_size=='40 FT') checked @endif @endif @if($mode=='VIEW') disabled readonly @endif  >
              40 FT
      </label>
      <label class="radio-inline">
        <input type="radio" name="rdoFtsize" id="rdoFtSize2" value="40 HC" @if($mode=='ADD') @if($proposaldetail[0]->freight_size=='40 HC') checked @endif @else @if($order->freight_size=='40 HC') checked @endif @endif @if($mode=='VIEW') disabled readonly @endif  >
              40 HC
      </label>
    </div>
  </div>

@if($mode=='ADD')
    @if($proposaldetail[0]->type_proposal=='CONTAINER')
    <div class="form-group" id="lclsize" hidden>
    @else
    <div class="form-group" id="lclsize">
    @endif
@else
    @if($order->type_order=='CONTAINER')
    <div class="form-group" id="lclsize" hidden>
    @else
    <div class="form-group" id="lclsize">
    @endif
@endif
    <label for="rdolclsize" class="control-label col-xs-2">LCL Size</label> 
    <div class="col-xs-6">
      <label class="radio-inline">
        <input type="radio" name="rdolclsize" id="rdolclsize1" value="M3" @if($mode=='ADD') @if($proposaldetail[0]->lcl_size=='M3') checked @endif @else @if($order->lcl_size=='M3') checked @endif @endif @if($mode=='VIEW') disabled readonly @endif  >
              M3
      </label>
      <label class="radio-inline">
        <input type="radio" name="rdolclsize" id="rdolclsize2" value="TON" @if($mode=='ADD') @if($proposaldetail[0]->lcl_size=='TON') checked @endif @else @if($order->lcl_size=='TON') checked @endif @endif @if($mode=='VIEW') disabled readonly @endif  >
              TON
      </label>
    </div>
  </div> 

    <div class="form-group">
    <label class="control-label col-xs-2" for="cboJenisBarang">Tipe Muatan</label> 
    <div class="col-xs-4">
      <select class="select form-control" name="cboJenisBarang" id="cboJenisBarang" @if($mode=='VIEW') disabled readonly @endif  >
      <option value="0">-- PILIH TIPE MUATAN --</option>
      @if($mode == 'ADD') 
        <option value="GENERAL" @if($proposaldetail[0]->jenis_barang=='GENERAL') selected @endif>General Cargo</option>
        <option value="DG" @if($proposaldetail[0]->jenis_barang=='DG') selected @endif>Dangerous Goods</option>
      @else
        <option value="GENERAL" @if($order->jenis_barang=='GENERAL') selected @endif>General Cargo</option>
        <option value="DG" @if($order->jenis_barang=='DG') selected @endif>Dangerous Goods</option>
      @endif
      </select>
      @if($errors->has('cboJenisBarang'))
      <span class="text-danger">{{$errors->first('cboJenisBarang')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtAlamatAsal">Alamat Asal</label> 
    <div class="col-xs-4">
      <textarea required="required" class="form-control" rows="3" cols="40" name="txtAlamatAsal" id="txtAlamatAsal" @if($mode=='VIEW')  readonly @endif>@if($mode == 'ADD') {{$proposaldetail[0]->alamat_asal}} @else {{$order->alamat_asal}} @endif</textarea>
      @if($errors->has('txtAlamatAsal'))
      <span class="text-danger">{{$errors->first('txtAlamatAsal')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtAlamatTujuan">Alamat Tujuan</label> 
    <div class="col-xs-4">
      <textarea required="required" class="form-control" rows="3" cols="40" name="txtAlamatTujuan" id="txtAlamatTujuan" @if($mode=='VIEW')  readonly @endif>@if($mode == 'ADD') {{$proposaldetail[0]->alamat_tujuan}} @else {{$order->alamat_tujuan}}  @endif</textarea>
      @if($errors->has('txtAlamatTujuan'))
      <span class="text-danger">{{$errors->first('txtAlamatTujuan')}}
      @endif
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-xs-2" for="cboPelayaran">Pelayaran</label> 
    <div class="col-xs-4">
      <select class="select form-control cboPelayaran" name="cboPelayaran" id="cboPelayaran" @if($mode=='VIEW') readonly disabled @endif>
      <option value="0">-- PILIH PELAYARAN --</option>
      @foreach($pelayaran as $item)
      @if($mode == 'ADD') 
        @if($proposaldetail[0]->id_pelayaran==$item->id_pelayaran) 
        <option value="{{ $item->id_pelayaran }}" selected>{{ $item->name_full }} </option>
        @else
        <option value="{{ $item->id_pelayaran }}">{{ $item->name_full }} </option>
        @endif
      @else
      @if($order->id_pelayaran==$item->id_pelayaran) 
        <option value="{{ $item->id_pelayaran }}" selected>{{ $item->name_full }} </option>
        @else
        <option value="{{ $item->id_pelayaran }}">{{ $item->name_full }} </option>
        @endif
      @endif
    @endforeach
      </select>
      @if($errors->has('cboPelayaran'))
      <span class="text-danger">{{$errors->first('cboPelayaran')}}
      @endif
    </div>
  </div>

  @if($mode=='ADD')
  <div class="form-group">
    <label class="control-label col-xs-2" for="chkPortToDoor">Nama Pelayaran Proposal</label> 
    <div class="col-xs-4">
      <label class="checkbox-inline" id="valNamaPelayaran">
        {{$proposaldetail[0]->nama_pelayaran}}
      </label>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="chkPortToDoor">Harga Pelayaran Proposal</label> 
    <div class="col-xs-4">
      <label class="checkbox-inline" id="valNominalPelayaran">
      
      </label>
    </div>
  </div>
  @else
  <!-- <div class="form-group">
    <label class="control-label col-xs-2" for="chkPortToDoor">Nama Pelayaran Proposal</label> 
    <div class="col-xs-4">
      <label class="checkbox-inline" id="valNamaPelayaran">
        
      </label>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="chkPortToDoor">Harga Pelayaran Proposal</label> 
    <div class="col-xs-4">
      <label class="checkbox-inline" id="valNominalPelayaran">
        
      </label>
      <input type="hidden" id="txtHargaPelayaranHidden" value=""/>
    </div>
  </div> -->
  @endif

  

    <div class="form-group">
    <label class="control-label col-xs-2" for="chkPortToDoor">Harga Pelayaran Saat Ini</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input required="required" class="form-control input-element" name="txtHargaPelayaran" id="txtHargaPelayaran" type="text" @if($mode=='ADD') value="{{$proposaldetail[0]->nominal_pelayaran}}" @else value="{{$order->nominal_pelayaran_order}}" @endif style="text-align: right"  onkeypress="return isNumberKey(event)"  @if($mode=='VIEW') readonly @endif>
        @if($errors->has('txtHargaPelayaran'))
      <span class="text-danger">{{$errors->first('txtHargaPelayaran')}}
      @endif
      </div>
    </div>
  </div>
  
  @if($mode=='ADD')
    @if($proposaldetail[0]->type_proposal=='LCL')
      @if($proposaldetail[0]->lcl_size=='M3')
      <div class="form-group" id="hargam3">
      @else
      <div class="form-group" id="hargam3" hidden>
      @endif
    @else
    <div class="form-group" id="hargam3" hidden>
    @endif
  @else
  @if($order->type_order=='LCL')
      @if($order->lcl_size=='M3')
      <div class="form-group" id="hargam3">
      @else
      <div class="form-group" id="hargam3" hidden>
      @endif
    @else
    <div class="form-group" id="hargam3" hidden>
    @endif
  @endif
    <label class="control-label col-xs-2" for="txtHargam3">Harga M3</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input class="form-control hargalcl input-element" name="txtHargam3" id="txtHargam3" type="text" @if($mode=='ADD') value="{{$proposaldetail[0]->nominal_m3}}" @else value="{{$order->nominal_m3}}" @endif style="text-align: right"  onkeypress="return isNumberKey(event)" @if($mode=='VIEW') readonly @endif>
      </div>
    </div>
    </div>

  @if($mode=='ADD')
    @if($proposaldetail[0]->type_proposal=='LCL')
      @if($proposaldetail[0]->lcl_size=='TON')
      <div class="form-group" id="hargaton">
      @else
      <div class="form-group" id="hargaton" hidden>
      @endif
    @else
      <div class="form-group" id="hargaton" hidden>
    @endif
  @else
    @if($order->type_order=='LCL')
      @if($order->lcl_size=='TON')
      <div class="form-group" id="hargaton">
      @else
      <div class="form-group" id="hargaton" hidden>
      @endif
    @else
      <div class="form-group" id="hargaton" hidden>
    @endif
  @endif
    <label class="control-label col-xs-2" for="txtHargaton">Harga Ton</label> 
      <div class="col-xs-4">
        <div class="input-group">
          <div class="input-group-addon">Rp.</div> 
          <input class="form-control hargalcl input-element" name="txtHargaton" id="txtHargaton" type="text" @if($mode=='ADD') value="{{$proposaldetail[0]->nominal_ton}}" @else value="{{$order->nominal_ton}}" @endif style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)" @if($mode=='VIEW') readonly @endif>
        </div>
      </div>
    </div>
    
@if($mode=='ADD')
    @if($proposaldetail[0]->type_proposal=='CONTAINER')
    <div id="listBiaya">
    @else
    <div id="listBiaya" hidden>
    @endif
@else
    @if($order->type_order=='CONTAINER')
    <div id="listBiaya">
    @else
    <div id="listBiaya" hidden>
    @endif
@endif
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtHargaOPTSurabaya">Harga OPT Surabaya</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input class="form-control hargacont input-element" name="txtHargaOptSby" id="txtHargaOptSby" type="text" @if($mode=='ADD') value="{{$proposaldetail[0]->nominal_optsurabaya}}" @else value="{{$order->nominal_optsurabaya}}" @endif style="text-align: right" onkeypress="return isNumberKey(event)" @if($mode=='VIEW') readonly @endif>
      </div>
    </div>
  </div>

    <div class="form-group">
    <label class="control-label col-xs-2" for="txtHargaTHCP">Harga THC P</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <td> <input class="form-control hargacont input-element" name="txtHargaTHCP" id="txtHargaTHCP" type="text" @if($mode=='ADD') value="{{$proposaldetail[0]->nominal_thcp}}" @else value="{{$order->nominal_thcp}}" @endif style="text-align: right"  onkeypress="return isNumberKey(event)" @if($mode=='VIEW') readonly @endif>
      </div>
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-xs-2" for="txtHargaTHCD">Harga THC D</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input class="form-control hargacont input-element" name="txtHargaTHCD" id="txtHargaTHCD" type="text" @if($mode=='ADD') value="{{$proposaldetail[0]->nominal_thcd}}" @else value="{{$order->nominal_thcd}}" @endif style="text-align: right"  onkeypress="return isNumberKey(event)" @if($mode=='VIEW') readonly @endif>
      </div>
    </div>
  </div>

  



</div>

@if($mode=='ADD')
    @if($proposaldetail[0]->type_proposal=='CONTAINER')
    <div class="form-group" id="jumlahorder">
    @else
    <div class="form-group" id="jumlahorder" hidden>
    @endif
@else
    @if($order->type_order=='CONTAINER')
    <div class="form-group" id="jumlahorder">
    @else
    <div class="form-group" id="jumlahorder" hidden>
    @endif
@endif
    <label class="control-label col-xs-2" for="txtJmlOrder">Jumlah Order</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <input class="form-control" name="txtJmlOrder" id="txtJmlOrder" @if($mode=='ADD') value="0" @else value="{{$order->jumlah_order}}" @endif type="text" onkeypress="return isNumberKey(event)" @if($mode=='VIEW') readonly @endif>
      </div>
    </div>
  </div> 
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtJadwal">Jadwal Pengambilan</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <input required="required" class="form-control" name="txtJadwal" id="txtJadwal" type="text" @if($mode!='ADD') value="{{Carbon\Carbon::parse($order->jadwal_pengambilan)->format('d-m-Y')}}" @endif @if($mode=='VIEW') readonly @endif>
        @if($errors->has('txtJadwal'))
      <span class="text-danger">{{$errors->first('txtJadwal')}}
      @endif
      </div>
    </div>
  </div> 
  
    
  <div class="form-group row">
    <div class="col-xs-offset-2 col-xs-4">
    @if($mode!='VIEW')
      @can('edit order')
      <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
      @endcan
    @endif
    <a href="{{route('transaction.customerorder')}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
    </div>
  </div>
</form>

@endsection

@section('footer_script')
<script>

$(".cboCustomer").select2();
$(".cboKotaAsal").select2();
$(".cboPortAsal").select2();
$(".cboKotaTujuan").select2();
$(".cboPortTujuan").select2();
$(".cboPelayaran").select2();

$('.input-element').toArray().forEach(function(field){
new Cleave(field, {
    numeral: true,
    numeralThousandsGroupStyle: 'thousand',
    autoUnmask : true
})
});

// @if($mode=='ADD')
// var totalpelayaran=$("#txtHargaPelayaran").val();
$("#valNominalPelayaran").html("Rp. " + addCommas($("#txtHargaPelayaran").val().toString()));
// @else
// var totalpelayaran=$("#txtHargaPelayaranHidden").val();
// $("#valNominalPelayaran").html("Rp. " + addCommas(totalpelayaran.toString()));
// @endif

$('#txtJadwal').datetimepicker({
    minView : 2, 
    format: 'dd-mm-yyyy',
    autoclose : true,
    todayBtn : true
});

    $("input[name='rdoType']").click(function() {
        var type = $(this).val();
        // alert(test);
       if(type=="LCL"){
           $("#freightsize").hide();
           $("#listBiaya").hide();
           $("#jumlahorder").hide();
           $("#lclsize").show();
           $("#hargam3").show();
           $(".hargacont").val("0");
           $("#txtJmlOrder").val("0");
           document.getElementById("rdolclsize1").checked=true;
       }else{
          $("#listBiaya").show();
           $("#freightsize").show();
           $("#jumlahorder").show();
           $("#lclsize").hide();
           $("#hargam3").hide();
           $("#hargaton").hide();
           $(".hargalcl").val("0");
           $("#txtJmlOrder").val("0");
           document.getElementById("rdoFtSize1").checked=true;
       }
        // $("#" + test).show();
    });
   
    $("input[name='rdolclsize']").click(function() {
        var typelcl = $(this).val();
        // alert(typelcl);
       if(typelcl=="TON"){
           $("#hargam3").hide();
           $("#txtHargam3").val("0");
           $("#hargaton").show();
       }else{
           $("#hargam3").show();
           $("#hargaton").hide();
           $("#txtHargaton").val("0");
       }
        // $("#" + test).show();
    });
    
</script>
@endsection
