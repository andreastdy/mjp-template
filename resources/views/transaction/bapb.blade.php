@extends('layouts.default')

@section('content_header')
<h1>
  BAPB
</h1>
<ol class="breadcrumb">
  <li><a href="#"> Transaction</a></li>
  <li class="active">BAPB</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@can('edit BAPB')
<a href="#" data-toggle="tooltip" title="Add BAPB from Order"><button type='button' class='btn btn-danger' data-toggle="modal" data-target="#AddModal"><i class='fa fa-plus'></i>&nbsp;From Order</button></a>
@endcan
@can('print BAPB')
<a href="#" data-toggle="tooltip" title="Print BAPB belum kembali"><button type='button' class='btn btn-danger' data-toggle="modal" data-target="#ListModal"><i class='fa fa-print'></i> BAPB Belum Kembali</button></a>
@endcan
<!--&nbsp; <a href="#" data-toggle="tooltip" title="Add BAPB Manual"><button type='button' class='btn btn-danger' data-toggle="modal" data-target="#ManualModal"><i class='fa fa-plus'></i>&nbsp;Manual</button></a>-->
<br />
<div class="portlet-body">
  <table class="table table-bordered" id="BAPBTable" style='background-color: white'>



  </table>
</div>

<div id="AddModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width:900px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">List Order</h4>
      </div>
      <div class="modal-body">

        <table class="table table-bordered" id="customerOrderTable" style='background-color: white'>

        </table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div id="Modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Tanggal BAPB</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <input type="hidden" name="idbapb" id="idbapb" value="">
          <div class="form-group">
            <label class="control-label col-xs-4" for="txtTglKirim">Tanggal Kirim</label>
            <div class="col-xs-4">
              <input type="text" class="form-control" name="txtTglKirim" id="txtTglKirim"></input>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-xs-4" for="txtTglKembali">Tanggal Kembali</label>
            <div class="col-xs-4">
              <input type="text" class="form-control" name="txtTglKembali" id="txtTglKembali"></input>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-xs-offset-4 col-xs-4">
              <button class="btn btn-primary" type="button" name="submit" data-dismiss='modal' onclick="updatedate()"><i class="fas fa-save"></i> Save</button>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="ListModal" class="modal fade" role="dialog" >
    <div class="modal-dialog" style="width:900px;">

        <!-- Modal content-->
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">List Dooring dengan BAPB belum kembali</h4>
            </div>
            <div class="modal-body">
            <table class="table table-bordered" id="ListDooringTable" style='background-color: white'>
        
        

    </table>
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


@endsection

@section('footer_script')
<script>
  function updatedate() {
    var idbapb = $("#idbapb").val();
    var obj = {
      _token: $('meta[name="csrf_token"]').attr('content'),
      tanggal_kirim: $("#txtTglKirim").val(),
      tanggal_kembali: $("#txtTglKembali").val()
    };
    $.ajax({
        url: route('transaction.bapb.updateDate', idbapb),
        data: obj,
        method: 'post'
      })
      .then(function(response) {
        if (response.status == 'error') {
          alert(response.description);
        } else if (response.status == 'success') {
          alert(response.description);
          // window.location = "{{route('transaction.bapb')}}";
          refresh();
        }
      });
  }

  function refresh() {
    $('#BAPBTable').bootstrapTable('refresh', {
      url: route('transaction.bapb.data'),
    });
  }

  $(document).ready(function() {
    $('#txtTglKirim').datetimepicker({
      format: 'dd-mm-yyyy hh:ii:ss',
      autoclose: true,
      todayBtn: true
    });

    $('#txtTglKembali').datetimepicker({
      format: 'dd-mm-yyyy hh:ii:ss',
      autoclose: true,
      todayBtn: true
    });



    $('#BAPBTable').bootstrapTable({
      classes: 'table table-hover table-no-bordered',
      url: route('transaction.bapb.data'),
      pagination: true,
      sidePagination: 'server',
      search: true,
      columns: [{
        title: 'NO',
        formatter: function(value, row, index) {
          return index + 1;
        }
      }, {
        field: 'no_bapb',
        title: 'NO BAPB',
        sortable: true
      }, {
        field: 'kode_trip',
        title: 'KODE TRIP',
        sortable: true
      }, {
        field: 'nama_dooring',
        title: 'DOORING',
        sortable: true
      }, {
        field: 'nama_consignee',
        title: 'PENERIMA',
        sortable: true
      }, {
        title: 'TANGGAL KIRIM',
        field: 'tanggal_kirim',
        formatter: function(value, row, index) {
          if (value == null) {
            return "-";
          } else {
            return moment(value).format('DD-MM-YYYY HH:mm:ss');
          }
          // return moment(value).format('DD-MM-YYYY HH:mm:ss');
        },
        sortable: true
      }, {
        title: 'TANGGAL KEMBALI',
        field: 'tanggal_kembali',
        formatter: function(value, row, index) {
          if (value == null) {
            return "-";
          } else {
            return moment(value).format('DD-MM-YYYY HH:mm:ss');
          }
          // return moment(value).format('DD-MM-YYYY HH:mm:ss');
        },
        sortable: true
      }, {
        title: 'AKSI',
        formatter: function(value, row, index) {
          var btnString = '';

          @can('edit BAPB')
          btnString += '<a href="' + route('transaction.bapb.edit', {
            id: row.id_bapb
          }) + '" data-toggle="tooltip" title="Edit BAPB"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>';
          @endcan

          @can('update tanggal BAPB')
          btnString += '<a href="#" data-toggle="tooltip" title="Update Tanggal BAPB "><button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#Modal"><i class="fa fa-calendar"></i></button></a>';
          @endcan

          return btnString;
        },
        events: {
          'click .btn-success': function(e, value, row, index) {
            if (row.tanggal_kirim != "null" || row.tanggal_kirim != null) {
              $('#txtTglKirim').val("");
            } else {
              $('#txtTglKirim').val(moment(row.tanggal_kirim).format('DD-MM-YYYY HH:mm:ss'));
            }
            if (row.tanggal_kembali == "null" || row.tanggal_kembali == null) {
              $("#txtTglKembali").val("");
            } else {
              $("#txtTglKembali").val(moment(row.tanggal_kembali).format('DD-MM-YYYY HH:mm:ss'));
            }
            $("#idbapb").val(row.id_bapb);
          }
        }
      }]
    });




    $('#customerOrderTable').bootstrapTable({
      classes: 'table table-hover table-no-bordered',
      url: route('transaction.suratjalan.dataorder'),
      pagination: true,
      search: true,
      columns: [{
        title: 'NO',
        formatter: function(value, row, index) {
          return index + 1;
        }
      }, {
        field: 'nama_customer',
        title: 'NAMA CUSTOMER',
        sortable: true
      }, {
        field: 'nama_kota_asal',
        title: 'KOTA ASAL',
        sortable: true
      }, {
        title: 'PELABUHAN ASAL',
        field: 'nama_pelabuhan_asal',
        formatter: function(value, row, index) {
          return row.nama_kotapelabuhan_asal + ' - ' + row.nama_pelabuhan_asal;
        },
        sortable: true
      }, {
        field: 'nama_kota_tujuan',
        title: 'KOTA TUJUAN',
        sortable: true
      }, {
        title: 'PELABUHAN TUJUAN',
        field: 'nama_pelabuhan_tujuan',
        formatter: function(value, row, index) {
          return row.nama_kotapelabuhan_tujuan + ' - ' + row.nama_pelabuhan_tujuan;
        },
        sortable: true
      }, {
        title: 'TIPE ORDER',
        field: 'type_order',
        formatter: function(value, row, index) {
          if (row.type_order == "CONTAINER") {
            return row.type_order + '<br/>(' + row.freight_size + ')';
          } else {
            return row.type_order + '<br/>(' + row.lcl_size + ')';
          }
        },
        sortable: true
      }, {
        title: 'AKSI',
        formatter: function(value, row, index) {
          var btnString = '<a href="' + route('transaction.bapb.add', {
            id: row.id_order
          }) + '" data-toggle="tooltip" title="Add BAPB"><button type="button" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i></button></a>'
          return btnString;
        }
      }]
    })


    $('#ListDooringTable').bootstrapTable({
  classes:'table table-hover table-no-bordered',
  url: route('transaction.bapb.datadooring'),
  pagination : true,
  search : true,
columns: [{
title: 'NO',
formatter: function (value, row, index) {
  return index+1;
}
}, {
    field: 'nama',
    title: 'NAMA DOORING',
    sortable: true
  },{
title:'AKSI',
formatter: function(value, row, index) {
  var btnString = '<a href="'+route('transaction.bapb.print',{iddooring:row.id_dooring})+'" data-toggle="tooltip" title="Print Report"><button type="button" class="btn btn-xs btn-primary"><i class="fa fa-print"></i></button></a>'
  return btnString;
}
}]
})

  });
</script>
@endsection