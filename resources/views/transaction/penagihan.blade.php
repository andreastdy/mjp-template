@extends('layouts.default')

@section('content_header')
<h1>
    PENAGIHAN
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">Penagihan</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@can('edit penagihan')
<a href="{{route('transaction.penagihan.add')}}" data-toggle="tooltip" title="Tambah Penagihan"><button type='button' class='btn btn-danger'><i class='fa fa-plus'></i> Buat Baru</button></a>
@endcan
<Br/>
<div class="portlet-body">

    <table class="table table-bordered" id="penagihanTable" style='background-color: white'>
        
        

    </table>
</div>






@endsection

@section('footer_script')
<script>

$('#penagihanTable').bootstrapTable({
        classes:'table table-hover table-no-bordered',
        url: route('transaction.penagihan.data'),
    pagination: true,
    sidePagination: 'server',
  search: true,
  columns: [{
    title: 'NO',
    formatter: function (value, row, index) {
        return index+1;
      }
  },  {
    field: 'kode_penagihan',
    title: 'KODE PENAGIHAN',
    sortable: true
  },{
    title: 'TANGGAL PENAGIHAN',
    field: 'tgl_penagihan',
    formatter : function (value, row, index) {
          return moment(value).format('DD-MM-YYYY');
      },
    sortable: true
  },{
    field: 'nama_customer',
    title: 'CUSTOMER',
    sortable: true
  },{
      title:'AKSI',
      formatter: function(value, row, index) {
        var btnString = '';
        @can('edit penagihan')
          btnString += '<a href="'+route('transaction.penagihan.edit',{id:row.id_penagihan})+'" data-toggle="tooltip" title="Edit BAPB"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>';
        @endcan
        return btnString;
      }
    }]
});

  
</script>
@endsection
