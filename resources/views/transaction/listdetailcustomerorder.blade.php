@extends('layouts.default')

@section('content_header')
<h1>
    LIST DETAIL CUSTOMER ORDER PEN00001
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">Customer Order</li>
</ol>
@endsection

@section('content')
<hr>

<input type="text" placeholder="search.." style="float: right; margin-top:1%">
<div class="portlet-body">
    <table class="table table-bordered" id="customerOrderTable" style='background-color: white'>
        <tr>
            <th>NO</th>
            <th>NAMA PELAYARAN</th>
            <th>KOTA ASAL</th>
            <th>KOTA PELABUHAN ASAL</th>
            <th>NAMA PELABUHAN ASAL</th>
            <th>KOTA TUJUAN</th>
            <th>KOTA PELABUHAN TUJUAN</th>
            <th>NAMA PELABUHAN TUJUAN</th>
            <th>TIPE PROPOSAL</th>
            <th>HARGA PELAYARAN</th>
            <th>HARGA THC</th>
            <th >AKSI</th>
        </tr>
        <tr>
            <td>1</td>
            <td>PT. TEST 1</td>
            <td>SURABAYA</td>
            <td>SURABAYA</td>
            <td>PORT JAMRUD</td>
            <td>JAKARTA</td>
            <td>JAKARTA</td>
            <td>TANJUNG PRIOK</td>
            <td>CONTAINER (20 FT)</td>
            <td>1.500.000</td>
            <td>1.000.000</td>
            <td>
                
                <a href="{{route('transaction.customerorder.add')}}" data-toggle="tooltip" title="Choose Penawaran "><button type='button' class='btn btn-primary'><i class='fa fa-edit'></i></button></a>
                <a href="#" data-toggle="tooltip" title="Delete Order "><button type='button' class='btn btn-danger'><i class='fa fa-times'></i></button></a>
                <a href="#" data-toggle="tooltip" title="Execute Order "><button type='button' class='btn btn-success'><i class='fa fa-check'></i></button></a>
                <a href="#" data-toggle="tooltip" title="View Detail"><button type='button' class='btn btn-primary' data-toggle="modal" data-target="#DetailModal"><i class='fa fa-eye'></i></button></a>
                
            </td>
        </tr>
        <tr>
            <td>2</td>
            <td>PT. TEST 1</td>
            <td>SURABAYA</td>
            <td>SURABAYA</td>
            <td>PORT JAMRUD</td>
            <td>JAKARTA</td>
            <td>JAKARTA</td>
            <td>TANJUNG PRIOK</td>
            <td>LCL</td>
            <td>1.500.000</td>
            <td>1.000.000</td>
            <td>
                <a href="{{route('transaction.customerorder.add')}}" data-toggle="tooltip" title="Choose Penawaran "><button type='button' class='btn btn-primary'><i class='fa fa-edit'></i></button></a>
                <a href="#" data-toggle="tooltip" title="Delete Order "><button type='button' class='btn btn-danger'><i class='fa fa-times'></i></button></a>
                <a href="#" data-toggle="tooltip" title="Execute Order "><button type='button' class='btn btn-success'><i class='fa fa-check'></i></button></a>
                <a href="#" data-toggle="tooltip" title="View Detail"><button type='button' class='btn btn-primary' data-toggle="modal" data-target="#DetailModal"><i class='fa fa-eye'></i></button></a>
                
                
            </td>
        </tr>
        

    </table>
</div>

<div id="DetailModal" class="modal fade" role="dialog" >
    <div class="modal-dialog" >

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Detail Order </h4>
            </div>
            <div class="modal-body">
            <label class="control-label">ORDER ID : ORD290191 </label>
            <br/>
            <label class="control-label">NAMA PELAYARAN : PT. TEST 1</label>
            <br/>   
            <label class="control-label">CUSTOMER : CUSTOMER A </label>
            <br/>
            <label class="control-label">KOTA ASAL : SURABAYA </label>
            <br/>
            <label class="control-label">KOTA TUJUAN : JAKARTA</label>
            <br/>
            <label class="control-label">TIPE PROPOSAL : CONTAINER </label>
            <br/>
            <label class="control-label">ALAMAT ASAL : JL. Pertama </label>
            <br/>
            <label class="control-label">ALAMAT TUJUAN : JL. Kedua </label>
            <br/>
            <label class="control-label">FREIGHT SIZE : 20 FT</label>
            <br/>
            <label class="control-label">PELAYARAN : PT. BERLAYAR</label>
            <br/>
            <label class="control-label">HARGA PELAYARAN : Rp. 1.500.000</label>
            <br/>
            <label class="control-label">HARGA OPT : Rp. 1.500.000</label>
            <br/>
            <label class="control-label">HARGA THC : Rp. 1.500.000</label>
            <br/>
            <label class="control-label">KRANI : - </label>
            <br/>
            <label class="control-label">SUPIR : -  </label>
            <br/>
            <label class="control-label">NO TRAILER : L9920WX</label>
            <br/>
            <label class="control-label">NO CONTAINER : MRTU298767</label>
            <br/>
            <label class="control-label">NO SEAL : 2891092</label>
            <br/>
            <label class="control-label">KODE TRIP : MXQKLE </label>
            <br/>
            <label class="control-label">JADWAL EKSEKUSI : 02-12-2018 09:00:00 </label>
            <br/>
            <label class="control-label">LIST CONTAINER :</label>
            <table class="table table-bordered" id="customerOrderTable" style='background-color: white'>
        <tr>
            <th>NO</th>
            <th>NO CONTAINER</th>
            <th>NO SEAL</th>
            <th>STATUS</th>
        </tr>
        <tr>
            <td>1</td>
            <td>MRTU2019202</td>
            <td>20192029</td>
            <td>KAPAL BERANGKAT</td>
        </tr>
        <tr>
            <td>2</td>
            <td>MRTU3119202</td>
            <td>31192029</td>
            <td>KAPAL BERANGKAT</td>
        </tr>
        

    </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<div id="AddModal" class="modal fade" role="dialog" >
    <div class="modal-dialog" style="width:900px;">

        <!-- Modal content-->
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">List Penawaran</h4>
            </div>
            <div class="modal-body">
            <table class="table table-bordered" id="customerOrderTable" style='background-color: white'>
        <tr>
            <th>NO</th>
            <th>NO PENAWARAN</th>
            <th>NAMA CUSTOMER</th>
            <th>KOTA ASAL</th>
            <th>KOTA TUJUAN</th>
            <th >AKSI</th>
        </tr>
        <tr>
            <td>1</td>
            <td>PEN00001</td>
            <td>CUSTOMER A</td>
            <td>SURABAYA</td>
            <td>JAKARTA</td>
            
            <td>
                <a href="{{route('transaction.customerorder.add')}}" data-toggle="tooltip" title="Choose Penawaran "><button type='button' class='btn btn-primary'><i class='fa fa-plus'></i> Buat Baru</button></a>
            </td>
        </tr>
        <tr>
            <td>2</td>
            <td>PEN00002</td>
            <td>CUSTOMER B</td>
            <td>SURABAYA</td>
            <td>JAKARTA</td>
            
            <td>
                <a href="{{route('transaction.customerorder.add')}}" data-toggle="tooltip" title="Choose Penawaran "><button type='button' class='btn btn-primary'><i class='fa fa-plus'></i> Buat Baru</button></a>
            </td>
        </tr>
        

    </table>
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


@endsection

@section('footer_script')
<script>

    $('#pelayaranTable').bootstrapTable({});

    function editPelayaran() {
        // window.open(' {{route('master.pelayaran.edit')}} ', "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, width=1400, height=800");
        //openPopup({{route('master.pelayaran.edit')}}, 'Title', 1400, 800, 'scrollbars,resizable', true, false);
    }
</script>
@endsection
