@extends('layouts.default')

@section('content_header')
<h1>
@if($mode=='ADD')
  ADD INVOICE LCL
@elseif($mode=='EDIT')
  EDIT INVOICE LCL 
@else
  VIEW INVOICE LCL
@endif
</h1>
<ol class="breadcrumb">
  <li><a href="#"> Transaction</a></li>
  <li class="active">Invoice LCL</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
<form class="form-horizontal">
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtNoInvoice">No Kuitansi</label>
    <div class="col-xs-3">
      <input class="form-control" name="txtNoInvoice" id="txtNoInvoice" type="text" @if($mode!='ADD') value="{{ $invoicelcl->no_invoice_lcl}}" @endif readonly>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtTglInvoice">Tanggal Kuitansi</label>
    <div class="col-xs-2">
      <input class="form-control" name="txtTglInvoice" id="txtTglInvoice" type="text" @if($mode=='ADD' ) value="{{date('d-m-Y', strtotime($trip->tanggal_berangkat))}}" @else value="{{date('d-m-Y', strtotime($invoicelcl->tgl_invoice_lcl))}}" @endif @if($mode=='VIEW') readonly @endif>
      <span class="text-danger" id="errortgl_invoice_lcl"></span>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtKodeTrip">Kode Trip</label>
    <div class="col-xs-3">
      <input class="form-control" name="txtKodeTrip" id="txtKodeTrip" type="text"  value="{{$trip->kode_trip}}"readonly>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtNamaCust">Nama Customer</label>
    <div class="col-xs-3">
      <input class="form-control" name="txtNamaCust" id="txtNamaCust" type="text" value="{{$customer->nama_pt}}"  readonly>
    </div>
  </div>
  <table class="table table-bordered" id="detailInvoiceLCLTable" style='background-color: white'>
    <tr>
      <th>NO</th>
      <th>NO SURAT JALAN</th>
      <th>NO BAPB</th>
      <th>NO PENAWARAN</th>
      <th style="text-align:right">TOTAL</th>
      <th style="text-align: right">BIAYA</th>
    </tr>
    @if($mode=='ADD')
      @php
      $i = 0;
      @endphp
      @foreach($datainvoicelcl as $item)
      @php $i+=1; @endphp
      <tr>
      <td><input type="checkbox" id="chkInvoice_{{$item->id_suratjalan}}" value="{{$item->id_suratjalan}}" onchange="changeHarga();"/>&nbsp;{{$i}}</td>
      <td>{{$item->no_suratjalan}}</td>
      <td>{{$item->no_bapb}}</td>
      <td>{{$item->no_proposal}}</td>
      <td style="text-align:right">{{$item->keterangan_total}}</td>
      <td> <input class="form-control input-element" name="txtNominalBiaya_{{$item->id_suratjalan}}" id="txtNominalBiaya_{{$item->id_suratjalan}}" type="text" value="{{$item->nominal_total}}" style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)"></td>
      </tr>
      <tr>
        <td></td>
        <td colspan="3" style="text-align: right">Biaya Tambahan</td>
        <td> <input class="form-control" name="txtBiayaTambahan1_{{$item->id_suratjalan}}" id="txtBiayaTambahan1_{{$item->id_suratjalan}}" type="text" placeholder="Keterangan biaya" onkeyup="changeHarga()"></td>
        <td> <input class="form-control input-element" name="txtNominalTambahan1_{{$item->id_suratjalan}}" id="txtNominalTambahan1_{{$item->id_suratjalan}}" type="text" value="0" style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)"></td>
      </tr>
      <tr>
        <td></td>
        <td colspan="3" style="text-align: right">Biaya Tambahan</td>
        <td> <input class="form-control" name="txtBiayaTambahan2_{{$item->id_suratjalan}}" id="txtBiayaTambahan2_{{$item->id_suratjalan}}" type="text" placeholder="Keterangan biaya" onkeyup="changeHarga()" ></td>
        <td> <input class="form-control input-element" name="txtNominalTambahan2_{{$item->id_suratjalan}}" id="txtNominalTambahan2_{{$item->id_suratjalan}}" type="text" value="0" style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)"></td>
      </tr>
      <tr>
        <td></td>
        <td colspan="3" style="text-align: right">Biaya Tambahan</td>
        <td> <input class="form-control" name="txtBiayaTambahan3_{{$item->id_suratjalan}}" id="txtBiayaTambahan3_{{$item->id_suratjalan}}" type="text" placeholder="Keterangan biaya" onkeyup="changeHarga()" ></td>
        <td> <input class="form-control input-element" name="txtNominalTambahan3_{{$item->id_suratjalan}}" id="txtNominalTambahan3_{{$item->id_suratjalan}}" type="text" value="0" style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)"></td>
      </tr>
      @endforeach
    @elseif($mode=='EDIT')
    @php
      $i = 0;
      @endphp
      @foreach($datainvoicelcl as $item)
      @php $i+=1; @endphp

      @if($item->id_invoice_lcl!=null && $item->deleted_at==null)
      <tr>
      <td><a href="#" onclick="removeCont({{$item->id_invoicedetail_lcl}});"><i class="fa fa-times"  style="color:red"></i></a>&nbsp;&nbsp;<input type="checkbox" id="chkInvoice_{{$item->id_suratjalan}}" value="{{$item->id_suratjalan}}" checked onchange="changeHarga();" hidden/>&nbsp;{{$i}}</td>
      <td>{{$item->no_suratjalan}}<input type="hidden" name="idinvoicedetaillcl_{{$item->id_suratjalan}}" id="idinvoicedetaillcl_{{$item->id_suratjalan}}" value="{{ $item->id_invoicedetail_lcl }}"></td>
      <td>{{$item->no_bapb}}</td>
      <td>{{$item->no_proposal}}</td>
      <td style="text-align:right">{{$item->keterangan_total}}</td>
      <td> <input class="form-control input-element" name="txtNominalBiaya_{{$item->id_suratjalan}}" id="txtNominalBiaya_{{$item->id_suratjalan}}" type="text" value="{{$item->biaya}}" style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)"></td>
      </tr>
      <tr>
        <td></td>
        <td colspan="3" style="text-align: right">Biaya Tambahan</td>
        @if($item->biaya_tambahan_1!=null)
        <td> <input class="form-control" name="txtBiayaTambahan1_{{$item->id_suratjalan}}" id="txtBiayaTambahan1_{{$item->id_suratjalan}}" type="text" placeholder="Keterangan biaya" value="{{$item->biaya_tambahan_1}}" onkeyup="changeHarga()"></td>
        <td> <input class="form-control input-element" name="txtNominalTambahan1_{{$item->id_suratjalan}}" id="txtNominalTambahan1_{{$item->id_suratjalan}}" type="text" value="{{$item->nominal_biaya_tambahan_1}}" style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)"></td>
        @else
        <td> <input class="form-control" name="txtBiayaTambahan1_{{$item->id_suratjalan}}" id="txtBiayaTambahan1_{{$item->id_suratjalan}}" type="text" placeholder="Keterangan biaya" onkeyup="changeHarga()"></td>
        <td> <input class="form-control input-element" name="txtNominalTambahan1_{{$item->id_suratjalan}}" id="txtNominalTambahan1_{{$item->id_suratjalan}}" type="text" value="0" style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)"></td>
        @endif
      </tr>
      <tr>
        <td></td>
        <td colspan="3" style="text-align: right">Biaya Tambahan</td>
        @if($item->biaya_tambahan_2!=null)
        <td> <input class="form-control" name="txtBiayaTambahan2_{{$item->id_suratjalan}}" id="txtBiayaTambahan2_{{$item->id_suratjalan}}" type="text" placeholder="Keterangan biaya" value="{{$item->biaya_tambahan_2}}" onkeyup="changeHarga()" ></td>
        <td> <input class="form-control input-element" name="txtNominalTambahan2_{{$item->id_suratjalan}}" id="txtNominalTambahan2_{{$item->id_suratjalan}}" type="text" value="{{$item->nominal_biaya_tambahan_2}}" style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)"></td>
        @else
        <td> <input class="form-control" name="txtBiayaTambahan2_{{$item->id_suratjalan}}" id="txtBiayaTambahan2_{{$item->id_suratjalan}}" type="text" placeholder="Keterangan biaya" onkeyup="changeHarga()" ></td>
        <td> <input class="form-control input-element" name="txtNominalTambahan2_{{$item->id_suratjalan}}" id="txtNominalTambahan2_{{$item->id_suratjalan}}" type="text" value="0" style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)"></td>
        @endif
      </tr>
      <tr>
        <td></td>
        <td colspan="3" style="text-align: right">Biaya Tambahan</td>
        @if($item->biaya_tambahan_3!=null)
        <td> <input class="form-control" name="txtBiayaTambahan3_{{$item->id_suratjalan}}" id="txtBiayaTambahan3_{{$item->id_suratjalan}}" type="text" placeholder="Keterangan biaya" value="{{$item->biaya_tambahan_3}}" onkeyup="changeHarga()" ></td>
        <td> <input class="form-control input-element" name="txtNominalTambahan3_{{$item->id_suratjalan}}" id="txtNominalTambahan3_{{$item->id_suratjalan}}" type="text" value="{{$item->nominal_biaya_tambahan_3}}" style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)"></td>
        @else
        <td> <input class="form-control" name="txtBiayaTambahan3_{{$item->id_suratjalan}}" id="txtBiayaTambahan3_{{$item->id_suratjalan}}" type="text" placeholder="Keterangan biaya" onkeyup="changeHarga()" ></td>
        <td> <input class="form-control input-element" name="txtNominalTambahan3_{{$item->id_suratjalan}}" id="txtNominalTambahan3_{{$item->id_suratjalan}}" type="text" value="0" style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)"></td>
        @endif
      </tr>
      @else
      <tr>
      <!-- <td><input type="checkbox" id="chkInvoice_{{$item->id_suratjalan}}" value="{{$item->id_suratjalan}}" onchange="changeHarga();" style="transform: scale(1.5); "/>&nbsp;{{$i}}</td> -->
      <td><input type="checkbox" id="chkInvoice_{{$item->id_suratjalan}}" value="{{$item->id_suratjalan}}" onchange="changeHarga();"/>&nbsp;{{$i}}</td>
      <td>{{$item->no_suratjalan}}</td>
      <td>{{$item->no_bapb}}</td>
      <td>{{$item->no_proposal}}</td>
      <td style="text-align:right">{{$item->keterangan_total}}</td>
      <td> <input class="form-control input-element" name="txtNominalBiaya_{{$item->id_suratjalan}}" id="txtNominalBiaya_{{$item->id_suratjalan}}" type="text" value="{{$item->nominal_total}}" style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)"></td>
      </tr>
      <tr>
        <td></td>
        <td colspan="3" style="text-align: right">Biaya Tambahan</td>
        <td> <input class="form-control" name="txtBiayaTambahan1_{{$item->id_suratjalan}}" id="txtBiayaTambahan1_{{$item->id_suratjalan}}" type="text" placeholder="Keterangan biaya" onkeyup="changeHarga()"></td>
        <td> <input class="form-control input-element" name="txtNominalTambahan1_{{$item->id_suratjalan}}" id="txtNominalTambahan1_{{$item->id_suratjalan}}" type="text" value="0" style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)"></td>
      </tr>
      <tr>
        <td></td>
        <td colspan="3" style="text-align: right">Biaya Tambahan</td>
        <td> <input class="form-control" name="txtBiayaTambahan2_{{$item->id_suratjalan}}" id="txtBiayaTambahan2_{{$item->id_suratjalan}}" type="text" placeholder="Keterangan biaya" onkeyup="changeHarga()" ></td>
        <td> <input class="form-control input-element" name="txtNominalTambahan2_{{$item->id_suratjalan}}" id="txtNominalTambahan2_{{$item->id_suratjalan}}" type="text" value="0" style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)"></td>
      </tr>
      <tr>
        <td></td>
        <td colspan="3" style="text-align: right">Biaya Tambahan</td>
        <td> <input class="form-control" name="txtBiayaTambahan3_{{$item->id_suratjalan}}" id="txtBiayaTambahan3_{{$item->id_suratjalan}}" type="text" placeholder="Keterangan biaya" onkeyup="changeHarga()" ></td>
        <td> <input class="form-control input-element" name="txtNominalTambahan3_{{$item->id_suratjalan}}" id="txtNominalTambahan3_{{$item->id_suratjalan}}" type="text" value="0" style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)"></td>
      </tr>
      @endif
      @endforeach
    @else
    @php
      $i = 0;
      @endphp
      @foreach($datainvoicelcl as $item)
      @php $i+=1; @endphp
      <tr>
      <td><input type="checkbox" id="chkInvoice_{{$item->id_suratjalan}}" value="{{$item->id_suratjalan}}" checked onchange="changeHarga();" hidden/>&nbsp;{{$i}}</td>
      <td>{{$item->no_suratjalan}}<input type="hidden" name="idinvoicedetaillcl_{{$item->id_suratjalan}}" id="idinvoicedetaillcl_{{$item->id_suratjalan}}" value="{{ $item->id_invoicedetail_lcl }}"></td>
      <td>{{$item->no_bapb}}</td>
      <td>{{$item->no_proposal}}</td>
      <td style="text-align:right">{{$item->keterangan_total}}</td>
      <td> <input class="form-control input-element" name="txtNominalBiaya_{{$item->id_suratjalan}}" id="txtNominalBiaya_{{$item->id_suratjalan}}" type="text" value="{{$item->biaya}}" style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)" readonly></td>
      </tr>
      <tr>
        <td></td>
        <td colspan="3" style="text-align: right">Biaya Tambahan</td>
        @if($item->biaya_tambahan_1!=null)
        <td> <input class="form-control" name="txtBiayaTambahan1_{{$item->id_suratjalan}}" id="txtBiayaTambahan1_{{$item->id_suratjalan}}" type="text" placeholder="Keterangan biaya" value="{{$item->biaya_tambahan_1}}" onkeyup="changeHarga()" readonly></td>
        <td> <input class="form-control input-element" name="txtNominalTambahan1_{{$item->id_suratjalan}}" id="txtNominalTambahan1_{{$item->id_suratjalan}}" type="text" value="{{$item->nominal_biaya_tambahan_1}}" style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)" readonly></td>
        @else
        <td> <input class="form-control" name="txtBiayaTambahan1_{{$item->id_suratjalan}}" id="txtBiayaTambahan1_{{$item->id_suratjalan}}" type="text" placeholder="Keterangan biaya" onkeyup="changeHarga()" readonly></td>
        <td> <input class="form-control input-element" name="txtNominalTambahan1_{{$item->id_suratjalan}}" id="txtNominalTambahan1_{{$item->id_suratjalan}}" type="text" value="0" style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)" readonly></td>
        @endif
      </tr>
      <tr>
        <td></td>
        <td colspan="3" style="text-align: right">Biaya Tambahan</td>
        @if($item->biaya_tambahan_2!=null)
        <td> <input class="form-control" name="txtBiayaTambahan2_{{$item->id_suratjalan}}" id="txtBiayaTambahan2_{{$item->id_suratjalan}}" type="text" placeholder="Keterangan biaya" value="{{$item->biaya_tambahan_2}}" onkeyup="changeHarga()" readonly></td>
        <td> <input class="form-control input-element" name="txtNominalTambahan2_{{$item->id_suratjalan}}" id="txtNominalTambahan2_{{$item->id_suratjalan}}" type="text" value="{{$item->nominal_biaya_tambahan_2}}" style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)" readonly></td>
        @else
        <td> <input class="form-control" name="txtBiayaTambahan2_{{$item->id_suratjalan}}" id="txtBiayaTambahan2_{{$item->id_suratjalan}}" type="text" placeholder="Keterangan biaya" onkeyup="changeHarga()" readonly></td>
        <td> <input class="form-control input-element" name="txtNominalTambahan2_{{$item->id_suratjalan}}" id="txtNominalTambahan2_{{$item->id_suratjalan}}" type="text" value="0" style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)" readonly></td>
        @endif
      </tr>
      <tr>
        <td></td>
        <td colspan="3" style="text-align: right">Biaya Tambahan</td>
        @if($item->biaya_tambahan_3!=null)
        <td> <input class="form-control" name="txtBiayaTambahan3_{{$item->id_suratjalan}}" id="txtBiayaTambahan3_{{$item->id_suratjalan}}" type="text" placeholder="Keterangan biaya" value="{{$item->biaya_tambahan_3}}" onkeyup="changeHarga()" readonly></td>
        <td> <input class="form-control input-element" name="txtNominalTambahan3_{{$item->id_suratjalan}}" id="txtNominalTambahan3_{{$item->id_suratjalan}}" type="text" value="{{$item->nominal_biaya_tambahan_3}}" style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)" readonly></td>
        @else
        <td> <input class="form-control" name="txtBiayaTambahan3_{{$item->id_suratjalan}}" id="txtBiayaTambahan3_{{$item->id_suratjalan}}" type="text" placeholder="Keterangan biaya" onkeyup="changeHarga()" readonly></td>
        <td> <input class="form-control input-element" name="txtNominalTambahan3_{{$item->id_suratjalan}}" id="txtNominalTambahan3_{{$item->id_suratjalan}}" type="text" value="0" style="text-align: right" onkeyup="changeHarga()" onkeypress="return isNumberKey(event)" readonly></td>
        @endif
      </tr>
      @endforeach
    @endif
    <tr>
      <td style="text-align: right" colspan="5">GROSS TOTAL</td>
      <td><input class="form-control input-element" name="txtGrossTotal" id="txtGrossTotal" type="text" value="0" style="text-align: right" readonly></td>
    </tr>
    <tr>
      <td style="text-align: right" colspan="5">PPN</td>
      <td><input type="hidden" name="ppn" id="ppn" value="{{$ppn->ppn}}"><input class="form-control input-element" name="txtPpn" id="txtPpn" type="text" value="0" style="text-align: right" readonly></td>
    </tr>
    <tr>
      <td style="text-align: right" colspan="5">NET TOTAL</td>
      <td><input class="form-control input-element" name="txtNetTotal" id="txtNetTotal" type="text" value="0" style="text-align: right" readonly></td>
    </tr>

  </table>



  <div id="detailRute">

  <div class="form-group">
    <label for="rdoTax" class="control-label col-xs-3">Include Tax</label> 
    <div class="col-xs-4">
      <label class="radio-inline">
      <input type="radio" name="rdoTax" id="rdoTax1" value="YES" @if($mode!='ADD') @if($invoicelcl->include_tax==1) checked @endif @else checked @endif @if($mode=='VIEW') disabled readonly @endif>
              YA
      </label>
      <label class="radio-inline">
        <input type="radio" name="rdoTax" id="rdoTax2" value="NO" @if($mode!='ADD') @if($invoicelcl->include_tax==0) checked @endif @endif @if($mode=='VIEW') disabled readonly @endif>
              TIDAK
      </label>
    </div>
  </div> 

    <div class="form-group">
      <label class="control-label col-xs-3" for="txtKota">Total Nota</label>
      <div class="col-xs-4">
        <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input class="form-control input-element" name="txtTotalBiaya" id="txtTotalBiaya" onkeypress="return isNumberKey(event)" type="text"  style="text-align: right" value="0" readonly >
      </div>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-xs-3" for="txtCatatan">Catatan<br/><span class="opsional"><sup>(opsional)</sup></span></label>
      <div class="col-xs-4">
        <textarea class="form-control" rows="3" cols="40" name="txtCatatan" id="txtCatatan" @if($mode=='VIEW') readonly @endif>@if($mode!='ADD') @if($invoicelcl->catatan!=null) {{$invoicelcl->catatan}} @endif @endif</textarea>
      </div>
    </div>

    <div class="form-group row">
      <div class="col-xs-offset-3 col-xs-4">
        @if($mode=='ADD')
          @can('add invoice lcl')
          <button class="btn btn-primary" type="button" name="submit" onclick="save({{$customer->id_customer}},{{$trip->id_trip}})"><i class="fas fa-save"></i> Simpan</button>
          @endcan
        @elseif($mode=='EDIT')
          @can('edit invoice lcl')
          <button class="btn btn-primary" type="button" name="submit" onclick="update({{$invoicelcl->id_invoice_lcl}},{{$customer->id_customer}},{{$trip->id_trip}})"><i class="fas fa-save"></i> Update</button>
          @endcan
        @endif
        <a href="{{route('transaction.invoicelcl')}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
      </div>
    </div>
</form>

</div>



@endsection

@section('footer_script')
<script>
$('.input-element').toArray().forEach(function(field){
new Cleave(field, {
    numeral: true,
    numeralThousandsGroupStyle: 'thousand',
    autoUnmask : true
})
});

  changeHarga();

  $('#txtTglInvoice').datetimepicker({
      minView: 2,
      format: 'dd-mm-yyyy',
      autoclose: true,
      todayBtn: true
    });

  function removeCont($idinvoicedetaillcl){
    if(confirm("Apakah anda yakin akan mengeluarkan surat jalan ini dari invoice?")){
      $.ajax({
              url: route('transaction.invoicelcl.deletecont',{id:$idinvoicedetaillcl}),
              data:{_token:$('meta[name="csrf_token"]').attr('content')},
              method:'post'
            })
            .then(function (response){
              if (response.status == 'error') {
                alert(response.description);
              } else if( response.status == 'success') {
                alert(response.description);
                location.reload();
              }
            }); 
    }
  }

  function changeHarga(){
    var $total = 0;
    var $ppn = {{$ppn->ppn}};
    var $nominalppn = 0;
    var nettotal = 0;
    $("#detailInvoiceLCLTable > tbody > tr").each(function (i, el) {
      var $tds = $(this).find('td');
      
      if($tds.eq(0).find('input').prop('checked') == true){
        var $idsuratjalan = $tds.eq(0).find('input').val();
        var $biaya = $("#txtNominalBiaya_"+$idsuratjalan).val().replace(/,/g, '');
        if($("#txtBiayaTambahan1_"+$idsuratjalan).val()==null || $("#txtBiayaTambahan1_"+$idsuratjalan).val()==""){
          var $biayatamb1 = 0;
        }else{
          var $biayatamb1 = $("#txtNominalTambahan1_"+$idsuratjalan).val().replace(/,/g, '');
        }
        if($("#txtBiayaTambahan2_"+$idsuratjalan).val()==null || $("#txtBiayaTambahan2_"+$idsuratjalan).val()==""){
          var $biayatamb2 = 0;
        }else{
          var $biayatamb2 = $("#txtNominalTambahan2_"+$idsuratjalan).val().replace(/,/g, '');
        }
        if($("#txtBiayaTambahan3_"+$idsuratjalan).val()==null || $("#txtBiayaTambahan3_"+$idsuratjalan).val()==""){
          var $biayatamb3 = 0;
        }else{
          var $biayatamb3 = $("#txtNominalTambahan3_"+$idsuratjalan).val().replace(/,/g, '');
        }
        
        $total = parseInt($total) + parseInt($biaya) + parseInt($biayatamb1) + parseInt($biayatamb2) + parseInt($biayatamb3);
        // alert($total);
      }
    });
    $("#txtGrossTotal").val(addCommas( String($total) ) );
    $nominalppn = parseInt($total) * (parseInt($ppn)/100);
    $("#txtPpn").val(addCommas( String($nominalppn) ) );
    $nettotal = parseInt($total) + parseInt($nominalppn);
    $("#txtNetTotal").val(addCommas( String($nettotal) ) );
    $("#txtTotalBiaya").val(addCommas( String($nettotal) ) );
    // alert(addCommas(1000));
  }

  function save($idcustomer,$idtrip){
    var $tax = 0;
    var $totalsuratjalan = 0;
    if($("#rdoTax2").prop('checked')==true){
      $tax=0;
    }else if($("#rdoTax1").prop('checked')==true){
      $tax=1;
    }
    // alert(tax);
    var obj = {_token:$('meta[name="csrf_token"]').attr('content'),id_trip:$idtrip,id_customer:$idcustomer,tgl_invoice_lcl:$("#txtTglInvoice").val(),bukti_bayar:$("#txtBuktiBayar").val(),
                tax:$("#txtPpn").val(),include_tax:$tax,total:$("#txtTotalBiaya").val(),catatan:$("#txtCatatan").val(),detail:[]};
    var detail = [];
    $("#detailInvoiceLCLTable > tbody > tr").each(function (i, el) {
      var $tds = $(this).find('td');
      if($tds.eq(0).find('input').prop('checked') == true){
        var $idsuratjalan = $tds.eq(0).find('input').val();
        var $biaya = $("#txtNominalBiaya_"+$idsuratjalan).val();
        if($("#txtBiayaTambahan1_"+$idsuratjalan).val()==null || $("#txtBiayaTambahan1_"+$idsuratjalan).val()==""){
          var $biayatamb1 = 0;
        }else{
          var $biayatamb1 = $("#txtNominalTambahan1_"+$idsuratjalan).val();
        }
        if($("#txtBiayaTambahan2_"+$idsuratjalan).val()==null || $("#txtBiayaTambahan2_"+$idsuratjalan).val()==""){
          var $biayatamb2 = 0;
        }else{
          var $biayatamb2 = $("#txtNominalTambahan2_"+$idsuratjalan).val();
        }
        if($("#txtBiayaTambahan3_"+$idsuratjalan).val()==null || $("#txtBiayaTambahan3_"+$idsuratjalan).val()==""){
          var $biayatamb3 = 0;
        }else{
          var $biayatamb3 = $("#txtNominalTambahan3_"+$idsuratjalan).val();
        }
        
        $totalsuratjalan = parseInt($biaya) + parseInt($biayatamb1) + parseInt($biayatamb2) + parseInt($biayatamb3);
        var objDetail = {id_suratjalan:$idsuratjalan,biaya:$biaya,
                          biaya_tambahan_1:$("#txtBiayaTambahan1_"+$idsuratjalan).val(),nominal_biaya_tambahan_1:$biayatamb1,
                          biaya_tambahan_2:$("#txtBiayaTambahan2_"+$idsuratjalan).val(),nominal_biaya_tambahan_2:$biayatamb2,
                          biaya_tambahan_3:$("#txtBiayaTambahan3_"+$idsuratjalan).val(),nominal_biaya_tambahan_3:$biayatamb3,
                          total:$totalsuratjalan};
        // alert($tds.eq(0).find('input').val());
        detail.push(objDetail);
      }
    });

    var count = detail.length;
  // alert(count);
    obj.detail = detail;
    if(count!=0){
      $.ajax({
        url: route('transaction.invoicelcl.create'),
        data:obj,
        method:'post'
      })
      .then(function (response){
        if (response.status == 'error') {
          alert(response.description);
        } else if( response.status == 'success') {
          alert(response.description);
          window.location = "{{route('transaction.invoicelcl')}}";
          // refresh();
        }
      })
      .catch(function (response) {
        console.log(response);
        if(response.status == '422') {
          let error = response.responseJSON.errors;
          $.each(error, function(index,item) {
            $('#error'+index).html(item.join(", "));
          });
          alert("Please check the form errors.");

        } else {
        alert(response.responseJSON.message);
        }
        
      }); 
    }else{
      alert("Tidak ada Surat Jalan yang dipilih!");
    }

  }

  function update($idinvoicelcl){
    var $tax = 0;
    var $totalsuratjalan = 0;
    if($("#rdoTax2").prop('checked')==true){
      $tax=0;
    }else if($("#rdoTax1").prop('checked')==true){
      $tax=1;
    }
    // alert(tax);
    var obj = {_token:$('meta[name="csrf_token"]').attr('content'),tgl_invoice_lcl:$("#txtTglInvoice").val(),bukti_bayar:$("#txtBuktiBayar").val(),
                tax:$("#txtPpn").val(),include_tax:$tax,total:$("#txtTotalBiaya").val(),catatan:$("#txtCatatan").val(),detail:[]};
    var detail = [];
    $("#detailInvoiceLCLTable > tbody > tr").each(function (i, el) {
      var $tds = $(this).find('td');
      if($tds.eq(0).find('input').prop('checked') == true){
        var $idsuratjalan = $tds.eq(0).find('input').val();
        var $biaya = $("#txtNominalBiaya_"+$idsuratjalan).val();
        if($("#idinvoicedetaillcl_"+$idsuratjalan).val()==null || $("#idinvoicedetaillcl_"+$idsuratjalan).val()=="" ){
          var $idinvoicedetaillcl = 0;
        }else{
          var $idinvoicedetaillcl = $("#idinvoicedetaillcl_"+$idsuratjalan).val();
        }
        if($("#txtBiayaTambahan1_"+$idsuratjalan).val()==null || $("#txtBiayaTambahan1_"+$idsuratjalan).val()==""){
          var $biayatamb1 = 0;
        }else{
          var $biayatamb1 = $("#txtNominalTambahan1_"+$idsuratjalan).val();
        }
        if($("#txtBiayaTambahan2_"+$idsuratjalan).val()==null || $("#txtBiayaTambahan2_"+$idsuratjalan).val()==""){
          var $biayatamb2 = 0;
        }else{
          var $biayatamb2 = $("#txtNominalTambahan2_"+$idsuratjalan).val();
        }
        if($("#txtBiayaTambahan3_"+$idsuratjalan).val()==null || $("#txtBiayaTambahan3_"+$idsuratjalan).val()==""){
          var $biayatamb3 = 0;
        }else{
          var $biayatamb3 = $("#txtNominalTambahan3_"+$idsuratjalan).val();
        }
        
        $totalsuratjalan = parseInt($biaya) + parseInt($biayatamb1) + parseInt($biayatamb2) + parseInt($biayatamb3);
        var objDetail = {id_invoicedetail_lcl:$idinvoicedetaillcl,id_suratjalan:$idsuratjalan,biaya:$biaya,
                          biaya_tambahan_1:$("#txtBiayaTambahan1_"+$idsuratjalan).val(),nominal_biaya_tambahan_1:$biayatamb1,
                          biaya_tambahan_2:$("#txtBiayaTambahan2_"+$idsuratjalan).val(),nominal_biaya_tambahan_2:$biayatamb2,
                          biaya_tambahan_3:$("#txtBiayaTambahan3_"+$idsuratjalan).val(),nominal_biaya_tambahan_3:$biayatamb3,
                          total:$totalsuratjalan};
        // alert($tds.eq(0).find('input').val());
        detail.push(objDetail);
      }
    });

    var count = detail.length;
  // alert(count);
    obj.detail = detail;
    if(count!=0){
      $.ajax({
        url: route('transaction.invoicelcl.update',$idinvoicelcl),
        data:obj,
        method:'post'
      })
      .then(function (response){
        if (response.status == 'error') {
          alert(response.description);
        } else if( response.status == 'success') {
          alert(response.description);
          window.location = "{{route('transaction.invoicelcl')}}";
          // refresh();
        }
      })
      .catch(function (response) {
        console.log(response);
        if(response.status == '422') {
          let error = response.responseJSON.errors;
          $.each(error, function(index,item) {
            $('#error'+index).html(item.join(", "));
          });
          alert("Please check the form errors.");

        } else {
        alert(response.responseJSON.message);
        }
        
      }); 
    }else{
      alert("Tidak ada Surat Jalan yang dipilih!");
    }

  }


</script>
@endsection