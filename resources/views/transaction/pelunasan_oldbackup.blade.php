@extends('layouts.default')

@section('content_header')
<h1>
  PELUNASAN
</h1>
<ol class="breadcrumb">
  <li><a href="#"> Transaction</a></li>
  <li class="active">Pelunasan</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@can('edit pelunasan')
<a href="#" data-toggle="tooltip" title="Add Pelunasan"><button type='button' class='btn btn-danger' data-toggle="modal" data-target="#ListModal"><i class='fa fa-plus'></i> Buat Baru</button></a>
@endcan
<div class="portlet-body">
  <table class="table table-bordered" id="pelunasanTable" style='background-color: white'>

  </table>
</div>



<div id="ListModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width:900px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">List Kuitansi</h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered" id="listInvoiceTable" style='background-color: white'>


        </table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>




@endsection

@section('footer_script')
<script>
  $('#pelunasanTable').bootstrapTable({
    classes: 'table table-hover table-no-bordered',
    url: route('transaction.pelunasan.data'),
    pagination: true,
    search: true,
    columns: [{
        title: 'NO',
        formatter: function(value, row, index) {
          return index + 1;
        }
      }, {
        field: 'no_invoice',
        title: 'NO KUITANSI',
      sortable: true
      }, {
        title: 'TANGGAL KUITANSI',
        field: 'tgl_invoice',
        formatter: function(value, row, index) {
          return moment(value).format('DD-MM-YYYY');
        },
      sortable: true
      }, {
        field: 'kode_trip',
        title: 'KODE TRIP',
      sortable: true
      },
      {
        field: 'bukti_bayar',
        title: 'BUKTI BAYAR',
      sortable: true
      }, {
        title: 'TANGGAL PELUNASAN',
        field: 'tgl_pelunasan',
        formatter: function(value, row, index) {
          return moment(value).format('DD-MM-YYYY');
        },
      sortable: true
      }, {
        title: 'AKSI',
        formatter: function(value, row, index) {
          var btnString = '';
          @can('edit pelunasan')
          btnString += '<a href="' + route('transaction.pelunasan.edit', {
            id: row.id_pelunasan,
            idinvoice: row.id_invoice,
            tipe_invoice: row.type_invoice
          }) + '" data-toggle="tooltip" title="Edit BAPB"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>';
          @endcan
          return btnString;
        }
      }
    ]
  });

  $('#listInvoiceTable').bootstrapTable({
    classes: 'table table-hover table-no-bordered',
    url: route('transaction.pelunasan.datainvoice'),
    pagination: true,
    search: true,
    columns: [{
      title: 'NO',
      formatter: function(value, row, index) {
        return index + 1;
      }
    }, {
      field: 'no_invoice',
      title: 'NO KUITANSI',
      sortable: true
    }, {
      title: 'TANGGAL KUITANSI',
      field: 'tgl_invoice',
      formatter: function(value, row, index) {
        return moment(value).format('DD-MM-YYYY');
      },
      sortable: true
    }, {
      field: 'kode_trip',
      title: 'KODE TRIP',
      sortable: true
    }, {
      field: 'bukti_bayar',
      title: 'BUKTI BAYAR',
      sortable: true
    }, {
      title: 'AKSI',
      formatter: function(value, row, index) {
        var btnString = '<a href="' + route('transaction.pelunasan.add', {
          id: row.id_invoice,
          tipe_invoice: row.type_invoice
        }) + '" data-toggle="tooltip" title="Add Invoice"><button type="button" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i></button></a>'
        return btnString;
      }
    }]
  });
</script>
@endsection