@extends('layouts.default')

@section('content_header')
<h1>
@if($mode=='ADD')
    ADD SURAT JALAN
@elseif($mode=='EDIT')
    EDIT SURAT JALAN
@else
    VIEW SURAT JALAN
@endif
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">Surat Jalan</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@if($mode=='ADD')
<form action="{{ route('transaction.suratjalan.create',$order->id_order) }}" method="post" class="form-horizontal">
@else
<form action="{{ route('transaction.suratjalan.update',$suratjalan->id_suratjalan) }}" method="post" class="form-horizontal">
@endif
<input type="hidden" name="_token" value="{{ csrf_token() }}">
@if($mode=='ADD')
<input type="hidden" name="typeorder" value="{{ $order->type_order }}">
@if( $order->type_order=='CONTAINER')
<input type="hidden" name="freightsize" value="{{ $order->freight_size }}">
@else
<input type="hidden" name="lclsize" value="{{ $order->lcl_size }}">
@endif
@else
<input type="hidden" name="typeorder" value="{{ $suratjalan->type_suratjalan }}">
@if( $suratjalan->type_suratjalan=='CONTAINER')
<input type="hidden" name="freightsize" value="{{ $suratjalan->freight_size }}">
@else
<input type="hidden" name="lclsize" value="{{ $suratjalan->lcl_size }}">
@endif
@endif

@if($mode=='ADD')
<input type="hidden" name="idsuratjalan" id="idsuratjalan" value="0">
@else
<input type="hidden" name="idsuratjalan" id="idsuratjalan" value="{{ $suratjalan->id_suratjalan }}">
@endif

    <div class="form-group">
    <label class="control-label col-xs-2" for="txtIdOrder">ID Order</label> 
    <div class="col-xs-3">
        <input class="form-control" name="txtIdOrder" id="txtIdOrder" type="text" @if($mode=='ADD') value="{{$order->id_order}}" @else value="{{$suratjalan->id_order}}" @endif readonly >
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="cboKodeTrip">Kode Trip</label> 
    <div class="col-xs-3">
      <select class="select form-control cboKodeTrip" name="cboKodeTrip" id="cboKodeTrip" onchange="changeKodeTrip()" @if($mode=='VIEW') disabled readonly @endif>
      <option value="0">-- PILIH KODE TRIP --</option>
      @foreach($trip as $item)
      @if($mode != 'ADD') 
        @if($suratjalan->id_kodetrip==$item->id_trip) 
        <option value="{{ $item->id_trip }}|{{ $item->nama_kapal }}|{{ Carbon\Carbon::parse($item->tanggal_berangkat)->format('d-m-Y') }}|{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}" selected>{{ $item->kode_trip }}</option>
        @else
        <option value="{{ $item->id_trip }}|{{ $item->nama_kapal }}|{{ Carbon\Carbon::parse($item->tanggal_berangkat)->format('d-m-Y') }}|{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}">{{ $item->kode_trip }}</option>
        @endif
      @else
        <option value="{{ $item->id_trip }}|{{ $item->nama_kapal }}|{{ Carbon\Carbon::parse($item->tanggal_berangkat)->format('d-m-Y') }}|{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}">{{ $item->kode_trip }}</option>
      @endif
    @endforeach
      </select>
      @if($errors->has('cboKodeTrip'))
      <span class="text-danger">{{$errors->first('cboKodeTrip')}}
      @endif
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtNamaKapal">Nama Kapal</label> 
    <div class="col-xs-3">
        <input class="form-control" name="txtNamaKapal" id="txtNamaKapal" type="text" value="" readonly>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtTglBerangkat">Tanggal Berangkat</label> 
    <div class="col-xs-3">
      <input class="form-control" name="txtTglBerangkat" id="txtTglBerangkat" type="text" value="" readonly>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtNoSuratJalan">No Surat Jalan</label> 
    <div class="col-xs-3">
      <input required="required" class="form-control" name="txtNoSuratJalan" id="txtNoSuratJalan" type="text" @if($mode!='ADD') value="{{$suratjalan->no_suratjalan}}" @endif  @if($mode=='VIEW') readonly @endif>
      @if($errors->has('txtNoSuratJalan'))
      <span class="text-danger">{{$errors->first('txtNoSuratJalan')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtAlamatPengambilan">Alamat Pengambilan</label> 
    <div class="col-xs-4">
      <textarea required="required" class="form-control" rows="3" cols="40" name="txtAlamatPengambilan" id="txtAlamatPengambilan" @if($mode=='VIEW') readonly @endif>@if($mode!='ADD') {{$suratjalan->alamat_pengambilan}} @endif  </textarea>
      @if($errors->has('txtAlamatPengambilan'))
      <span class="text-danger">{{$errors->first('txtAlamatPengambilan')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="cboConsignee">Penerima</label> 
    <div class="col-xs-3">
      <select class="select form-control cboConsignee" name="cboConsignee" id="cboConsignee" @if($mode=='VIEW') readonly disabled @endif>
      <option value="0">-- PILIH PENERIMA --</option>
      @foreach($consignee as $item)
      @if($mode != 'ADD') 
        @if($suratjalan->id_consignee==$item->id_consignee) 
        <option value="{{ $item->id_consignee }}" selected>{{ $item->nama }}</option>
        @else
        <option value="{{ $item->id_consignee }}">{{ $item->nama }}</option>
        @endif
      @else
        <option value="{{ $item->id_consignee }}">{{ $item->nama }}</option>
      @endif
    @endforeach
        
      </select>
      @if($errors->has('cboConsignee'))
      <span class="text-danger">{{$errors->first('cboConsignee')}}
      @endif
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtNoPlatTruck">No Plat Truck</label> 
    <div class="col-xs-3">
      <input required="required" class="form-control" name="txtNoPlatTruck" id="txtNoPlatTruck" type="text" @if($mode!='ADD') value="{{$suratjalan->no_plattruck}}" @endif  @if($mode=='VIEW') readonly  @endif>
      @if($errors->has('txtNoPlatTruck'))
      <span class="text-danger">{{$errors->first('txtNoPlatTruck')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="lblPelabuhanTujuan">Pelabuhan Tujuan</label> 
    <div class="col-xs-4">
      <label class="checkbox-inline" id="lblPelabuhanTujuan">
        
      </label>
    </div>
  </div>
  
  @if($mode=='ADD')
    @if($order->type_order=='CONTAINER')
      <div class="form-group">
      <label class="control-label col-xs-2" for="txtNoContainer">No Container</label> 
      <div class="col-xs-3">
        <input class="form-control" name="txtNoContainer" id="txtNoContainer" type="text" @if($mode!='ADD') value="{{$suratjalan->no_container}}" @endif  @if($mode=='VIEW') readonly  @endif>
      </div>
    </div>
      <div class="form-group">
      <label class="control-label col-xs-2" for="txtSeal">Seal</label> 
      <div class="col-xs-3">
        <input class="form-control" name="txtSeal" id="txtSeal" type="text" @if($mode!='ADD') value="{{$suratjalan->seal}}" @endif  @if($mode=='VIEW') readonly  @endif>
      </div>
    </div>
    @endif
  @else
  @if($suratjalan->type_suratjalan=='CONTAINER')
  <div class="form-group">
      <label class="control-label col-xs-2" for="txtKeteranganTotal">Keterangan Total</label> 
      <div class="col-xs-3">
        <input class="form-control" style="text-align: right" name="txtKeteranganTotal" id="txtKeteranganTotal" type="text" @if($mode=='ADD') value="{{$order->jumlah_order}}" @else value="{{$suratjalan->keterangan_total}}" @endif   @if($mode=='VIEW') readonly  @endif>
      </div>
    </div>
      <div class="form-group">
      <label class="control-label col-xs-2" for="txtNoContainer">No Container</label> 
      <div class="col-xs-3">
        <input class="form-control" name="txtNoContainer" id="txtNoContainer" type="text" @if($mode!='ADD') value="{{$suratjalan->no_container}}" @endif  @if($mode=='VIEW') readonly  @endif>
      </div>
    </div>
      <div class="form-group">
      <label class="control-label col-xs-2" for="txtSeal">Seal</label> 
      <div class="col-xs-3">
        <input class="form-control" name="txtSeal" id="txtSeal" type="text" @if($mode!='ADD') value="{{$suratjalan->seal}}" @endif  @if($mode=='VIEW') readonly  @endif>
      </div>
    </div>
    @endif
  @endif
  
    
    
  <div class="form-group row">
    <div class="col-xs-offset-2 col-xs-6">
    @can('edit surat jalan')
      @if($mode=='ADD')
      <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
      @elseif($mode=='EDIT')
      <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Update</button>
      @endif
    @endcan
      <a href="{{route('transaction.suratjalan')}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
    </div>
  </div>
</form>

@if($mode!='ADD')
<div id="detailRute">
@if($mode!='VIEW')
@can('edit surat jalan')
<a href="{{route('transaction.suratjalan.doadd',$suratjalan->id_suratjalan)}}" data-toggle="tooltip" title="Add DO"><button type='button' class='btn btn-danger'><i class='fa fa-plus'></i> ADD DO</button></a>
@endcan
@endif
<div class="portlet-body">
    <table class="table table-bordered" id="DOTable" style='background-color: white'>
        
    </table>
</div>

</div>

@endif

<div id="DetailModal" class="modal fade" role="dialog" >
    <div class="modal-dialog"  style="width:900px">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Detail DO</h4>
            </div>
            <div class="modal-body">
            <table class="table table-bordered" id="detailBarangTable" style='background-color: white'>

    </table>
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>



@endsection

@section('footer_script')
<script>
  $(".cboKodeTrip").select2();
  $(".cboConsignee").select2();
    
    function changeKodeTrip(){
      var trip = $("#cboKodeTrip").val().split("|");
      var namakapal = trip[1];
      var tanggalberangkat = trip[2];
      var pelabuhantujuan = trip[3];
      
      $("#txtNamaKapal").val(namakapal);
      $("#txtTglBerangkat").val(tanggalberangkat);
      $("#lblPelabuhanTujuan").html(pelabuhantujuan);
        
    }

    changeKodeTrip();

    function refresh(){
    $('#DOTable').bootstrapTable('refresh',{
                  url: route('transaction.suratjalan.datado',$("#idsuratjalan").val()),
                });
}

$('#DOTable').bootstrapTable({
        classes:'table table-hover table-no-bordered',
        url: route('transaction.suratjalan.datado',$("#idsuratjalan").val()),
  columns: [{
    title: 'NO',
    formatter: function (value, row, index) {
        return index+1;
      }
  }, {
    field: 'no_do',
    title: 'NO DO',
    sortable:true
  }, {
    field: 'nama_consignee',
    title: 'NAMA PENERIMA',
    sortable:true
  },{
    field: 'alamat',
    title: 'ALAMAT',
    sortable:true
  },{
    field: 'total',
    title: 'TOTAL',
    sortable:true
  },{
      title:'AKSI',
      formatter: function(value, row, index) {
        var btnString = '';
        @if($mode!='VIEW')
        if(row.status!=0){
          @can('edit surat jalan')
          btnString += '<a href="'+route('transaction.suratjalan.doedit',{id:row.id_do})+'" data-toggle="tooltip" title="Edit DO"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>';
          btnString +='<button type="button" class="btn btn-delete btn-xs btn-danger" title="Delete DO"><i class="fa fa-times"></i></button></a>';
          @endcan
          // btnString += '<a href="'+route('transaction.suratjalan.viewdo',{id:row.id_do})+'" data-toggle="tooltip" title="View DO"><button type="button" class="btn  btn-primary btn-xs"><i class="fa fa-eye"></i></button></a>';
          btnString += '<button type="button" class="btn btn-view btn-primary btn-xs" data-toggle="modal" data-target="#DetailModal"><i class="fa fa-eye"></i></button></a>';
        }else {
            btnString += '<a href="'+route('transaction.suratjalan.viewdo',{id:row.id_do})+'" data-toggle="tooltip" title="View DO"><button type="button" class="btn  btn-primary btn-xs"><i class="fa fa-eye"></i></button></a>';
        }
        @else
        // btnString += '<a href="'+route('transaction.suratjalan.viewfromviewdo',{id:row.id_do})+'" data-toggle="tooltip" title="View DO"><button type="button" class="btn  btn-primary btn-xs"><i class="fa fa-eye"></i></button></a>';
        btnString += '<button type="button" class="btn btn-view btn-primary" data-toggle="modal" data-target="#DetailModal"><i class="fa fa-eye"></i></button></a>';
        @endif
        
        return btnString;
      },
      events:{
        'click .btn-delete': function(e, value, row, index) {
          if (confirm('Anda yakin akan menghapus delivery order ini?')) {
            $.ajax({
              url: route('transaction.suratjalan.deletedo',{id:row.id_do}),
              data:{_token:$('meta[name="csrf_token"]').attr('content')},
              method:'post'
            })
            .then(function (response){
              if (response.status == 'error') {
                alert(response.description);
              } else if( response.status == 'success') {
                alert(response.description);
                refresh();
              }
            }); 
          }
        },
        'click .btn-view': function(e, value, row, index) {
          $('#detailBarangTable').bootstrapTable('refresh',{
                  url: route('transaction.suratjalan.listbarang',{id:row.id_do}),
                });
          $('#detailBarangTable').bootstrapTable({
            classes:'table table-hover table-no-bordered',
            url: route('transaction.suratjalan.listbarang',{id:row.id_do}),
              columns: [{
                title: 'NO',
                formatter: function (value, row, index) {
                    return index+1;
                  }
              }, {
                field: 'kode_barang',
                title: 'KODE BARANG',
                sortable:true
              }, {
                field: 'nama_barang',
                title: 'NAMA BARANG',
                sortable:true
              },{
                field: 'satuan',
                title: 'SATUAN',
                sortable:true
              },{
                field: 'panjang',
                title: 'PANJANG',
                sortable:true
              },{
                field: 'lebar',
                title: 'LEBAR',
                sortable:true
              },{
                field: 'tinggi',
                title: 'TINGGI',
                sortable:true
              },{
                field: 'm3',
                title: 'M3',
                sortable:true
              },{
                field: 'ton',
                title: 'TON',
                sortable:true
              },{
                field: 'kolli',
                title: 'KOLLI',
                sortable:true
              }]
          });
        }
      }
    }]
});


$('#DetailModal').on('hidden.bs.modal', function() {
  var $table = $('#DetailModal');
$table.bootstrapTable('destroy');
});

</script>
@endsection
