@extends('layouts.default')

@section('content_header')
<h1>
    PACKING LIST
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">Packing List</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
<div class="portlet-body">
    <table class="table table-bordered" id="packingListTable" style='background-color: white'>
        
        

    </table>
</div>

<div id="DetailModal" class="modal fade" role="dialog" >
    <div class="modal-dialog" style="width:900px;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Detail Packing List </h4>
            </div>
            <div class="modal-body">
            <table class="table table-bordered" id="detailTable" style='background-color: white'>
        
        

    </table>
    </br>
            <div class="form-group row">
    <div class="col-xs-offset-10 col-xs-4">
    @can('print packing list')
      <a href="#"><button class="btn btn-primary" type="submit" name="submit">PRINT</button></a>
    @endcan
    </div>
  </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>




@endsection

@section('footer_script')
<script>

$('#packingListTable').bootstrapTable({
        classes:'table table-hover table-no-bordered',
        url: route('transaction.packinglist.data'),
    pagination: true,
  search: true,
  columns: [{
    title: 'NO',
    formatter: function (value, row, index) {
        return index+1;
      }
  },{
    field: 'nama',
    title: 'DOORING',
    sortable: true
  },{
    field: 'kode_trip',
    title: 'KODE TRIP',
    sortable: true
  },{
    field: 'name_full',
    title: 'NAMA PELAYARAN'
  },{
    field: 'nama_kapal',
    title: 'NAMA KAPAL',
    sortable: true
  },{
    title: 'TANGGAL BERANGKAT',
    field: 'tanggal_berangkat',
    formatter : function (value, row, index) {
        return moment(value).format('DD-MM-YYYY HH:mm:ss');
      },
    sortable: true
  },{
    field: 'total',
    title: 'TOTAL',
    sortable: true
  },{
      title:'AKSI',
      formatter: function(value, row, index) {
        var btnString = '';
          btnString += '<a href="#" data-toggle="tooltip" title="View Detail"><button type="button" class="btn btn-primary btn-xs btn-view" data-toggle="modal" data-target="#DetailModal"><i class="fa fa-eye"></i></button></a>';
        return btnString;
      },
      events:{
        'click .btn-view': function(e, value, row, index) {
          $('#detailTable').bootstrapTable('refresh',{
                  url: route('transaction.packinglist.datadetail',{idtrip:row.id_trip,iddoor:row.id_dooring,idpelayaran:row.id_pelayaran}),
                });
          $('#detailTable').bootstrapTable({
            classes:'table table-hover table-no-bordered',
            url: route('transaction.packinglist.datadetail',{idtrip:row.id_trip,iddoor:row.id_dooring,idpelayaran:row.id_pelayaran}),
              columns: [{
                title: 'NO',
                formatter: function (value, row, index) {
                    return index+1;
                  }
              }, {
                field: 'no_bapb',
                title: 'NO BAPB',
                sortable: true
              }, {
                field: 'no_suratjalan',
                title: 'NO SURAT JALAN',
                sortable: true
              },{
                field: 'no_do',
                title: 'NO DO',
                sortable: true
              },{
                field: 'no_container',
                title: 'NO CONTAINER',
                sortable: true
              },{
                field: 'seal',
                title: 'SEAL',
                sortable: true
              },{
                field: 'nama_consignee',
                title: 'NAMA PENERIMA',
                sortable: true
              }]
          });
        }
      }
    }]
});

</script>
@endsection
