@extends('layouts.default')

@section('content_header')
<h1>
    PROPOSAL SETTING
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">Proposal</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
<form class="form-horizontal">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="form-group">
    <label class="control-label col-xs-2" for="txtKeteranganMuat">Keterangan Muat</label> 
    <div class="col-xs-4">
      <textarea class="form-control" rows="3" cols="40" name="txtKeteranganMuat" id="txtKeteranganMuat">{{$proposal->keterangan_muat}}</textarea>
    </div>
  </div>
  
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtTermCondition">Term & Conditions</label> 
    <div class="col-xs-6">
      <textarea class="form-control" rows="20" cols="100" name="txtTermCondition" id="txtTermCondition">{!! $proposal->termcondition !!}</textarea>
    </div>
  </div>
  
  <div class="form-group row">
    <div class="col-xs-offset-2 col-xs-5">
      <button class="btn btn-primary" type="button" name="submit" onclick="saveprint({{$proposal->id_proposal}})"><i class="fas fa-print"></i> Print</button>
      @if($mode=='approval')
      <a href="{{route('transaction.proposalapproval')}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
      @else
      <a href="{{route('transaction.customerproposal')}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
      @endif
    </div>
  </div>
</form>

@endsection

@section('footer_script')
<script>

$('#txtTermCondition').summernote('code', '{!! $proposal->termcondition !!}');

        function saveprint(idproposal){
            // alert($('#txtTermCondition').val());
            $.ajax({
                url: route('transaction.proposalapproval.saveprint',{id:idproposal}),
                data:{_token:$('meta[name="csrf_token"]').attr('content'),keterangan_muat:$("#txtKeteranganMuat").val(),termcondition:$("#txtTermCondition").val()},
                method:'post'
            })
            .then(function (response){
                if (response.status == 'error') {
                alert(response.description);
                } else if( response.status == 'success') {
                //   alert(response.description);
                window.location = "{{route('transaction.proposalapproval.print',$proposal->id_proposal)}}";
                // refresh();
                }
            }); 
        }
    
</script>
@endsection
