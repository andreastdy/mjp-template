@extends('layouts.default')

@section('content_header')
<h1>
  CUSTOMER PROPOSAL
</h1>
<ol class="breadcrumb">
  <li><a href="#"> Transaction</a></li>
  <li class="active">Customer Proposal</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@can('edit proposal')
<a href="{{route('transaction.customerproposal.add')}}" data-toggle="tooltip" title="Tambah Customer Proposal"><button type='button' class='btn btn-danger'><i class='fa fa-plus'></i>&nbsp; ADD NEW</button></a>
<button type='button' class='btn btn-danger' data-toggle="modal" data-target="#AddModal"><i class='fa fa-plus'></i>&nbsp; COPY FROM OTHER PROPOSAL</button></a>
@endcan

<div class="portlet-body">

  <table class="table table-bordered" id="customerProposalTable" style='background-color: white'>

  </table>
</div>

<div id="AddModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width:900px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">List Penawaran</h4>
      </div>
      <div class="modal-body">

        <table class="table table-bordered" id="listCustomerProposalTable" style='background-color: white'>

        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>




@endsection

@section('footer_script')
<script>
  $(document).ready(function() {
    function refresh() {
      $('#customerProposalTable').bootstrapTable('refresh', {
        url: route('transaction.customerproposal.data'),
      });
    }

    $('#customerProposalTable').bootstrapTable({
      classes: 'table table-hover table-no-bordered',
      url: route('transaction.customerproposal.data'),
      pagination: true,
      search: true,
      sidePagination:'server',
      columns: [{
        title: 'NO',
        formatter: function(value, row, index) {
          return index + 1;
        }
      }, {
        field: 'no_proposal',
        title: 'NO PROPOSAL',
        sortable : true
      }, {
        field: 'nama_customer',
        title: 'NAMA CUSTOMER',
        sortable : true
      }, {
        title: 'CREATED',
        field: 'created_at',
        formatter: function(value, row, index) {
          return row.created_by + '<br/>(' + moment(row.created_at).format('DD-MM-YYYY HH:mm:ss') + ')';
        },
        sortable : true
      }, {
        title: 'STATUS',
        field: 'status',
        formatter: function(value, row, index) {
          if (row.status == "1") {
            return '<font color=blue>PENDING</font>';
          } else if (row.status == 2) {
            return '<font color=black>WAITING FOR APPROVAL</font>';
          } else if (row.status == 3) {
            return '<font color=green>APPROVED</font>';
          } else if (row.status == 4) {
            return '<font color=red>REJECTED</font>';
          } else if (row.status == 5) {
            return '<font color=red>ARCHIVED</font>';
          }
        },
        sortable : true
      }, {
        title: 'APPROVED',
        field: 'approved_at',
        formatter: function(value, row, index) {
          if (row.approved_at == null) {
            return "-";
          } else {
            return row.approved_by + '<br/>(' + moment(row.approved_at).format('DD-MM-YYYY HH:mm:ss') + ')';
          }

        },
        sortable : true
      }, {
        title: 'AKSI',
        formatter: function(value, row, index) {
          var btnString = '';
          if (row.status == 1) {
            @can('edit proposal')
            btnString += '<a href="' + route('transaction.customerproposal.edit', {
              id: row.id_proposal
            }) + '" data-toggle="tooltip" title="Edit Proposal"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>';
            btnString += '<button type="button" class="btn btn-delete btn-xs btn-danger" title="archive proposal"><i class="fa fa-times"></i></button></a>';
            @endcan
          } else if (row.status == 2 || row.status == 4) {
            
            btnString += '<a href="' + route('transaction.customerproposal.edit', {
              id: row.id_proposal
            }) + '" data-toggle="tooltip" title="View Proposal"><button type="button" class="btn  btn-primary btn-xs"><i class="fa fa-eye"></i></button></a>';
            @can('edit proposal')
            btnString += '<button type="button" class="btn btn-delete btn-xs btn-danger" title="archive proposal"><i class="fa fa-times"></i></button></a>';
            @endcan
          } else if (row.status == 3) {
            btnString += '<a href="' + route('transaction.customerproposal.edit', {
              id: row.id_proposal
            }) + '" data-toggle="tooltip" title="View Proposal"><button type="button" class="btn  btn-primary btn-xs"><i class="fa fa-eye"></i></button></a>';

            @can('edit proposal')
            btnString += '<button type="button" class="btn btn-delete btn-xs btn-danger" title="archive proposal"><i class="fa fa-times"></i></button></a>';
            @endcan
            
            @can('print proposal')
            btnString += '<a href="' + route('transaction.proposalapproval.previewpdf', {
              id: row.id_proposal,
              mode: 'master'
            }) + '" data-toggle="tooltip" title="Print Proposal"><button type="button" class="btn  btn-primary btn-xs"><i class="fa fa-print"></i></button></a>'
            @endcan
          } else {
            @can('edit proposal')
            btnString += '<a href="' + route('transaction.customerproposal.edit', {
              id: row.id_proposal
            }) + '" data-toggle="tooltip" title="Edit Proposal"><button type="button" class="btn  btn-primary btn-xs"><i class="fa fa-eye"></i></button></a>';
            @endcan
          }

          return btnString;
        },
        events: {
          'click .btn-delete': function(e, value, row, index) {
            if (confirm('Are you sure you want to archive this record?')) {
              $.ajax({
                  url: route('transaction.customerproposal.archive', {
                    id: row.id_proposal
                  }),
                  data: {
                    _token: $('meta[name="csrf_token"]').attr('content')
                  },
                  method: 'post'
                })
                .then(function(response) {
                  if (response.status == 'error') {
                    alert(response.description);
                  } else if (response.status == 'success') {
                    alert(response.description);
                    refresh();
                  }
                });
            }
          }
        }
      }]
    });

    $('#listCustomerProposalTable').bootstrapTable({
      classes: 'table table-hover table-no-bordered',
      url: route('transaction.customerproposal.data'),
      pagination: true,
      search: true,
      columns: [{
        title: 'NO',
        formatter: function(value, row, index) {
          return index + 1;
        }
      }, {
        field: 'no_proposal',
        title: 'NO PROPOSAL'
      }, {
        field: 'nama_customer',
        title: 'NAMA CUSTOMER'
      }, {
        title: 'PERIODE AWAL',
        field: 'valid_from',
        formatter: function(value, row, index) {
          return moment(value).format('DD-MM-YYYY');
        }
      }, {
        title: 'PERIODE AKHIR',
        field: 'valid_to',
        formatter: function(value, row, index) {
          return moment(value).format('DD-MM-YYYY');
        }
      }, {
        title: 'AKSI',
        formatter: function(value, row, index) {
          var btnString = '<a href="' + route('transaction.customerproposal.copyproposal', {
            id: row.id_proposal
          }) + '" data-toggle="tooltip" title="Copy Proposal"><button type="button" class="btn  btn-primary"><i class="fa fa-plus"></i></button></a>'
          return btnString;
        }
      }]
    })

  });
</script>
@endsection