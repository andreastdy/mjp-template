@extends('layouts.default')

@section('content_header')
<h1>
    ARCHIVED ORDER
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Transaction</a></li>
    <li class="active">Archived Order</li>
</ol>
@endsection

@section('content')
<hr>
<div class="portlet-body">
    <table class="table table-bordered" id="customerOrderTable" style='background-color: white'>
        <tr>
            <th>NO</th>
            <th>NO ORDER</th>
            <th>NAMA PELAYARAN</th>
            <th>NAMA CUSTOMER</th>
            <th>KOTA ASAL</th>
            <th>KOTA TUJUAN</th>
            <th>TIPE PROPOSAL</th>
            <th>JADWAL EKSEKUSI</th>
            <th>CREATED</th>
            <th>STATUS</th>
            <th >AKSI</th>
        </tr>
        <tr>
            <td>1</td>
            <td>ORD290191</td>
            <td>PT. TEST 1</td>
            <td>CUSTOMER A</td>
            <td>SURABAYA</td>
            <td>JAKARTA</td>
            <td>CONTAINER</td>
            <td>02-12-2018 09:00:00</td>
            <td>Admin <Br/>(01-12-2018 16:22:01)</td>
            <td>SCHEDULED</td>
            <td>
               
                <a href="#" data-toggle="tooltip" title="View Detail"><button type='button' class='btn btn-primary' data-toggle="modal" data-target="#DetailModal"><i class='fa fa-eye'></i></button></a>
            </td>
        </tr>
        <tr>
            <td>1</td>
            <td>ORD310291</td>
            <td>PT. TEST 1</td>
            <td>CUSTOMER B</td>
            <td>SURABAYA</td>
            <td>JAKARTA</td>
            <td>LCL</td>
            <td>05-12-2018 12:30:00</td>
            <td>Admin <Br/>(02-12-2018 10:32:11)</td>
            <td>SCHEDULED</td>
            <td>
                <a href="#" data-toggle="tooltip" title="View Detail"><button type='button' class='btn btn-primary' data-toggle="modal" data-target="#DetailModal"><i class='fa fa-eye'></i></button></a>
            </td>
        </tr>
        

    </table>
</div>

<div id="DetailModal" class="modal fade" role="dialog" >
    <div class="modal-dialog" >

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Detail Order </h4>
            </div>
            <div class="modal-body">
            <label class="control-label">ORDER ID : ORD290191 </label>
            <br/>
            <label class="control-label">NAMA PELAYARAN : PT. TEST 1</label>
            <br/>   
            <label class="control-label">CUSTOMER : CUSTOMER A </label>
            <br/>
            <label class="control-label">KOTA ASAL : SURABAYA </label>
            <br/>
            <label class="control-label">KOTA TUJUAN : JAKARTA</label>
            <br/>
            <label class="control-label">TIPE PROPOSAL : CONTAINER </label>
            <br/>
            <label class="control-label">ALAMAT ASAL : JL. Pertama </label>
            <br/>
            <label class="control-label">ALAMAT TUJUAN : JL. Kedua </label>
            <br/>
            <label class="control-label">FREIGHT SIZE : 20 FT</label>
            <br/>
            <label class="control-label">PELAYARAN : PT. BERLAYAR</label>
            <br/>
            <label class="control-label">HARGA PELAYARAN : Rp. 1.500.000</label>
            <br/>
            <label class="control-label">KRANI : - </label>
            <br/>
            <label class="control-label">SUPIR : -  </label>
            <br/>
            <label class="control-label">NO TRAILER : L9920WX</label>
            <br/>
            <label class="control-label">NO CONTAINER : MRTU298767</label>
            <br/>
            <label class="control-label">NO SEAL : 2891092</label>
            <br/>
            <label class="control-label">KODE TRIP : MXQKLE </label>
            <br/>
            <label class="control-label">JADWAL EKSEKUSI : 02-12-2018 09:00:00 </label>
            <br/>
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>




@endsection

@section('footer_script')
<script>

    $('#pelayaranTable').bootstrapTable({});

    function editPelayaran() {
        // window.open(' {{route('master.pelayaran.edit')}} ', "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, width=1400, height=800");
        //openPopup({{route('master.pelayaran.edit')}}, 'Title', 1400, 800, 'scrollbars,resizable', true, false);
    }
</script>
@endsection
