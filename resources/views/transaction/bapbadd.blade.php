@extends('layouts.default')

@section('content_header')
<h1>
  @if($mode=='ADD')
  ADD BAPB
  @else
  EDIT BAPB
  @endif
</h1>
<ol class="breadcrumb">
  <li><a href="#"> Transaction</a></li>
  <li class="active">BAPB</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
<form class="form-horizontal">
  <input type="hidden" name="typeorder" id="typeorder" @if($mode=='ADD' ) value="{{ $order->type_order }}" @endif>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtIdOrder">ID Order</label>
    <div class="col-xs-3">
      <input class="form-control" name="txtIdOrder" id="txtIdOrder" type="text" @if($mode=='ADD' ) value="{{ $order->id_order }}" @else value="{{ $bapb->id_order }}" @endif readonly>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtNamaCustomer">Nama Customer</label>
    <div class="col-xs-3">
      <input class="form-control" name="txtNamaCustomer" id="txtNamaCustomer" type="text" @if($mode=='ADD' ) value="{{ $order->nama_pt }}" @else value="{{ $bapb->nama_pt }}" @endif readonly>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtNoBAPB">No BAPB</label>
    <div class="col-xs-3">
      <input class="form-control" name="txtNoBAPB" id="txtNoBAPB" type="text" @if($mode!='ADD' ) value="{{ $bapb->no_bapb }}" @endif readonly>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="cboKodeTrip">Kode Trip</label>
    <div class="col-xs-3">
      <select class="select form-control cboKodeTrip" name="cboKodeTrip" id="cboKodeTrip" onchange="changeKodeTrip()" @if($mode!='ADD' ) readonly disabled @endif>
      <option value="0">-- PILIH KODE TRIP --</option>
        @foreach($trip as $item)
        @if($mode == 'ADD')
        <option value="{{ $item->id_trip }}">{{ $item->kode_trip }}</option>
        @else
        @if($bapb->id_trip==$item->id_trip)
        <option value="{{ $item->id_trip }}" selected>{{ $item->kode_trip }}</option>
        @else
        <option value="{{ $item->id_trip }}">{{ $item->kode_trip }}</option>
        @endif
        @endif
        @endforeach
      </select>
      <span class="text-danger" id="errorid_trip"></span>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="cboDooring">Dooring</label>
    <div class="col-xs-3">
      <select class="select form-control cboDooring" name="cboDooring" id="cboDooring" @if($mode!='ADD' ) readonly disabled @endif>
      <option value="0">-- PILIH DOORING --</option>
        @foreach($dooring as $item)
        @if($mode == 'ADD')
        <option value="{{ $item->id_dooring }}">{{ $item->nama }}</option>
        @else
        @if($bapb->id_dooring==$item->id_dooring)
        <option value="{{ $item->id_dooring }}" selected>{{ $item->nama }}</option>
        @else
        <option value="{{ $item->id_dooring }}">{{ $item->nama }}</option>
        @endif
        @endif
        @endforeach
      </select>
      <span class="text-danger" id="errorid_dooring"></span>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="cboConsignee">Penerima</label>
    <div class="col-xs-3">
      <select class="select form-control cboConsignee" name="cboConsignee" id="cboConsignee" onchange="changeConsignee()" @if($mode!='ADD' ) readonly disabled @endif>
      <option value="0">-- PILIH PENERIMA --</option>
        @foreach($consignee as $item)
        @if($mode == 'ADD')
          @if($order->id_consignee==$item->id_consignee)
          <option value="{{ $item->id_consignee }}|{{ $item->alamat }}" selected>{{ $item->nama }}</option>
          @else
          <option value="{{ $item->id_consignee }}|{{ $item->alamat }}">{{ $item->nama }}</option>
          @endif
        @else
          @if($bapb->id_consignee==$item->id_consignee)
          <option value="{{ $item->id_consignee }}|{{ $item->alamat }}" selected>{{ $item->nama }}</option>
          @else
          <option value="{{ $item->id_consignee }}|{{ $item->alamat }}">{{ $item->nama }}</option>
          @endif
        @endif
        @endforeach
      </select>
      <span class="text-danger" id="errorid_consignee"></span>
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-xs-2" for="txtAlamat">Alamat</label>
    <div class="col-xs-4">
      <textarea class="form-control" rows="3" cols="40" name="txtAlamat" id="txtAlamat" readonly></textarea>
    </div>
  </div>

  <div id="detailRute">

    <div class="portlet-body">
      <table class="table table-bordered" id="detailDOTable" style='background-color: white'>
        <thead>
          <tr>
            <th>NO</th>
            <th>NO DO</th>
            <th>ALAMAT</th>
            <th>PENERIMA</th>
            <th>NO CONTAINER</th>
            <th>SEAL</th>
            <!--            <th>KETERANGAN TOTAL</th>-->
            <th>AKSI</th>
          </tr>
        </thead>
        <tbody>
        </tbody>


      </table>
    </div>
    <div class="form-group row">
      <div class="col-xs-offset-2 col-xs-6">
        @if($mode=='ADD')
        <button class="btn btn-primary" type="button" name="submit" onclick="save()"><i class="fas fa-save"></i> Simpan</button>
        @else
        <button class="btn btn-primary" type="button" name="submit" onclick="update({{$bapb->id_bapb}})"><i class="fas fa-save"></i> Simpan</button>
        @endif
        <a href="{{route('transaction.bapb')}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
      </div>
    </div>
</form>

</div>


<div id="DetailModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width:900px">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detail DO</h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered" id="detailBarangTable" style='background-color: white'>


        </table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

@endsection

@section('footer_script')
<script>
$(".cboKodeTrip").select2();
$(".cboDooring").select2();
$(".cboConsignee").select2();

  function changeConsignee() {
    var consignee = $("#cboConsignee").val().split("|");
    var alamat = consignee[1];

    $("#txtAlamat").html(alamat);
  }

  function changeKodeTrip() {
    var idorder = $("#txtIdOrder").val();
    var idtrip = $("#cboKodeTrip").val();
    var x = document.getElementById('detailDOTable');
    $('#detailDOTable tbody').html('');
    var new_row = '';
    var rownum = 0;
    $.ajax({
        url: route('transaction.bapb.datado', {
          idorder: idorder,
          kodetrip: idtrip
        }),
        method: 'get'
      })
      .then(function(response) {
        if (response.total == 0) {
          // alert("Kode Barang Tidak Ditemukan!");
          new_row += '<tr>' +
            '<td colspan=7 style="text-align:center"><font color=red><b>TIDAK ADA DO</b></font></td>' +
            '</tr>';
          $("table#detailDOTable").append(new_row);
        } else {
          var data = response.rows;
          @php
          $a = 0;
          @endphp
          $.each(data, function(i, val) {
            rownum += 1;

            @if($mode == 'ADD')
            if (val.id_bapb == null) {
              @php $a += 1;
              @endphp
              new_row += '<tr>' +
                '<td><input type="checkbox" value="' + val.id_do + '"/></td>' +
                '<td>' + val.no_do + '</td>' +
                '<td>' + val.alamat + '</td>' +
                '<td>' + val.nama + '</td>' +
                '<td>' + val.no_container + '</td>' +
                '<td>' + val.seal + '</td>' +
                '<td><a href="#" data-toggle="tooltip" title="View Detail"><button type="button" class="btn btn-primary btn-xs" onclick="viewDetail(' + val.id_do + ')"><i class="fa fa-eye"></i></button></a></td>' +
                '</tr>';
            }
            @else
            @if($DOcount == 0)
            if (val.id_bapb == null) {
              @php $a += 1;
              @endphp
              new_row += '<tr>' +
                '<td><input type="checkbox" value="' + val.id_do + '"/></td>' +
                '<td>' + val.no_do + '</td>' +
                '<td>' + val.alamat + '</td>' +
                '<td>' + val.nama + '</td>' +
                '<td>' + val.no_container + '</td>' +
                '<td>' + val.seal + '</td>' +
                '<td><a href="#" data-toggle="tooltip" title="View Detail"><button type="button" class="btn btn-primary btn-xs" onclick="viewDetail(' + val.id_do + ')"><i class="fa fa-eye"></i></button></a></td>' +
                '</tr>';
            }
            @else

            if (val.id_bapb == "{{$DO[$a]->id_bapb}}") {
              @php $a += 1;
              @endphp
              new_row += '<tr>' +
                '<td><a href="#" onclick="removeDO(' + val.id_do + ');"><i class="fa fa-times"  style="color:red"></i></a></td>' +
                '<td>' + val.no_do + '</td>' +
                '<td>' + val.alamat + '</td>' +
                '<td>' + val.nama + '</td>' +
                '<td>' + val.no_container + '</td>' +
                '<td>' + val.seal + '</td>' +
                '<td><a href="#" data-toggle="tooltip" title="View Detail"><button type="button" class="btn btn-primary btn-xs" onclick="viewDetail(' + val.id_do + ')"><i class="fa fa-eye"></i></button></a></td>' +
                '</tr>';
            } else {
              if (val.id_bapb == null) {
                @php $a += 1;
                @endphp
                new_row += '<tr>' +
                  '<td><input type="checkbox" value="' + val.id_do + '"/></td>' +
                  '<td>' + val.no_do + '</td>' +
                  '<td>' + val.alamat + '</td>' +
                  '<td>' + val.nama + '</td>' +
                  '<td>' + val.no_container + '</td>' +
                  '<td>' + val.seal + '</td>' +
                  '<td><a href="#" data-toggle="tooltip" title="View Detail"><button type="button" class="btn btn-primary btn-xs" onclick="viewDetail(' + val.id_do + ')"><i class="fa fa-eye"></i></button></a></td>' +
                  '</tr>';
              }
            }
            @endif
            @endif

            @php
            $a = $a + 1;
            @endphp

          });
          $("table#detailDOTable").append(new_row);
        }

      });
  }

  function removeDO($id_do) {
    if (confirm("Apakah anda yakin akan mengeluarkan DO ini dari BAPB?")) {
      //  alert("DO dikeluarkan");
      $.ajax({
          url: route('transaction.bapb.deletedo', {
            id: $id_do
          }),
          data: {
            _token: $('meta[name="csrf_token"]').attr('content')
          },
          method: 'post'
        })
        .then(function(response) {
          if (response.status == 'error') {
            alert(response.description);
          } else if (response.status == 'success') {
            alert(response.description);
            location.reload();
          }
        });

    }
  }

  function viewDetail($id_do) {
    //  alert($id_do);
    $('#DetailModal').modal('toggle');
    $('#detailBarangTable').bootstrapTable('refresh', {
      url: route('transaction.suratjalan.listbarang', $id_do),
    });
    $('#detailBarangTable').bootstrapTable({
      classes: 'table table-hover table-no-bordered',
      url: route('transaction.suratjalan.listbarang', $id_do),
      columns: [{
        title: 'NO',
        formatter: function(value, row, index) {
          return index + 1;
        }
      }, {
        field: 'kode_barang',
        title: 'KODE BARANG',
        sortable:true
      }, {
        field: 'nama_barang',
        title: 'NAMA BARANG',
        sortable:true
      }, {
        field: 'satuan',
        title: 'SATUAN',
        sortable:true
      }, {
        field: 'panjang',
        title: 'PANJANG',
        sortable:true
      }, {
        field: 'lebar',
        title: 'LEBAR',
        sortable:true
      }, {
        field: 'tinggi',
        title: 'TINGGI',
        sortable:true
      }, {
        field: 'm3',
        title: 'M3',
        sortable:true
      }, {
        field: 'ton',
        title: 'TON',
        sortable:true
      }, {
        field: 'kolli',
        title: 'KOLLI',
        sortable:true
      }]
    });
  }

  function save() {
    var consignee = $("#cboConsignee").val().split("|");
    var obj = {
      _token: $('meta[name="csrf_token"]').attr('content'),
      id_order: $("#txtIdOrder").val(),
      no_bapb: $("#txtNoBAPB").val(),
      id_trip: $("#cboKodeTrip").val(),
      id_dooring: $("#cboDooring").val(),
      id_consignee: consignee[0],
      type_bapb: $("#typeorder").val(),
      detail: []
    };
    var detail = [];
    $("#detailDOTable > tbody > tr").each(function(i, el) {
      var $tds = $(this).find('td');
      if ($tds.eq(0).find('input').prop('checked') == true) {
        var objDetail = {
          id_do: $tds.eq(0).find('input').val()
        };
        // alert($tds.eq(0).find('input').val());
        detail.push(objDetail);
      }

    });
    var count = detail.length;
    // alert(count);
    obj.detail = detail;
    if (count != 0) {
      $.ajax({
          url: route('transaction.bapb.create'),
          data: obj,
          method: 'post'
        })
        .then(function(response) {
          if (response.status == 'error') {
            alert(response.description);
          } else if (response.status == 'success') {
            alert(response.description);
            window.location = "{{route('transaction.bapb')}}";
            // refresh();
          }
        })
        .catch(function (response) {
        console.log(response);
        if(response.status == '422') {
          let error = response.responseJSON.errors;
          $.each(error, function(index,item) {
            $('#error'+index).html(item.join(", "));
          });
          alert("Please check the form errors.");

        } else {
        alert(response.responseJSON.message);
        }
        
      });
    } else {
      alert("Tidak ada DO yang dipilih!");
    }


    // alert(objDO.alamat);
  }

  function update(idbapb) {

    var obj = {
      _token: $('meta[name="csrf_token"]').attr('content'),
      detail: []
    };
    var detail = [];
    $("#detailDOTable > tbody > tr").each(function(i, el) {
      var $tds = $(this).find('td');
      if ($tds.eq(0).find('input').prop('checked') == true) {
        var objDetail = {
          id_do: $tds.eq(0).find('input').val()
        };
        // alert($tds.eq(0).find('input').val());
        detail.push(objDetail);
      }

    });
    var count = detail.length;
    // alert(count);
    obj.detail = detail;
    $.ajax({
        url: route('transaction.bapb.update', idbapb),
        data: obj,
        method: 'post'
      })
      .then(function(response) {
        if (response.status == 'error') {
          alert(response.description);
        } else if (response.status == 'success') {
          alert(response.description);
          window.location = "{{route('transaction.bapb')}}";
          // refresh();
        }
      })
      .catch(function (response) {
        console.log(response);
        if(response.status == '422') {
          let error = response.responseJSON.errors;
          $.each(error, function(index,item) {
            $('#error'+index).html(item.join(", "));
          });
          alert("Please check the form errors.");

        } else {
        alert(response.responseJSON.message);
        }
        
      });

    // alert(objDO.alamat);
  }

  $(document).ready(function() {
    changeConsignee();
    changeKodeTrip();
  });
</script>
@endsection