<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf_token" content="{{ csrf_token() }}" />
  <title>MJP</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
  <!-- DataTables -->
  <!-- <link href="{{ asset('css/mdb.min.css') }}" rel="stylesheet" >
    <link href="{{ asset('css/datatables.min.css') }}" rel="stylesheet" > -->
  <link href="{{ asset('css/bootstrap-table.min.css') }}" rel="stylesheet">
  <!-- Font Awesome -->
  <script defer src="{{ asset('js/all.min.js') }}"></script>
  <!--load all styles -->
  {{-- <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css"> --}}
  <!-- Ionicons -->
  {{-- <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css"> --}}
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('css/skin-red-light.min.css') }}">
  <!-- Morris chart -->
  {{-- <link rel="stylesheet" href="bower_components/morris.js/morris.css"> --}}
  <!-- jvectormap -->
  {{-- <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css"> --}}
  <!-- Date Time Picker -->
  <link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css')}}">
  <!-- Date Picker -->
  {{-- <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"> --}}
  <!-- Daterange picker -->
  {{-- <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css"> --}}
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{ asset('css/bootstrap3-wysihtml5.min.css')}}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-red-light fixed sidebar-collapse ">
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="#" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>MJP</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>MJP</b></span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">

      </nav>
    </header>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

      <!-- Content Header (Page header) -->
      <section class="content-header">

      </section>

      <!-- Main content -->
      <section class="content">
        <form class="form-horizontal" method="post" action="{{route('authenticate')}}">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            @include('includes/notifications')
            <div class="box box-solid " style="text-align:center;align:justify;margin-top:100px">
              <div class="box-header with-border">
                <h4>LOGIN</h4>
              </div>
              <div class="box-body">
                <div class="form-group">

                  <label class="control-label col-xs-3" for="txtUsername">Username</label>
                  <div class="col-xs-6">
                    <input class="form-control" name="txtUsername" id="txtUsername" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-xs-3" for="txtPassword">Password</label>
                  <div class="col-xs-6">
                    <input class="form-control" name="txtPassword" id="txtPassword" type="password">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-xs-offset-4 col-xs-4">
                    <button class="btn btn-info" type="submit" name="submit">
                      <i class="fas fa-sign-in-alt"></i> Login
                    </button>
                  </div>
                </div>
              </div>

            </div>
          </div>



    </div>

    </form>
    </section>
    <!-- /.content -->

  </div>
  @include('partials.footer')


  </div>
  @routes
  <!-- ./wrapper -->

  <!-- jQuery 3 -->
  <script src="{{ asset('js/jquery.min.js' )}}"></script>
  <!-- <script src="{{ asset('js/jquery-3.4.1.min.js' )}}"></script> -->
  <!-- jQuery UI 1.11.4 -->
  <script src="{{ asset('js/jquery-ui.min.js' ) }}"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button);
  </script>
  <!-- Bootstrap 3.3.7 -->
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/bootstrap-table.min.js') }}"></script>
  <!-- Data tables -->
  <!-- <script src="{{ asset('js/mdb.min.js') }}"></script> -->
  <!-- <script src="{{ asset('js/datatables.min.js') }}"></script> -->
  <!-- Morris.js charts -->
  {{-- <script src="bower_components/raphael/raphael.min.js"></script>
    <script src="bower_components/morris.js/morris.min.js"></script> --}}
  <!-- Sparkline -->
  {{-- <script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script> --}}
  <!-- jvectormap -->
  {{-- <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script> --}}
  <!-- jQuery Knob Chart -->
  {{-- <script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script> --}}
  <!-- datertimepicker -->
  <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
  <!-- daterangepicker -->
  <script src="{{ asset('js/moment.min.js') }}"></script>
  {{-- <script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script> --}}
  <!-- datepicker -->
  <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
  <!-- Bootstrap WYSIHTML5 -->
  <script src="{{ asset('js/bootstrap3-wysihtml5.all.min.js') }}"></script>
  <!-- Slimscroll -->
  <script src="{{ asset('js/jquery.slimscroll.min.js') }}"></script>
  <!-- FastClick -->
  <script src="{{ asset('js/fastclick.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('js/adminlte.min.js' ) }}"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  {{-- <script src="dist/js/pages/dashboard.js"></script> --}}
  <!-- AdminLTE for demo purposes -->
  {{-- <script src="dist/js/demo.js"></script> --}}
  <script src="{{ asset('js/laroute.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/ziggy.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/route.js') }}" type="text/javascript"></script>
  @yield('footer_script')

</body>

</html>