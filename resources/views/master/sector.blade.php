@extends('layouts.default')

@section('content_header')
<h1>
    SECTOR
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Master</a></li>
    <li class="active">Sector</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@can('edit sector')
<a href="{{route('master.sector.add')}}" data-toggle="tooltip" title="Tambah Sector"><button type='button' class='btn btn-danger'><i class='fa fa-plus'></i> Buat Baru</button></a>
@endcan

<div class="portlet-body">
    <table class="table table-bordered" id="sectorTable" style='background-color: white'>
       
        

    </table>
</div>



@endsection

@section('footer_script')
<script>

$(document).ready(function () {
  
    $('[data-toggle="tooltip"]').tooltip();   

    function refresh() {
    $('#sectorTable').bootstrapTable('refresh',{
      url:route('master.sector.data')
    });
    
  }

    $('#sectorTable').bootstrapTable({
        url: route('master.sector.data'),
    pagination: true,
    sidePagination: 'server',
  search: true,
  columns: [{
    title: 'NO',
    formatter: function (value, row, index) {
        return index+1;
      }
  }, {
    field: 'sector',
    title: 'SEKTOR',
    sortable:true
  }, {
    field: 'alamat',
    title: 'ALAMAT',
    sortable:true,
    formatter: function(value, row, index) {
      return row.alamat + ' &nbsp;<i class="fa fa-window-restore" data-toggle="tooltip" title="'+row.keterangan+'"></i>';
    }
  },{
      title:'AKSI',
      formatter: function(value, row, index) {
        var btnString = '';

        @can('edit sector')
         btnString = '<a href="'+route('master.sector.ubah',{id:row.id_sector})+'" data-toggle="tooltip" title="Edit Sektor"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>'
         btnString +='<button type="button" class="btn btn-delete btn-xs btn-danger" title="delete sektor"><i class="fa fa-trash"></i></button></a>'
         @endcan
         
        return btnString;
      },
      events:{
        'click .btn-delete': function(e, value, row, index) {
          if (confirm('Are you sure you want to delete this record?')) {
            $.ajax({
              url: route('master.sector.delete',{id:row.id_sector}),
              data:{_token:$('meta[name="csrf_token"]').attr('content')},
              method:'post'
            })
            .then(function (response){
              if (response.status == 'error') {
                alert(response.description);
              } else if( response.status == 'success') {
                alert(response.description);
                refresh();
              }
            }); 
          }
        }
      }
    }]
})
});
    
</script>
@endsection
