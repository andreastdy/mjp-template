@extends('layouts.default')

@section('content_header')
<h1>
    CUSTOMER
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Master</a></li>
    <li class="active">Customer</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@can('edit customer')
<a href="{{route('master.customer.add')}}" data-toggle="tooltip" title="Tambah Customer"><button type='button' class='btn btn-danger'><i class='fa fa-plus'></i> Buat Baru</button></a>
@endcan

<div class="portlet-body">
    <table class="table table-bordered" id="customerTable" style='background-color: white'>
       
        

    </table>
</div>

<div id="DetailModal" class="modal fade" role="dialog" >
    <div class="modal-dialog" style="width:1200px;">

        <!-- Modal content-->
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">DETAIL CUSTOMER</h4>
            </div>
            <div class="modal-body">
            <table class="table table-bordered" id="detailCustomerTable" style='background-color: white'>
        <tr>
            <th>NAMA</th>
            <th >ALAMAT GUDANG 1 </th>
            <th >ALAMAT GUDANG 2 </th>
            <th >ALAMAT GUDANG 3 </th>
            <th >EMAIL 1 </th>
            <th >EMAIL 2 </th>
            <th >EMAIL 3 </th>
            <th >KETERANGAN </th>
        </tr>
        <tr>
            <td id="namapt"></td>
            <td id="gudang1"></td>
            <td id="gudang2"></td>
            <td id="gudang3"></td>
            <td id="email1"></td>
            <td id="email2"></td>
            <td id="email3"></td>
            <td id="keterangan"></td>
        </tr>
        

    </table>
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


@endsection

@section('footer_script')
<script>
$(document).ready(function () {
    function refresh() {
    $('#customerTable').bootstrapTable('refresh',{
      url:route('master.customer.data')
    });
    
  }

    $('#customerTable').bootstrapTable({
        url: route('master.customer.data'),
    pagination: true,
    sidePagination:'server',
  search: true,
  columns: [{
    title: 'NO',
    formatter: function (value, row, index) {
        return index+1;
      }
  }, {
    field: 'nama_pt',
    title: 'NAMA PT',
    sortable:true
  }, {
    field: 'name_short',
    title: 'SHORT NAME',
    sortable:true
  },{
    field: 'alamat_pt',
    title: 'ALAMAT',
    sortable:true
  },{
    field: 'nama_kota',
    title: 'KOTA',
    sortable:true
  },{
    field: 'kodepos',
    title: 'KODE POS',
    sortable:true
  },{
    field: 'telp',
    title: 'TELEPON',
    sortable:true
  },{
    field: 'name_cp',
    title: 'NAMA CP',
    sortable:true
  },{
    field: 'telp_cp',
    title: 'TELEPON CP',
    sortable:true
  },{
    field: 'lama_kredit',
    title: 'CREDIT DAYS',
    sortable:true
  },{
      title:'AKSI',
      formatter: function(value, row, index) {
        var btnString = '<a href="#" data-toggle="tooltip" title="View Detail"><button type="button" class="btn btn-primary btn-xs btn-show" data-toggle="modal" data-target="#DetailModal"><i class="fa fa-eye"></i></button></a>';
        
        @can('edit customer')
        btnString += '<a href="'+route('master.customer.ubah',{id:row.id_customer})+'" data-toggle="tooltip" title="Edit Customer"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>';
         btnString +='<button type="button" class="btn btn-delete btn-xs btn-danger" title="delete customer"><i class="fa fa-trash"></i></button></a>';
         @endcan
         
        return btnString;
      },
      events:{
        'click .btn-delete': function(e, value, row, index) {
          if (confirm('Are you sure you want to delete this record?')) {
            $.ajax({
              url: route('master.customer.delete',{id:row.id_customer}),
              data:{_token:$('meta[name="csrf_token"]').attr('content')},
              method:'post'
            })
            .then(function (response){
              if (response.status == 'error') {
                alert(response.description);
              } else if( response.status == 'success') {
                alert(response.description);
                refresh();
              }
            }); 
          }
        },
        'click .btn-show': function(e, value, row, index) {
          document.getElementById("namapt").innerHTML = row.nama_pt;
          document.getElementById("gudang1").innerHTML = row.alamat_gudang_1;
          document.getElementById("gudang2").innerHTML = row.alamat_gudang_2;
          document.getElementById("gudang3").innerHTML = row.alamat_gudang_3;
          document.getElementById("email1").innerHTML = row.email_1;
          document.getElementById("email2").innerHTML = row.email_2;
          document.getElementById("email3").innerHTML = row.email_3;
          document.getElementById("keterangan").innerHTML = row.keterangan;
        }
      }
    }]
})



});
</script>
@endsection
