@extends('layouts.default')

@section('content_header')
<h1>
    UPDATE HARGA TRUCKING 
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Master</a></li>
    <li class="active">Trucking</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<label class="control-label">ID TRUCKING : {{$trucking->id_trucking}}</label>
<Br/>
<label class="control-label">NAMA TRUCKING : {{$trucking->nama}}</label>
<Br/>
<hr>



    <table class="table table-bordered" id="hargaTruckingTable" style='background-color: white'>
        <thead>
        <tr>
            <th></th>
            <th>SEKTOR</th>
            <th>ALAMAT</th>
            <th>SIZE</th>
            <th style="text-align: right">HARGA</th>
        </tr>
        </thead>
        <tbody>
        @foreach($truckingdetail as $item)
        <tr>
            <td><input type="checkbox" name="chkHargaTrucking_{{$item->id_truckingdetail}}" checked/></td>
            <td><input type="hidden" name="idDetailTrucking" value="{{$item->id_truckingdetail}}"/>{{$item->Sector[0]->sector}}</td>
            <td>{{$item->Sector[0]->alamat}}</td>
            <td>{{$item->size}}</td>
                     
       <td> <input class="form-control input-element" name="txtHarga" id="txtHarga" type="text" value="{{$item->harga}}" style="text-align: right"></td>
        </tr>
        @endforeach
        </tbody>
  
    </table>
    <div class="form-group row">
    <div class="col-xs-offset-2 col-xs-6">
      <button class="btn btn-primary" type="submit" name="submit" onclick="submit()"><i class="fas fa-save"></i> Simpan</button>
      <a href="{{route('master.trucking.detail',$trucking->id_trucking)}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
    </div>
  </div>


@endsection

@section('footer_script')
<script>

    
    function submit(){
        var success = 0;
        var input = 0;
        var arr2=[];
        $("#hargaTruckingTable > tbody > tr").each(function (i, el) {
            var $tds = $(this).find('td');
            if($tds.eq(0).find('input').prop('checked') == true){
                // alert("masuk");
            input++;
            $iddetail = $tds.eq(1).find('input').val();
            $harga = $tds.eq(4).find('input').val();
            console.log($iddetail + " " + $harga);
            arr2.push(
            $.ajax({
              url: route('master.trucking.updateharga',{{$trucking->id_trucking}}),
              data:{_token:$('meta[name="csrf_token"]').attr('content'), iddetail:$iddetail, harga:$harga},
              method:'post'
            })
            .then(function (response){
              if (response.status == 'error') {
                alert(response.description);
              } else if( response.status == 'success') {
                success++;
              }
            })); 
            }
        });

        $.when.apply($, arr2).done(function () {
            console.log(input);
            console.log(success);
            if(input==success){
                alert("Data Successfully updated");
            }else{
                alert("Data Failed to procced");
            }
        });
    }
   
    
</script>
@endsection
