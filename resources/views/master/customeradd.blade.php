@extends('layouts.default')

@section('content_header')
<h1>
@if($mode == 'ADD')
    ADD CUSTOMER
@else
    EDIT CUSTOMER
@endif
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Master</a></li>
    <li class="active">Customer</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@if($mode == 'ADD')
<form action="{{ route('master.customer.create') }}" method="post" class="form-horizontal">
@else
<form action="{{ route('master.customer.update',$customer->id_customer) }}" method="post" class="form-horizontal">
@endif
<input type="hidden" name="_token" value="{{ csrf_token() }}">
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtName">Nama PT</label> 
    <div class="col-xs-6">
      <input required="required" class="form-control" name="txtName" id="txtName" type="text"  @if($mode == 'EDIT') value="{{$customer->nama_pt}}" @endif>
      @if($errors->has('txtName'))
      <span class="text-danger">{{$errors->first('txtName')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtShortName">Short Name</label> 
    <div class="col-xs-3">
      <input required="required" class="form-control" name="txtShortName" id="txtShortName" type="text" @if($mode == 'EDIT') value="{{$customer->name_short}}" @endif>
      @if($errors->has('txtShortName'))
      <span class="text-danger">{{$errors->first('txtShortName')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtAlamat">Alamat</label> 
    <div class="col-xs-4">
      <textarea required="required" class="form-control" rows="3" cols="40" name="txtAlamat" id="txtAlamat">@if($mode == 'EDIT') {{$customer->alamat_pt}} @endif</textarea>
      @if($errors->has('txtAlamat'))
      <span class="text-danger">{{$errors->first('txtAlamat')}}
      @endif
    </div>
  </div>
     
     <div class="form-group">
    <label class="control-label col-xs-2" for="txtAlamatGudang1">Alamat Gudang 1</label> 
    <div class="col-xs-4">
      <textarea required="required" class="form-control" rows="3" cols="40" name="txtAlamatGudang1" id="txtAlamatGudang1">@if($mode == 'EDIT') {{$customer->alamat_gudang_1}} @endif</textarea>
      @if($errors->has('txtAlamatGudang1'))
      <span class="text-danger">{{$errors->first('txtAlamatGudang1')}}
      @endif
    </div>
  </div>
     <div class="form-group">
    <label class="control-label col-xs-2" for="txtAlamatGudang2">Alamat Gudang 2<br/><span class="opsional"><sup>(opsional)</sup></span></label> 
    <div class="col-xs-4">
      <textarea class="form-control" rows="3" cols="40" name="txtAlamatGudang2" id="txtAlamatGudang2">@if($mode == 'EDIT') {{$customer->alamat_gudang_2}} @endif</textarea>
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtAlamatGudang3">Alamat Gudang 3<br/><span class="opsional"><sup>(opsional)</sup></span></label> 
    <div class="col-xs-4">
      <textarea class="form-control" rows="3" cols="40" name="txtAlamatGudang3" id="txtAlamatGudang3">@if($mode == 'EDIT') {{$customer->alamat_gudang_3}} @endif</textarea>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="cboKota">Kota</label> 
    <div class="col-xs-3">
    <select class="select form-control cboKota" name="cboKota" id="cboKota" style="width:100%">
    <option value="0">-- PILIH KOTA --</option>
    @foreach($city as $item)
      @if($mode == 'EDIT') 
        @if($customer->id_kota==$item->id_kota) 
        <option value="{{ $item->id_kota }}" selected>{{ $item->nama_kota }}</option>
        @else
        <option value="{{ $item->id_kota }}" >{{ $item->nama_kota }}</option>
        @endif
      @else
      <option value="{{ $item->id_kota }}" >{{ $item->nama_kota }}</option>
      @endif
    @endforeach
      </select>
      @if($errors->has('cboKota'))
      <span class="text-danger">{{$errors->first('cboKota')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtKodepos">Kode Pos</label> 
    <div class="col-xs-2">
      <input required="required" class="form-control" name="txtKodepos" id="txtKodepos" type="text" @if($mode == 'EDIT') value="{{$customer->kodepos}}" @endif>
      @if($errors->has('txtKodepos'))
      <span class="text-danger">{{$errors->first('txtKodepos')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtTelp">Telepon</label> 
    <div class="col-xs-6">
      <input required="required" class="form-control" name="txtTelp" id="txtTelp" type="text" @if($mode == 'EDIT') value="{{$customer->telp}}" @endif>
      @if($errors->has('txtTelp'))
      <span class="text-danger">{{$errors->first('txtTelp')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtNamaCP">Nama CP</label> 
    <div class="col-xs-6">
      <input required="required" class="form-control" name="txtNamaCP" id="txtNamaCP" type="text" @if($mode == 'EDIT') value="{{$customer->name_cp}}" @endif>
      @if($errors->has('txtNamaCP'))
      <span class="text-danger">{{$errors->first('txtNamaCP')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtTeleponCP">Telepon CP</label> 
    <div class="col-xs-6">
      <input required="required" class="form-control" name="txtTeleponCP" id="txtTeleponCP" type="text" @if($mode == 'EDIT') value="{{$customer->telp_cp}}" @endif>
      @if($errors->has('txtTeleponCP'))
      <span class="text-danger">{{$errors->first('txtTeleponCP')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtEmail1">Email 1</label> 
    <div class="col-xs-6">
      <input required="required" class="form-control" name="txtEmail1" id="txtEmail" type="text" @if($mode == 'EDIT') value="{{$customer->email_1}}" @endif>
      @if($errors->has('txtEmail'))
      <span class="text-danger">{{$errors->first('txtEmail')}}
      @endif
    </div>
  </div> 
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtEmail2">Email 2<br/><span class="opsional"><sup>(opsional)</sup></span></label> 
    <div class="col-xs-6">
      <input class="form-control" name="txtEmail2" id="txtEmail2" type="text" @if($mode == 'EDIT') value="{{$customer->email_2}}" @endif>
    </div>
  </div> 
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtEmail3">Email 3<br/><span class="opsional"><sup>(opsional)</sup></span></label> 
    <div class="col-xs-6">
      <input class="form-control" name="txtEmail3" id="txtEmail3" type="text" @if($mode == 'EDIT') value="{{$customer->email_3}}" @endif>
    </div>
  </div> 
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtLamaKredit">Lama Kredit</label> 
    <div class="col-xs-2">
    <div class="input-group">
      <input required="required" class="form-control" name="txtLamaKredit" id="txtLamaKredit" type="text" @if($mode == 'EDIT') value="{{$customer->lama_kredit}}" @endif>
      @if($errors->has('txtLamaKredit'))
      <span class="text-danger">{{$errors->first('txtLamaKredit')}}
      @endif
      <div class="input-group-addon">Hari</div> 
      
    </div>
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtKeterangan">Keterangan<br/><span class="opsional"><sup>(opsional)</sup></span></label> 
    
    <div class="col-xs-6">
    <textarea class="form-control" rows="3" cols="40" name="txtKeterangan" id="txtKeterangan">@if($mode == 'EDIT') {{$customer->keterangan}} @endif</textarea>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-xs-offset-2 col-xs-6">
      <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
      <a href="{{route('master.customer')}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button>
    </div>
  </div>
</form>



@endsection

@section('footer_script')
<script>
    $('.cboKota').select2();
</script>
@endsection
