@extends('layouts.default')

@section('content_header')
<h1>
    CONSIGNEE
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Master</a></li>
    <li class="active">Consignee</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@can('edit consignee')
<a href="{{route('master.consignee.add')}}" data-toggle="tooltip" title="Tambah Consignee"><button type='button' class='btn btn-danger'><i class='fa fa-plus'></i> Buat Baru</button></a>
@endcan

<div class="portlet-body">
    <table class="table table-bordered" id="consigneeTable" style='background-color: white'>
        
        

    </table>
</div>


@endsection

@section('footer_script')
<script>
$(document).ready(function () {
    function refresh() {
    $('#consigneeTable').bootstrapTable('refresh',{
      url:route('master.consignee.data')
    });
    
  }

    $('#consigneeTable').bootstrapTable({
        url: route('master.consignee.data'),
    pagination: true,
    sidePagination:'server',
  search: true,
  columns: [{
    title: 'NO',
    formatter: function (value, row, index) {
        return index+1;
      }
  }, {
    field: 'nama',
    title: 'NAMA CONSIGNEE',
    sortable:true
  }, {
    field: 'alamat',
    title: 'ALAMAT',
    sortable:true
  },{
    field: 'nama_kota',
    title: 'KOTA',
    sortable:true
  },{
    field: 'telp',
    title: 'TELEPON',
    sortable:true
  },{
    field: 'name_cp',
    title: 'NAMA CP',
    sortable:true
  },{
    field: 'telp_cp',
    title: 'TELP CP',
    sortable:true
  },{
    field: 'keterangan',
    title: 'KETERANGAN',
    sortable:true
  },{
      title:'AKSI',
      formatter: function(value, row, index) {
        var btnString = "";

        @can('edit consignee')
         btnString = '<a href="'+route('master.consignee.ubah',{id:row.id_consignee})+'" data-toggle="tooltip" title="Edit Consignee"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>'
         btnString +='<button type="button" class="btn btn-delete btn-xs btn-danger" title="Delete Consignee"><i class="fa fa-trash"></i></button></a>';
         @endcan
         
        return btnString;
      },
      events:{
        'click .btn-delete': function(e, value, row, index) {
          if (confirm('Are you sure you want to delete this record?')) {
            $.ajax({
              url: route('master.consignee.delete',{id:row.id_consignee}),
              data:{_token:$('meta[name="csrf_token"]').attr('content')},
              method:'post'
            })
            .then(function (response){
              if (response.status == 'error') {
                alert(response.description);
              } else if( response.status == 'success') {
                alert(response.description);
                refresh();
              }
            }); 
          }
        }
      }
    }]
})
});
</script>
@endsection
