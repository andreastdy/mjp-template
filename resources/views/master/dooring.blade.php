@extends('layouts.default')

@section('content_header')
<h1>
    DOORING
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Master</a></li>
    <li class="active">Dooring</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@can('edit dooring')
<a href="{{route('master.dooring.add')}}" data-toggle="tooltip" title="Tambah Dooring"><button type='button' class='btn btn-danger'><i class='fa fa-plus'></i> Buat Baru</button></a>
@endcan

<div class="portlet-body">
    <table class="table table-bordered" id="dooringTable" style='background-color: white'>

    </table>
</div>



@endsection

@section('footer_script')
<script>

$(document).ready(function () {
    $('#dooringTable').bootstrapTable({
        classes:'table table-hover table-no-bordered',
        url: route('master.dooring.data'),
    pagination: true,
    sidePagination:'server',
  search: true,
  columns: [{
    title: 'NO',
    formatter: function (value, row, index) {
        return index+1;
      }
  }, {
    field: 'nama',
    title: 'NAMA DOORING',
    sortable: true
  }, {
    field: 'alamat',
    title: 'ALAMAT',
    sortable: true
  },{
    field: 'nama_kota',
    title: 'KOTA',
    sortable: true
  },{
    field: 'rute',
    title: 'RUTE',
    sortable: true
  },{
    field: 'rekening',
    title: 'REKENING',
    sortable: true
  },{
    field: 'name_cp',
    title: 'NAMA CP',
    sortable: true
  },{
    field: 'telp_cp',
    title: 'TELEPON CP',
    sortable: true
  },{
    field: 'email',
    title: 'EMAIL',
    sortable: true
  },{
      title:'AKSI',
      formatter: function(value, row, index) {
        var btnString = "";

        @can('edit dooring')
         btnString = '<a href="'+route('master.dooring.ubah',{id:row.id_dooring})+'" data-toggle="tooltip" title="Edit Pelayaran"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>';
         @endcan
         btnString +='<a href="'+route('master.dooring.detail',{id:row.id_dooring})+'" data-toggle="tooltip" title="View Detail"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></button></a>';
         

        return btnString;
      }
    }]
})
});

</script>
@endsection
