@extends('layouts.default')

@section('content_header')
<h1>
  @if ($mode == 'ADD')
  ADD SECTOR
  @else
  EDIT SECTOR
  @endif
</h1>
<ol class="breadcrumb">
  <li><a href="#"> Master</a></li>
  <li class="active">Sector</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@if ($mode == 'ADD')
<form action="{{ route('master.sector.create') }}" method="post" class="form-horizontal">
  @else
  <form action="{{ route('master.sector.update',$sector->id_sector) }}" method="post" class="form-horizontal">
    @endif
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
      <label class="control-label col-xs-2" for="txtSector">Nama Sector</label>
      <div class="col-xs-2">
        <input required="required" class="form-control" name="txtSector" id="txtSector" type="text" @if($mode=='EDIT' ) value="{{$sector->sector}}" @endif>
        @if($errors->has('txtSector'))
      <span class="text-danger">{{$errors->first('txtSector')}}
      @endif
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-xs-2" for="txtAlamat">Alamat</label>
      <div class="col-xs-4">
        <textarea required="required" class="form-control" rows="3" cols="40" name="txtAlamat" id="txtAlamat">@if($mode == 'EDIT') {{$sector->alamat}} @endif</textarea>
        @if($errors->has('txtAlamat'))
      <span class="text-danger">{{$errors->first('txtAlamat')}}
      @endif
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-xs-2" for="txtKeterangan">Keterangan<br/><span class="opsional"><sup>(opsional)</sup></span></label>
      <div class="col-xs-4">
        <textarea class="form-control" rows="3" cols="40" name="txtKeterangan" id="txtKeterangan">@if($mode == 'EDIT') {{$sector->keterangan}} @endif</textarea>
      </div>
    </div>



    <div class="form-group row">
      <div class="col-xs-offset-2 col-xs-6">
        <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
        <a href="{{route('master.sector')}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button>
      </div>
    </div>
  </form>



  @endsection

  @section('footer_script')
  <script>

  </script>
  @endsection