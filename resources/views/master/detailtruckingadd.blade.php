@extends('layouts.default')

@section('content_header')
<h1>
@if($mode == 'ADD')
    ADD DETAIL TRUCKING 
@else
    EDIT DETAIL TRUCKING
@endif
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Master</a></li>
    <li class="active">Trucking</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@if($mode=='EDIT')
<form action="{{ route('master.trucking.detailupdate',$truckingdetail->id_truckingdetail) }}" method="post" class="form-horizontal">
@else
<form action="{{ route('master.trucking.detailcreate',$trucking->id_trucking) }}" method="post" class="form-horizontal">
@endif
<input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
    <label class="control-label col-xs-2" for="di">ID Trucking</label> 
    <div class="col-xs-4">
      <label class="checkbox-inline">
      @if($mode=='ADD')
        {{$trucking->id_trucking}}
      @else
      {{$truckingdetail->Trucking->id_trucking}}
      @endif
      </label>
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="chkPortToDoor">Nama Trucking</label> 
    <div class="col-xs-4">
      <label class="checkbox-inline">
      @if($mode=='ADD')
        {{$trucking->nama}}
      @else
      {{$truckingdetail->Trucking->nama}}
      @endif
      </label>
    </div>
  </div>
  
  <div class="form-group">
    <label class="control-label col-xs-2" for="cboSector">Sektor</label> 
    <div class="col-xs-4">
      <select class="select form-control cboSector" name="cboSector" id="cboSector" onchange="changeSector()">
      <option value="0">-- PILIH SEKTOR --</option>
      @foreach($sector as $item)
      @if($mode == 'EDIT') 
        @if($truckingdetail->id_sector==$item->id_sector) 
        <option value="{{ $item->id_sector }}|{{ $item->alamat }}" selected>{{ $item->sector }}</option>
        @else
        <option value="{{ $item->id_sector }}|{{ $item->alamat }}">{{ $item->sector }}</option>
        @endif
      @else
      <option value="{{ $item->id_sector }}|{{ $item->alamat }}">{{ $item->sector }}</option>
      @endif
    @endforeach
      </select>
      @if($errors->has('cboSector'))
      <span class="text-danger">{{$errors->first('cboSector')}}
      @endif
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtAlamat">Alamat</label> 
    <div class="col-xs-4">
      <textarea class="form-control" rows="3" cols="40" name="txtAlamat" id="txtAlamat" readonly>@if($mode == 'EDIT') {{$truckingdetail->alamat}} @endif</textarea>
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtKeterangan">Keterangan<br/><span class="opsional"><sup>(opsional)</sup></span></label> 
    <div class="col-xs-4">
      <textarea class="form-control" rows="3" cols="40" name="txtKeterangan" id="txtKeterangan">@if($mode == 'EDIT') {{$truckingdetail->keterangan}} @endif</textarea>
    </div>
  </div>
    
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtHarga">Harga</label> 
    <div class="col-xs-4">
      <div class="input-group">
        <div class="input-group-addon">Rp.</div> 
        <input class="form-control input-element" name="txtHarga" id="txtHarga" type="text" style="text-align: right" @if($mode == 'EDIT') value="{{$truckingdetail->harga}}" @else value="0" @endif>
        @if($errors->has('txtHarga'))
      <span class="text-danger">{{$errors->first('txtHarga')}}
      @endif
      </div>
    </div>
  </div> 
  <div class="form-group">
    <label class="control-label col-xs-2" for="cboSize">Size</label> 
    <div class="col-xs-4">
      <select class="select form-control" name="cboSize" id="cboSize">
        <option value="20" @if($mode == 'EDIT') @if($truckingdetail->size == '20') selected @endif @endif>20</option>
        <option value="40" @if($mode == 'EDIT') @if($truckingdetail->size == '40') selected @endif @endif>40</option>
        <option value="lcl" @if($mode == 'EDIT') @if($truckingdetail->size == 'lcl') selected @endif @endif>LCL</option>
        <option value="combo" @if($mode == 'EDIT') @if($truckingdetail->size == 'combo') selected @endif @endif>Combo</option>
      </select>
      @if($errors->has('cboSize'))
      <span class="text-danger">{{$errors->first('cboSize')}}
      @endif
    </div>
  </div>
  
   
  <div class="form-group row">
    <div class="col-xs-offset-2 col-xs-4">
      <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
      @if($mode=='ADD')
      <a href="{{route('master.trucking.detail',$trucking->id_trucking)}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
      @else
      <a href="{{route('master.trucking.detail',$truckingdetail->Trucking->id_trucking)}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
      @endif
    </div>
  </div>
</form>

@endsection

@section('footer_script')
<script>
     function changeSector(){
          var str = document.getElementById("cboSector").value;
          var res = str.split("|");
          if(res[0]=="0"){
            document.getElementById("txtAlamat").value = "";
          }else{
            document.getElementById("txtAlamat").value = res[1];
          }
          
        }

    $(document).ready(function () {
      $( "#cboSector" ).trigger( "change" );
      $(".cboSector").select2();
    });
    
    var cleave = new Cleave('.input-element', {
    numeral: true,
    numeralThousandsGroupStyle: 'thousand',
    autoUnmask : true
});
</script>
@endsection
