@extends('layouts.default')

@section('content_header')
<h1>
@if($mode == 'ADD')
    ADD BARANG
@else
    EDIT BARANG
@endif
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Master</a></li>
    <li class="active">Barang</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@if($mode == 'ADD')
<form action="{{ route('master.barang.create') }}" method="post" class="form-horizontal" >
@else
<form action="{{ route('master.barang.update',$barang->id_barang) }}" method="post" class="form-horizontal" >
@endif
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="form-group">
    <label class="control-label col-xs-2" for="txtKodeBarang">Kode Barang</label> 
    <div class="col-xs-2">
      <input required="required" class="form-control" name="txtKodeBarang" id="txtKodeBarang" type="text" @if($mode == 'EDIT') value="{{$barang->kode_barang}}" @endif>
      @if($errors->has('txtKodeBarang'))
      <span class="text-danger">{{$errors->first('txtKodeBarang')}}
      @endif
    </div>
  </div>
 <div class="form-group">
    <label class="control-label col-xs-2" for="txtNamaBarang">Nama</label> 
    <div class="col-xs-4">
      <input required="required" class="form-control" name="txtNamaBarang" id="txtNamaBarang" type="text" @if($mode == 'EDIT') value="{{$barang->nama_barang}}" @endif>
      @if($errors->has('txtNamaBarang'))
      <span class="text-danger">{{$errors->first('txtNamaBarang')}}
      @endif
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtSatuan">Satuan</label> 
    <div class="col-xs-2">
      <input required="required" class="form-control" name="txtSatuan" id="txtSatuan" type="text" @if($mode == 'EDIT') value="{{$barang->satuan}}" @endif>
      @if($errors->has('txtSatuan'))
      <span class="text-danger">{{$errors->first('txtSatuan')}}
      @endif
    </div>
  </div>
  
  
    
  <div class="form-group row">
    <div class="col-xs-offset-2 col-xs-6">
      <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
      <a href="{{route('master.barang')}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button>
    </div>
  </div>
</form>



@endsection

@section('footer_script')
<script>
    
  
</script>
@endsection
