@extends('layouts.default')

@section('content_header')

<h1>
@if($mode=='ADD')
    ADD PELAYARAN 
@else
    EDIT PELAYARAN 
@endif
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Master</a></li>
    <li class="active">Pelayaran</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@if($mode=='EDIT')
<form action="{{ route('master.pelayaran.update',$pelayaran->id_pelayaran) }}" method="post" class="form-horizontal">
@else
<form action="{{ route('master.pelayaran.create') }}" method="post" class="form-horizontal">
@endif
<input type="hidden" name="_token" value="{{ csrf_token() }}">
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtShortname">Shortname</label> 
    <div class="col-xs-6">
      <input required="required" class="form-control" name="txtShortname" id="txtShortname" type="text"  @if($mode == 'EDIT') value="{{$pelayaran->name_short}}" @endif>
      @if($errors->has('txtShortname'))
      <span class="text-danger">{{$errors->first('txtShortname')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtFullname">Fullname</label> 
    <div class="col-xs-6">
      <input required="required" class="form-control" name="txtFullname" id="txtFullname" type="text" @if($mode == 'EDIT') value="{{$pelayaran->name_full}}" @endif>
      @if($errors->has('txtFullname'))
      <span class="text-danger">{{$errors->first('txtFullname')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtAlamat">Alamat</label> 
    <div class="col-xs-4">
      <textarea required="required" class="form-control" rows="3" cols="40" name="txtAlamat"  id="txtAlamat">@if($mode == 'EDIT') {{$pelayaran->alamat}} @endif</textarea>
      @if($errors->has('txtAlamat'))
      <span class="text-danger">{{$errors->first('txtAlamat')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="cboKota">Kota</label> 
    <div class="col-xs-3">
    <select class="select form-control cboKota" name="cboKota" id="cboKota" style="width:100%">
  <option value="0">-- PILIH KOTA --</option>
    @foreach($city as $item)
      @if($mode == 'EDIT') 
        @if($pelayaran->id_kota==$item->id_kota) 
        <option value="{{ $item->id_kota }}" selected>{{ $item->nama_kota }}</option>
        @else
        <option value="{{ $item->id_kota }}" >{{ $item->nama_kota }}</option>
        @endif
      @else
      <option value="{{ $item->id_kota }}" >{{ $item->nama_kota }}</option>
      @endif
    @endforeach
      </select>
      @if($errors->has('cboKota'))
      <span class="text-danger">{{$errors->first('cboKota')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtKodepos">Kode Pos</label> 
    <div class="col-xs-2">
      <input required="required" class="form-control" name="txtKodepos" id="txtKodepos" type="text"  @if($mode == 'EDIT') value="{{$pelayaran->kodepos}}" @endif>
      @if($errors->has('txtKodepos'))
      <span class="text-danger">{{$errors->first('txtKodepos')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtTelepon">Telepon</label> 
    <div class="col-xs-6">
      <input required="required" class="form-control" name="txtTelepon" id="txtTelepon" type="text"  @if($mode == 'EDIT') value="{{$pelayaran->telp}}" @endif>
      @if($errors->has('txtTelepon'))
      <span class="text-danger">{{$errors->first('txtTelepon')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtNamaCP">Nama CP</label> 
    <div class="col-xs-6">
      <input required="required" class="form-control" name="txtNamaCP" id="txtNamaCP" type="text"  @if($mode == 'EDIT') value="{{$pelayaran->name_cp}}" @endif>
      @if($errors->has('txtNamaCP'))
      <span class="text-danger">{{$errors->first('txtNamaCP')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtTeleponCP">Telepon CP</label> 
    <div class="col-xs-6">
      <input required="required" class="form-control" name="txtTeleponCP" id="txtTeleponCP" type="text"  @if($mode == 'EDIT') value="{{$pelayaran->telp_cp}}" @endif>
      @if($errors->has('txtTeleponCP'))
      <span class="text-danger">{{$errors->first('txtTeleponCP')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtEmail">Email</label> 
    <div class="col-xs-6">
      <input required="required" class="form-control" name="txtEmail" id="txtEmail" type="text"  @if($mode == 'EDIT') value="{{$pelayaran->email}}" @endif>
      @if($errors->has('txtEmail'))
      <span class="text-danger">{{$errors->first('txtEmail')}}
      @endif
    </div>
  </div> 
  <div class="form-group row">
    <div class="col-xs-offset-2 col-xs-6">
      <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
      <a href="{{route('master.pelayaran')}}"><button class="btn btn-danger" type="button" name="cancel" >Cancel</button></a>
    </div>
  </div>
</form>



@endsection

@section('footer_script')
<script>
    $(".cboKota").select2();
</script>
@endsection
