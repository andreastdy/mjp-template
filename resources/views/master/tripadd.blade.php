@extends('layouts.default')

@section('content_header')
<h1>
  @if($mode == 'ADD')
  ADD TRIP
  @else
  EDIT TRIP
  @endif
</h1>
<ol class="breadcrumb">
  <li><a href="#"> Master</a></li>
  <li class="active">Trip</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@if($mode == 'ADD')
<form action="{{ route('master.trip.create') }}" method="post" class="form-horizontal">
  @else
  <form action="{{ route('master.trip.update',$trip->id_trip) }}" method="post" class="form-horizontal">
    @endif
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
      <label class="control-label col-xs-2" for="cboKodeTripKota">Kode Trip</label>
      <div class="col-xs-4">
        <select class="select form-control cboKodeTripKota" name="cboKodeTripKota" id="cboKodeTripKota" style="width:50%" @if($mode=='EDIT' ) readonly @endif>
        <option value="0">-- PILIH KODE TRIP --</option>
          @foreach($city as $item)
          @if($mode == 'EDIT')
          @if($trip->kode_trip_kota==$item->kode_kota)
          <option value="{{ $item->kode_kota }}" selected>{{ $item->kode_kota }}</option>
          @else
          <option value="{{ $item->kode_kota }}">{{ $item->kode_kota }}</option>
          @endif
          @else
          <option value="{{ $item->kode_kota }}">{{ $item->kode_kota }}</option>
          @endif
          @endforeach
        </select>
        @if($errors->has('cboKodeTripKota'))
      <span class="text-danger">{{$errors->first('cboKodeTripKota')}}
      @endif
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-xs-2" for="txtNamaKapal">Nama Kapal</label>
      <div class="col-xs-4">
        <input required="required" class="form-control" name="txtNamaKapal" id="txtNamaKapal" type="text" @if($mode=='EDIT' ) value="{{$trip->nama_kapal}}" @endif>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-xs-2" for="txtTglBerangkat">Tanggal Berangkat</label>
      <div class="col-xs-2">
        <input required="required" class="form-control" name="txtTglBerangkat" id="txtTglBerangkat" type="text" @if($mode=='EDIT' ) value="{{Carbon\Carbon::parse($trip->tanggal_berangkat)->format('d-m-Y H:i:s')}}" @endif>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-xs-2" for="cboPelabuhanTujuan">Kota Pelabuhan Tujuan</label>
      <div class="col-xs-4">
        <select class="select form-control cboPelabuhanTujuan" name="cboPelabuhanTujuan" id="cboPelabuhanTujuan" style="width:100%">
        <option value="0">-- PILIH KOTA PELABUHAN --</option>
          @foreach($pelabuhan as $item)
          @if($mode == 'EDIT')
          @if($trip->id_pelabuhan_tujuan==$item->id_pelabuhan)
          <option value="{{ $item->id_pelabuhan }}" selected>{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}</option>
          @else
          <option value="{{ $item->id_pelabuhan }}">{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}</option>
          @endif
          @else
          <option value="{{ $item->id_pelabuhan }}">{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}</option>
          @endif
          @endforeach
        </select>
        @if($errors->has('cboPelabuhanTujuan'))
      <span class="text-danger">{{$errors->first('cboPelabuhanTujuan')}}
      @endif
      </div>
    </div>

    <div class="form-group row">
      <div class="col-xs-offset-2 col-xs-5">
        <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
        <a href="{{route('master.trip')}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
      </div>
    </div>
  </form>



  @endsection

  @section('footer_script')
  <script>
    $('#txtTglBerangkat').datetimepicker({
      minView: 2,
      format: 'dd-mm-yyyy',
      autoclose: true,
      todayBtn: true
    });

    $('.cboKodeTripKota').select2();
    $('.cboPelabuhanTujuan').select2();
  </script>
  @endsection