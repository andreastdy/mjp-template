@extends('layouts.default')

@section('content_header')
<h1>
  TRIP
</h1>
<ol class="breadcrumb">
  <li><a href="#"> Master</a></li>
  <li class="active">Trip</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@can('edit trip')
<a href="{{route('master.trip.add')}}" data-toggle="tooltip" title="Tambah Trip"><button type='button' class='btn btn-danger'><i class='fa fa-plus'></i> Buat Baru</button></a>
@endcan

<div class="portlet-body">
  <table class="table table-bordered" id="tripTable" style='background-color: white'>



  </table>
</div>



@endsection

@section('footer_script')
<script>
  function refresh() {
    $('#tripTable').bootstrapTable('refresh', {
      url: route('master.trip.data'),
    });
  }

  $(document).ready(function() {

    $('#tripTable').bootstrapTable({
      classes: 'table table-hover table-no-bordered',
      url: route('master.trip.data'),
      pagination: true,
      sidePagination: 'server',
      search: true,
      columns: [{
        title: 'NO',
        formatter: function(value, row, index) {
          return index + 1;
        }
      }, {
        field: 'kode_trip',
        title: 'KODE TRIP',
        sortable: true
      }, {
        field: 'nama_kapal',
        title: 'NAMA KAPAL',
        sortable: true
      }, {
        title: 'TGL BERANGKAT',
        field: 'tanggal_berangkat',
        formatter: function(value, row, index) {
          return moment(value).format('DD-MM-YYYY');
        },
        sortable: true
      }, {
        field: 'nama_kota_tujuan',
        title: 'KOTA PELABUHAN TUJUAN',
        sortable: true
      }, {
        field: 'nama_pelabuhan_tujuan',
        title: 'NAMA PELABUHAN TUJUAN',
        sortable: true
      }, {
        title: 'STATUS',
        field: 'status',
        formatter: function(value, row, index) {
          if (value == "0") {
            return "ACTIVE";
          } else {
            return "COMPLETE";
          }

        },
        sortable: true
      }, {
        title: 'AKSI',
        formatter: function(value, row, index) {
          var btnString = '';
          if (row.status == "0") {
            @can('edit trip')
             btnString = '<a href="' + route('master.trip.ubah', {
              id: row.id_trip
            }) + '" data-toggle="tooltip" title="Edit Trip"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>';
            @endcan

            @can('open akses trip')
            btnString += '<button type="button" class="btn btn-success btn-xs" title="Complete Trip"><i class="fa fa-check"></i></button></a>';
            @endcan
          } else {
            @can('open akses trip')
             btnString = '<button type="button" class="btn btn-reopen btn-primary btn-xs" title="Open Trip"><i class="fa fa-history"></i></button></a>';
            @endcan

          }
          return btnString;
        },
        events: {
          'click .btn-success': function(e, value, row, index) {
            if (confirm('Anda yakin akan menyelesaikan trip ini?')) {
              $.ajax({
                  url: route('master.trip.complete', {
                    id: row.id_trip
                  }),
                  data: {
                    _token: $('meta[name="csrf_token"]').attr('content')
                  },
                  method: 'post'
                })
                .then(function(response) {
                  if (response.status == 'error') {
                    alert(response.description);
                  } else if (response.status == 'success') {
                    alert(response.description);
                    refresh();
                  }
                });
            }
          },
          'click .btn-reopen': function(e, value, row, index) {
            if (confirm('Anda yakin akan membuka kembali trip ini?')) {
              $.ajax({
                  url: route('master.trip.reopen', {
                    id: row.id_trip
                  }),
                  data: {
                    _token: $('meta[name="csrf_token"]').attr('content')
                  },
                  method: 'post'
                })
                .then(function(response) {
                  if (response.status == 'error') {
                    alert(response.description);
                  } else if (response.status == 'success') {
                    alert(response.description);
                    refresh();
                  }
                });
            }
          }
        }

      }]
    })
  });
</script>
@endsection