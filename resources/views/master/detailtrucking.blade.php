@extends('layouts.default')

@section('content_header')
<h1>
    TRUCKING
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Master</a></li>
    <li class="active">Trucking</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<label class="control-label">ID TRUCKING : {{$trucking->id_trucking}}</label>
<Br/>
<label class="control-label">NAMA TRUCKING : {{$trucking->nama}}</label>

<Br/>
<a href="{{route('master.trucking')}}" data-toggle="tooltip" title="Back"><button type='button' class='btn btn-primary'><i class='fa fa-reply'></i> Kembali</button></a>&nbsp;
@can('edit trucking')
<a href="{{route('master.trucking.detailadd',$trucking->id_trucking)}}" data-toggle="tooltip" title="Add Detail"><button type='button' class='btn btn-danger'><i class='fa fa-plus'></i> Buat Baru</button></a>
&nbsp;<a href="{{route('master.trucking.updateharga',$trucking->id_trucking)}}" data-toggle="tooltip" title="Update Harga"><button type='button' class='btn btn-success'><i class='fas fa-list-alt'></i>&nbsp;UPDATE HARGA</button></a>
@endcan

<div class="portlet-body">
    <table class="table table-bordered" id="detailTruckingTable" style='background-color: white'>
        
        

    </table>
</div>



@endsection

@section('footer_script')
<script>

$(document).ready(function () {
    function refresh() {
    $('#detailTruckingTable').bootstrapTable('refresh',{
      url:route('master.trucking.datadetail',{{$trucking->id_trucking}})
    });
    
  }

    $('#detailTruckingTable').bootstrapTable({
        classes:'table table-hover table-no-bordered',
        url: route('master.trucking.datadetail',{{$trucking->id_trucking}}),
    pagination: true,
    // sidePagination:'server',
  search: true,
  columns: [{
    title: 'NO',
    formatter: function (value, row, index) {
        return index+1;
      }
  }, {
    field: 'sector.0.sector',
    title: 'SECTOR',
    sortable:true
  }, {
    field: 'sector.0.alamat',
    title: 'ALAMAT',
    sortable:true
  },{
    field: 'keterangan',
    title: 'KETERANGAN',
    sortable:true
  },{
    field: 'size',
    title: 'SIZE',
    sortable:true
  },{
    title: 'HARGA',
    field: 'harga',
    align:'right',
    sortable : true,
    formatter: function (value, row, index) {
        return addCommas(row.harga.toString());
    },
    sortable:true
  },{
      title:'AKSI',
      formatter: function(value, row, index) {
        var btnString = '';

        @can('edit trucking')
         btnString = '<a href="'+route('master.trucking.ubahdetail',{id:row.id_truckingdetail})+'" data-toggle="tooltip" title="Edit Trucking Detail"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>'
        btnString +='<button type="button" class="btn btn-delete btn-xs btn-danger" title="Delete Trucking Detail"><i class="fa fa-trash"></i></button></a>'
        @endcan

        return btnString;
      },
      events:{
        'click .btn-delete': function(e, value, row, index) {
          if (confirm('Are you sure you want to delete this record?')) {
            $.ajax({
              url: route('master.trucking.detaildelete',{id:row.id_truckingdetail}),
              data:{_token:$('meta[name="csrf_token"]').attr('content')},
              method:'post'
            })
            .then(function (response){
              if (response.status == 'error') {
                alert(response.description);
              } else if( response.status == 'success') {
                alert(response.description);
                refresh();
              }
            }); 
          }
        }
      }
    }]
})
});

    
</script>
@endsection
