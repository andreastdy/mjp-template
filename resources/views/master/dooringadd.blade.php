@extends('layouts.default')

@section('content_header')
<h1>
@if($mode == 'ADD') 
    ADD DOORING
@else
    EDIT DOORING
@endif
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Master</a></li>
    <li class="active">Dooring</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@if($mode == 'ADD') 
<form action="{{ route('master.dooring.create') }}" method="post" class="form-horizontal">
@else
<form action="{{ route('master.dooring.update',$dooring->id_dooring) }}" method="post" class="form-horizontal">
@endif
<input type="hidden" name="_token" value="{{ csrf_token() }}">
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtName">Nama PT</label> 
    <div class="col-xs-6">
      <input required="required" class="form-control" name="txtName" id="txtName" type="text" @if($mode == 'EDIT') value="{{$dooring->nama}}" @endif>
      @if($errors->has('txtName'))
      <span class="text-danger">{{$errors->first('txtName')}}
      @endif
    </div>
  </div>
 <div class="form-group">
    <label class="control-label col-xs-2" for="txtAlamat">Alamat</label> 
    <div class="col-xs-4">
      <textarea required="required" class="form-control" rows="3" cols="40" name="txtAlamat" id="txtAlamat">@if($mode == 'EDIT') {{$dooring->alamat}} @endif</textarea>
      @if($errors->has('txtAlamat'))
      <span class="text-danger">{{$errors->first('txtAlamat')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="cboKota">Kota</label> 
    <div class="col-xs-3">
    <select class="select form-control cboKota" name="cboKota" id="cboKota" style="width:100%">
    <option value="0">-- PILIH KOTA --</option>
    @foreach($city as $item)
      @if($mode == 'EDIT') 
        @if($dooring->id_kota==$item->id_kota) 
        <option value="{{ $item->id_kota }}" selected>{{ $item->nama_kota }}</option>
        @else
        <option value="{{ $item->id_kota }}" >{{ $item->nama_kota }}</option>
        @endif
      @else
      <option value="{{ $item->id_kota }}" >{{ $item->nama_kota }}</option>
      @endif
    @endforeach
      </select>
      @if($errors->has('cboKota'))
      <span class="text-danger">{{$errors->first('cboKota')}}
      @endif
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtRute">Rute</label> 
    <div class="col-xs-4">
      <textarea required="required" class="form-control" rows="3" cols="40" name="txtRute" id="txtRute">@if($mode == 'EDIT') {{$dooring->rute}} @endif</textarea>
      @if($errors->has('txtRute'))
      <span class="text-danger">{{$errors->first('txtRute')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtRekening">Rekening</label> 
    <div class="col-xs-5">
      <input required="required" class="form-control" name="txtRekening" id="txtRekening" type="text" @if($mode == 'EDIT') value="{{$dooring->rekening}}" @endif>
      @if($errors->has('txtRekening'))
      <span class="text-danger">{{$errors->first('txtRekening')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtNamaCP">Nama CP</label> 
    <div class="col-xs-6">
      <input required="required" class="form-control" name="txtNamaCP" id="txtNamaCP" type="text" @if($mode == 'EDIT') value="{{$dooring->name_cp}}" @endif>
      @if($errors->has('txtNamaCP'))
      <span class="text-danger">{{$errors->first('txtNamaCP')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtTeleponCP">Telepon CP</label> 
    <div class="col-xs-3">
      <input required="required" class="form-control" name="txtTeleponCP" id="txtTeleponCP" type="text" @if($mode == 'EDIT') value="{{$dooring->telp_cp}}" @endif>
      @if($errors->has('txtTeleponCP'))
      <span class="text-danger">{{$errors->first('txtTeleponCP')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtEmail">Email</label> 
    <div class="col-xs-4">
      <input required="required" class="form-control" name="txtEmail" id="txtEmail" type="text" @if($mode == 'EDIT') value="{{$dooring->email}}" @endif>
      @if($errors->has('txtEmail'))
      <span class="text-danger">{{$errors->first('txtEmail')}}
      @endif
    </div>
  </div> 
    
  <div class="form-group row">
    <div class="col-xs-offset-2 col-xs-6">
      <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
      <a href="{{route('master.dooring')}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
    </div>
  </div>
</form>



@endsection

@section('footer_script')
<script>
    $('.cboKota').select2();
   
</script>
@endsection
