@extends('layouts.default')

@section('content_header')
<h1>
  BARANG
</h1>
<ol class="breadcrumb">
  <li><a href="#"> Master</a></li>
  <li class="active">Barang</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@can('edit barang')
<a href="{{route('master.barang.add')}}" data-toggle="tooltip" title="Tambah Barang"><button type='button' class='btn btn-danger'><i class='fa fa-plus'></i> Buat Baru</button></a>
@endcan
<br />
<br />
<div class="portlet-body">
  <table class="table table-bordered" id="barangTable" style='background-color: white'>

  </table>
</div>



@endsection

@section('footer_script')
<script>
  $(document).ready(function() {
    function refresh() {
      $('#barangTable').bootstrapTable('refresh', {
        url: route('master.barang.data')
      });

    }

    $('#barangTable').bootstrapTable({
      url: route('master.barang.data'),
      pagination: true,
      sidePagination: 'server',
      search: true,
      columns: [{
        title: 'NO',
        formatter: function(value, row, index) {
          return index + 1;
        }
      }, {
        field: 'kode_barang',
        title: 'KODE BARANG',
        sortable: true
      }, {
        field: 'nama_barang',
        title: 'NAMA BARANG',
        sortable: true
      }, {
        field: 'satuan',
        title: 'SATUAN',
        sortable: true
      }, {
        title: 'AKSI',
        formatter: function(value, row, index) {
          var btnString = "";

          @can('edit barang')
           btnString = '<a href="' + route('master.barang.ubah', {
            id: row.id_barang
          }) + '" data-toggle="tooltip" title="Edit Barang"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>'
          btnString += '<button type="button" class="btn btn-delete btn-xs btn-danger" title="delete barang"><i class="fa fa-trash"></i></button></a>'
          @endcan

          return btnString;
        },
        events: {
          'click .btn-delete': function(e, value, row, index) {
            if (confirm('Are you sure you want to delete this record?')) {
              $.ajax({
                  url: route('master.barang.delete', {
                    id: row.id_barang
                  }),
                  data: {
                    _token: $('meta[name="csrf_token"]').attr('content')
                  },
                  method: 'post'
                })
                .then(function(response) {
                  if (response.status == 'error') {
                    alert(response.description);
                  } else if (response.status == 'success') {
                    alert(response.description);
                    refresh();
                  }
                });
            }
          }
        }
      }]
    })
  });
</script>
@endsection