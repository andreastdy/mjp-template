@extends('layouts.default')

@section('content_header')
<h1>
  TRUCKING
</h1>
<ol class="breadcrumb">
  <li><a href="#"> Master</a></li>
  <li class="active">Trucking</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@can('edit trucking')
<a href="{{route('master.trucking.add')}}" data-toggle="tooltip" title="Tambah Trucking"><button type='button' class='btn btn-danger'><i class='fa fa-plus'></i> Buat Baru</button></a>
@endcan
<div class="portlet-body">
  <table class="table table-bordered" id="truckingTable" style='background-color: white'>
  </table>
</div>
@endsection

@section('footer_script')
<script>
  $(document).ready(function() {
    $('#truckingTable').bootstrapTable({
      classes: 'table table-hover table-no-bordered',
      url: route('master.trucking.data'),
      pagination: true,
      sidePagination: 'server',
      search: true,
      columns: [{
        title: 'NO',
        formatter: function(value, row, index) {
          return index + 1;
        }
      }, {
        field: 'nama',
        title: 'NAMA PT',
        sortable: true
      }, {
        field: 'alamat',
        title: 'ALAMAT',
        sortable: true
      }, {
        field: 'rekening',
        title: 'REKENING',
        sortable: true
      }, {
        field: 'name_cp',
        title: 'NAMA CP',
        sortable: true
      }, {
        field: 'telp_cp',
        title: 'TELEPON CP',
        sortable: true
      }, {
        field: 'email',
        title: 'EMAIL',
        sortable: true
      }, {
        title: 'AKSI',
        formatter: function(value, row, index) {
          var btnString = '';
          
          @can('edit trucking')
           btnString = '<a href="' + route('master.trucking.ubah', {
            id: row.id_trucking
          }) + '" data-toggle="tooltip" title="Edit Pelayaran"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>';
          @endcan

          btnString += '<a href="' + route('master.trucking.detail', {
            id: row.id_trucking
          }) + '" data-toggle="tooltip" title="View Detail"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></button></a>';
          return btnString;
        }
      }]
    })
  });
</script>
@endsection