@extends('layouts.default')

@section('content_header')
<h1>
@if($mode == 'ADD')
    ADD DETAIL DOORING 
@else
    EDIT DETAIL DOORING
@endif
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Master</a></li>
    <li class="active">Dooring</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@if($mode=='EDIT')
<form action="{{ route('master.dooring.detailupdate',$dooringdetail->id_dooringdetail) }}" method="post" class="form-horizontal">
@else
<form action="{{ route('master.dooring.detailcreate',$dooring->id_dooring) }}" method="post" class="form-horizontal">
@endif
<input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
    <label class="control-label col-xs-2" for="chkPortToDoor">ID</label> 
    <div class="col-xs-4">
      <label class="checkbox-inline">
        @if($mode == 'ADD')
        {{$dooring->id_dooring}}
        @else
        {{$dooringdetail->Dooring->id_dooring}}
        @endif
      </label>
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="chkPortToDoor">Nama Dooring</label> 
    <div class="col-xs-4">
      <label class="checkbox-inline">
      @if($mode == 'ADD')
      {{$dooring->nama}}
      @else
      {{$dooringdetail->Dooring->nama}}
      @endif
      </label>
    </div>
  </div>
  
  <div class="form-group">
    <label class="control-label col-xs-2" for="cboKotaPelabuhanAsal">Dari Kota Pelabuhan</label> 
    <div class="col-xs-4">
      <select class="select form-control cboKotaPelabuhanAsal" name="cboKotaPelabuhanAsal" id="cboKotaPelabuhanAsal">
      <option value="0">-- PILIH KOTA PELABUHAN --</option>
      @foreach($kotapelabuhan as $item)
      @if($mode == 'EDIT') 
        @if($dooringdetail->id_kota_pelabuhan==$item->id_kota) 
        <option value="{{ $item->id_kota }}" selected>{{ $item->nama_kota }}</option>
        @else
        <option value="{{ $item->id_kota }}" >{{ $item->nama_kota }}</option>
        @endif
      @else
      <option value="{{ $item->id_kota }}" >{{ $item->nama_kota }}</option>
      @endif
    @endforeach
      </select>
      @if($errors->has('cboKotaPelabuhanAsal'))
      <span class="text-danger">{{$errors->first('cboKotaPelabuhanAsal')}}
      @endif
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="cboKotaPelabuhanTujuan">Tujuan Kota</label> 
    <div class="col-xs-4">
      <select class="select form-control cboKotaPelabuhanTujuan" name="cboKotaPelabuhanTujuan" id="cboKotaPelabuhanTujuan">
      <option value="0">-- PILIH TUJUAN KOTA --</option>
      @foreach($kotapelabuhan as $item)
      @if($mode == 'EDIT') 
        @if($dooringdetail->id_kota_tujuan==$item->id_kota) 
        <option value="{{ $item->id_kota }}" selected>{{ $item->nama_kota }}</option>
        @else
        <option value="{{ $item->id_kota }}" >{{ $item->nama_kota }}</option>
        @endif
      @else
      <option value="{{ $item->id_kota }}" >{{ $item->nama_kota }}</option>
      @endif
    @endforeach
      </select>
      @if($errors->has('cboKotaPelabuhanTujuan'))
      <span class="text-danger">{{$errors->first('cboKotaPelabuhanTujuan')}}
      @endif
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtKeterangan">Keterangan<br/><span class="opsional"><sup>(opsional)</sup></span></label> 
    <div class="col-xs-4">
      <textarea class="form-control" rows="3" cols="40" name="txtKeterangan" id="txtKeterangan">@if($mode == 'EDIT') {{$dooringdetail->keterangan}} @endif</textarea>
    </div>
  </div>

   
  <div class="form-group row">
    <div class="col-xs-offset-2 col-xs-4">
      <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
      @if($mode=='ADD')
      <a href="{{route('master.dooring.detail',$dooring->id_dooring)}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
      @else
      <a href="{{route('master.dooring.detail',$dooringdetail->Dooring->id_dooring)}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
      @endif
    </div>
  </div>
</form>

@endsection

@section('footer_script')
<script>
    $('.cboKotaPelabuhanAsal').select2();
    $('.cboKotaPelabuhanTujuan').select2();
   
</script>
@endsection
