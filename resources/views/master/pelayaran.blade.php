@extends('layouts.default')

@section('content_header')
<h1>
  PELAYARAN
</h1>
<ol class="breadcrumb">
  <li><a href="#"> Master</a></li>
  <li class="active">Pelayaran</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@can('edit pelayaran')
<a href="{{route('master.pelayaran.add')}}" data-toggle="tooltip" title="Tambah Pelayaran"><button type='button' class='btn btn-danger'><i class='fa fa-plus'></i> Buat Baru</button></a>
@endcan

<div class="portlet-body">
  <table class="table table-bordered" id="pelayaranTable" style='background-color: white'>



  </table>
</div>



@endsection

@section('footer_script')
<script>
  $(document).ready(function() {
    $('#pelayaranTable').bootstrapTable({
      classes: 'table table-hover table-no-bordered',
      url: route('master.pelayaran.data'),
      pagination: true,
      search: true,
      sidePagination:'server',
      columns: [{
        title: 'NO',
        formatter: function(value, row, index) {
          return index + 1;
        }
      }, {
        field: 'name_full',
        title: 'NAMA PELAYARAN',
        sortable: true
      }, {
        field: 'alamat',
        title: 'ALAMAT',
        sortable: true
      }, {
        field: 'kodepos',
        title: 'KODE POS',
        sortable: true
      }, {
        field: 'telp',
        title: 'TELEPON',
        sortable: true
      }, {
        field: 'name_cp',
        title: 'NAMA CP',
        sortable: true
      }, {
        field: 'telp_cp',
        title: 'TELEPON CP',
        sortable: true
      }, {
        field: 'email',
        title: 'EMAIL',
        sortable: true
      }, {
        title: 'AKSI',
        formatter: function(value, row, index) {
          var btnString = "";

          @can('edit pelayaran')
          btnString = '<a href="' + route('master.pelayaran.ubah', {
            id: row.id_pelayaran
          }) + '" data-toggle="tooltip" title="Edit Pelayaran"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>'
          @endcan

          btnString += '<a href="' + route('master.pelayaran.detail', {
            id: row.id_pelayaran
          }) + '" data-toggle="tooltip" title="View Detail"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></button></a>'
          return btnString;
        }
      }]
    })
  });
</script>
@endsection