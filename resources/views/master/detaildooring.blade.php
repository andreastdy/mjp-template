@extends('layouts.default')

@section('content_header')
<h1>
  DETAIL DOORING
</h1>
<ol class="breadcrumb">
  <li><a href="#"> Master</a></li>
  <li class="active">Dooring</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<label class="control-label">ID : {{$dooring->id_dooring}}</label>
<Br />
<label class="control-label">NAMA DOORING : {{$dooring->nama}}</label>
<br />
<a href="{{route('master.dooring')}}" data-toggle="tooltip" title="Back"><button type='button' class='btn btn-success'><i class='fa fa-reply'></i> Kembali</button></a>&nbsp;
@can('edit dooring')
<a href="{{route('master.dooring.detailadd',$dooring->id_dooring)}}" data-toggle="tooltip" title="Add Detail"><button type='button' class='btn btn-danger'><i class='fa fa-plus'></i> Buat Baru</button></a>
@endcan

<div class="portlet-body">
  <table class="table table-bordered" id="detailDooringTable" style='background-color: white'>



  </table>
</div>



@endsection

@section('footer_script')
<script>
  $(document).ready(function() {
    function refresh() {
      $('#detailDooringTable').bootstrapTable('refresh', {
        url: route('master.dooring.datadetail', {{ $dooring->id_dooring }} )
      });
    }

    $('#detailDooringTable').bootstrapTable({
      url: route('master.dooring.datadetail', {{ $dooring->id_dooring }}),
      pagination: true,
      search: true,
      columns: [{
        title: 'NO',
        formatter: function(value, row, index) {
          return index + 1;
        }
      }, {
        field: 'nama_kota_pelabuhan',
        title: 'DARI KOTA PELABUHAN'
      }, {
        field: 'nama_kota_tujuan',
        title: 'TUJUAN KOTA'
      }, {
        field: 'keterangan',
        title: 'KETERANGAN'
      }, {
        title: 'AKSI',
        formatter: function(value, row, index) {
          var btnString = '';
          
          @can('edit dooring')
           btnString = '<a href="' + route('master.dooring.ubahdetail', {
            id: row.id_dooringdetail
          }) + '" data-toggle="tooltip" title="Edit Biaya"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>'
          btnString += '<button type="button" class="btn btn-delete btn-xs btn-danger" title="delete biaya"><i class="fa fa-trash"></i></button></a>'
          @endcan

          return btnString;
        },
        events: {
          'click .btn-delete': function(e, value, row, index) {
            if (confirm('Are you sure you want to delete this record?')) {
              $.ajax({
                  url: route('master.dooring.detaildelete', {
                    id: row.id_dooringdetail
                  }),
                  data: {
                    _token: $('meta[name="csrf_token"]').attr('content')
                  },
                  method: 'post'
                })
                .then(function(response) {
                  if (response.status == 'error') {
                    alert(response.description);
                  } else if (response.status == 'success') {
                    alert(response.description);
                    refresh();
                  }
                });
            }
          }
        }
      }]
    })
  });
</script>
@endsection