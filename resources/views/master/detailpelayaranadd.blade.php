@extends('layouts.default')

@section('content_header')
<h1>
@if($mode=='ADD')
    ADD DETAIL PELAYARAN 
@else
    EDIT DETAIL PELAYARAN
@endif
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Master</a></li>
    <li class="active">Pelayaran</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@if($mode=='EDIT')
<form action="{{ route('master.pelayaran.detailupdate',$pelayarandetail->id_pelayarandetail) }}" method="post" class="form-horizontal">
@else
<form action="{{ route('master.pelayaran.detailcreate',$pelayaran->id_pelayaran) }}" method="post" class="form-horizontal">
@endif
<input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
    <label class="control-label col-xs-2" for="chkPortToDoor">ID</label> 
    <div class="col-xs-4">
      <label class="checkbox-inline">
      @if($mode=='ADD')
        {{$pelayaran->id_pelayaran}}
      @else
      {{$pelayarandetail->Pelayaran->id_pelayaran}}
      @endif
      </label>
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="chkPortToDoor">Nama Pelayaran</label> 
    <div class="col-xs-4">
      <label class="checkbox-inline">
      @if($mode=='ADD')
      {{$pelayaran->name_full}}
      @else
      {{$pelayarandetail->Pelayaran->name_full}}
      @endif
      </label>
    </div>
  </div>
  
  <div class="form-group">
    <label class="control-label col-xs-2" for="cboPortAsal">Pelabuhan Asal</label> 
    <div class="col-xs-4">
      <select class="select form-control cboPortAsal" name="cboPortAsal" id="cboPortAsal">
      <option value="0">-- PILIH PELABUHAN --</option>
      @foreach($kotapelabuhan as $item)
      @if($mode == 'EDIT') 
        @if($pelayarandetail->id_pelabuhan_asal==$item->id_pelabuhan) 
        <option value="{{ $item->id_pelabuhan }}" selected>{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}</option>
        @else
        <option value="{{ $item->id_pelabuhan }}" >{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}</option>
        @endif
      @else
      <option value="{{ $item->id_pelabuhan }}" >{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}</option>
      @endif
    @endforeach
      </select>
      @if($errors->has('cboPortAsal'))
      <span class="text-danger">{{$errors->first('cboPortAsal')}}
      @endif
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="cboPortBongkar">Pelabuhan Bongkar</label> 
    <div class="col-xs-4">
      <select class="select form-control cboPortBongkar" name="cboPortBongkar" id="cboPortBongkar">
      <option value="0">-- PILIH PELABUHAN --</option>
      @foreach($kotapelabuhan as $item)
      @if($mode == 'EDIT') 
        @if($pelayarandetail->id_pelabuhan_bongkar==$item->id_pelabuhan) 
        <option value="{{ $item->id_pelabuhan }}" selected>{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}</option>
        @else
        <option value="{{ $item->id_pelabuhan }}" >{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}</option>
        @endif
      @else
      <option value="{{ $item->id_pelabuhan }}" >{{ $item->nama_kota }} - {{ $item->nama_pelabuhan }}</option>
      @endif
    @endforeach
      </select>
      @if($errors->has('cboPortBongkar'))
      <span class="text-danger">{{$errors->first('cboPortBongkar')}}
      @endif
    </div>
  </div>
  
  <div class="form-group row">
    <div class="col-xs-offset-2 col-xs-4">
      <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
      @if($mode=='ADD')
      <a href="{{route('master.pelayaran.detail',$pelayaran->id_pelayaran)}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
      @else
      <a href="{{route('master.pelayaran.detail',$pelayarandetail->Pelayaran->id_pelayaran)}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
      @endif
    </div>
  </div>
</form>

@endsection

@section('footer_script')
<script>
$(".cboPortAsal").select2();
$(".cboPortBongkar").select2();
</script>
@endsection
