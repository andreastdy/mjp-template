@extends('layouts.default')

@section('content_header')
<h1>
@if ($mode == 'ADD')
    ADD CONSIGNEE
@else
    EDIT CONSIGNEE
@endif
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Master</a></li>
    <li class="active">Consignee</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<hr>
@if ($mode == 'ADD')
<form action="{{ route('master.consignee.create') }}" method="post" class="form-horizontal">
@else
<form action="{{ route('master.consignee.update',$consignee->id_consignee) }}" method="post" class="form-horizontal">
@endif
<input type="hidden" name="_token" value="{{ csrf_token() }}">
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtNama">Nama Consignee</label> 
    <div class="col-xs-6">
      <input required="required" class="form-control" name="txtNama" id="txtNama" type="text" @if($mode == 'EDIT') value="{{$consignee->nama}}" @endif>
      @if($errors->has('txtNama'))
      <span class="text-danger">{{$errors->first('txtNama')}}
      @endif
    </div>
  </div>
  
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtAlamat">Alamat</label> 
    <div class="col-xs-4">
      <textarea required="required" class="form-control" rows="3" cols="40" name="txtAlamat" id="txtAlamat">@if($mode == 'EDIT') {{$consignee->alamat}} @endif</textarea>
      @if($errors->has('txtAlamat'))
      <span class="text-danger">{{$errors->first('txtAlamat')}}
      @endif
    </div>
  </div>
     
     
  <div class="form-group">
    <label class="control-label col-xs-2" for="cboKota">Kota</label> 
    <div class="col-xs-3">
    <select class="select form-control cboKota" name="cboKota" id="cboKota" style="width:100%">
    <option value="0">-- PILIH KOTA --</option>
    @foreach($city as $item)
      @if($mode == 'EDIT') 
        @if($consignee->id_kota==$item->id_kota) 
        <option value="{{ $item->id_kota }}" selected>{{ $item->nama_kota }}</option>
        @else
        <option value="{{ $item->id_kota }}" >{{ $item->nama_kota }}</option>
        @endif
      @else
      <option value="{{ $item->id_kota }}" >{{ $item->nama_kota }}</option>
      @endif
    @endforeach
      </select>
      @if($errors->has('cboKota'))
      <span class="text-danger">{{$errors->first('cboKota')}}
      @endif
    </div>
  </div>
  
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtTelp">Telepon</label> 
    <div class="col-xs-6">
      <input required="required" class="form-control" name="txtTelp" id="txtTelp" type="text" @if($mode == 'EDIT') value="{{$consignee->telp}}" @endif>
      @if($errors->has('txtTelp'))
      <span class="text-danger">{{$errors->first('txtTelp')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtNamaCP">Nama CP</label> 
    <div class="col-xs-6">
      <input required="required" class="form-control" name="txtNamaCP" id="txtNamaCP" type="text" @if($mode == 'EDIT') value="{{$consignee->name_cp}}" @endif>
      @if($errors->has('txtNamaCP'))
      <span class="text-danger">{{$errors->first('txtNamaCP')}}
      @endif
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-2" for="txtTeleponCP">Telepon CP</label> 
    <div class="col-xs-6">
      <input required="required" class="form-control" name="txtTelpCP" id="txtTelpCP" type="text" @if($mode == 'EDIT') value="{{$consignee->telp_cp}}" @endif>
      @if($errors->has('txtTelpCP'))
      <span class="text-danger">{{$errors->first('txtTelpCP')}}
      @endif
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-xs-2" for="txtKeterangan">Keterangan<br/><span class="opsional"><sup>(opsional)</sup></span></label> 
    <div class="col-xs-4">
      <textarea class="form-control" rows="3" cols="40" name="txtKeterangan" id="txtKeterangan">@if($mode == 'EDIT') {{$consignee->keterangan}} @endif</textarea>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-xs-offset-2 col-xs-6">
      <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i> Simpan</button>
      <a href="{{route('master.consignee')}}"><button type="button" class="btn btn-danger" name="cancel">Cancel</button></a>
    </div>
  </div>
</form>



@endsection

@section('footer_script')
<script>
    
    $('.cboKota').select2();
    
</script>
@endsection
