@extends('layouts.default')

@section('content_header')
<h1>
    DETAIL PELAYARAN
</h1>
<ol class="breadcrumb">
    <li><a href="#"> Master</a></li>
    <li class="active">Pelayaran</li>
</ol>
@endsection

@section('content')
@include('includes/notifications')
<label class="control-label">ID PELAYARAN : {{$pelayaran->id_pelayaran}}</label>
<Br/>
<label class="control-label">NAMA PELAYARAN : {{$pelayaran->name_full}}</label>

<Br/>
<a href="{{route('master.pelayaran')}}" data-toggle="tooltip" title="Back"><button type='button' class='btn btn-success'><i class='fa fa-reply'></i> Kembali</button></a>&nbsp;@can('edit pelayaran')<a href="{{route('master.pelayaran.detailadd',$pelayaran->id_pelayaran)}}" data-toggle="tooltip" title="Add Detail"><button type='button' class='btn btn-danger'><i class='fa fa-plus'></i> Buat Baru</button></a>@endcan

<div class="portlet-body">
    <table class="table table-bordered" id="detailPelayaranTable" style='background-color: white'>
        
                

    </table>
</div>


@endsection

@section('footer_script')
<script>

$(document).ready(function () {
    function refresh() {
    $('#detailPelayaranTable').bootstrapTable('refresh',{
      url:route('master.pelayaran.datadetail',{{$pelayaran->id_pelayaran}})
    });
    
  }

    $('#detailPelayaranTable').bootstrapTable({
        url: route('master.pelayaran.datadetail',{{$pelayaran->id_pelayaran}}),
    pagination: true,
   
  search: true,
  columns: [{
    title: 'NO',
    formatter: function (value, row, index) {
        return index+1;
      }
  }, {
    field: 'nama_kota_asal',
    title: 'KOTA PELABUHAN ASAL',
    sortable: true
  }, {
    field: 'nama_pelabuhan_asal',
    title: 'NAMA PELABUHAN ASAL',
    sortable: true
  },{
    field: 'nama_kota_bongkar',
    title: 'KOTA PELABUHAN BONGKAR',
    sortable: true
  },{
    field: 'nama_pelabuhan_bongkar',
    title: 'NAMA PELABUHAN BONGKAR',
    sortable: true
  },{
      title:'AKSI',
      formatter: function(value, row, index) {
        var btnString = "";

        @can('edit pelayaran')
        btnString = '<a href="'+route('master.pelayaran.ubahdetail',{id:row.id_pelayarandetail})+'" data-toggle="tooltip" title="Edit Consignee"><button type="button" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></button></a>'
         btnString +='<button type="button" class="btn btn-delete btn-xs btn-danger" title="Delete Consignee"><i class="fa fa-trash"></i></button></a>';
         @endcan
         
        return btnString;
      },
      events:{
        'click .btn-delete': function(e, value, row, index) {
          if (confirm('Hapus detail pelayaran ini?')) {
            $.ajax({
              url: route('master.pelayaran.detaildelete',{id:row.id_pelayarandetail}),
              data:{_token:$('meta[name="csrf_token"]').attr('content')},
              method:'post'
            })
            .then(function (response){
              if (response.status == 'error') {
                alert(response.description);
              } else if( response.status == 'success') {
                alert(response.description);
                refresh();
              }
            }); 
          }
        }
      }
    }]
})
});
</script>
@endsection
