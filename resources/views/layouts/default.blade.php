<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf_token" content="{{ csrf_token() }}" />
  <title>MJP</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
  <!-- DataTables -->
  <!-- <link href="{{ asset('css/mdb.min.css') }}" rel="stylesheet" >
    <link href="{{ asset('css/datatables.min.css') }}" rel="stylesheet" > -->
  <link href="{{ asset('css/bootstrap-table.min.css') }}" rel="stylesheet">
  <!-- Font Awesome -->
  <script defer src="{{ asset('js/all.min.js') }}"></script>
  <!--load all styles -->
  {{-- <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css"> --}}
  <!-- Ionicons -->
  {{-- <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css"> --}}
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('css/skin-red-light.min.css') }}">
  <!-- Morris chart -->
  {{-- <link rel="stylesheet" href="bower_components/morris.js/morris.css"> --}}
  <!-- jvectormap -->
  {{-- <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css"> --}}
  <!-- Date Time Picker -->
  <link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css')}}">
  <!-- Date Picker -->
  {{-- <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"> --}}
  <!-- Daterange picker -->
  {{-- <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css"> --}}
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{ asset('css/bootstrap3-wysihtml5.min.css')}}">

  <link rel="stylesheet" href="{{ asset('css/select2.min.css')}}">

  <!-- summernote text editor -->
  <link rel="stylesheet" href="{{ asset('css/summernote.css')}}">

  <link rel="stylesheet" href="{{ asset('css/custom.css')}}">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <style>
    .select2-container--default .select2-selection--single .select2-selection__rendered {
      line-height:20px;
    }
    </style>

</head>

<body class="hold-transition skin-red-light fixed sidebar-mini">
  <div class="wrapper">

    @include('partials.header')
    @include('partials.sidebar')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

      <!-- Content Header (Page header) -->
      <section class="content-header">
        @yield('content_header')
      </section>

      <!-- Main content -->
      <section class="content">
        @yield('content')
      </section>
      <!-- /.content -->

    </div>
    @include('partials.footer')


  </div>
  @routes
  <!-- ./wrapper -->

  <!-- jQuery 3 -->
  <!-- <script src="{{ asset('js/jquery.min.js' )}}"></script> -->
  <script src="{{ asset('js/jquery-3.4.1.min.js' )}}"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="{{ asset('js/jquery-ui.min.js' ) }}"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button);
  </script>
  <!-- Bootstrap 3.3.7 -->
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/bootstrap-table.min.js') }}"></script>
  <!-- Data tables -->
  <!-- <script src="{{ asset('js/mdb.min.js') }}"></script> -->
  <!-- <script src="{{ asset('js/datatables.min.js') }}"></script> -->
  <!-- Morris.js charts -->
  {{-- <script src="bower_components/raphael/raphael.min.js"></script>
    <script src="bower_components/morris.js/morris.min.js"></script> --}}
  <!-- Sparkline -->
  {{-- <script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script> --}}
  <!-- jvectormap -->
  {{-- <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script> --}}
  <!-- jQuery Knob Chart -->
  {{-- <script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script> --}}
  <!-- datertimepicker -->
  <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
  <!-- daterangepicker -->
 <script src="{{ asset('js/moment.min.js') }}"></script>
  <script src="{{ asset('js/moment-with-locales.js') }}"></script>
  {{-- <script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script> --}}
  <!-- datepicker -->
  <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
  <!-- Bootstrap WYSIHTML5 -->
  <script src="{{ asset('js/bootstrap3-wysihtml5.all.min.js') }}"></script>
  <!-- Slimscroll -->
  <script src="{{ asset('js/jquery.slimscroll.min.js') }}"></script>
  <!-- FastClick -->
  <script src="{{ asset('js/fastclick.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('js/adminlte.min.js' ) }}"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  {{-- <script src="dist/js/pages/dashboard.js"></script> --}}
  <!-- AdminLTE for demo purposes -->
  {{-- <script src="dist/js/demo.js"></script> --}}
  <script src="{{ asset('js/select2.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/laroute.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/ziggy.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/route.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/summernote.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/cleave.js') }}" type="text/javascript"></script>
  <script>
    function addCommas(Nominal) {
      var bilangan_baru = "";
      var bilangan_depan = "";
      var angka1 = Nominal;
      //nama.value;
      var angka = "";
      // console.log(Nominal);
      //normalkan bentuk angka dari titik koma
      var str_hasil = "";
      var temp;
      var koma = "";
      var negative = Nominal.indexOf("-");
      angka1 = Nominal.replace("-", "");
      for (var i = 0; i < angka1.length; i++) {
        if (angka1[i] == ",") {
          temp = "";
        } else if (angka1[i] == ".") {
          if (angka1[i + 1] != null) {
            if (angka1[i + 1] != ".") {
              koma += angka1[i + 1];
            }
          }
          if (angka1[i + 2] != null) {
            if (angka1[i + 2] != ".") {
              koma += angka1[i + 2];
            }
          }
          break;
        } else {
          temp = angka1[i];
        }
        str_hasil += temp;
      }
      var nilai = parseFloat(str_hasil);

      if (str_hasil == "" || nilai == 0) {
        return 0;
      }
      angka = nilai.toString();

      if (angka.length > 3) {
        var titik = 0;
        var depan = 0;
        var mulai_ambil = 0;
        titik = parseInt(angka.length / 3);
        depan = angka.length % 3;

        if (depan != 0) {
          bilangan_depan = angka.substr(0, depan) + ",";
          mulai_ambil = depan;
        }
        for (var i = 0; i < titik; i++) {
          mulai_ambil = (i * 3) + (depan);
          if (i == 0) {
            bilangan_baru = angka.substr(mulai_ambil, 3);
          } else {
            bilangan_baru = bilangan_baru + "," + (angka.substr(mulai_ambil, 3));
          }
        }
        bilangan_baru = bilangan_depan + bilangan_baru;
      } else {
        bilangan_baru = angka;
      }

      if (koma != "") {
        bilangan_baru = bilangan_baru + "." + koma;
      }

      return (negative >= 0 ? "-" : "") + bilangan_baru;
    }

    function isNumberKey(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
      return true;
    }

    function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}
  </script>
  @yield('footer_script')

</body>

</html>