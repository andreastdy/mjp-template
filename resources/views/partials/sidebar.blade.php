<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- search form -->
    <!--         <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fas fa-fw fa-search"></i>
                    </button>
                </span>
            </div>
        </form> -->
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
    @if(auth()->user()->can('view pelayaran') || auth()->user()->can('view customer') || auth()->user()->can('view consignee') || auth()->user()->can('view barang') || auth()->user()->can('view dooring') || auth()->user()->can('view trucking') || auth()->user()->can('view sector') || auth()->user()->can('view city') || auth()->user()->can('view pelabuhan') || auth()->user()->can('view trip') )
      <li class="header">MASTER</li>
      @can('view pelayaran')
      <li>
        <a href="{{route('master.pelayaran')}}">
          <i class="fas fa-fw fa-ship"></i> <span>Pelayaran</span>
        </a>
      </li>
      @endcan
      @can('view customer')
      <li>
        <a href="{{route('master.customer') }}">
          <i class="fas fa-fw fa-users"></i> <span>Customer</span>
        </a>
      </li>
      @endcan
      @can('view consignee')
      <li>
        <a href="{{route('master.consignee') }}">
          <i class="fas fa-fw fa-user-tie"></i> <span>Consignee</span>

        </a>
      </li>
      @endcan
      @can('view barang')
      <li>
        <a href="{{route('master.barang') }}">
          <i class="fas fa-fw fa-boxes"></i> <span>Barang</span>

        </a>
      </li>
      @endcan
      @can('view dooring')
      <li>
        <a href="{{route('master.dooring')}}">
          <i class="fas fa-fw fa-home"></i> <span>Dooring</span>

        </a>
      </li>
      @endcan
      @can('view trucking')
      <li>
        <a href="{{route('master.trucking')}}">
          <i class="fas fa-fw fa-truck"></i> <span>Trucking</span>

        </a>
      </li>
      @endcan
      @can('view sector')
      <li>
        <a href="{{route('master.sector')}}">
          <i class="fas fa-fw fa-globe"></i> <span>Sector</span>

        </a>
      </li>
      @endcan
      @can('view city')
      <li>
        <a href="{{ route('setting.city') }}">
          <i class="fas fa-fw fa-building"></i> <span>City</span>

        </a>
      </li>
      @endcan
      @can('view pelabuhan')
      <li>
        <a href="{{ route('setting.pelabuhan') }}">
          <i class="fas fa-fw fa-anchor"></i> <span>Pelabuhan</span>

        </a>
      </li>
      @endcan
      @can('view trip')
      <li>
        <a href="{{ route('master.trip') }}">
          <i class="fas fa-fw fa-compass"></i> <span>Trip</span>

        </a>
      </li>
      @endcan
      @endif

      @if(auth()->user()->can('view proposal') || auth()->user()->can('view order') || auth()->user()->can('view surat jalan') || auth()->user()->can('view BAPB') || auth()->user()->can('view packing list') || auth()->user()->can('view invoice') || auth()->user()->can('view invoice lcl') || auth()->user()->can('view custom invoice') || auth()->user()->can('view penagihan') || auth()->user()->can('view pelunasan') )
      <li class="header">TRANSACTION</li>
      
      @if(auth()->user()->can('view proposal') || auth()->user()->can('approve proposal') )
      <li class="treeview">
        <a href="#">
          <i class="fas fa-fw fa-scroll"></i>
          <span>Proposal</span>

        </a>
        <ul class="treeview-menu">
          @can('view proposal')
          <li><a href="{{route('transaction.customerproposal')}}"><i class="far fa-fw fa-circle"></i> Customer Proposal</a></li>
          @endcan
          @can('approve proposal')
          <li><a href="{{route('transaction.proposalapproval')}}"><i class="far fa-fw fa-circle"></i> Proposal Approval</a></li>
          @endcan

        </ul>
      </li>
      @endif

      @if(auth()->user()->can('view order') ||  auth()->user()->can('update status order'))
      <li class="treeview">
        <a href="#">
          <i class="fas fa-fw fa-sticky-note"></i>
          <span>Order</span>

        </a>
        <ul class="treeview-menu">
        @can('view order')
          <li><a href="{{route('transaction.customerorder')}}"><i class="far fa-fw fa-circle"></i> Customer Order</a></li>
        @endcan
        @can('view monitoring order')
          <li><a href="{{route('transaction.updateorder')}}"><i class="far fa-fw fa-circle"></i> Monitoring Order</a></li>
        @endcan
        </ul>
      </li>
      @endif

      @can('view surat jalan')
      <li>
        <a href="{{route('transaction.suratjalan')}}">
          <i class="fas fa-fw fa-clone"></i> <span>Surat Jalan</span>

        </a>

      </li>
      @endcan

      @can('view BAPB')
      <li>
        <a href="{{route('transaction.bapb')}}">
          <i class="fas fa-fw fa-map"></i> <span>BAPB</span>

        </a>

      </li>
      @endcan

      @can('view packing list')
      <li>
        <a href="{{route('transaction.packinglist')}}">
          <i class="fas fa-fw fa-file-alt"></i> <span>Packing List</span>

        </a>

      </li>
      @endcan

      @if(auth()->user()->can('view invoice') || auth()->user()->can('view invoice lcl') || auth()->user()->can('view custom invoice') || auth()->user()->can('view extra cost claim') || auth()->user()->can('view penagihan') || auth()->user()->can('view pelunasan') )
      <li class="treeview">
        <a href="#">
          <i class="fas fa-fw fa-tags"></i>
          <span>Pembayaran</span>

        </a>
        <ul class="treeview-menu">
          <!-- <li><a href="{{route('transaction.kasir')}}"><i class="far fa-fw fa-circle"></i>KASIR</a></li> -->
          @can('view invoice')
          <li><a href="{{route('transaction.invoice')}}"><i class="far fa-fw fa-circle"></i> Invoice</a></li>
          @endcan
          @can('view invoice lcl')
          <li><a href="{{route('transaction.invoicelcl')}}"><i class="far fa-fw fa-circle"></i> Invoice LCL</a></li>
          @endcan
          @can('view custom invoice')
          <li><a href="{{route('transaction.custominvoice')}}"><i class="far fa-fw fa-circle"></i> Custom Invoice</a></li>
          @endcan
          @can('view penagihan')
          <li><a href="{{route('transaction.penagihan')}}"><i class="far fa-fw fa-circle"></i> Penagihan</a></li>
          @endcan
          @can('view extra cost claim')
          <li><a href="{{route('transaction.extracostclaim')}}"><i class="far fa-fw fa-circle"></i> Extra Cost/Claim</a></li>
          @endcan
          @can('view pelunasan')
          <li><a href="{{route('transaction.pelunasan')}}"><i class="far fa-fw fa-circle"></i> Pelunasan</a></li>
          @endcan
        </ul>
      </li>
      @endif

      @endif

      @if(auth()->user()->can('edit ppn') || auth()->user()->can('view user') || auth()->user()->can('view role') || auth()->user()->can('view biaya'))
      <li class="header">SETTING</li>
      @can('view role')
      <li>
        <a href="{{ route('setting.role') }}">
          <i class="fas fa-fw fa-users-cog"></i> <span>Role</span>

        </a>
      </li>
      @endcan
      @can('view user')
      <li>
        <a href="{{ route('setting.user') }}">
          <i class="fas fa-fw fa-users-cog"></i> <span>User</span>

        </a>
      </li>
      @endcan
      @can('view biaya')
      <li>
        <a href="{{ route('setting.biaya') }}">
          <i class="fas fa-fw fa-money-bill-alt"></i> <span>Biaya</span>

        </a>
      </li>
      @endcan
      <!-- <li>
                <a href="{{ route('setting.asset') }}">
                    <i class="fas fa-fw fa-suitcase"></i> <span>ASSET</span>
                    
                </a>
            </li> -->
      @can('edit ppn')      
      <li>
        <a href="{{ route('setting.ppn') }}">
          <i class="fas fa-fw fa-percent"></i> <span>Ppn</span>

        </a>
      </li>
      @endcan
    </ul>
    @endif
  </section>
  <!-- /.sidebar -->
</aside>