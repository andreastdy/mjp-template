<header class="main-header">
  <!-- Logo -->
  <a href="/" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>MJP</b></span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>MJP</b></span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
      <span class="fas fa-bars"></span>
    </a>

    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown messages-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fas fa-user"></i>
            <span class="hidden-xs">{{ Auth::user()->name }} <span class="caret"></span></span>
          </a>
          <ul class="dropdown-menu">
            <li>
              <ul class="menu">
                <li>
                  <a href="{{route('ubah_password')}}">
                    <i class="fa fa-key"></i> Change Password
                  </a>
                </li>
                <li>
                  <a href="{{route('logout')}}">
                    <i class="fa fa-sign-out-alt"></i> Logout
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>